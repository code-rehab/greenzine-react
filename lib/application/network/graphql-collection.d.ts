import { GraphQLProviderProps } from "./graphql-provider";
import { RecordData } from "./base-model";
import { GraphQLModel } from "./graphql-model";
import { DefaultStore } from "../storage/store";
export interface Collection<T> {
    items: T[];
    loading: boolean;
    hasItems: boolean;
    fetch(variables?: Record<string, any>): Promise<void>;
    find(id: string): T | undefined;
    where(key: string, value: any): Collection<T>;
    where(key: string, operator: ComparisonOperators, value: string | number | boolean): Collection<T>;
}
declare type ComparisonOperators = ">" | "<" | ">=" | "<=" | "!=" | "==" | "!==" | "===";
interface ComparisonProps {
    key: string;
    operator: ComparisonOperators;
    value: string | number | boolean;
}
declare type CollectionFilters = Record<string, ComparisonProps>;
export declare class GraphQLCollection<T extends GraphQLModel<TRecord>, TRecord extends RecordData> {
    private _items;
    protected provider: GraphQLProviderProps<T, TRecord> & DefaultStore<TRecord>;
    loading: boolean;
    filters: CollectionFilters;
    get hasItems(): boolean;
    constructor(_items: TRecord[] | undefined, provider: GraphQLProviderProps<T, TRecord> & DefaultStore<TRecord>);
    get items(): T[];
    applyFilers: (items: T[], filters: Record<string, ComparisonProps>) => T[];
    fetch: (variables?: Record<string, any>) => Promise<void>;
    where: (...args: [string, string | number | boolean] | [string, ComparisonOperators, string | number | boolean]) => this;
    find: (id: string) => T | undefined;
}
export {};
