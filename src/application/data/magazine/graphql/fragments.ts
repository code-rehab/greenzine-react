import { ArticleFragment } from "../../article/graphql/fragments";

// import { ArticleFragment } from "../../article/graphql/fragments";

export const MagazineFragment = `
  id
  image
  logowhite
  title
  description
  logo
  type
  config {
    theme
    poll
    print
    gaCode
    tenant
  }
  about {
    config
    data {
      section
      component
      props
      style
    }
  }
  slug
  editors {
    type
    users
  }
  editions {
    id
    slug
    title
    subtitle
    description 
    public
    pollSlug
    image
    articles {
      ${ArticleFragment}
    }
}
`;
