import { WithStyles } from "@material-ui/styles";
import * as React from "react";
import { PresenterProps } from "../../helpers/with-presenter";
import { InterfacePresenter } from "./interface-presenter";
interface OwnProps extends WithStyles<"root"> {
}
export declare const Component: ({ classes, presenter }: OwnProps & PresenterProps<InterfacePresenter>) => JSX.Element;
export declare const DefaultInterface: React.ComponentType<Pick<OwnProps, never> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
