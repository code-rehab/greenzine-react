var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { Grid } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";
export var LayoutColumnsDesktop = function (_a) {
    var data = _a.data;
    return (React.createElement("div", null,
        React.createElement(Grid, __assign({ container: true, spacing: 4 }, data.container), data.sections.map(function (cfg, i) {
            return (React.createElement(Grid, __assign({ key: i, item: true, xs: true }, (cfg.props || {})), cfg.data.map(function (d, index) {
                return React.createElement(RenderElement, { key: i + "-" + index, element: d });
            })));
        }))));
};
//# sourceMappingURL=desktop.js.map