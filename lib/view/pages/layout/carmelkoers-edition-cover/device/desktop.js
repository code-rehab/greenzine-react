import * as React from "react";
import { withStyles, Typography, Grid, Button, Hidden, } from "@material-ui/core";
import { observer } from "mobx-react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { mapEvent } from "../../../../../helpers/formatters";
import { Carousel } from "../../../../interface/partials/carousel";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
    return ({
        content: (_a = {
                position: "relative",
                zIndex: 999
            },
            _a[theme.breakpoints.down("md")] = {
                "& h1": {
                    fontSize: "3.4em",
                },
                "& h3": {
                    fontSize: "2em",
                },
                "& span br": {
                    display: "none",
                },
            },
            _a),
        root: (_b = {
                // overlay
                "&::before": {
                    content: "' '",
                    position: "absolute",
                    top: 0,
                    left: 0,
                    bottom: 0,
                    width: "40vw",
                    background: "linear-gradient(to right,#000,transparent)",
                    zIndex: 0,
                }
            },
            _b[theme.breakpoints.up("md")] = {
                width: "100%",
                minHeight: "calc(100vh - 60px)",
                padding: theme.spacing(6),
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
            },
            _b[theme.breakpoints.up("lg")] = {
                fontSize: "1.4em",
                padding: "5vw",
            },
            _b[theme.breakpoints.down("md")] = {
                // paddingBottom: 120,
                minHeight: "100vh",
                display: "flex",
                flexDirection: "column",
            },
            _b),
        primary: (_c = {
                fontSize: "calc(3em + 1.1vw)",
                marginBottom: theme.spacing(2)
            },
            _c[theme.breakpoints.down("sm")] = {
                fontSize: "5em",
            },
            _c),
        secondary: (_d = {
                fontSize: "1.1em",
                marginBottom: theme.spacing(2)
            },
            _d[theme.breakpoints.down("md")] = {
                fontSize: "2.5em",
            },
            _d[theme.breakpoints.down("sm")] = {
                fontSize: "3em",
            },
            _d),
        highlights: (_e = {
                display: "inline-block",
                zIndex: 99,
                position: "fixed",
                transformOrigin: "left",
                transform: "scale(.6)",
                left: 20,
                bottom: 70
            },
            _e[theme.breakpoints.up("md")] = {
                left: 60,
                bottom: 100,
                transform: "scale(1)",
            },
            _e[theme.breakpoints.up("lg")] = {
                left: 60,
                bottom: 90,
                transformOrigin: "left",
            },
            _e[theme.breakpoints.up("xl")] = {
                left: 100,
                bottom: 100,
                transformOrigin: "left",
            },
            _e),
        highlight: {
            // borderLeft: "1px solid #f0c33b",
            padding: theme.spacing(0.5, 10, 0, 1) + " !important",
            maxWidth: 375,
            marginTop: theme.spacing(2),
            "&:before": {
                content: "''",
                position: "absolute",
                left: 0,
                top: 0,
                width: 1,
                backgroundColor: "#D8D8D8",
                height: 150,
            },
        },
        highlightCategory: {
            color: "#f0c33b",
            textTransform: "uppercase",
            marginTop: theme.spacing(-1),
            marginBottom: theme.spacing(1),
            fontFamily: "Montserrat",
            fontWeight: 600,
        },
        highlightTitle: {
            fontSize: ".85em",
            lineHeight: 1.25,
            textDecoration: "underline",
            cursor: "pointer",
            "&:hover": {
                "& a": {
                    color: "#f0c33b",
                },
            },
            "& > a": {
                transition: "color 0.2s ease",
                color: "#fff",
            },
        },
        buttonReadMore: (_f = {
                padding: "7px 53px",
                borderRadius: 40,
                marginTop: theme.spacing(3),
                marginLeft: 50,
                fontSize: "1em"
            },
            _f[theme.breakpoints.up("md")] = {
                fontSize: "2em",
            },
            _f[theme.breakpoints.up("lg")] = {
                fontSize: ".75em",
            },
            _f["@media (max-height:800px)"] = { marginTop: 15 },
            _f),
        logo: (_g = {
                position: "absolute",
                right: 100,
                top: 100
            },
            _g[theme.breakpoints.down("md")] = {
                right: 30,
                top: 30,
                transformOrigin: "right",
                transform: "scale(.55)",
            },
            _g),
        imageWrap: (_h = {
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            },
            _h[theme.breakpoints.down("md")] = {
                position: "fixed",
                maxWidth: "60%",
                top: "unset",
                left: "unset",
                bottom: "5%",
                right: "5%",
                width: 500,
            },
            _h),
        image: (_j = {},
            // Mobile /////////////////////
            _j[theme.breakpoints.up("xs")] = {
                marginTop: theme.spacing(10),
                width: "80%",
                marginLeft: "10%",
                "@media (orientation: landscape)": {
                    marginTop: 45,
                    display: "none",
                },
            },
            // Tablet portait /////////////////////
            _j[theme.breakpoints.up("sm")] = {
                marginTop: theme.spacing(10),
                width: "80%",
                marginLeft: "10%",
                "@media (orientation: landscape)": {
                    display: "flex",
                },
            },
            // tablet landscape and smaller desktop
            _j[theme.breakpoints.up("md")] = {
                width: "50%",
                marginTop: theme.spacing(0),
                marginLeft: "0%",
            },
            //larger desktop
            _j[theme.breakpoints.up("lg")] = {
                width: "40%",
                marginTop: theme.spacing(6),
                marginLeft: "0%",
            },
            _j),
    });
};
export var LayoutEditionCoverDesktop = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, config = _a.config, presenter = _a.presenter;
    var styles = config.styles || {};
    return (React.createElement("div", { style: { position: "relative", width: "100%" } },
        React.createElement("div", { className: classes.root },
            React.createElement("div", { className: classes.logo },
                React.createElement("img", { src: "/assets/images/carmel/logo.svg" })),
            React.createElement("div", { className: classes.content },
                React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                    React.createElement(Typography, { className: classes.primary, variant: "h1", style: styles.primary || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.primary } }))),
                React.createElement(SlideFade, { direction: "up", timeout: 1100 },
                    React.createElement(Typography, { className: classes.secondary, variant: "h3", style: styles.secondary || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.secondary } }))),
                React.createElement(SlideFade, { direction: "up", timeout: 1200 },
                    React.createElement("img", { src: "/assets/images/carmel/toekomstbouwers.svg", style: styles.image || {}, className: classes.image }),
                    React.createElement("br", null),
                    React.createElement(Button, { onClick: presenter.nextArticle, variant: "contained", color: "primary", className: classes.buttonReadMore }, "Let's go!"))),
            React.createElement("div", { className: classes.highlights },
                React.createElement(Grid, { container: true, justify: "flex-start" },
                    React.createElement(Hidden, { mdUp: true },
                        React.createElement(Carousel, { color: "#f0c33b", data: config.highlightedContent })),
                    React.createElement(Hidden, { smDown: true }, (config.highlightedContent || []).map(function (content, index) { return (React.createElement(SlideFade, { key: index, direction: "up", timeout: 1400 + index * 100 },
                        React.createElement(Grid, { item: true, className: classes.highlight },
                            React.createElement(Typography, { className: classes.highlightCategory }, content.title),
                            React.createElement(Typography, { variant: "body2", className: classes.highlightTitle },
                                React.createElement("div", { onClick: mapEvent(presenter.selectArticle, content.link) }, content.content))))); })))))));
}));
//# sourceMappingURL=desktop.js.map