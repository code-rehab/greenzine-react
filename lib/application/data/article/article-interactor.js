var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { computed, observable } from "mobx";
import { route } from "../../../config/routes";
import { Application } from "../../application";
var DefaultArticleInteractor = /** @class */ (function () {
    function DefaultArticleInteractor(_articleProvider, _magazineInteractor) {
        var _this = this;
        this._articleProvider = _articleProvider;
        this._magazineInteractor = _magazineInteractor;
        this.created = undefined;
        this.selectedArticle = undefined;
        this.selectedPage = undefined;
        this.previous = undefined;
        this.transition = "cross-fade";
        this._listeners = [];
        this._animating = false;
        this.nextArticle = function () {
            if (_this._animating) {
                return;
            }
            _this.transition = "slide-left";
            var index = _this.articleIndex + 1;
            if (_this.articles[index]) {
                _this.selectArticle(_this.articles[index].id);
            }
        };
        this.previousArticle = function () {
            if (_this._animating) {
                return;
            }
            _this.transition = "slide-right";
            var index = _this.articleIndex - 1;
            if (_this.articles[index]) {
                _this.selectArticle(_this.articles[index].id);
            }
        };
        this.nextPage = function () {
            if (_this._animating) {
                return;
            }
            _this.transition = "slide-up";
            var index = _this.pageIndex + 1;
            if (_this.pages[index]) {
                _this.selectPage(_this.pages[index].id);
            }
        };
        this.previousPage = function () {
            if (_this._animating) {
                return;
            }
            _this.transition = "slide-down";
            var index = _this.pageIndex - 1;
            if (_this.pages[index]) {
                _this.selectPage(_this.pages[index].id);
            }
        };
        this.selectPage = function (id, redirect) {
            if (redirect === void 0) { redirect = true; }
            return __awaiter(_this, void 0, void 0, function () {
                var result;
                var _this = this;
                return __generator(this, function (_a) {
                    if (id === (this.selectedPage && this.selectedPage.id) || this._animating) {
                        return [2 /*return*/];
                    }
                    result = this.selectedArticle &&
                        this.selectedArticle.pages &&
                        this.selectedArticle.pages.filter(function (page) {
                            return page.id === id;
                        });
                    this.selectedPage = result && result[0];
                    if (redirect && this.selectedPage) {
                        this._animating = true;
                        setTimeout(function () {
                            _this._animating = false;
                        }, 500);
                        Application.router &&
                            Application.router.history.push(route("article.page", {
                                magazine: (this._magazineInteractor.selectedMagazine &&
                                    this._magazineInteractor.selectedMagazine.id) ||
                                    "undefined",
                                edition: (this._magazineInteractor.selectedEdition &&
                                    this._magazineInteractor.selectedEdition.id) ||
                                    "undefined",
                                article: (this.selectedArticle &&
                                    (this.selectedArticle.slug || this.selectedArticle.id)) ||
                                    "undefined",
                                page: (this.selectedPage &&
                                    (this.selectedPage.slug || this.selectedPage.id)) ||
                                    "undefined"
                            }));
                    }
                    return [2 /*return*/];
                });
            });
        };
        this.selectArticle = function (id, redirect) {
            if (redirect === void 0) { redirect = true; }
            return __awaiter(_this, void 0, void 0, function () {
                var found, pages;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (id === (this.selectedArticle && this.selectedArticle.id) ||
                                this._animating) {
                                return [2 /*return*/];
                            }
                            found = this._articleProvider.get(id);
                            if (!found) return [3 /*break*/, 2];
                            this.selectedPage = undefined;
                            return [4 /*yield*/, found.fetch()];
                        case 1:
                            _a.sent();
                            this.selectedArticle = found;
                            this._listeners.forEach(function (func) { return func(); });
                            pages = (this.selectedArticle && this.selectedArticle.pages) || [];
                            this.selectPage(pages[0] && pages[0].id, redirect);
                            _a.label = 2;
                        case 2: return [2 /*return*/];
                    }
                });
            });
        };
        this.onArticleChange = function (func) {
            _this._listeners.push(func);
        };
    }
    Object.defineProperty(DefaultArticleInteractor.prototype, "articles", {
        get: function () {
            return ((this._magazineInteractor.selectedEdition &&
                this._magazineInteractor.selectedEdition.articles) ||
                []);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultArticleInteractor.prototype, "articleIndex", {
        get: function () {
            var _this = this;
            return this.articles.findIndex(function (article) {
                return article.id === (_this.selectedArticle && _this.selectedArticle.id);
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultArticleInteractor.prototype, "pages", {
        get: function () {
            return (this.selectedArticle && this.selectedArticle.pages) || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultArticleInteractor.prototype, "pageIndex", {
        get: function () {
            var _this = this;
            return this.pages.findIndex(function (page) { return page.id === (_this.selectedPage && _this.selectedPage.id); });
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], DefaultArticleInteractor.prototype, "created", void 0);
    __decorate([
        observable
    ], DefaultArticleInteractor.prototype, "selectedArticle", void 0);
    __decorate([
        observable
    ], DefaultArticleInteractor.prototype, "selectedPage", void 0);
    __decorate([
        observable
    ], DefaultArticleInteractor.prototype, "previous", void 0);
    __decorate([
        computed
    ], DefaultArticleInteractor.prototype, "articles", null);
    __decorate([
        computed
    ], DefaultArticleInteractor.prototype, "articleIndex", null);
    __decorate([
        computed
    ], DefaultArticleInteractor.prototype, "pages", null);
    __decorate([
        computed
    ], DefaultArticleInteractor.prototype, "pageIndex", null);
    return DefaultArticleInteractor;
}());
export { DefaultArticleInteractor };
//# sourceMappingURL=article-interactor.js.map