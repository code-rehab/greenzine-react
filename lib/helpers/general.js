export function replaceAll(str, find, replace) {
    return str && str.replace(new RegExp(find, "g"), replace);
}
export function take(obj, keys, returnUndefinedValues) {
    if (returnUndefinedValues === void 0) { returnUndefinedValues = false; }
    return keys.reduce(function (result, key) {
        if (obj[key] || returnUndefinedValues) {
            result[key] = obj[key];
        }
        return result;
    }, {});
}
//# sourceMappingURL=general.js.map