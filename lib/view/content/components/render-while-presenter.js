var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed } from "mobx";
var RenderWhilePresenter = /** @class */ (function () {
    function RenderWhilePresenter(_printInteractor) {
        this._printInteractor = _printInteractor;
        //
    }
    Object.defineProperty(RenderWhilePresenter.prototype, "isPrint", {
        get: function () {
            return this._printInteractor.printActive;
        },
        enumerable: true,
        configurable: true
    });
    RenderWhilePresenter.prototype.mount = function () {
        // alert('mounted');
        //
    };
    RenderWhilePresenter.prototype.unmount = function () {
        //
    };
    __decorate([
        computed
    ], RenderWhilePresenter.prototype, "isPrint", null);
    return RenderWhilePresenter;
}());
export { RenderWhilePresenter };
//# sourceMappingURL=render-while-presenter.js.map