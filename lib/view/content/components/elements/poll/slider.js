var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from "react";
import { withStyles } from "@material-ui/core";
import { observer } from "mobx-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        maxWidth: 400
    },
    black: {
        color: "black"
    },
    slider: {
        color: "white"
    },
    heartBar: {
        display: "flex",
        justifyContent: "space-between",
        width: "100%",
        fontSize: "20px"
    },
    heart: {
        margin: 10,
        cursor: "pointer",
        "&:active": {
            outline: "#0000"
        },
        WebkitTapHighlightColor: "#0000"
    }
}); };
var SliderComponent = /** @class */ (function (_super) {
    __extends(SliderComponent, _super);
    function SliderComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.drawHearts = function () {
            var _a = _this.props, classes = _a.classes, maxValue = _a.maxValue, currentValue = _a.currentValue;
            var el = [];
            var _loop_1 = function (i) {
                el.push(React.createElement(FontAwesomeIcon, { key: i, className: classes.heart, style: { opacity: (currentValue || 0) <= i ? 0.6 : 1 }, icon: faHeart, size: "lg", onClick: function () {
                        if (_this.props.onChange) {
                            _this.props.onChange([(i + 1).toString()]);
                        }
                    } }));
            };
            for (var i = 0; i < (maxValue || 100); i++) {
                _loop_1(i);
            }
            return el;
        };
        _this.render = function () {
            var classes = _this.props.classes;
            return (React.createElement("div", { className: classes.root },
                React.createElement("div", { className: classes.heartBar }, _this.drawHearts())));
        };
        return _this;
    }
    SliderComponent = __decorate([
        observer
    ], SliderComponent);
    return SliderComponent;
}(React.Component));
export var HeartSlider = withStyles(styles)(SliderComponent);
//# sourceMappingURL=slider.js.map