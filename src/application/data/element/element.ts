import { BaseModel, Model, RecordData } from "../../network/base-model";
import { data } from "../../network/decorators/graphql-data";
import { computed, observable } from "mobx";

import { v4 as uuid } from "uuid";
import { ElementUnion, ElementProps, AnimationProps } from "./types/element";

export enum ElementType {
  Typography = "Typography",
  Quote = "Quote",
  ReadTime = "ReadTime",
  Video = "Video",
  Image = "Image",
  WistJeDat = "WistJeDat",
  Divider = "Divider",
  Author = "Author",
  List = "List",
  Popup = "Popup",
  Index = "Index"
}

export interface ElementBase extends Model<ElementRecord> {
  //
}

export interface ElementRecord extends RecordData {
  section: string;
  component: ElementType;
  props: ElementProps | string;
  animation: AnimationProps;
}

export type Element = ElementUnion;

export class ElementModel extends BaseModel<ElementRecord> implements ElementBase {
  public animation: AnimationProps = { duration: 0, delay: 0 };

  @data id: string = uuid();
  @data section: string = "";
  @data component: ElementType = ElementType.Typography;

  @computed public get props(): ElementProps {
    let props = this.changes.props || this.record.props;
    const distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];

    if (!props || typeof props === "string") {
      props = replaceAll(props || "{}", "/article/images/", distribution + "/ncf/bondig/");

      props = observable(JSON.parse(props));
    }

    return (props || {}) as ElementProps;
  }

  public set props(props: ElementProps) {
    this.changes.props = props;
  }

  public serialize = () => {
    return {
      ...this._values,
      props: JSON.stringify(this.props),
    };
  };
}

function replaceAll(str: string, find: string, replace: string) {
  return str && str.replace(new RegExp(find, "g"), replace);
}
