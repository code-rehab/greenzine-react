import * as React from "react";
import { withStyles } from "@material-ui/styles";
import { IconButton, Typography } from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react";
import { mapEvent } from "../../../helpers/formatters";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import classnames from "classnames";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            position: "absolute",
            left: "50%",
            height: "100%",
            transform: "translateX(-50%)",
            display: "flex",
            alignItems: "center",
            alignContent: "stretch",
            // marginRight: "auto",
            // marginLeft: "auto",
            color: "#4A4A4A"
        },
        counter: {
            padding: theme.spacing(0, 2.5),
            fontWeight: 500,
            fontFamily: "Montserrat",
            fontSize: "1.1rem"
        },
        buttonNext: {},
        buttonPrevious: {},
        buttonDisabled: {},
        button: (_a = {
                borderRadius: 0,
                height: "100%",
                width: theme.spacing(5),
                color: "#4A4A4A",
                borderRight: "1px solid #E7E7E7",
                borderLeft: "1px solid #E7E7E7"
            },
            _a[theme.breakpoints.down("xs")] = {
                width: theme.spacing(3.5)
            },
            _a["&:hover"] = {
                color: "#48587e",
                backgroundColor: "#fff"
            },
            _a)
    });
};
var ArticleNavigatorComponent = observer(function (_a) {
    var list = _a.list, classes = _a.classes, current = _a.current, onNext = _a.onNext, onPrevious = _a.onPrevious;
    var prevDisabled = list.indexOf(current) <= 0;
    var nextDisabled = list.indexOf(current) >= list.length - 1;
    return (React.createElement("div", { className: classes.root },
        React.createElement(IconButton, { disabled: prevDisabled, className: classnames(classes.buttonPrevious, classes.button), onClick: mapEvent(onPrevious, list.indexOf(current) - 1) },
            React.createElement(FontAwesomeIcon, { size: "sm", icon: faChevronLeft })),
        React.createElement(Typography, { className: classes.counter, variant: "body1" },
            list.indexOf(current) + 1,
            "/",
            list.length),
        React.createElement(IconButton, { disabled: nextDisabled, className: classnames(classes.buttonNext, classes.button), onClick: mapEvent(onNext, list.indexOf(current) + 1) },
            React.createElement(FontAwesomeIcon, { size: "sm", icon: faChevronRight }))));
});
export var ArticleNavigator = withStyles(styles)(ArticleNavigatorComponent);
//# sourceMappingURL=navigator-article.js.map