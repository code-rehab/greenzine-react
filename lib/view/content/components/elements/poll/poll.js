import { Button, Typography, withStyles } from "@material-ui/core";
import { toJS } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import { withRouter } from "react-router";
import { withPresenter } from "../../../../../helpers/with-presenter";
import { SlideFade } from "../../effects/slide-fade";
import { MultipleChoice } from "./multiple-choice";
import { PollPresenter } from "./poll-presenter";
import { ProgressBar } from "./progress-bar";
import { HeartSlider } from "./slider";
import LoadingAnimation from "../../../../pages/loading";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    progressBar: {
        maxWidth: 400,
        width: "100%",
        margin: "auto",
    },
    button: {
        margin: "auto",
        transition: "color cubic-bezier(0.4, 0, 0.2, 1) 0ms",
        color: "white",
        borderColor: "#fff",
        position: "relative",
        zIndex: 99999,
        "&:hover": {
            backgroundColor: "white",
            color: "#4a4a4a",
        },
    },
    question: {
        marginBottom: 40,
    },
    answers: {
        marginBottom: 40,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center",
    },
    answer: {
        width: "100%",
        color: "white",
        fontSize: "16px",
        backgroundColor: "transparent",
        height: 200,
        padding: 10,
        borderRadius: "15px",
        border: "1px solid",
        "&:focus": {
            outline: "none",
        },
    },
    disclaimer: {
        marginTop: 30,
    },
    modal: {
        backgroundColor: "transparent",
        color: "white",
        padding: theme.spacing(2),
        boxShadow: "none",
    },
    montserrat: {
        fontFamily: "Montserrat",
    },
    textarea: {
        width: "100%",
        maxWidth: 500,
        color: "white",
        fontSize: "16px",
        backgroundColor: "transparent",
        height: 200,
        padding: 10,
        borderRadius: "15px",
        border: "1px solid",
        "&::placeholder": {
            color: "rgba(255,255,255,0.5)",
            fontFamily: "Domaine",
        },
        "&:focus": {
            outline: "none",
        },
    },
}); };
var PollComponent = observer(function (_a) {
    var classes = _a.classes, presenter = _a.presenter;
    if (presenter.loading) {
        return React.createElement(LoadingAnimation, null);
    }
    var delay = 100;
    return ((presenter.currentQuestion && (React.createElement("div", { className: classes.root, key: presenter.pollId },
        React.createElement(SlideFade, { direction: "up", delay: delay },
            React.createElement("div", { className: classes.progressBar },
                React.createElement(ProgressBar, { subject: "Vraag", total: presenter.questionCount, current: presenter.currentQuestionIndex + 1 }))),
        React.createElement(SlideFade, { direction: "up", delay: (delay += 100) },
            React.createElement("div", { className: classes.question },
                React.createElement(Typography, { variant: "h6", className: classes.montserrat, align: "center" }, presenter.currentQuestion.question))),
        React.createElement(SlideFade, { direction: "up", delay: (delay += 100) },
            React.createElement("div", { className: classes.answers },
                presenter.currentQuestion.type === "multiselect" && (React.createElement(MultipleChoice, { multiselect: true, options: toJS(presenter.currentQuestion.options) || [], answers: presenter.currentValue || [], onChange: presenter.updateValue })),
                presenter.currentQuestion.type === "rating" && (React.createElement(HeartSlider, { maxValue: 5, onChange: presenter.updateValue, currentValue: presenter.currentValue })),
                presenter.currentQuestion.type === "select" && (React.createElement(MultipleChoice, { options: toJS(presenter.currentQuestion.options) || [], answers: presenter.currentValue || [], onChange: presenter.updateValue })),
                presenter.currentQuestion.type === "text" && (React.createElement("textarea", { className: classes.textarea, placeholder: "Type hier jouw bericht", onChange: function (e) { return presenter.updateValue(e.target.value); }, defaultValue: (presenter.currentValue && presenter.currentValue[0]) || "" })))),
        presenter.errorMessage && (React.createElement(SlideFade, { direction: "up", delay: (delay += 100) },
            React.createElement(Typography, { gutterBottom: true, variant: "body2" }, presenter.errorMessage))),
        React.createElement(SlideFade, { direction: "up", delay: (delay += 100) },
            React.createElement("div", null,
                React.createElement(Button, { onClick: presenter.onSubmit, className: classes.button, variant: "outlined" }, "Antwoord verzenden"))),
        React.createElement(SlideFade, { direction: "up", delay: (delay += 100) },
            React.createElement(Typography, { className: classes.disclaimer },
                React.createElement("small", null, "Jouw antwoorden worden vertrouwelijk gebruikt.")))))) || (React.createElement(Typography, { gutterBottom: true, variant: "h4", align: "center" }, "dankjewel")));
});
export var Poll = withStyles(styles)(withRouter(withPresenter(function (_a, _b) {
    var match = _a.match, history = _a.history, location = _a.location, staticContext = _a.staticContext, questionIndex = _a.questionIndex, pollId = _a.pollId;
    var provider = _b.provider;
    return new PollPresenter(pollId, questionIndex, provider.poll, {
        match: match,
        history: history,
        location: location,
        staticContext: staticContext,
    });
}, PollComponent)));
//# sourceMappingURL=poll.js.map