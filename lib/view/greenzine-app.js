import * as React from "react";
import { Router } from "react-router-dom";
import { DefaultInterface } from "./interface/interface";
import Amplify from "aws-amplify";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Routes from "./routes";
import "./greenzine-app.css";
import ReactGA from "react-ga4";
import { Content } from "@coderehab/greenzeen-content";
import { Themes, useTheme } from "@coderehab/greenzeen-themes";
import { createBrowserHistory } from "history";
export var GreenzineApp = function (_a) {
    var config = _a.config, theme = _a.theme, tenant = _a.tenant;
    Amplify.configure(config.aws);
    Themes.configure({ tenant: tenant });
    Content.configure({ tenant: tenant });
    var MuiTheme = useTheme().MuiTheme;
    React.useEffect(function () {
        document.addEventListener("DOMContentLoaded", function () {
            // set theme color for chrome based on themes primary color
            var themeColor = document.head.querySelector('meta[name="theme-color"]');
            if (themeColor) {
                themeColor.content = MuiTheme.palette.primary.main;
            }
        });
    });
    var history = createBrowserHistory();
    // Initialize google analytics page view tracking
    history.listen(function (location) {
        ReactGA.set({ page: location.pathname }); // Update the user's current page
        ReactGA.pageview(location.pathname); // Record a pageview for the given page
    });
    return (React.createElement(ThemeProvider, { theme: MuiTheme },
        React.createElement(CssBaseline, null),
        React.createElement("div", { className: "App", style: { position: "relative" } },
            React.createElement(Router, { history: history },
                React.createElement(Routes, null),
                React.createElement(DefaultInterface, null)))));
};
//# sourceMappingURL=greenzine-app.js.map