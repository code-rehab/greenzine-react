import * as React from "react";
import "./greenzine-app.css";
import { Theme } from "@material-ui/core";
interface GreenzineAppConfig {
    aws: any;
}
export interface GreenzineAppProps {
    config: GreenzineAppConfig;
    theme?: Theme;
    tenant: string;
}
export declare const GreenzineApp: React.FC<GreenzineAppProps>;
export {};
