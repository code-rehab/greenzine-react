import * as React from "react";
import {
  Theme,
  WithStyles,
  withStyles,
  StyleRules,
  Typography,
  Grid,
  Button,
  Hidden,
} from "@material-ui/core";

import { observer } from "mobx-react";
import { CarmelkennismakingEditionCoverPresenter } from "../carmelkennismaking-edition-cover-presenter";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { mapEvent } from "../../../../../helpers/formatters";
import { Carousel } from "../../../../interface/partials/carousel";

type OwnProps = { config: any; presenter: CarmelkennismakingEditionCoverPresenter } & WithStyles<
  | "root"
  | "title"
  | "subtitle"
  | "highlight"
  | "highlightCategory"
  | "highlightTitle"
  | "highlights"
  | "buttonReadMore"
  | "imageWrap"
  | "image"
  | "content"
  | "logo"
>;
const styles = (theme: Theme): StyleRules => ({
  logo: {
    position: "absolute",
    left: 200,
    top: 100,
    [theme.breakpoints.down("md")]: {
      left: 20,
      top: 30,
      transformOrigin: "left",
      transform: "scale(.55)",
    },
  },

  root: {
    // overlay
    "&::before": {
      content: "' '",
      position: "absolute",
      top: 0,
      left: 0,
      bottom: 0,
      width: "100vw",
      background: "linear-gradient(to right,#000,transparent)",
      zIndex: 0,
    },
    [theme.breakpoints.up("md")]: {
      width: "100%",
      minHeight: "calc(100vh - 60px)",
      padding: theme.spacing(6),
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: "1.4em",
      padding: "5vw",
    },

    [theme.breakpoints.down("md")]: {
      // paddingBottom: 120,
      minHeight: "100vh",
      display: "flex",
      flexDirection: "column",
    },
  },
  content: {
    position: "absolute",
    zIndex: 999,
    top: 0,
    bottom: 0,
    left: 200,
    // poaddingLeft: 200,
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
    justifyContent: "center",

    [theme.breakpoints.up("md")]: {
      width: "65vw",
    },
    [theme.breakpoints.down("md")]: {
      top: -100,
      left: 20,
      fontSize: "2em",
      "& h1": {
        fontSize: "3.4em",
      },
      "& h3": {
        fontSize: "2em",
      },
      "& span br": {
        display: "none",
      },
    },
  },
  title: {
    fontSize: "calc(1rem + 2.2em + 1vw)",
    marginBottom: theme.spacing(2),
    '@media screen and (max-height: 860px) and (max-width: 1400px)': {
      fontSize: "calc(1rem + 1.4em + 1vw)",
    },
  },
  subtitle: {
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down("md")]: {
      fontSize: 22
    }
  },
  buttonReadMore: {
    padding: "7px 38px",
    borderRadius: 40,
    marginTop: theme.spacing(3),
    // marginLeft: 50,
    fontFamily: "Amaranth",
    fontSize: 17,
  },

  highlights: {
    display: "inline-block",
    zIndex: 999,
    position: "fixed",
    transformOrigin: "left",
    transform: "scale(1)",
    left: 20,
    bottom: 100,

    [theme.breakpoints.up("md")]: {
      left: 60,
      bottom: 100,
      transform: "scale(1)",
    },

    [theme.breakpoints.up("lg")]: {
      left: 60,
      bottom: 90,
      transformOrigin: "left",
    },

    [theme.breakpoints.up("xl")]: {
      left: 100,
      bottom: 100,
      transformOrigin: "left",
    },

    '@media screen and (max-height: 860px) and (max-width: 1400px)': {
      bottom: 60,
    },
  },
  highlight: {
    padding: theme.spacing(0.5, 5, 0, 1) + " !important",
    maxWidth: 375,
    marginTop: theme.spacing(2),
    transformOrigin: "center",
    '@media screen and (max-height: 860px) and (max-width: 1400px)': {
      maxWidth: 230,
      transform: "scale(.8)"
    },

    '@media screen and (max-width: 1200px)': {
      maxWidth: 180,
      transform: "scale(.8)"
    },

    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      top: 0,
      width: 1,
      backgroundColor: "#D8D8D8",
      height: 150,
    },
  },
  highlightCategory: {
    color: "#f0c33b",
    textTransform: "uppercase",
    marginTop: theme.spacing(-1),
    marginBottom: theme.spacing(1),
    fontFamily: "Amaranth",
    fontSize: 17,
  },
  highlightTitle: {
    fontSize: 25,
    '@media screen and (max-width: 1200px)': {
      fontSize: 20,
    },
    lineHeight: 1.25,
    textDecoration: "underline",
    fontFamily: "Amaranth",
    cursor: "pointer",
    "&:hover": {
      "& a": {
        color: "#f0c33b",
      },
    },

    "& > a": {
      transition: "color 0.2s ease",
      color: "#fff",
    },
  },

  imageWrap: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,

    [theme.breakpoints.down("md")]: {
      position: "fixed",
      maxWidth: "60%",
      top: "unset",
      left: "unset",
      bottom: "5%",
      right: "5%",
      width: 500,
    },
  },
  image: {
    // Mobile /////////////////////
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(10),
      width: "80%",
      marginLeft: "10%",
    },

    // Tablet portait /////////////////////
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10),
      width: "80%",
      marginLeft: "10%",
    },
    // tablet landscape and smaller desktop
    [theme.breakpoints.up("md")]: {
      width: "50%",
      marginTop: theme.spacing(0),
      marginLeft: "0%",
    },
    //larger desktop
    [theme.breakpoints.up("lg")]: {
      width: "40%",
      marginTop: theme.spacing(6),
      marginLeft: "0%",
    },
  },
});

export const LayoutEditionCoverDesktop = withStyles(styles)(


  observer(({ classes, config, presenter }: OwnProps) => {
    const styles = config.styles || {};

    return (
      <div style={{ position: "relative", width: "100%" }}>
        <div className={classes.root}>
          <div className={classes.logo}>
            <img src="/assets/images/carmel/logo.svg" />
          </div>
          <div className={classes.content}>
            <SlideFade direction="up" timeout={1200}>
              <SlideFade direction="up" timeout={1000}>
                <Typography className={classes.title} variant="h1" style={styles.title || {}}>
                  Ruimte in verbinding
                </Typography>
              </SlideFade>
              <SlideFade direction="up" timeout={1000}>
                <Typography className={classes.subtitle} variant="h4" style={styles.subtitle || {}}>
                  Kennismaken met Carmel
                </Typography>
              </SlideFade>
              <Button
                onClick={presenter.nextArticle}
                variant="contained"
                color="primary"
                className={classes.buttonReadMore}
              >
                Lees verder
              </Button>
            </SlideFade>
          </div>
          <div className={classes.highlights}>
            <Grid container justify="flex-start">
              <Hidden mdUp>
                <Carousel color={"#f0c33b"} data={config.highlightedContent} />
              </Hidden>
              <Hidden smDown>
                {(config.highlightedContent || []).map((content: any, index: number) => (
                  <SlideFade key={index} direction="up" timeout={1400 + index * 100}>
                    <Grid item className={classes.highlight}>
                      <Typography className={classes.highlightCategory}>{content.title}</Typography>
                      <Typography variant={"body2"} className={classes.highlightTitle}>
                        <div onClick={mapEvent(presenter.selectArticle, content.link)}>
                          {content.content}
                        </div>
                      </Typography>
                    </Grid>
                  </SlideFade>
                ))}
              </Hidden>
            </Grid>
          </div>
        </div>
      </div>
    );
  })
);
