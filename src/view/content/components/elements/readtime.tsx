import * as React from "react";
import { withStyles, Typography, Theme } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import { RenderWhile } from "../render-while";
import { SlideFade } from "../effects/slide-fade";

interface OwnProps extends WithStyles<"root" | "icon"> {
  time?: string;
  category: string;
  align?: string;
  animation?: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    alignItems: "center",

    marginBottom: "0.5em",
    [theme.breakpoints.up("md")]: {
      marginBottom: theme.spacing(2),
    },
  },
  icon: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 20,
  },
});

export const ReadTime = withStyles(styles)(({ classes, time, category, align, animation }: any) => {
  animation = animation || { duration: 0, delay: 0 };

  return (
    <>
      <RenderWhile desktop tablet mobile>
        <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
          <div className={classes.root} style={{ justifyContent: "align", flexWrap: "wrap" }}>
            <Typography variant="body2">{category}</Typography>
            {time && (
              <>
                <FontAwesomeIcon className={classes.icon} icon={faClock}></FontAwesomeIcon>
                <Typography variant="body2">Leestijd: {time}</Typography>
              </>
            )}
          </div>
        </SlideFade>
      </RenderWhile>
      <RenderWhile print>
        <Typography variant="body2">
          {category} {time && ` - Leestijd: ${time}`}
        </Typography>
      </RenderWhile>
    </>
  );
});
