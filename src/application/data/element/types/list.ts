import { ElementType, ElementBase } from "../element";

export interface ListElementProps {
  icon?: any;
  items: any[];
  variant: string;
}

export interface ListElement extends ElementBase {
  component: ElementType.List;
  props: ListElementProps;
}
