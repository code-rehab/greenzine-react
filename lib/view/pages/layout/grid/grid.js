var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { LayoutColumnsDesktop } from "./device/desktop";
import { LayoutColumnsPrint } from "./device/print";
import { RenderWhile } from "../../../content/components/render-while";
import { LayoutColumnsMobile } from "./device/mobile";
export var LayoutGrid = function (_a) {
    var config = _a.config, data = _a.data;
    data.forEach(function (d, i) {
        d.animation = {
            duration: 700,
            delay: i * 50,
        };
    });
    var result = (Object.keys(config).length && {
        container: config.container || {},
        sections: Object.keys(config.sections).map(function (section_id) {
            var sectionData = data.filter(function (d) { return d.section === section_id; });
            return __assign(__assign({}, config.sections[section_id]), { section: section_id, data: sectionData });
        }),
    }) || {
        container: {},
        sections: [
            {
                data: data,
                section: "main",
            },
        ],
    };
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { desktop: true, tablet: true },
            React.createElement(LayoutColumnsDesktop, { data: result })),
        React.createElement(RenderWhile, { mobile: true },
            React.createElement(LayoutColumnsMobile, { data: result })),
        React.createElement(RenderWhile, { print: true },
            React.createElement(LayoutColumnsPrint, { data: result }))));
};
//# sourceMappingURL=grid.js.map