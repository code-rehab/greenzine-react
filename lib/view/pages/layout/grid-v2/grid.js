import * as React from "react";
import { observer } from "mobx-react";
import { LayoutRenderer } from "./layout-renderer";
import { PageContainer } from "./page-container";
import { ArticleRedirect } from "../../../content/components/article-redirect";
export var GridV2 = observer(function (_a) {
    var print = _a.print, page = _a.page, breakpoint = _a.breakpoint, style = _a.style, presenter = _a.presenter;
    page.data.reverse().forEach(function (d, i) {
        d.animation = {
            duration: 700,
            delay: i * 50,
        };
    });
    return (React.createElement(PageContainer, { print: print, page: page, style: style || {} },
        React.createElement(LayoutRenderer, { print: print || false, layout: page.layouts[breakpoint || "lg"] || [], elements: page.data }),
        presenter &&
            !presenter.hasNextPage &&
            presenter.hasNextArticle &&
            presenter.currentPage &&
            presenter.currentPage.layout !== "bondig-edition-cover" &&
            presenter.currentPage.layout !== "mst-edition-2020-cover" &&
            presenter.currentPage.layout !== "carmelkennismaking-edition-cover" &&
            presenter.currentPage.layout !== "carmelkoers-edition-cover" && (React.createElement(ArticleRedirect, { onClick: presenter.toNextArticle, thumbnail: presenter.nextArticle &&
                (presenter.nextArticle.featuredImage
                    ? presenter.nextArticle.featuredImage
                    : presenter.nextArticle.image) }, presenter.nextArticle && presenter.nextArticle.title))));
});
//# sourceMappingURL=grid.js.map