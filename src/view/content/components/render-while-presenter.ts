import { computed } from "mobx";
import { IPresenter } from "../../../helpers/with-presenter";
import { PrintInteractor } from "../../../application/business/interactor/print-interactor";

export class RenderWhilePresenter implements IPresenter {
  @computed public get isPrint() {
    return this._printInteractor.printActive;
  }

  constructor(private _printInteractor: PrintInteractor) {
    //
  }

  public mount() {
    // alert('mounted');
    //
  }

  public unmount() {
    //
  }
}
