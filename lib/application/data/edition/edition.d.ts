import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { Article, ArticleRecord } from "../article/article";
export interface EditionData {
    id: string;
    index: string;
    public: string;
    pollSlug: string;
    subtitle: string;
    slug: string;
    title: string;
    description: string;
    image: string;
    articles: ArticleRecord[];
    magazineId: string;
    theme?: string;
}
export interface Edition extends GraphQLModel<EditionData>, EditionData {
    articles: Article[];
}
export interface EditionValues {
}
export declare class EditionModel extends GraphQLBase<EditionData> implements Edition {
    slug: string;
    title: string;
    public: string;
    pollSlug: string;
    index: string;
    version: string;
    content: string;
    type: string;
    description: any;
    image: string;
    subtitle: string;
    editors: any;
    coverArticle: string;
    magazineId: string;
    get articles(): Article[];
}
