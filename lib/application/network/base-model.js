var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { observable, toJS, computed } from "mobx";
var BaseModel = /** @class */ (function () {
    function BaseModel(_record) {
        var _this = this;
        this.changes = {};
        this.defaultValues = {};
        this.serialize = function () {
            return _this._values;
        };
        this.take = function (keys) {
            var data = _this._values;
            return keys.reduce(function (result, key) {
                result[key] = data[key];
                return result;
            }, {});
        };
        this._record = _record;
    }
    Object.defineProperty(BaseModel.prototype, "record", {
        get: function () {
            return this._record;
        },
        set: function (record) {
            this._record = record;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseModel.prototype, "id", {
        get: function () {
            return this._record.id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseModel.prototype, "_values", {
        get: function () {
            return toJS(__assign(__assign(__assign(__assign({}, this.defaultValues), this.record), this.changes), { id: this.id }));
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], BaseModel.prototype, "changes", void 0);
    __decorate([
        observable
    ], BaseModel.prototype, "_record", void 0);
    __decorate([
        computed
    ], BaseModel.prototype, "record", null);
    return BaseModel;
}());
export { BaseModel };
//# sourceMappingURL=base-model.js.map