var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed, observable, toJS } from "mobx";
import { removeNullValues } from "../../../../application/network/helpers";
function replaceAll(str, find, replace) {
    return str && str.replace(new RegExp(find, "g"), replace);
}
var GridPresenter = /** @class */ (function () {
    function GridPresenter(article, page) {
        var _this = this;
        this.article = article;
        this.page = page;
        this.rowHeight = 12;
        this.margin = [24, 0];
        this.cols = 24;
        this._refs = {};
        this.isPublishing = false;
        this.pageEditing = false;
        this._tmpRefs = {};
        this.mount = function () {
            _this.updateElementSizes();
            window.addEventListener("resize", function () {
                clearTimeout(_this._resizeTimeout);
                _this._resizeTimeout = setTimeout(_this.updateElementSizes, 500);
            });
        };
        this.unmount = function () {
            //
        };
        this.updateElementSizes = function () {
            setTimeout(function () {
                var _refs = Object.keys(_this._tmpRefs).reduce(function (refs, key) {
                    var item = _this._tmpRefs[key];
                    refs[item.id] = { height: (item.ref && item.ref.clientHeight) || 0 };
                    return refs;
                }, {});
                _this._refs = __assign(__assign({}, _this._refs), _refs);
            }, 200);
        };
        this.registerRef = function (id, ref) {
            if (ref) {
                _this._tmpRefs[id] = { id: id, ref: ref };
            }
        };
        //
    }
    Object.defineProperty(GridPresenter.prototype, "layout", {
        get: function () {
            var _this = this;
            return removeNullValues(toJS((this.page.layouts && this.page.layouts.lg) || []).map(function (l) {
                var h = Math.ceil(_this._refs[l.i] &&
                    _this._refs[l.i].height &&
                    _this._refs[l.i].height / (_this.rowHeight + _this.margin[1])) || 2;
                l.h = h;
                l.minW = 2;
                l.w = l.w < 2 ? _this.cols : l.w;
                return l;
            }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridPresenter.prototype, "style", {
        get: function () {
            var distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
            var articleStyleStr = replaceAll(this.article.style || "{}", "/article/images/", distribution + "/ncf/bondig/");
            var pageStyleStr = replaceAll(this.page.style || "{}", "/article/images/", distribution + "/ncf/bondig/");
            var articleStyle = JSON.parse(articleStyleStr);
            var pageStyle = JSON.parse(pageStyleStr);
            var styles = __assign(__assign({}, articleStyle), pageStyle);
            if (pageStyle.maxWidth) {
                this.gridWidth = pageStyle.maxWidth;
            }
            delete styles.maxWidth;
            delete styles.backgroundColor;
            delete styles.background;
            delete styles.color;
            return styles;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], GridPresenter.prototype, "rowHeight", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "margin", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "cols", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "_refs", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "isPublishing", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "pageEditing", void 0);
    __decorate([
        observable
    ], GridPresenter.prototype, "gridWidth", void 0);
    __decorate([
        computed
    ], GridPresenter.prototype, "layout", null);
    __decorate([
        computed
    ], GridPresenter.prototype, "style", null);
    return GridPresenter;
}());
export { GridPresenter };
//# sourceMappingURL=grid-presenter.js.map