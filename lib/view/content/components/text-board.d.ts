import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root" | "image" | "title" | "body" | "montserrat" | "subtitleWrapper" | "subtitle" | "header"> {
    title?: React.ReactNode;
    subtitle?: React.ReactNode;
    children?: React.ReactNode;
    color?: string;
    titleColor?: string;
    image?: string;
}
export declare const TextBoard: React.ComponentType<Pick<OwnProps, "title" | "color" | "image" | "subtitle" | "children" | "titleColor"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
