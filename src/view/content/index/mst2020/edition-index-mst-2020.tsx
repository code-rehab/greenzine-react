import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { toJS } from "mobx";
import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
import { RenderWhile } from "../../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
  },

  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  text:{
    color: "#fff"
  }
  ,
  title: {
    color: "#FFF",
    marginBottom: 20, 
    lineHeight: 0.9,
    fontWeight: 700,
    fontSize: 80,
    [theme.breakpoints.down("sm")]: {
      fontSize: "2.6em",
    },
  },
  subtitle: {
    fontWeight: 700,
    margin: "20px 0",
    color: "#4D4D4F",
  },
  description: {
    paddingRight: theme.spacing(1),
  },
  collapseContainer: {
    marginBottom: theme.spacing(3),
    position: "relative",
    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <>
      <Typography variant={"h1"} className={classes.title}>
        Jaar <br/>bericht<br/> 2020
      </Typography>
      <Typography className={classes.text} paragraph>
        Het jaar waar alles veranderde, waar saamhorigheid de boventoon voerde en waar al onze medewerkers ontzettend hard hebben gewerkt om continu de beste zorg te blijven bieden. 
      </Typography>
      <Typography className={classes.text}>
        Een jaar vol verandering, digitalisering en verbroedering.  Lees hier verder over het bijzondere jaar, 2020.
      </Typography>
      {/* <Typography variant={"h2"} className={classes.subtitle}>inhoud</Typography>  */}
      {/* <Typography className={classes.description}>{description}Met trots presenteren wij u ons digitale jaarverslag 2019. Een jaar waarin veel is gebeurd.</Typography> */}
    </>
  );
};

const EditionIndexMSTComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div>
        <RenderWhile desktop print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        {/* <RenderWhile mobile>
          <Collapse in={open} collapsedHeight={110} classes={{ container: classes.collapseContainer }}>            
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }} 
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile> */}
      </div>
    </>
  );
};

export const EditionIndexMST2020 = withStyles(styles)(EditionIndexMSTComponent);
