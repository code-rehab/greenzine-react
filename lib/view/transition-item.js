import * as React from "react";
export var TransitionItem = function (route) { return function (routerprops) {
    var transition = typeof route.transition === "function" ? route.transition(route, routerprops) : route.transition;
    return (React.createElement("div", { className: "transition-item " + (transition || "fade-in-out") },
        React.createElement(route.component, null)));
}; };
//# sourceMappingURL=transition-item.js.map