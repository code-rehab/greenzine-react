import * as React from "react";
import { Typography, LinearProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        flexDirection: "column",
        marginBottom: 80
    },
    montserrat: {
        fontFamily: "Montserrat",
        fontWeight: 500
    },
    barRoot: {
        backgroundColor: "#fff5",
        height: 6
    },
    bar: {
        backgroundColor: "white"
    }
}); };
export var ProgressBar = withStyles(styles)(function (_a) {
    var classes = _a.classes, subject = _a.subject, total = _a.total, current = _a.current;
    return (React.createElement("div", { className: classes.root },
        React.createElement(Typography, { gutterBottom: true, className: classes.montserrat, align: "center" },
            subject,
            " ",
            current,
            "/",
            total),
        React.createElement(LinearProgress, { variant: "determinate", value: (current / total) * 100, classes: { bar: classes.bar, root: classes.barRoot } })));
});
//# sourceMappingURL=progress-bar.js.map