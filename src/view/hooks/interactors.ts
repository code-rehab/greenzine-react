import { Application } from "../../application/application";

export const useArticleInteractor = () => {
  return Application.business.interactor.article;
};

export const useMagazineInteractor = () => {
  return Application.business.interactor.magazine;
};
