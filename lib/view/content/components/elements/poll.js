import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { Poll } from "./poll/poll";
import { RenderWhile } from "../render-while";
var styles = function (theme) { return ({
    root: {},
}); };
export var PollElement = withStyles(styles)(function (props) {
    var animation = props.animation || { duration: 0, delay: 0 };
    var pollId = props.pollId;
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { desktop: true, tablet: true, mobile: true },
            React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
                React.createElement(Poll, { pollId: pollId }))),
        React.createElement(RenderWhile, { print: true },
            React.createElement(Typography, null, "De printfunctie van de poll is momenteel niet beschikbaar"))));
});
//# sourceMappingURL=poll.js.map