import * as React from "react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { Theme, WithStyles, StyleRules, withStyles, Grid, Typography } from "@material-ui/core";
import { observer } from "mobx-react";

type OwnProps = { logo: string } & WithStyles<"root" | "logo">;
const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(4, 0),
    },
  },
  logo: {
    display: "inline-block",
    "@media all and (-ms-high-contrast:none)": {
      width: 600,
    },
    "& img": {
      marginBottom: 20,
      [theme.breakpoints.down("sm")]: {
        width: 120,
        paddingRight: 25,
      },
    },
    [theme.breakpoints.down("sm")]: {
      maxWidth: "160px",
    },
  },
  logo2: {
    [theme.breakpoints.down("md")]: {
      width: "15vw",
    },
  },
});

export const MSTEdition2020CoverLogos = withStyles(styles)(
  observer(({ classes, logo }: OwnProps) => {
    return (
      <header className={classes.root}>
        <SlideFade direction="up" timeout={900}>
          <div className={classes.logo}>
            <Grid container spacing={0} alignItems="center">
              <Grid item sm={true} style={{ position: "relative" }}>
                <img src={logo || ""} alt="MST" />
              </Grid>
              <Grid item sm={true}></Grid>
            </Grid>
          </div>
        </SlideFade>
      </header>
    );
  })
);
