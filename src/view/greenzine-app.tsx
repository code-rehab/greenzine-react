import * as React from "react";
import { Router } from "react-router-dom";
import { DefaultInterface } from "./interface/interface";
import Amplify from "aws-amplify";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Routes from "./routes";
import "./greenzine-app.css";
import { Theme } from "@material-ui/core";

import ReactGA from "react-ga4";


import { Content } from "@coderehab/greenzeen-content";

interface GreenzineAppConfig {
  aws: any;
}

export interface GreenzineAppProps {
  config: GreenzineAppConfig;
  theme?: Theme;
  tenant: string;
}

import { Themes, useTheme } from "@coderehab/greenzeen-themes";
import { createBrowserHistory } from "history";

export const GreenzineApp: React.FC<GreenzineAppProps> = ({ config, theme, tenant }) => {
  Amplify.configure(config.aws);
  Themes.configure({ tenant });
  Content.configure({ tenant });

  const { MuiTheme } = useTheme();

  React.useEffect(() => {
    document.addEventListener("DOMContentLoaded", function () {
      // set theme color for chrome based on themes primary color
      const themeColor: any = document.head.querySelector('meta[name="theme-color"]');
      if (themeColor) {
        themeColor.content = MuiTheme.palette.primary.main;
      }
    });
  });
  const history = createBrowserHistory();
  // Initialize google analytics page view tracking
  history.listen((location) => {
    ReactGA.set({ page: location.pathname }); // Update the user's current page
    ReactGA.pageview(location.pathname); // Record a pageview for the given page
  });

  return (
    <ThemeProvider theme={MuiTheme}>
      <CssBaseline />
      <div className="App" style={{ position: "relative" }}>
        <Router history={history}>
          <Routes />
          <DefaultInterface />
        </Router>
      </div>
    </ThemeProvider>
  );
};
