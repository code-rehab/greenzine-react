import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { CarmelkoersEditionCoverPresenter } from "../carmelkoers-edition-cover-presenter";
declare type OwnProps = {
    config: any;
    presenter: CarmelkoersEditionCoverPresenter;
} & WithStyles<"root" | "primary" | "secondary" | "highlight" | "highlightCategory" | "highlightTitle" | "highlights" | "buttonReadMore" | "imageWrap" | "image" | "content" | "logo">;
export declare const LayoutEditionCoverDesktop: React.ComponentType<Pick<OwnProps, "config" | "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
