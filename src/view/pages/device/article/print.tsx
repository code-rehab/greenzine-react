import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ArticlePresenter } from "../../article-presenter";
import { observer } from "mobx-react-lite";
import classnames from "classnames";
import { Theme, Button, Typography } from "@material-ui/core";
import { faPrint } from "@fortawesome/free-solid-svg-icons";
import { withStyles, WithStyles, StyleRules } from "@material-ui/styles";
import { Error404Content } from "../../error-404";
import { LayoutGrid } from "../../layout/grid/grid";
import { LayoutBondigEditionCover } from "../../layout/bondig-edition-cover/bondig-edition-cover";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";

import { GridV2 } from "../../layout/grid-v2/grid";
import { PrintContainer } from "@coderehab/greenzeen-content";

const styles = (theme: Theme): StyleRules => ({
  root: {
    "@media screen": {
      minHeight: "100vh",
      padding: theme.spacing(4, 2, 8, 2),
      backgroundColor: "#eee",
    },
  },
  container: {
    "@media screen": {
      position: "relative",
      margin: "auto",
      maxWidth: 900,
    },
  },
  button: {
    margin: theme.spacing(0, 0, 0, 1),
    "@media print": {
      display: "none",
    },
  },
  content: {
    fontSize: "0.4rem",
    "& h1, & h2, & h3, & h4, & h5, & h6": { margin: "1em 0 0.5em 0" },
    "& p": { margin: "0.5em 0 1.2em 0", lineHeight: 1.5, fontSize: 13 },
    "& br": { display: "none" },
    "& img": { display: "none" },
    color: "black",
    "& *": {
      background: "none !important",
    },
    "@media screen": {
      padding: 48,
      backgroundColor: "#fff",
      boxShadow: theme.shadows[3],
    },
  },
  nav: {
    position: "fixed",
    padding: theme.spacing(2),
    right: 0,
    top: 0,
  },
});

interface PrintContentProps
  extends WithStyles<"root" | "container" | "button" | "content" | "nav"> {
  presenter: ArticlePresenter;
}

interface PageProps extends WithStyles<"root"> {
  page: any;
}

const Page = withStyles((theme: Theme) => ({
  root: {
    "& *": {
      position: "relative",
    },
  },
}))(({ page, classes }: PageProps) => {
  if (page.layouts && page.layouts.lg && page.layouts.lg.length) {
    return (
      <GridV2
        article={{ styleObj: {} } as any}
        page={page}
        breakpoint="sm"
        print={true}
        style={{
          maxWidth: "100%",
          minHeight: 0,
          padding: 0,
          paddingBottom: 0,
          backgroundImage: "none !important",
          backgroundColor: "#fff",
          color: "#000 !important",
        }}
      />
    );
  }

  if (page.layout === "grid") {
    return <LayoutGrid config={JSON.parse(page.layoutConfig || "{}")} data={page.data} />;
  }

  if (page.layout === "bondig-edition-cover") {
    return (
      <div className={classes.root}>
        <Typography>De printfunctie van de cover is momenteel niet beschikbaar</Typography>
        {/* <LayoutBondigEditionCover config={JSON.parse(page.layoutConfig || "{}")} /> */}
      </div>
    );
  }

  if (page.layout === "articleCover") {
    return (
      <LayoutArticleCover
        config={JSON.parse(page.layoutConfig || "{}")}
        data={page.data}
        style={page.style}
      />
    );
  }

  return <>Unknown layout</>;
});

export const ArticlePrintContent = withStyles(styles)(
  observer(({ classes, presenter }: PrintContentProps) => {
    if (presenter.loading) {
      return null;
    }

    if (!presenter.article || !presenter.article.pages.length) {
      return <Error404Content />;
    }

    return (
      <PrintContainer>
        {presenter.article.pages.map((page, index) => {
          return <Page key={index} page={page} />;
        })}
      </PrintContainer>
    );
  })
);
