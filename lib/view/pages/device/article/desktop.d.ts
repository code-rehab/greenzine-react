import { WithStyles } from "@material-ui/core";
import * as React from "react";
import { ArticlePresenter } from "../../article-presenter";
interface PageProps extends WithStyles<"root" | "grid"> {
    page: any;
    article: any;
}
export declare const Page: React.ComponentType<Pick<PageProps, "article" | "page"> & import("@material-ui/core").StyledComponentProps<"root" | "grid"> & object>;
interface OwnProps extends WithStyles<"root"> {
    presenter: ArticlePresenter;
}
export declare const ArticleDesktopContent: React.ComponentType<Pick<OwnProps, "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
