import * as React from "react";
import { withStyles, Theme, Typography, Grid, Link } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";
import { BR } from "./linebreak";

interface OwnProps extends WithStyles<"root" | "montserrat" | "small" | "text"> {}

const styles = (theme: Theme): StyleRules => ({
  root: {
    width: "100%",
  },
});

export const ColofonCarmel = withStyles(styles)(({ classes }: OwnProps) => (
  <div
    className={classes.root}
    ref={(elem) => {
      if (elem) {
        elem.style.height = elem.clientHeight + "px";
      }
    }}
    style={{color: "#000"}}
  >
    <Grid container>
      <Grid item xs={12}>
        <Typography
          variant="h4"
          style={{
            fontWeight: 400,
            lineHeight: "1.1 !important",
            marginBottom: 12,
          }}
        >
          Colofon
        </Typography>
      </Grid>
      <Grid
        item
        xs={12}
        sm={12}
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Typography className={classes.montserrat} paragraph>
          <small className={classes.small}>
          Teksten: Anne-Marie Bos, Titus Brandsma Instituut.    
          <br/>Redactie: Sylvia Goossens, Franciska Soepboer, Fijke Hoogendijk (Stichting Carmelcollege) Suzanne Visser (Perspect). 
          <br/>Wij hebben geprobeerd alle rechthebbenden van het beeldmateriaal te achterhalen. 
          <br/>Personen of instanties die menen dat hun rechten desondanks niet gerespecteerd worden, <br/>kunnen contact opnemen met de redactie via <a href="mailto:communicatie@carmel.nl">communicatie@carmel.nl</a></small>
        </Typography>
    
        <Typography className={classes.montserrat}>
          <small className={classes.small}>Concept en ontwerp</small>
        </Typography>
        <Typography className={classes.text}>
          <small className={classes.small}>
            <Link href="https://creativebastards.nl" color="inherit" underline="always" target="_blank">
              Creative Bastards
            </Link>
          </small>
        </Typography>
      </Grid>

      <Grid item xs={12} sm={12}>
        <Typography className={classes.montserrat}>
          <small className={classes.small}>Technische realisatie</small>
        </Typography>
        <Typography paragraph>
          <small className={classes.small}>
            <Link href="https://code.rehab/" color="inherit" underline="always" target="_blank" paragraph>
              Code.Rehab
            </Link>
          </small>
        </Typography>

        <Typography className={classes.montserrat}>
          <small className={classes.small}>&copy; Stichting Carmelcollege</small>
        </Typography>
      </Grid>
    </Grid>
  </div>
));
