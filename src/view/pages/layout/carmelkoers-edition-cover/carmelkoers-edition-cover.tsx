import * as React from "react";
import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
import { CarmelkoersEditionCoverPresenter } from "./carmelkoers-edition-cover-presenter";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
// import { LayoutEditionCoverMobile } from "./device/mobile";

const Component = ({ config, presenter }: OwnProps & PresenterProps<CarmelkoersEditionCoverPresenter>) => {
  return <LayoutEditionCoverDesktop config={config} presenter={presenter} />;
};

interface OwnProps {
  config: any;
}

export const LayoutCarmelkoersEditionCover = withPresenter<CarmelkoersEditionCoverPresenter, OwnProps>(
  (_props, { interactor }) =>
    new CarmelkoersEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print),
  Component
);
