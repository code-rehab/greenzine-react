var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from "react";
import { withStyles } from "@material-ui/core";
import classnames from "classnames";
var styles = function (theme) { return ({
    root: {
        opacity: 0,
        transform: "scale(0.8, 0.8)",
        transition: "opacity 1.5s ease-out, transform 1.5s ease-out"
    },
    animate: {
        opacity: 1,
        transform: "scale(1, 1)"
    }
}); };
var ZoomFadeComponent = /** @class */ (function (_super) {
    __extends(ZoomFadeComponent, _super);
    function ZoomFadeComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            play: false
        };
        return _this;
    }
    ZoomFadeComponent.prototype.componentDidMount = function () {
        var _this = this;
        setTimeout(function () {
            _this.setState({ play: true });
        }, this.props.timeout || 0);
    };
    ZoomFadeComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, children = _a.children, classes = _a.classes;
        return (React.createElement(React.Fragment, null, children &&
            React.Children.map(children, function (child) {
                return React.cloneElement(child, {
                    className: classnames(classes.root, _this.state.play && classes.animate, child.props.className)
                });
            }))
        // <div
        //   className={classnames(
        //     classes.root,
        //     classes[direction],
        //     this.state.play && classes.animate
        //   )}
        // >
        //   {children}
        // </div>
        );
    };
    return ZoomFadeComponent;
}(React.PureComponent));
export var ZoomFade = withStyles(styles)(ZoomFadeComponent);
//# sourceMappingURL=zoom-fade.js.map