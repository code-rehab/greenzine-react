import { Theme } from "@material-ui/core";
import { StyleRules, withStyles, WithStyles } from "@material-ui/styles";
import { observer } from "mobx-react";
import * as React from "react";
import { Helmet } from "react-helmet";
import { PresenterProps, withPresenter } from "../../helpers/with-presenter";
import { InterfacePresenter } from "./interface-presenter";
import { Navigator } from "./partials/navigator";
import { PageNavigator } from "./partials/navigator-page";
import { PollButton } from "./partials/poll-button";
import { Breadcrumb } from "./partials/breadcrumb";

interface OwnProps extends WithStyles<"root"> {
  //
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "absolute",
    top: 0,
    left: 0,
    "@media print": {
      display: "none",
    },
  },
});

export const Component = observer(
  ({ classes, presenter }: OwnProps & PresenterProps<InterfacePresenter>) => (
    <div className={classes.root}>
      <PageNavigator presenter={presenter} />
      <Navigator location="bottom" presenter={presenter} />
      <Breadcrumb
        icon={process.env.REACT_APP_BREADCRUMB_ICON == "true" ? true : false}
        title="Carmel Toekomstbouwers"
        article={presenter.article}
        url={presenter.edition}
      />
      <PollButton presenter={presenter} />
    </div>
  )
);

export const DefaultInterface = withStyles(styles)(
  withPresenter<InterfacePresenter, OwnProps>(
    (_props, { interactor, provider }) =>
      new InterfacePresenter(
        interactor.magazine,
        provider.magazine,
        interactor.article,
        provider.article,
        interactor.print
      ),
    Component
  )
);
