import { observer } from "mobx-react";
import * as React from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import { routes, checkRedirect } from "../config/routes";
import PageTransition from "react-router-page-transition";
import { TransitionItem } from "./transition-item";
import "./transitions/transitions.css";
import "./transitions/slide-left.css";
import "./transitions/slide-right.css";
import "./transitions/slide-up.css";
import "./transitions/slide-down.css";
import "./transitions/fade-in-out.css";
import "./transitions/cross-fade.css";
import "./transitions/circle-grow.css";
export var Routes = observer(function () {
    var redirection = checkRedirect();
    return redirection ? (React.createElement(Redirect, { to: redirection.to })) : (React.createElement(PageTransition, { timeout: 700 },
        React.createElement(Switch, null, routes.map(function (route) { return (React.createElement(Route, { key: route.path, exact: true, path: route.path, component: TransitionItem(route) })); }))));
});
export default withRouter(Routes);
//# sourceMappingURL=routes.js.map