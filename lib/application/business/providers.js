var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { DefaultArticleProvider } from "../data/article/article-provider";
import { DefaultEditionProvider } from "../data/edition/edition-provider";
import { DefaultMagazineProvider } from "../data/magazine/magazine-provider";
import { DefaultPollProvider } from "../data/poll/poll-provider";
import { BaseModule } from "../module";
import { DefaultPageProvider } from "../data/page/page-provider";
var DefaultBusinessProviders = /** @class */ (function (_super) {
    __extends(DefaultBusinessProviders, _super);
    function DefaultBusinessProviders(_network) {
        var _this = _super.call(this) || this;
        _this._network = _network;
        return _this;
    }
    Object.defineProperty(DefaultBusinessProviders.prototype, "magazine", {
        get: function () {
            return this.loadProvider("magazine", DefaultMagazineProvider, this._network);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessProviders.prototype, "edition", {
        get: function () {
            return this.loadProvider("edition", DefaultEditionProvider, this._network);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessProviders.prototype, "article", {
        get: function () {
            return this.loadProvider("article", DefaultArticleProvider, this._network);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessProviders.prototype, "poll", {
        get: function () {
            return this.loadProvider("poll", DefaultPollProvider, this._network);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessProviders.prototype, "page", {
        get: function () {
            return this.loadProvider("page", DefaultPageProvider, this._network);
        },
        enumerable: true,
        configurable: true
    });
    return DefaultBusinessProviders;
}(BaseModule));
export { DefaultBusinessProviders };
//# sourceMappingURL=providers.js.map