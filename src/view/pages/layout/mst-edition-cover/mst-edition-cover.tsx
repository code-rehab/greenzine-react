import * as React from "react";
import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
import { MSTEditionCoverPresenter } from "./mst-edition-cover-presenter";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
// import { LayoutEditionCoverMobile } from "./device/mobile";

const Component = ({ config, presenter }: OwnProps & PresenterProps<MSTEditionCoverPresenter>) => {
  return <LayoutEditionCoverDesktop config={config} presenter={presenter} />;
};

interface OwnProps {
  config: any;
}

export const LayoutMSTEditionCover = withPresenter<MSTEditionCoverPresenter, OwnProps>(
  (_props, { interactor }) => new MSTEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print),
  Component
);
