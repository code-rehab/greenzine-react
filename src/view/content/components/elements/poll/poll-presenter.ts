import { API } from "aws-amplify";
import { computed, observable, toJS } from "mobx";
import { RouteComponentProps } from "react-router";
import { Poll, PollQuestion } from "../../../../../application/data/poll/poll";
import { PollProvider } from "../../../../../application/data/poll/poll-provider";
import { IPresenter } from "../../../../../helpers/with-presenter";
import { BasicPagePresenter } from "../../../../pages/_page-basic-presenter";

export class PollPresenter extends BasicPagePresenter implements IPresenter {
  constructor(
    public pollId: string,
    _questionIndex: number | undefined,
    private _pollProvider: PollProvider,
    protected _router: RouteComponentProps
  ) {
    super(_router);
    this.poll = this._pollProvider.get(this.pollId);
    if (_questionIndex) {
      this.currentQuestionIndex = _questionIndex;
    }
  }

  @observable public poll: Poll;
  @observable public currentValue: any = null;
  @observable public loading: boolean = false;
  @observable public dialogClosed: boolean = false;
  @observable public errorMessage: string | null = null;
  @observable public fetched: boolean = false;
  @observable public currentQuestionIndex: number = 0;

  @computed public get finished(): boolean {
    const answerCount = this.poll.questions.reduce((count, { answers }) => {
      return (answers || []).length ? count + 1 : count;
    }, 0);
    return answerCount >= this.poll.questions.length;
  }

  @computed public get currentQuestion(): PollQuestion | undefined {
    return this.poll.questions[this.currentQuestionIndex || 0];
  }

  @computed public get questionCount(): number {
    return this.poll.questions.length;
  }

  @computed public get isFinalQuestion(): boolean {
    const index = this.currentQuestionIndex;
    return index >= this.poll.questions.length + 1;
  }

  @computed public get isFinished(): boolean {
    return this.finished;
  }

  public get state() {
    const state = this.poll.questions.reduce((result, { id, answers }) => {
      result[id] = { answers };
      return result;
    }, {});

    return {
      state,
      finished: this.finished
    };
  }

  public mount = async () => {
    if (!this.fetched) {
      this.loading = true;
      await this.poll.fetch();
      this.loading = false;
    }
    this.initPoll(this.pollId);
  };

  public unmount = () => {
    //
  };

  public initPoll = (id: string) => {
    const stored = this._getPollState(id);
    const state = stored.state;

    this.poll.questions.forEach((question, index) => {
      const questionState = state && state[question.id];
      question.answers = (questionState && questionState.answers) || [];
    });

    this.nextQuestion();
  };

  public onSubmit = () => {
    this.errorMessage = null;

    if (this.currentValue && this.currentValue.length) {
      this.submitAnswer(this.currentValue, this.pollId);
      this.nextQuestion();
      this.currentValue =
        (this.currentQuestion && this.currentQuestion.answers) || null;
    } else {
      this.errorMessage = "Gelieve eerst een antwoord te geven.";
    }
  };

  public submitAnswer = async (value: string | number | any[], id: string) => {
    const question = this.currentQuestion;
    let formattedValue: string[] = [];

    if (question) {
      if (typeof value === "string") {
        formattedValue = [value];
      } else if (typeof value === "number") {
        formattedValue = [value.toString()];
      } else if (Array.isArray(toJS(value))) {
        formattedValue = value;
      } else {
      }

      question.answers = formattedValue;

      await API.graphql({
        query: `mutation submitPollAnswer($pollID:ID!, $input:inputPollQuestion){
      submitPollAnswer(pollID:$pollID, input:$input){
        id
      }
    }`,
        variables: {
          pollID: id,
          input: toJS(question)
        }
      });

      this._setPollState(id);
    }
  };

  public previousQuestion = () => {
    this.currentQuestionIndex--;
  };

  public nextQuestion = () => {
    let nextQuestionIndex: number | undefined = undefined;

    this.poll.questions.forEach((question, index) => {
      nextQuestionIndex =
        nextQuestionIndex === undefined && !question.answers.length
          ? index
          : nextQuestionIndex;
    });

    this.currentQuestionIndex =
      nextQuestionIndex === undefined
        ? this.poll.questions.length
        : nextQuestionIndex;
  };

  public updateValue = (value: any) => {
    this.currentValue = value;
  };

  public closeDialog = () => {
    this.dialogClosed = true;
  };

  private _getPollState(id: string) {
    const status = window.localStorage.getItem("poll-state-" + id);
    return (status && JSON.parse(status)) || {};
  }

  private _setPollState = (id: string) => {
    window.localStorage.setItem("poll-state-" + id, JSON.stringify(this.state));
  };
}
