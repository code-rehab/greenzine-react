export declare const QueryUser: string;
export declare const QueryUserCollection: string;
export declare const QueryUserDesigns = "\nquery User($id:ID!) {\n  User(id:$id) {\n    designs{\n      id\n      version\n      company\n      status\n      project\n      pdf\n    }\n  }\n}";
export declare const QueryUserNotifications = "\nquery User($id:ID!) {\n  User(id:$id) {\n    notifications{\n      id\n    }\n  }\n}";
