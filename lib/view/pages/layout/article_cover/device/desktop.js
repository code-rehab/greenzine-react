import * as React from "react";
import { withStyles } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";
import classNames from "classnames";
var styles = function (theme) { return ({
    root: {
        textAlign: "left",
        padding: "0 7vw",
        "& > *": {
            display: "flex",
            marginRight: "auto",
            flexDirection: "column",
        },
    },
    centered: {
        textAlign: "center",
        "& > *": {
            display: "inline-block",
            margin: "auto",
            textAlign: "center",
            "& > *": {
                display: "inline-block",
                margin: "auto",
                textAlign: "center",
            },
        },
    },
    right: {
        textAlign: "right",
        "& > *": {
            marginLeft: "auto",
        },
    },
}); };
export var ArticleCoverDesktop = withStyles(styles)(function (_a) {
    var config = _a.config, data = _a.data, classes = _a.classes, style = _a.style;
    return (React.createElement("section", { className: classNames(classes.root) },
        React.createElement("div", { className: classNames(classes.inner, classes[config.variant]) }, data.map(function (d) {
            return (React.createElement("div", null,
                React.createElement(RenderElement, { element: d })));
        }))));
});
//# sourceMappingURL=desktop.js.map