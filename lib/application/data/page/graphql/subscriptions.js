import { PageFragment } from "./fragments";
export var PageCreated = "\nsubscription {\n  pageCreated {\n    " + PageFragment + "\n  }\n}\n";
export var PageUpdated = "\nsubscription {\n  pageUpdated {\n    " + PageFragment + "\n  }\n}\n";
export var PageDeleted = "\nsubscription {\n  pageDeleted {\n    " + PageFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map