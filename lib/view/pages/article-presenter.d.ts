import { IPresenter } from "../../helpers/with-presenter";
import { PagePresenter } from "./_page-default-presenter";
import { Page } from "../../application/data/page/page";
export declare class ArticlePresenter extends PagePresenter implements IPresenter {
    currentPageComponent: Element | undefined;
    atBottom: boolean;
    atTop: boolean;
    filter: string;
    _pages: Page[];
    get enrichedTemp(): boolean;
    get layout(): string;
    get contentData(): any;
    get pages(): any;
    get content(): string;
    content_by_id(id: string): string;
    mount: () => Promise<void>;
    unmount: () => void;
    selectArticle: (id: string) => void;
    toNextPage: () => void;
    toPreviousPage: () => void;
    toNextArticle: () => void;
    toPreviousArticle: () => void;
    selectPage: (id: string) => void;
    setPage(): void;
    handleKeyDown: (e: KeyboardEvent) => void;
}
