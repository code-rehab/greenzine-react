import { ElementType, ElementBase } from "../element";
export interface VideoElementProps {
    embed: string;
    caption: string;
    maxWidth: string;
}
export interface VideoElement extends ElementBase {
    component: ElementType.Video;
    props: VideoElementProps;
}
