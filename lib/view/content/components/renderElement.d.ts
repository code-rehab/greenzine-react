export declare const RenderElement: ({ element }: {
    element: import("../../..").ElementUnion;
}) => JSX.Element;
