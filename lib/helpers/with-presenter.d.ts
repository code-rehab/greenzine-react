import * as React from "react";
import { IApplication } from "../application/application";
import { BusinessModules } from "../application/business/business";
export interface IPresenter {
    mount(): void;
    unmount(): void;
}
export interface PresenterProps<P extends IPresenter> {
    presenter: P;
}
export declare function withPresenter<P extends IPresenter, Props>(presenter: (props: Props, app: IApplication & BusinessModules) => P, Component: React.ComponentClass<Props & PresenterProps<P>> | React.FunctionComponent<Props & PresenterProps<P>>): React.ComponentClass<Props>;
