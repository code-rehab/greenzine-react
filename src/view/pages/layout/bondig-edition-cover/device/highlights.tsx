import { makeStyles } from "@material-ui/core";
import * as React from "react";
import { RenderWhile } from "../../../../content/components/render-while";
import { Carousel } from "../../../../interface/partials/carousel";
import { ArticleButton } from "./article-button";

interface OwnProps {
  articles: Array<{
    link: string;
    content: string;
    title: string;
  }>;
  classes?: Record<"root" | "articleWrapper", string>;
}

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    maxWidth: "75vw",
  },
  articleWrapper: {
    // flex: 1,
    padding: "0 20px 0 10px",
    borderLeft: "1px solid #FFC583",
  },
}));

export const EditionHighlights: React.FC<OwnProps> = ({ articles }) => {
  const classes = useStyles();
  return (
    <>
      <RenderWhile mobile>
        <Carousel color={"#FFC583"} data={articles} />
      </RenderWhile>

      <RenderWhile tablet desktop print>
        <div className={classes.root}>
          {articles.map((article) => (
            <div key={article.title} className={classes.articleWrapper}>
              <ArticleButton id={article.link} category={article.title} title={article.content} />
            </div>
          ))}
        </div>
      </RenderWhile>
    </>
  );
};
