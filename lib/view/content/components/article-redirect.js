import * as React from "react";
import { withStyles, Typography } from "@material-ui/core";
import { SlideFade } from "./effects/slide-fade";
var styles = function (theme) {
    var _a, _b, _c;
    return ({
        root: (_a = {
                // margin: theme.spacing(2, 2, 2, 2),
                display: "flex",
                justifyContent: "center",
                height: "100px",
                backgroundColor: "#0000",
                border: "none",
                cursor: "pointer",
                position: "relative",
                alignItems: "center",
                "&:hover": {
                    "& $arrow": {
                        transform: "translate(-50%, 50%) scale(1.2)",
                    },
                }
            },
            _a[theme.breakpoints.down("xs")] = {
                marginRight: "auto",
            },
            _a),
        imageContainer: (_b = {
                borderRadius: "100%",
                height: "100px",
                width: "100px",
                overflow: "hidden",
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                marginRight: theme.spacing(1)
            },
            _b[theme.breakpoints.down("xs")] = {
                height: "70px",
                width: "70px",
                marginRight: theme.spacing(2),
            },
            _b),
        image: {
            height: "100%",
            width: "auto",
        },
        nextArticle: (_c = {
                fontFamily: "Montserrat !important"
            },
            _c[theme.breakpoints.down("sm")] = {
                maxWidth: 200
            },
            _c),
        article: {
            fontFamily: "Montserrat !important"
        }
    });
};
export var ArticleRedirect = withStyles(styles)(function (_a) {
    var children = _a.children, thumbnail = _a.thumbnail, classes = _a.classes, onClick = _a.onClick;
    return (React.createElement(SlideFade, { delay: 900, direction: "up" },
        React.createElement("div", { className: classes.root, onClick: onClick, id: "next-article" },
            React.createElement("div", { className: classes.imageContainer },
                React.createElement("img", { src: thumbnail, className: classes.image, alt: "" })),
            children && (React.createElement("div", { className: classes.nextArticle },
                React.createElement(Typography, { style: { fontFamily: "Montserrat" } },
                    React.createElement("strong", null, "Volgend artikel:")),
                React.createElement(Typography, { style: { lineHeight: 1, fontFamily: "Montserrat" } },
                    React.createElement("small", null, children)))))));
});
//# sourceMappingURL=article-redirect.js.map