export declare class BaseModule<Providers, Interactors, Modules> {
    protected _cache: any;
    protected loadProvider<ClassName extends {
        new (...args: any): any;
    }>(key: keyof Providers, ProviderClass: ClassName, ...providerProps: ConstructorParameters<ClassName>): any;
    protected loadInteractor<ClassName extends {
        new (...args: any): any;
    }>(key: keyof Interactors, InteractorClass: ClassName, ...providerProps: ConstructorParameters<ClassName>): any;
    protected loadModule<ClassName extends {
        new (...args: any): any;
    }>(key: keyof Modules, ModuleClass: ClassName, ...providerProps: ConstructorParameters<ClassName>): any;
}
