import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Article, ArticleRecord, ArticleModel } from "./article";
export interface ArticleProvider extends GraphQLProviderProps<Article, ArticleRecord> {
}
export declare class DefaultArticleProvider extends GraphQLProvider<Article, ArticleRecord> implements ArticleProvider {
    model: typeof ArticleModel;
    listOperation: () => {
        query: any;
        variables: {};
    };
    createOperation: (article: Article) => {
        query: any;
        variables: {};
    };
    fetchOperation: (article: Article) => {
        query: any;
        variables: {};
    };
    updateOperation: (article: Article) => {
        query: any;
        variables: {};
    };
    deleteOperation: (article: Article) => {
        query: any;
        variables: {};
    };
}
