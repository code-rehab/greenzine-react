import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<any> {
    about: any;
}
export declare const About: React.ComponentType<Pick<OwnProps, "about"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
