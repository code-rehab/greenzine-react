import { ElementType, ElementBase } from "../element";
export interface QuoteElementProps {
    content: string;
    author?: string;
    color?: string;
}
export interface QuoteElement extends ElementBase {
    component: ElementType.Quote;
    props: QuoteElementProps;
}
