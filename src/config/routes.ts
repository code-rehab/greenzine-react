// import Application from "../application/application";

import { generatePath, RouteComponentProps } from "react-router";
import { Application } from "../application/application";
import { PageIndex } from "../view/pages";
import { ArticlePage } from "../view/pages/article-page";
import { Error404 } from "../view/pages/error-404";
import { PageIndexMST } from "../view/pages/indexMST";
import { PageIndexMST2020 } from "../view/pages/indexMST2020";
import { PageIndexCarmelKoers } from "../view/pages/indexCarmelKoers";
import { PageIndexCarmelKennismaking } from "../view/pages/indexCarmelKennismaking";
import { PageIndexGreenzeen } from "../view/pages/indexGreenzeen";
import { PageIndexTwenteboard } from "../view/pages/indexTwenteboard";

export type PageTransition =
  | undefined
  | "slide-left"
  | "slide-right"
  | "slide-up"
  | "slide-down"
  | "cross-fade"
  | "fade-in-out"
  | "circle-grow"
  | "slide";
export type PageTransitionInterceptor = (
  route: RouteInfo,
  routerprops: RouteComponentProps
) => PageTransition;

export interface RouteInfo {
  path: string;
  title: string;
  component: any;
  mainmenu?: boolean;
  disabled?: boolean;
  public?: boolean;
  redirect?: string;
  transition?: PageTransition | PageTransitionInterceptor;
}

export interface PossibleRouterParams {
  magazine?: string;
  edition?: string;
  article?: string;
  page?: string;
}

export interface RedirectionParams {
  from: string;
  to: string;
}

export const redirections: RedirectionParams[] = [];

let PageIndexComponent = null;

console.log("THEME",process.env.REACT_APP_CLIENT_THEME);
switch (process.env.REACT_APP_CLIENT_THEME) {
  case "mst":
    // code block
    PageIndexComponent = PageIndexMST;
    break;
    case "mst2020":
    // code block
    PageIndexComponent = PageIndexMST2020;
    break;
  case "carmelKoers2025":
    // code block
    PageIndexComponent = PageIndexCarmelKoers;
    break;

  case "carmelKennismaking":
    // code block
    PageIndexComponent = PageIndexCarmelKennismaking; 
    break;
  case "greenzeen":
    PageIndexComponent = PageIndexGreenzeen;
    break;
    case "twenteboard":
    // code block
    PageIndexComponent = PageIndexTwenteboard; 
    break;
  default:
    PageIndexComponent = PageIndex;
  // code block
}

export const routeMap: Record<
  "index.editions" | "index.articles" | "article.homepage" | "article.page" | "error.404",
  RouteInfo
> = {
  "index.editions": {
    path: "/",
    title: "Edities",
    component: PageIndex,
    transition: "fade-in-out",
  },
  "index.articles": {
    path: "/:edition",
    title: "Artikelen",
    component: PageIndexComponent,
    transition: "fade-in-out",
  },
  "article.homepage": {
    path: "/:edition/:article",
    title: "Artikelen",
    component: ArticlePage,
    transition: () => Application.business.interactor.article.transition,
  },
  "article.page": {
    path: "/:edition/:article/:page",
    title: "Artikelen",
    component: ArticlePage,
    transition: () => Application.business.interactor.article.transition,
  },

  "error.404": {
    path: "*",
    title: "Error 404 - Resource not found",
    component: Error404,
    transition: "fade-in-out",
  },
};

export type RouteNames = keyof typeof routeMap;

export const route = (name: RouteNames, args?: Record<string, string | boolean | number>) => {
  return generatePath(routeMap[name].path, args);
};

export const routes: RouteInfo[] = Object.keys(routeMap).map((name: string) => {
  return routeMap[name];
});

export const checkRedirect = () =>
  redirections.filter(
    ({ from }) => from.replace(/\/$/, "") === window.location.pathname.replace(/\/$/, "")
  )[0];
