import * as React from "react";
import { withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
import { MSTEditionCoverPresenter } from "./mst-edition-cover-presenter";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
// import { LayoutEditionCoverMobile } from "./device/mobile";
var Component = function (_a) {
    var config = _a.config, presenter = _a.presenter;
    return React.createElement(LayoutEditionCoverDesktop, { config: config, presenter: presenter });
};
export var LayoutMSTEditionCover = withPresenter(function (_props, _a) {
    var interactor = _a.interactor;
    return new MSTEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print);
}, Component);
//# sourceMappingURL=mst-edition-cover.js.map