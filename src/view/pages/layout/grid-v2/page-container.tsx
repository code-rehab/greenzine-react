import * as React from "react";
import { observer } from "mobx-react";
import { withStyles, WithStyles, StyleRules, Theme } from "@material-ui/core";
import { Page } from "../../../../application/data/page/page";
import { take } from "../../../../helpers/general";
import { useMediaQuery } from "react-responsive";

type OwnProps = React.PropsWithChildren<{
  page: Page;
  print?: boolean;
  style?: React.CSSProperties;
}> &
  WithStyles<"root" | "container">;

const styles = (theme: Theme): StyleRules => ({
  root: {
    width: "100vw",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: "calc(" + theme.spacing(2) + "px + 3vw)",
    paddingBottom: "calc(" + theme.spacing(7) + "px + 3vw)",
    fontSize: "calc(1rem + 0.3vw)",

    [theme.breakpoints.up("md")]: {
      minHeight: "100vh",
    },

    [theme.breakpoints.down("md")]: {
      fontSize: "0.5em",
    },

    [theme.breakpoints.down("sm")]: {
      fontSize: "0.5em",
    },
  },
  container: {
    width: "100%",
    margin: "auto",
  },
});

export const PageContainer = withStyles(styles)(
  observer(({ page, classes, children, style, print }: OwnProps) => {
    const isMobile = useMediaQuery({ query: "(max-device-width: 640px)" });
    const rootStyle = {
      ...take(page.composedStyleObj, [
        "background",
        "backgroundColor",
        "backgroundImage",
        "backgroundPosition",
        "backgroundRepeat",
        "backgroundSize",
        "color",
        "padding",
      ]),
      ...style,
    };

    if (rootStyle.padding) {
      rootStyle.paddingBottom = "calc(" + rootStyle.paddingBottom + " + 60px)";
    }

    // rootStyle.minHeight = "100vh";
    // console.clear;

    // isMobile ? (rootStyle.minHeight = "unset") : (rootStyle.minHeight = print ? "unset" : "100vh");

    // mobile check here

    // rootStyle.color = rootStyle.color + " !important";

    // rootStyle.position = "relative";

    const containerStyle = take(page.composedStyleObj, ["maxWidth"]);

    containerStyle.maxWidth =
      typeof containerStyle.maxWidth === "string"
        ? containerStyle.maxWidth
        : containerStyle.maxWidth + "px";
    containerStyle.maxWidth = "calc(" + containerStyle.maxWidth + " + 5vw)";
    containerStyle.margin = 0;
    // containerStyle.justifyContent = "unset";
    // containerStyle.borderBottom = "solid 10px purple";

    // if (process.env.REACT_APP_ENVIRONMENT !== "management") {
    //   containerStyle.display = "flex";
    //   containerStyle.flexDirection = "column";
    //   containerStyle.justifyContent = "center";
    // }

    containerStyle.height = print ? "auto" : "100%";

    // overrule the colors and use the color set on the page.
    // let overRuleColor = false;
    // if (rootStyle.color && Array.isArray(children)) {
    //   children.map(function (item: any) {
    //     if (item && item["props"] && item["props"]["elements"]) {
    //       const elements = item["props"]["elements"];
    //       elements.map(function (elem: any) {
    //         if (elem && elem["props"]) {
    //           elem["props"]["customStyle"] = { color: rootStyle.color };
    //         }
    //       });
    //     }
    //   });
    // }

    if (print) {
      return <>{children}</>;
    }

    return (
      <article
        className={classes.root}
        style={rootStyle}
        ref={(elem) => {
          if (elem) {
            if ((window.document as any).documentMode) {
              setTimeout(() => {
                if (print) {
                } else {
                  elem.style.height = elem.clientHeight + "px";
                }
              }, 500);
            }
          }
        }}
      >
        <div className={classes.container} style={containerStyle}>
          {children}
        </div>
      </article>
    );
  })
);
