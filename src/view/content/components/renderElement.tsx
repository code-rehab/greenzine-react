import * as React from "react";
import { Typography } from "@material-ui/core";
import { Quote } from "./elements/quote";
import { BondigTypography } from "./elements/typography";
import { Image } from "./elements/image";
import { Paper } from "./elements/paper";
import { Video } from "./elements/video";
import { ReadTime } from "./elements/readtime";
import { Author } from "./elements/author";
import { Media } from "./elements/media";
import { List } from "./elements/list";
import { Divider } from "./elements/divider";
import { WistJeDat } from "./elements/wist-je-dat";
import { RenderWhile } from "./render-while";
import { PollElement } from "./elements/poll";
import { LocationInfo } from "./elements/location-info";
import {Index} from "./elements/index"; 
import { observer } from "mobx-react";
import { Element } from "../../../application/data/element/element";

const components = {
  Typography: BondigTypography,
  Quote,
  Image,
  Paper,
  Video,
  ReadTime,
  Author,
  Media,
  List,
  Divider,
  Poll: PollElement,
  WistJeDat,
  LocationInfo,
  Index
};

export const RenderElement = observer(({ element }: { element: Element }) => {
  const Component = element && element.component && components[element.component];
  let componentProps = element.props;

  if (typeof element.props === "string") {
    componentProps = JSON.parse(componentProps as any);
  }

  if (Component) {
    return (
      <>
        <RenderWhile desktop>
          <Component
            {...componentProps}
            // style={element.style || {}}
            animation={element.animation}
            renderFor="desktop"
          />
        </RenderWhile>

        <RenderWhile tablet>
          <Component
            {...componentProps}
            // style={element.style || {}}
            animation={element.animation}
            renderFor="tablet"
          />
        </RenderWhile>

        <RenderWhile mobile>
          <Component
            {...componentProps}
            // style={element.style || {}}
            animation={element.animation}
            renderFor="mobile"
          />
        </RenderWhile>

        <RenderWhile print>
          <Component
            {...componentProps}
            // style={element.style || {}}
            animation={element.animation}
            renderFor="print"
          />
        </RenderWhile>
      </>
    );
  }

  return <Typography>Element not found</Typography>;
});
