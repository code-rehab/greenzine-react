var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from "react";
import { Typography, Button, Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { mapEvent } from "../../../../../helpers/formatters";
import classnames from "classnames";
var styles = function (theme) { return ({
    root: {
        maxWidth: 900
    },
    montserrat: {
        fontFamily: "Montserrat",
        fontWeight: 500
    },
    barRoot: {
        backgroundColor: "#fff5",
        height: 6
    },
    bar: {
        backgroundColor: "white"
    },
    question: {
    // maxWidth: 550
    },
    answer: {
    // minWidth: "100%",
    // width: "max-content"
    },
    selected: {
        backgroundColor: "#fff",
        color: "#005555",
        borderColor: "#fff",
        "&:hover": {
            backgroundColor: "#fff",
            color: "#005555",
            borderColor: "#fff"
        }
    },
    radio: {
        display: "flex",
        width: 17,
        height: 17,
        color: "white",
        border: "1px solid #fff",
        borderRadius: "100%",
        padding: 3,
        "& > span": {
            display: "block",
            borderRadius: "100%",
            flex: 1,
            background: "#fff"
        }
    },
    answers: {},
    buttonBefore: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        left: 0,
        top: 0,
        width: theme.spacing(3),
        height: "100%",
        position: "absolute"
    }
}); };
var MultipleChoiceComponent = /** @class */ (function (_super) {
    __extends(MultipleChoiceComponent, _super);
    function MultipleChoiceComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = [];
        _this._onSelectHandler = function (answer) {
            var _a = _this.props, multiselect = _a.multiselect, onChange = _a.onChange, answers = _a.answers;
            var index = answers.indexOf(answer);
            if (multiselect) {
                if (index >= 0) {
                    answers.splice(index, 1);
                }
                else {
                    answers.push(answer);
                }
                if (onChange) {
                    onChange(answers);
                }
            }
            else {
                if (onChange) {
                    onChange([answer]);
                }
            }
        };
        return _this;
    }
    MultipleChoiceComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, classes = _a.classes, options = _a.options, multiselect = _a.multiselect, answers = _a.answers;
        return (React.createElement(React.Fragment, null,
            React.createElement(Grid, { spacing: 2, container: true, className: classes.root },
                React.createElement(Grid, { item: true, xs: 12 },
                    React.createElement("div", { className: classes.question }, multiselect && (React.createElement(Typography, { className: classes.montserrat, variant: "subtitle2", align: "center", paragraph: true }, "Meerdere opties mogelijk")))),
                options.map(function (answer, index) {
                    var selected = answers.indexOf(answer.toString()) > -1;
                    return (React.createElement(Grid, { item: true, xs: 12, md: 6 },
                        React.createElement(Button, { key: index, disableRipple: true, className: classnames(classes.answer, !multiselect && selected && classes.selected), color: "inherit", variant: "outlined", fullWidth: true, onClick: mapEvent(_this._onSelectHandler, answer.toString()), classes: { text: classes.buttonText } },
                            React.createElement("div", { className: classes.buttonBefore }, multiselect && React.createElement("span", { className: classes.radio }, selected && React.createElement("span", null))),
                            answer)));
                }))));
    };
    __decorate([
        observable
    ], MultipleChoiceComponent.prototype, "value", void 0);
    MultipleChoiceComponent = __decorate([
        observer
    ], MultipleChoiceComponent);
    return MultipleChoiceComponent;
}(React.Component));
export var MultipleChoice = withStyles(styles)(MultipleChoiceComponent);
//# sourceMappingURL=multiple-choice.js.map