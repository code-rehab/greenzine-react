import { Button, Collapse, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "auto",
            marginTop: theme.spacing(3),
        },
        drawerBackdrop: {},
        header: {},
        content: {},
        backdrop: {
            backgroundColor: "transparant",
        },
        title: (_a = {
                color: "#fff",
                // margin: "20px 0",
                // fontFamily: "Amaranth",
                // lineHeight: 0.9,
                // fontSize: 42,
                "& span": {
                    color: "#F90000",
                }
            },
            _a[theme.breakpoints.down("sm")] = {
                marginTop: 50,
                fontSize: "2.6em",
            },
            _a),
        intro: {
            fontWeight: 700,
            margin: "20px 0",
            color: "#FFF",
        },
        logo: {
            marginTop: theme.spacing(4),
        },
        description: {
            // fontSize: 17,
            color: "#fff",
            paddingRight: theme.spacing(1),
        },
        collapseContainer: {
            marginBottom: theme.spacing(3),
            position: "relative",
            "&:after": {
                content: "''",
                display: "block",
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
            },
            "&.MuiCollapse-entered": {
                "&:after": {
                    boxShadow: "none",
                },
            },
        },
        contentPiece: {
            fontFamily: "Montserrat",
        },
        marginBottom: {
            marginBottom: "1em",
        },
    });
};
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement("div", { className: classes.root },
        React.createElement(Typography, { variant: "h1", className: classes.title },
            React.createElement("span", null, "Investeren"),
            " ",
            React.createElement("br", null),
            "in Twente"),
        React.createElement(Typography, { className: classes.intro, paragraph: true }, "Publieksversie voortgangsrapportage investeringsprogramma\u2019s"),
        React.createElement(Typography, { className: classes.description, paragraph: true }, "In 2020 heeft de Twente Board een nieuw strategisch plan 2020 \u2013 2025 ontwikkeld, voor het benutten van economische kansen. Het behoud en de versterking van bedrijvigheid, de werkgelegenheid en het verdienvermogen van Twente staan hierin centraal, met als doel het stimuleren van de brede welvaart in de regio.")));
};
var EditionIndexTwenteboardComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, tablet: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })),
            React.createElement(RenderWhile, { mobile: true },
                React.createElement(Collapse, { in: open, collapsedHeight: 110, classes: { wrapper: classes.collapseContainer } },
                    React.createElement(Info, { edition: edition, classes: classes })),
                React.createElement(Button, { color: "primary", variant: "contained", style: { display: "flex", marginLeft: "auto" }, onClick: function () { return setOpen(!open); } }, open ? "Verberg" : "Lees meer")))));
};
export var EditionIndexTwenteboard = withStyles(styles)(EditionIndexTwenteboardComponent);
//# sourceMappingURL=edition-index-twenteboard.js.map