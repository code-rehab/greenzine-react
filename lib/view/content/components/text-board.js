import * as React from "react";
import { Hidden } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { SlideFade } from "./effects/slide-fade";
import { RenderWhile } from "./render-while";
var styles = function (theme) {
    var _a;
    var maxWidth = 460;
    return {
        root: {
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            width: "100%",
            transition: "height 0.1s, width 0.1s",
        },
        header: {
            display: "flex",
            position: "relative",
            alignItems: "flex-end",
            width: "100%",
            padding: "0 20px",
        },
        image: {
            maxWidth: "100%",
            marginBottom: "-20%",
            marginLeft: "-20px",
            padding: "0 20px",
        },
        montserrat: {
            fontWeight: "bold",
            fontFamily: "Montserrat",
        },
        title: {
            display: "inline-block",
            maxWidth: "max-content",
            maxHeight: "fit-content",
            padding: theme.spacing(1, 2),
            clipPath: "polygon(0% 10%, 100% 0%, 98% 100%, 2% 100%)",
            margin: "0 auto -25px auto",
            zIndex: 2,
            flexShrink: 0,
            "& *": {
                fontWeight: 500,
                margin: "0 !important",
            },
            transition: "height 0.1s, width 0.1s",
        },
        body: {
            color: "inherit",
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            maxWidth: maxWidth,
            margin: "0 auto",
            transition: "height 0.1s, width 0.1s",
        },
        subtitleWrapper: (_a = {
                height: "auto",
                width: "100%",
                backgroundColor: "white",
                padding: theme.spacing(3, 2, 2, 2)
            },
            _a[theme.breakpoints.up("md")] = {
                padding: theme.spacing(3, 2, 2, 2),
            },
            _a.display = "flex",
            _a.justifyContent = "center",
            _a.alignItems = "center",
            _a.clipPath = "polygon(0% 3%, 100% 0%, 96% 100%, 2% 100%)",
            _a.color = "#4a4a4a",
            _a.marginBottom = theme.spacing(2),
            _a.transition = "height 0.1s, width 0.1s",
            _a),
        subtitle: {
            maxWidth: maxWidth,
            margin: "0 auto",
        },
    };
};
export var TextBoard = withStyles(styles)(function (_a) {
    var classes = _a.classes, title = _a.title, children = _a.children, color = _a.color, subtitle = _a.subtitle, titleColor = _a.titleColor, image = _a.image;
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { mobile: true, desktop: true },
            React.createElement("div", { className: classes.root },
                React.createElement("header", { className: classes.header },
                    React.createElement("div", { className: classes.title, style: { backgroundColor: color, color: titleColor } },
                        React.createElement(SlideFade, { direction: "up", timeout: 500 }, title)),
                    React.createElement(SlideFade, { direction: "down", timeout: 700 },
                        React.createElement("div", { style: { overflowY: "hidden" } },
                            React.createElement(Hidden, { smDown: true },
                                React.createElement("img", { className: classes.image, src: image, alt: "" }))))),
                React.createElement("div", { className: classes.subtitleWrapper },
                    React.createElement(SlideFade, { direction: "up", timeout: 900 },
                        React.createElement("div", { className: classes.subtitle }, subtitle))),
                React.createElement("div", { className: classes.body },
                    React.createElement(SlideFade, { direction: "up", timeout: 1100 }, children)))),
        React.createElement(RenderWhile, { print: true },
            title,
            subtitle,
            children)));
});
//# sourceMappingURL=text-board.js.map