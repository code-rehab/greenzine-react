interface DefaultRecordData {
    id: string;
}
interface ModelFunctions<TData extends DefaultRecordData> {
    serialize(): RecordData<TData>;
    defaultValues: Partial<TData>;
    changes: Partial<TData>;
}
export declare type RecordData<TData extends DefaultRecordData = DefaultRecordData> = TData;
export declare type Model<TData extends RecordData = RecordData> = ModelFunctions<TData> & TData;
export declare class BaseModel<TData extends RecordData> implements Model {
    changes: Partial<TData>;
    protected _record: RecordData & Partial<TData>;
    defaultValues: Partial<TData>;
    constructor(_record: RecordData & Partial<TData>);
    get record(): RecordData & Partial<TData>;
    set record(record: RecordData & Partial<TData>);
    get id(): string;
    serialize: () => TData;
    take: (keys: (keyof TData)[]) => TData;
    protected get _values(): TData;
}
export {};
