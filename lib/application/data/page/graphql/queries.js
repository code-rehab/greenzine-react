import { PageFragment } from "./fragments";
export var Page = "\n  query Page($id:ID!) {\n    Page(id:$id){\n      " + PageFragment + "\n    }\n  }\n";
export var PageCollection = "\n  query PageCollection {\n    PageCollection{\n      " + PageFragment + "\n    }\n  }\n";
//# sourceMappingURL=queries.js.map