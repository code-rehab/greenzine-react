export declare function isset(val: any): boolean;
export declare function updateValue(obj: Record<any, any>, key: string, val: any): void;
export declare const removeNullValues: (data: any, recursive?: boolean) => any;
