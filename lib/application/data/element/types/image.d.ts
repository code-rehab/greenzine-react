import { ElementType, ElementBase } from "../element";
export interface ImageElementProps {
    src: string;
    caption?: string;
    url?: string;
    target?: boolean;
    shadow?: boolean;
}
export interface ImageElement extends ElementBase {
    component: ElementType.Image;
    props: ImageElementProps;
}
