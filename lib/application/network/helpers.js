export function isset(val) {
    return !(typeof val === "undefined" || val === null);
}
export function updateValue(obj, key, val) {
    obj[key] = val;
}
export var removeNullValues = function (data, recursive) {
    if (recursive === void 0) { recursive = true; }
    if (data.filter) {
        data = data.filter(Boolean);
    }
    for (var key in data) {
        if (data[key] !== null &&
            data[key] !== undefined &&
            data[key] !== Infinity) {
            if (typeof data[key] === "object" && recursive) {
                data[key] = removeNullValues(data[key]);
            }
        }
        else {
            delete data[key];
        }
    }
    return data;
};
//# sourceMappingURL=helpers.js.map