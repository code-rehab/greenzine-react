import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Page, PageRecord, PageModel } from "./page";
export interface PageProvider extends GraphQLProviderProps<Page, PageRecord> {
}
export declare class DefaultPageProvider extends GraphQLProvider<Page, PageRecord> implements PageProvider {
    model: typeof PageModel;
    listOperation: () => {
        query: any;
        variables: {};
    };
    createOperation: (page: Page) => {
        query: any;
        variables: {};
    };
    fetchOperation: (page: Page) => {
        query: any;
        variables: {};
    };
    updateOperation: (page: Page) => {
        query: any;
        variables: {};
    };
    deleteOperation: (page: Page) => {
        query: any;
        variables: {};
    };
}
