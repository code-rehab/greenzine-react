import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Page, PageRecord, PageModel } from "./page";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";

export interface PageProvider extends GraphQLProviderProps<Page, PageRecord> {}

export class DefaultPageProvider extends GraphQLProvider<Page, PageRecord> implements PageProvider {
  public model = PageModel;

  public listOperation = () => {
    return graphqlOperation(Query.PageCollection, {});
  };

  public createOperation = (page: Page) => {
    return graphqlOperation(Mutation.CreatePage, { input: page.serialize() });
  };

  public fetchOperation = (page: Page) => {
    return graphqlOperation(Query.Page, { id: page.id });
  };

  public updateOperation = (page: Page) => {
    return graphqlOperation(Mutation.UpdatePage, {
      input: page.serialize()
    });
  };

  public deleteOperation = (page: Page) => {
    return graphqlOperation(Mutation.DeletePage, { id: page.id });
  };
}
