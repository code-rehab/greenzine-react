import { computed } from "mobx";
import { PrintInteractor } from "../../../../application/business/interactor/print-interactor";
import { ArticleInteractor } from "../../../../application/data/article/article-interactor";
import { EditionData } from "../../../../application/data/edition/edition";
import { MagazineData } from "../../../../application/data/magazine/magazine";
import { MagazineInteractor } from "../../../../application/data/magazine/magazine-interactor";
import { IPresenter } from "../../../../helpers/with-presenter";

export class BondigEditionCoverPresenter implements IPresenter {
  public mount = () => {};
  public unmount = () => {};

  constructor(
    protected _magazineInteractor: MagazineInteractor,
    protected _articleInteractor: ArticleInteractor,
    protected _printInteractor: PrintInteractor
  ) {
    //
  }

  @computed public get edition(): EditionData | undefined {
    return this._magazineInteractor.selectedEdition;
  }

  @computed public get magazine(): MagazineData | undefined {
    return this._magazineInteractor.selectedMagazine;
  }

  public nextArticle = () => {
    this._articleInteractor.nextArticle();
  };

  public selectArticle = (article: any) => {
    this._articleInteractor.selectArticle(article);
  };
}
