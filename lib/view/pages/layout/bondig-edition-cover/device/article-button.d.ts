import * as React from "react";
interface OwnProps {
    id: string;
    category: string;
    title: string;
    classes?: Record<"root" | "title" | "category", string>;
}
export declare const ArticleButton: React.FC<OwnProps>;
export {};
