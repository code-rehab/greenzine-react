import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
import { Poll, PollData, PollModel } from "./poll";

export interface PollProvider extends GraphQLProviderProps<Poll, PollData> {}

export class DefaultPollProvider extends GraphQLProvider<Poll, PollData> implements PollProvider {
  public model = PollModel;

  protected listOperation = () => {
    return graphqlOperation(Query.PollCollection, {});
  };

  protected createOperation = (poll: Poll) => {
    return graphqlOperation(Mutation.CreatePoll, { input: poll.serialize() });
  };

  protected fetchOperation = (poll: Poll) => {
    return graphqlOperation(Query.Poll, { id: poll.id });
  };

  protected updateOperation = (poll: Poll) => {
    return graphqlOperation(Mutation.UpdatePoll, poll.serialize());
  };

  protected deleteOperation = (poll: Poll) => {
    return graphqlOperation(Mutation.DeletePoll, { id: poll.id });
  };
}
