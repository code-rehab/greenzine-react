import { ElementType, ElementBase } from "../element";
export interface WistJeDatElementProps {
    title: string;
    description: string;
    step: string;
    stepBackground: string;
    stepColor: string;
}
export interface WistJeDatElement extends ElementBase {
    component: ElementType.WistJeDat;
    props: WistJeDatElementProps;
}
