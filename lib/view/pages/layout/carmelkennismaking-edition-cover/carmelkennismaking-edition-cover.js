import * as React from "react";
import { withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
import { CarmelkennismakingEditionCoverPresenter } from "./carmelkennismaking-edition-cover-presenter";
// import { LayoutEditionCoverMobile } from "./device/mobile";
var Component = function (_a) {
    var config = _a.config, presenter = _a.presenter;
    return React.createElement(LayoutEditionCoverDesktop, { config: config, presenter: presenter });
};
export var LayoutCarmelkennismakingEditionCover = withPresenter(function (_props, _a) {
    var interactor = _a.interactor;
    return new CarmelkennismakingEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print);
}, Component);
//# sourceMappingURL=carmelkennismaking-edition-cover.js.map