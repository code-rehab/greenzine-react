import { GraphQLProviderProps } from "./graphql-provider";
import { RecordData } from "./base-model";
import { GraphQLModel } from "./graphql-model";
import { DefaultStore } from "../storage/store";
import { computed, observable } from "mobx";

export interface Collection<T> {
  items: T[];
  loading: boolean;
  hasItems: boolean;
  fetch(variables?: Record<string, any>): Promise<void>;
  find(id: string): T | undefined;
  where(key: string, value: any): Collection<T>;
  where(
    key: string,
    operator: ComparisonOperators,
    value: string | number | boolean
  ): Collection<T>;
}

type ComparisonOperators = ">" | "<" | ">=" | "<=" | "!=" | "==" | "!==" | "===";

interface ComparisonProps {
  key: string;
  operator: ComparisonOperators;
  value: string | number | boolean;
}

type CollectionFilters = Record<string, ComparisonProps>;

export class GraphQLCollection<T extends GraphQLModel<TRecord>, TRecord extends RecordData> {
  @observable public loading: boolean = false;
  @observable public filters: CollectionFilters = {};

  public get hasItems() {
    return this.provider.allRecords.length ? true : false;
  }

  constructor(
    private _items: TRecord[] | undefined = undefined,
    protected provider: GraphQLProviderProps<T, TRecord> & DefaultStore<TRecord>
  ) {}

  @computed public get items(): T[] {
    const items = (this._items || this.provider.allRecords).map((record) =>
      this.provider.createInstance(record)
    );
    return this.applyFilers(items, this.filters) || [];
  }

  public applyFilers = (items: T[], filters: CollectionFilters) => {
    return items;
  };

  public fetch = async (variables: Record<string, any> = {}) => {
    this.loading = true;
    await this.provider.fetchList(variables);

    this.loading = false;
  };

  public where = (
    ...args:
      | [string, string | number | boolean]
      | [string, ComparisonOperators, string | number | boolean]
  ) => {
    let key = args[0];
    let value = typeof args[2] === "string" ? args[2] : args[1];
    let operator = (args.length === 3 ? args[2] : "===") as ComparisonOperators;

    this.filters[key + operator + value] = { key, operator, value };

    return this;
  };

  public find = (id: string) => {
    return this.items.find((item) => item.id === id);
  };
}

// function compare(prop: string, operator: ComparisonOperators, value: any) {
//   switch (operator) {
//     case ">":
//       return prop > value;
//     case "<":
//       return prop < value;
//     case ">=":
//       return prop >= value;
//     case "<=":
//       return prop <= value;
//     case "==":
//       return prop == value;
//     case "!=":
//       return prop != value;
//     case "===":
//       return prop === value;
//     case "!==":
//       return prop !== value;
//   }
// }
