import { ArticleFragment } from "../../article/graphql/fragments";
// import { ArticleFragment } from "../../article/graphql/fragments";
export var EditionFragment = "\n    id\n    slug\n    title\n    public \n    description \n    image\n    theme\n    articles {\n      " + ArticleFragment + "\n    }\n";
//# sourceMappingURL=fragments.js.map