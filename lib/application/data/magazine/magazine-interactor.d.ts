import { Edition } from "../edition/edition";
import { Magazine } from "./magazine";
import { MagazineProvider } from "./magazine-provider";
export interface MagazineInteractor {
    selectedMagazine: Magazine | undefined;
    selectedEdition: Edition | undefined;
    selectMagazine(id: string, redirect?: boolean): Promise<void>;
    selectEdition(id: string, redirect?: boolean): Promise<void>;
}
export declare class DefaultMagazineInteractor implements MagazineInteractor {
    private _magazineProvider;
    constructor(_magazineProvider: MagazineProvider);
    selectedMagazine: Magazine | undefined;
    selectedEdition: Edition | undefined;
    selectMagazine: (id: string, redirect?: boolean) => Promise<void>;
    selectEdition: (id: string, redirect?: boolean) => Promise<void>;
}
