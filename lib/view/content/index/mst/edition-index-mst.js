import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "auto",
        },
        drawerBackdrop: {},
        header: {},
        content: {},
        backdrop: {
            backgroundColor: "transparant",
        },
        title: (_a = {
                color: "#1C9B9B",
                marginTop: -100,
                lineHeight: 0.9,
                fontWeight: 700
            },
            _a[theme.breakpoints.down("sm")] = {
                marginTop: 50,
                fontSize: "2.6em",
            },
            _a),
        subtitle: {
            fontWeight: 700,
            margin: "20px 0",
            color: "#4D4D4F",
        },
        description: {
            paddingRight: theme.spacing(1),
        },
        collapseContainer: {
            marginBottom: theme.spacing(3),
            position: "relative",
            "&:after": {
                content: "''",
                display: "block",
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
            },
            "&.MuiCollapse-entered": {
                "&:after": {
                    boxShadow: "none",
                },
            },
        },
        contentPiece: {
            fontFamily: "Montserrat",
        },
        marginBottom: {
            marginBottom: "1em",
        },
    });
};
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement(React.Fragment, null,
        React.createElement(Typography, { variant: "h1", className: classes.title }, "MST Jaarbericht 2019")));
};
var EditionIndexMSTComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })))));
};
export var EditionIndexMST = withStyles(styles)(EditionIndexMSTComponent);
//# sourceMappingURL=edition-index-mst.js.map