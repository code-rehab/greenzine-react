import * as React from "react";
import { Theme, WithStyles, withStyles, StyleRules, Typography, Grid, Button, Hidden } from "@material-ui/core";

import { observer } from "mobx-react";
import { MSTEdition2020CoverPresenter } from "../mst-edition-cover-presenter";
import { MSTEdition2020CoverLogos } from "../partials/logos";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";

type OwnProps = { config: any; presenter: MSTEdition2020CoverPresenter } & WithStyles<
  | "root"
  | "subtitle"
  | "title"
  | "highlight"
  | "highlightCategory"
  | "highlightTitle"
  | "highlights"
  | "buttonReadMore"
  | "coverImage"
  | "logo"
  | "info"
  | "content"
  | "backgroundShape"
>;
const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "relative",
    [theme.breakpoints.up("lg")]: {
      fontSize: "1.4em",
      padding: "5vw",
    },
    [theme.breakpoints.up("md")]: {
      width: "100%",
      padding: "0 72px",
      display: "flex",
      flexDirection: "column",
    },
  },

  backgroundShape: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    "&:after": {
      content: "' '",
      display: "block",
      position: "fixed",
      backgroundImage: "url('https://d6j399hnl3eyg.cloudfront.net/mst2020/COVER-Group 1629-def.jpg')",
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      zIndex: -1,
    },
  },

  logo: {
    position: "absolute",
    right: 0,
    top: 0,
    maxWidth: 240,
    [theme.breakpoints.down("md")]: {
      maxWidth: 220,
      right: 40,
    },
  },
  title: {
    position: "relative",
    marginBottom: theme.spacing(2),
    color: "#FFF",
    fontFamily: "Poppins",
    fontSize: 160,
    lineHeight: "88%",
    zIndex: 2,
    [theme.breakpoints.down("md")]: {
      paddingTop: 15,
      fontSize: 60,
      "& br": {
        display: "none",
      },
    },
  },
  subtitle: {
    marginBottom: theme.spacing(2),
    color: "#FFF",
    maxWidth: 500,
    fontWeight: 700,
    fontSize: 19,
    [theme.breakpoints.down("md")]: {
      fontSize: 16,
    },
  },

  buttonReadMore: {
    color: "#fff",
    backgroundColor: "#3DBCBD",
    borderRadius: 0,
    border: 0,
    maxWidth: 185,
    marginBottom: theme.spacing(3),
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: 15,
    padding: "7px 12px",
    "&:hover": {
      backgroundColor: "#2aa2a2",
      color: "#fff"
    },
  },

  coverImage: {
    maxWidth: 750,
    position: "relative",
    zIndex: 0,
    height: "auto",
    marginTop: 100,
    marginLeft: -100,
    [theme.breakpoints.down("md")]: {
      position: "relative",
      maxWidth: "80%",
      margin: "100px 10% 0 10%",
    },
  },
  info: {
    // backgroundColor: "green",
    display: "flex",
    margin: theme.spacing(3, 0),
    [theme.breakpoints.down("md")]: {
      flexDirection: "column-reverse",
    },
  },
  content: {
    paddingLeft: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",

    paddingTop: 40,
    [theme.breakpoints.down("md")]: {
      // marginTop: 150,
      marginLeft: 60,
      paddingBottom: 50,
      paddingRight: theme.spacing(3),
    },
  },
});

export const LayoutEdition2020CoverDesktop = withStyles(styles)(
  observer(({ classes, config, presenter }: OwnProps) => {
    const styles = config.styles || {};

    return (
      <div className={classes.root}>
        <div className={classes.backgroundShape}></div>
        <ZoomFade timeout={200}>
          <img className={classes.logo} src={"https://d6j399hnl3eyg.cloudfront.net/mst2020/Logo%20MST.svg"} alt="MST" />
        </ZoomFade>

        <div className={classes.info}>

          <div className={classes.content}>
            <SlideFade direction="up" timeout={1000}>
              <Typography className={classes.title} variant="h2" style={styles.title || {}}>
              Jaar <br/>
              bericht <br/>
              2020
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1100}>
              <Typography className={classes.subtitle} variant="h3" style={styles.subtitle || {}}>
                <span dangerouslySetInnerHTML={{ __html: config.head_article.secondary }} />
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1200}>
              <Button onClick={presenter.nextArticle} variant="outlined" className={classes.buttonReadMore}>
                Lees verder
              </Button>
            </SlideFade>
          </div>

          <div>
            <ZoomFade timeout={1900}>
              <img className={classes.coverImage} src={config.cover_image} alt="" style={{}} />
            </ZoomFade>
          </div>
        </div>
      </div>
    );
  })
);
