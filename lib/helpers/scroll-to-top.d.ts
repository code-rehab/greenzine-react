import * as React from "react";
import { RouteComponentProps } from "react-router";
declare class ScrollToTop extends React.Component<RouteComponentProps> {
    componentDidUpdate(prevProps: RouteComponentProps): void;
    render(): React.ReactNode;
}
declare const _default;
export default _default;
