import { makeStyles, Typography } from "@material-ui/core";
import * as React from "react";
import { useArticleInteractor } from "../../../../hooks/interactors";

interface OwnProps {
  id: string;
  category: string;
  title: string;
  classes?: Record<"root" | "title" | "category", string>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: "pointer",

    [theme.breakpoints.up("md")]: {
      maxWidth: 320
    },   
    [theme.breakpoints.up("lg")]: {
      maxWidth: 270,
    }
  },
  title: {
    textDecoration: "underline",
    lineHeight: "1.33",
    fontSize: "calc(14px + 0.2vw)",
  },
  category: {
    color: "#FFC583",
    marginTop: "-6px",
    fontFamily: "Montserrat",
    fontWeight: 600,
    marginBottom: 12,
    textTransform: "uppercase",
    fontSize: "calc(14px + 0.2vw)",
    lineHeight: "1.33",
  },
}));

export const ArticleButton: React.FC<OwnProps> = (props) => {
  const classes = useStyles(props);
  const interactor = useArticleInteractor();

  return (
    <div className={classes.root} onClick={() => interactor.selectArticle(props.id)}>
      <Typography className={classes.category}>{props.category}</Typography>
      <Typography variant={"body2"} className={classes.title}>
        {props.title}
      </Typography>
    </div>
  );
};
