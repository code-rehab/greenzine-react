var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { DefaultArticleInteractor } from "../data/article/article-interactor";
import { DefaultMagazineInteractor } from "../data/magazine/magazine-interactor";
import { BaseModule } from "../module";
import { PrintInteractor } from "./interactor/print-interactor";
var DefaultBusinessInteractors = /** @class */ (function (_super) {
    __extends(DefaultBusinessInteractors, _super);
    function DefaultBusinessInteractors(_providers) {
        var _this = _super.call(this) || this;
        _this._providers = _providers;
        return _this;
    }
    Object.defineProperty(DefaultBusinessInteractors.prototype, "magazine", {
        get: function () {
            return this.loadInteractor("magazine", DefaultMagazineInteractor, this._providers.magazine);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessInteractors.prototype, "article", {
        get: function () {
            return this.loadInteractor("article", DefaultArticleInteractor, this._providers.article, this.magazine);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessInteractors.prototype, "print", {
        get: function () {
            return this.loadInteractor("print", PrintInteractor);
        },
        enumerable: true,
        configurable: true
    });
    return DefaultBusinessInteractors;
}(BaseModule));
export { DefaultBusinessInteractors };
//# sourceMappingURL=interactors.js.map