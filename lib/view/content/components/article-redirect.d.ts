import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root" | "imageContainer" | "image" | "nextArticle" | "article" | "arrow" | "icon"> {
    thumbnail: any;
    children?: any;
    onClick?: any;
}
export declare const ArticleRedirect: React.ComponentType<Pick<OwnProps, "children" | "innerRef" | "onClick" | "thumbnail"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
