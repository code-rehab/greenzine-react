var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import * as React from "react";
import { withStyles } from "@material-ui/styles";
import { LayoutGrid } from "../../layout/grid/grid";
import { observer } from "mobx-react";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";
import { LayoutBondigEditionCover } from "../../layout/bondig-edition-cover/bondig-edition-cover";
import { ArticleRedirect } from "../../../content/components/article-redirect";
import { GridV2 } from "../../layout/grid-v2/grid";
import { take } from "../../../../helpers/general";
import { LayoutMSTEditionCover } from "../../layout/mst-edition-cover/mst-edition-cover";
import LoadingAnimation from "../../loading";
import { LayoutCarmelkoersEditionCover } from "../../layout/carmelkoers-edition-cover/carmelkoers-edition-cover";
import { LayoutCarmelkennismakingEditionCover } from "../../layout/carmelkennismaking-edition-cover/carmelkennismaking-edition-cover";
import { LayoutMSTEdition2020Cover } from "../../layout/mst-2020-edition-cover/mst-edition-cover";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {
                // minHeight: `100vh`,
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                padding: theme.spacing(3, 3, 3, 3),
                fontSize: "calc(1rem + 0.3vw)"
            },
            _a[theme.breakpoints.down("md")] = {
                fontSize: "0.5em",
            },
            _a[theme.breakpoints.down("sm")] = {
                fontSize: "calc(.5rem + 0.3vw)",
            },
            _a),
    });
};
var Page = withStyles(function (theme) { return ({
    root: {
        width: "100%",
    },
}); })(function (_a) {
    var page = _a.page, classes = _a.classes, article = _a.article, style = _a.style;
    style = style || {};
    if (page.layouts && page.layouts.sm && page.layouts.sm.length && page.layout !== "carmelkennismaking-edition-cover") {
        return React.createElement(GridV2, { article: article, page: page, breakpoint: "sm", style: style });
    }
    var oldstyle = __assign(__assign({}, (page.style || {})), (style || {}));
    if (page.layout === "grid") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutGrid, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data })));
    }
    if (page.layout === "bondig-edition-cover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutBondigEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })));
    }
    if (page.layout === "carmelkoers-edition-cover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutCarmelkoersEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })));
    }
    if (page.layout === "carmelkennismaking-edition-cover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutCarmelkennismakingEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })));
    }
    if (page.layout === "mst-edition-cover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutMSTEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })));
    }
    if (page.layout === "mst-edition-2020-cover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutMSTEdition2020Cover, { config: JSON.parse(page.layoutConfig || "{}") })));
    }
    if (page.layout === "articleCover") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: oldstyle.maxWidth } },
            React.createElement(LayoutArticleCover, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data, style: page.style })));
    }
    // fall back to desktop
    else if (page.layouts && page.layouts.lg && page.layouts.lg.length) {
        return React.createElement(GridV2, { article: article, page: page, breakpoint: "lg", style: style });
    }
    return React.createElement("div", null, "Unknown layout");
});
export var ArticleMobileContent = withStyles(styles)(observer(function (_a) {
    var presenter = _a.presenter, classes = _a.classes;
    var article = presenter.article;
    if (presenter.loading) {
        return React.createElement(LoadingAnimation, null);
    }
    if (!presenter.currentPage || !presenter.article) {
        return React.createElement("div", null, "Page not found");
    }
    var styles = JSON.parse(presenter.article.style) || {};
    var pages = __spreadArrays(presenter.article.pages);
    // const firstPAge = pages[0];
    return (React.createElement("div", { style: __assign(__assign({}, take(styles, ["background", "backgroundColor", "color"])), { overflowX: "hidden" }) }, pages.map(function (page, index) {
        var pagestyles = JSON.parse(page.style || "{}") || {};
        var mobileStyle = (page.layoutConfig && JSON.parse(page.layoutConfig || "").mobileStyle) || {};
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { key: page.id, className: ((page.layout === "grid" ||
                    page.layout === "bondig-edition-cover" ||
                    page.layout === "articleCover") &&
                    classes.root) ||
                    "", style: __assign(__assign({}, mobileStyle), (index === 0 &&
                    take(pagestyles, [
                        "background",
                        "color",
                        "backgroundSize",
                        "backgroundPosition",
                        // "backgroundImage",
                        "backgroundRepeat",
                        "backgroundColor",
                        "alignItems",
                        "minHeight"
                    ]))) },
                React.createElement("div", null,
                    React.createElement(Page, { page: page, article: article, style: __assign(__assign(__assign({}, (index > 0 && {
                            paddingTop: 40,
                            paddingBottom: 0,
                            minHeight: "unset !important",
                            backgroundImage: "none",
                            justifyContent: "unset",
                        })), (index === 1 &&
                            pages.length > 1 && {
                            minHeight: "unset !important",
                            paddingTop: 60,
                        })), (index === 0 && { minHeight: "100vh" })) }),
                    index === pages.length - 1 &&
                        presenter.currentPage &&
                        presenter.currentPage.layout !== "bondig-edition-cover" &&
                        presenter.currentPage.layout !== "mst-edition-2020-cover" &&
                        presenter.currentPage.layout !== "carmelkoers-edition-cover" &&
                        presenter.currentPage.layout !== "carmelkennismaking-edition-cover" &&
                        presenter.nextArticle && (React.createElement("div", { style: { transform: pages.length == 1 ? "translateY(-100px)" : "" } },
                        React.createElement(ArticleRedirect, { onClick: presenter.toNextArticle, thumbnail: presenter.nextArticle &&
                                (presenter.nextArticle.featuredImage
                                    ? presenter.nextArticle.featuredImage
                                    : presenter.nextArticle.image) }, presenter.nextArticle && presenter.nextArticle.title))),
                    index === pages.length - 1 && pages.length > 1 && React.createElement("div", { style: { height: 120 } })))));
    })));
}));
//# sourceMappingURL=mobile.js.map