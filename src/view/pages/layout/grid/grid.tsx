import * as React from "react";
import { LayoutColumnsDesktop } from "./device/desktop";
import { LayoutColumnsPrint } from "./device/print";
import { RenderWhile } from "../../../content/components/render-while";
import { LayoutColumnsMobile } from "./device/mobile";

export const LayoutGrid = ({ config, data }: any) => {
  data.forEach((d: any, i: number) => {
    d.animation = {
      duration: 700,
      delay: i * 50,
    };
  });

  const result = (Object.keys(config).length && {
    container: config.container || {},
    sections: Object.keys(config.sections).map((section_id) => {
      const sectionData: any[] = data.filter((d: any) => d.section === section_id);
      return { ...config.sections[section_id], section: section_id, data: sectionData };
    }),
  }) || {
    container: {},
    sections: [
      {
        data,
        section: "main",
      },
    ],
  };

  return (
    <>
      <RenderWhile desktop tablet>
        <LayoutColumnsDesktop data={result} />
      </RenderWhile>

      <RenderWhile mobile>
        <LayoutColumnsMobile data={result} />
      </RenderWhile>

      <RenderWhile print>
        <LayoutColumnsPrint data={result} />
      </RenderWhile>
    </>
  );
};
