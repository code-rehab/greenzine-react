import { PageFragment } from "../../page/graphql/fragments";
export var ArticleFragment = "\n  id\n  image\n  backgroundImage\n  title\n  content\n  type\n  index\n  slug\n  color\n  featuredImage\n  style\n  pages {\n   " + PageFragment + "\n  }\n  pageOrder\n";
//# sourceMappingURL=fragments.js.map