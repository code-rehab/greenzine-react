import { useTheme, withStyles } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import * as React from "react";
import { useMediaQuery } from "react-responsive";
import { withPresenter } from "../../../helpers/with-presenter";
import { RenderWhilePresenter } from "./render-while-presenter";
var styles = function (theme) { return ({
    root: {},
}); };
var RenderWhileComponent = observer(function (props) {
    var children = props.children, mobile = props.mobile, tablet = props.tablet, print = props.print, desktop = props.desktop, presenter = props.presenter;
    var theme = useTheme();
    var isPrint = presenter.isPrint;
    var isMobile = useMediaQuery({
        maxWidth: theme.breakpoints.values.md - 1,
    });
    var isTablet = useMediaQuery({
        minWidth: theme.breakpoints.values.md,
        maxWidth: theme.breakpoints.values.lg - 1,
    });
    var isDesktop = useMediaQuery({ minWidth: theme.breakpoints.values.lg });
    var isIE11 = useMediaQuery({
        query: "@media screen and (-ms-high-contrast: active), screen and (-ms-high-contrast: none)",
    });
    if (print && isPrint) {
        return React.createElement(React.Fragment, null, children);
    }
    if (mobile && isMobile && !isPrint && !isIE11) {
        return React.createElement(React.Fragment, null, children);
    }
    if (tablet && isTablet && !isPrint) {
        return React.createElement(React.Fragment, null, children);
    }
    if (desktop && isDesktop && !isPrint) {
        return React.createElement(React.Fragment, null, children);
    }
    return null;
});
export var RenderWhile = withStyles(styles)(withPresenter(function (props, _a) {
    var interactor = _a.interactor;
    return new RenderWhilePresenter(interactor.print);
}, RenderWhileComponent));
//# sourceMappingURL=render-while.js.map