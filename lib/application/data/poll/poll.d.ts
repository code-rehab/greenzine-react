import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { StorageRecord } from "../../storage/store";
export declare type PollQuestionType = "rating" | "multiselect" | "select" | "text";
export interface PollQuestion {
    id: string;
    type: PollQuestionType;
    question: string;
    answers: Array<string | number | boolean>;
    options?: Array<string | number | boolean>;
    validations?: string[];
}
export interface PollAttributes {
    id?: string;
    name?: string;
    questions?: PollQuestion[];
    pollData?: PollData;
    status?: string;
}
export interface PollData extends StorageRecord {
    id: string;
    name: string;
    attributes: PollAttributes;
    questions: PollQuestion[];
    submissions: PollSubmission[];
}
export interface PollSubmission {
    pollID: string;
    questionID: string;
    answers: string[];
    type: string;
}
export interface Poll extends GraphQLModel<PollData>, PollData {
    groupedSubmissions: any;
}
export declare class PollModel extends GraphQLBase<PollData> implements GraphQLModel<PollData> {
    typename: string;
    name: string;
    questions: never[];
    pollData: {};
    status: string;
    attributes: {};
    submissions: PollSubmission[];
    process(): void;
}
