import { observable, computed } from "mobx";
import { createRef } from "react";
import { RouteComponentProps } from "react-router";
import { Article, ArticleRecord } from "../../application/data/article/article";
import { ArticleInteractor } from "../../application/data/article/article-interactor";
import { ArticleProvider } from "../../application/data/article/article-provider";
import { EditionData } from "../../application/data/edition/edition";
import { Magazine } from "../../application/data/magazine/magazine";
import { MagazineInteractor } from "../../application/data/magazine/magazine-interactor";
import { MagazineProvider } from "../../application/data/magazine/magazine-provider";
import { RouteInfo, routes } from "../../config/routes";
import { IPresenter } from "../../helpers/with-presenter";
import { BasicPagePresenter } from "./_page-basic-presenter";
import { Page } from "../../application/data/page/page";
import { defaultMagazine } from "../../config/defaults";
import ReactGA from "react-ga4";

export class PagePresenter extends BasicPagePresenter implements IPresenter {
  @observable public page: RouteInfo;
  @observable public loading: boolean = true;
  @observable public article: Article | undefined = undefined;
  @observable public edition: EditionData | undefined = undefined;
  @observable public magazine: Magazine | undefined = undefined;
  @observable public currentPage: Page | undefined = undefined;
  @observable public currentPageRef: React.RefObject<HTMLDivElement>;
  @observable public hasNextPage: boolean = false;
  @observable public hasNextArticle: boolean = false;
  @observable public nextArticle: Article | undefined = undefined;

  constructor(
    protected _magazineInteractor: MagazineInteractor,
    protected _magazineProvider: MagazineProvider,
    protected _articleInteractor: ArticleInteractor,
    protected _articleProvider: ArticleProvider,
    _router: RouteComponentProps
  ) {
    super(_router);
    this.page = routes[this._router.match.path];
    this.setup();
    this.currentPageRef = createRef();
  }

  public setup = async () => {
    this.loading = true;
    const params = this._router.match.params;

    if (!this._magazineInteractor.selectedMagazine) {
      await this._magazineInteractor.selectMagazine(
        defaultMagazine.id, // set the default used magazine
        false
      );
    }

    if (params.edition) {
      await this._magazineInteractor.selectEdition(params.edition, false);
    } else {
      // select first edition if there is only one.
      if (this._magazineInteractor.selectedMagazine && this._magazineInteractor.selectedMagazine.editions) {
        let editions = this._magazineInteractor.selectedMagazine.editions;
        if (editions.length == 1) {
          window.location.href = "/" + editions[0].id + "/" + editions[0].articles[0].id;
        } else {
          this._magazineInteractor.selectedEdition = undefined;
        }
      } else {
        this._magazineInteractor.selectedEdition = undefined;
      }
    }

    this.edition = this._magazineInteractor.selectedEdition;

    if (params.article) {
      if (this.edition) {
        const foundBySlug = this.edition.articles.filter((art) => art.slug === params.article)[0];
        params.article = (foundBySlug && foundBySlug.id) || params.article;
      }
      await this._articleInteractor.selectArticle(params.article || "", false);
    } else {
      this._articleInteractor.selectedArticle = undefined;
    }

    this.article = this._articleInteractor.selectedArticle;

    if (params.page) {
      if (this.article) {
        const foundBySlug = this.article.pages.filter((p) => p.slug === params.page)[0];
        params.page = (foundBySlug && foundBySlug.id) || params.page;
      }
      await this._articleInteractor.selectPage(params.page, false);
    } else {
      if (this.article && this.article.pages.length) {
        await this._articleInteractor.selectPage(this.article.pages[0].id);
      }
    }

    this.magazine = this._magazineInteractor.selectedMagazine;
    this.currentPage = this._articleInteractor.selectedPage;

    // Google analytics
    if(this.magazine?.config.gaCode){      
      const trackingId = this.magazine?.config.gaCode;
      if (trackingId) {
        ReactGA.initialize(trackingId);
      }         
    }

    this.loading = false;

    const pageIndex = this._articleInteractor.pageIndex;
    const articleIndex = this._articleInteractor.articleIndex;

    this.hasNextPage = this._articleInteractor.pages[pageIndex + 1] ? true : false;
    this.hasNextArticle = this._articleInteractor.articles[articleIndex + 1] ? true : false;

    this.nextArticle =
      this._articleInteractor.articleIndex + 1 < this._articleInteractor.articles.length
        ? this._articleInteractor.articles[this._articleInteractor.articleIndex + 1]
        : undefined;
  };
}
