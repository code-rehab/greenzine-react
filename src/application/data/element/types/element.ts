import { TypographyElementProps, TypographyElement } from "./typography";
import { QuoteElementProps, QuoteElement } from "./quote";
import { ReadTimeElementProps, ReadTimeElement } from "./readtime";
import { VideoElementProps, VideoElement } from "./video";
import { ImageElementProps, ImageElement } from "./image";
import { WistJeDatElement, WistJeDatElementProps } from "./wistjedat";
import { DividerElementProps, DividerElement } from "./divider";
import { AuthorElementProps, AuthorElement } from "./author";
import { ListElementProps, ListElement } from "./list";

export type ElementProps =
  | TypographyElementProps
  | QuoteElementProps
  | ReadTimeElementProps
  | VideoElementProps
  | ImageElementProps
  | WistJeDatElementProps
  | DividerElementProps
  | AuthorElementProps
  | ListElementProps;
export type ElementUnion =
  | TypographyElement
  | QuoteElement
  | ReadTimeElement
  | VideoElement
  | ImageElement
  | WistJeDatElement
  | DividerElement
  | AuthorElement
  | ListElement;

export interface AnimationProps {
  duration: number;
  delay: number;
}
