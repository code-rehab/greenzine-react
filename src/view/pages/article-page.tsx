import { observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { PresenterProps, withPresenter } from "../../helpers/with-presenter";
import { RenderWhile } from "../content/components/render-while";
import { ArticlePresenter } from "./article-presenter";
import { ArticleDesktopContent } from "./device/article/desktop";
import { ArticleMobileContent } from "./device/article/mobile";
import { ArticlePrintContent } from "./device/article/print";
import { ArticleTabletContent } from "./device/article/tablet";

interface OwnProps extends RouteComponentProps {
  id: string;
  render: boolean;
  color?: string;
}

export interface PageContentProps {
  presenter: ArticlePresenter;
}

const ArticlePageComponent = observer(
  ({ presenter }: OwnProps & PresenterProps<ArticlePresenter>) => {
    return (
      <>
        <RenderWhile print>
          <ArticlePrintContent presenter={presenter} />
        </RenderWhile>

        <RenderWhile desktop>       
          <ArticleDesktopContent presenter={presenter} />
        </RenderWhile>

        <RenderWhile tablet>
          <ArticleTabletContent presenter={presenter} /> 
        </RenderWhile>

        <RenderWhile mobile>
          <ArticleMobileContent presenter={presenter} />
        </RenderWhile>
      </>
    );
  }
);

export const ArticlePage:any = withRouter(
  withPresenter<ArticlePresenter, OwnProps>(
    ({ match, history, location, staticContext }: OwnProps, { interactor, provider }) =>
      new ArticlePresenter(
        interactor.magazine,
        provider.magazine,
        interactor.article,
        provider.article,
        {
          match,
          history,
          location,
          staticContext,
        }
      ),
    ArticlePageComponent
  )
);
