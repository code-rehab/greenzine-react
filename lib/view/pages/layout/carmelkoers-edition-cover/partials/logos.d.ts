import * as React from "react";
import { WithStyles } from "@material-ui/core";
declare type OwnProps = {
    logo1: string;
    logo2: string;
    editionNr: string;
} & WithStyles<"root" | "logo1" | "logo2">;
export declare const BondigEditionCoverLogos: React.ComponentType<Pick<OwnProps, "logo1" | "logo2" | "editionNr"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
