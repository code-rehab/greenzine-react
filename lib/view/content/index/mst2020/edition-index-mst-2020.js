import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "auto",
        },
        drawerBackdrop: {},
        header: {},
        content: {},
        backdrop: {
            backgroundColor: "transparant",
        },
        text: {
            color: "#fff"
        },
        title: (_a = {
                color: "#FFF",
                marginBottom: 20,
                lineHeight: 0.9,
                fontWeight: 700,
                fontSize: 80
            },
            _a[theme.breakpoints.down("sm")] = {
                fontSize: "2.6em",
            },
            _a),
        subtitle: {
            fontWeight: 700,
            margin: "20px 0",
            color: "#4D4D4F",
        },
        description: {
            paddingRight: theme.spacing(1),
        },
        collapseContainer: {
            marginBottom: theme.spacing(3),
            position: "relative",
            "&:after": {
                content: "''",
                display: "block",
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
            },
            "&.MuiCollapse-entered": {
                "&:after": {
                    boxShadow: "none",
                },
            },
        },
        contentPiece: {
            fontFamily: "Montserrat",
        },
        marginBottom: {
            marginBottom: "1em",
        },
    });
};
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement(React.Fragment, null,
        React.createElement(Typography, { variant: "h1", className: classes.title },
            "Jaar ",
            React.createElement("br", null),
            "bericht",
            React.createElement("br", null),
            " 2020"),
        React.createElement(Typography, { className: classes.text, paragraph: true }, "Het jaar waar alles veranderde, waar saamhorigheid de boventoon voerde en waar al onze medewerkers ontzettend hard hebben gewerkt om continu de beste zorg te blijven bieden."),
        React.createElement(Typography, { className: classes.text }, "Een jaar vol verandering, digitalisering en verbroedering.  Lees hier verder over het bijzondere jaar, 2020.")));
};
var EditionIndexMSTComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })))));
};
export var EditionIndexMST2020 = withStyles(styles)(EditionIndexMSTComponent);
//# sourceMappingURL=edition-index-mst-2020.js.map