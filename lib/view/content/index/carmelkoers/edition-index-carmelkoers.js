import { Button, Collapse, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "auto",
        },
        drawerBackdrop: {},
        header: {},
        content: {},
        backdrop: {
            backgroundColor: "transparant",
        },
        title: (_a = {
                color: "#000",
                margin: "50px 0 20px",
                lineHeight: 0.9,
                "& br": {
                    display: "none",
                }
            },
            _a[theme.breakpoints.down("sm")] = {
                marginTop: 50,
                fontSize: "2.6em",
            },
            _a),
        subtitle: {
            fontWeight: 700,
            margin: "20px 0",
            color: "#4D4D4F",
        },
        description: {
            paddingRight: theme.spacing(1),
        },
        collapseContainer: {
            marginBottom: theme.spacing(3),
            position: "relative",
            "&:after": {
                content: "''",
                display: "block",
                position: "absolute",
                left: 0,
                bottom: 0,
                width: "100%",
                height: "100%",
                boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
            },
            "&.MuiCollapse-entered": {
                "&:after": {
                    boxShadow: "none",
                },
            },
        },
        contentPiece: {
            fontFamily: "Montserrat",
        },
        marginBottom: {
            marginBottom: "1em",
        },
    });
};
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement("div", { className: classes.description },
        React.createElement(Typography, { variant: "h2", className: classes.title },
            "Toekomst-",
            React.createElement("br", null),
            "bouwers"),
        React.createElement(Typography, { paragraph: true }, "Je staat op het punt om de publieksversie van onze Koers 2025 te lezen. Dit digitale magazine is een ingekorte en meer toegankelijke versie van het offici\u00EBle koersdocument. In dit magazine vind je allerlei linkjes naar de formele tekst, die vaak wat meer verdieping biedt."),
        React.createElement(Typography, { paragraph: true, className: classes.description },
            "De volledige, offici\u00EBle tekst van Koers 2025 lees je",
            " ",
            React.createElement("a", { target: "_blank", href: "https://carmel.nl/specials/Koers2025/download-de-gehele-koers-pdf" }, "hier"),
            "."),
        React.createElement(Typography, { className: classes.description }, "- Veel leesplezier!")));
};
var EditionIndexCarmelKoersComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, tablet: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })),
            React.createElement(RenderWhile, { mobile: true },
                React.createElement(Collapse, { in: open, collapsedHeight: 210, classes: { wrapper: classes.collapseContainer } },
                    React.createElement(Info, { edition: edition, classes: classes })),
                React.createElement(Button, { color: "primary", variant: "contained", style: { display: "flex", marginLeft: "auto" }, onClick: function () { return setOpen(!open); } }, open ? "Verberg" : "Lees meer")))));
};
export var EditionIndexCarmelKoers = withStyles(styles)(EditionIndexCarmelKoersComponent);
//# sourceMappingURL=edition-index-carmelkoers.js.map