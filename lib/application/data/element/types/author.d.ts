import { ElementType, ElementBase } from "../element";
export interface AuthorElementProps {
    avatar: any;
    credits: {
        credit: string;
        name: string;
    }[];
}
export interface AuthorElement extends ElementBase {
    component: ElementType.Author;
    props: AuthorElementProps;
}
