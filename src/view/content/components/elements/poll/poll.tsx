import { Button, Theme, Typography, withStyles, WithStyles } from "@material-ui/core";
import { StyleRules } from "@material-ui/styles";
import { toJS } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { PresenterProps, withPresenter } from "../../../../../helpers/with-presenter";
import { SlideFade } from "../../effects/slide-fade";
import { MultipleChoice } from "./multiple-choice";
import { PollPresenter } from "./poll-presenter";
import { ProgressBar } from "./progress-bar";
import { HeartSlider } from "./slider";
import LoadingAnimation from "../../../../pages/loading";
import { RenderWhile } from "../../render-while";

interface OwnProps
  extends RouteComponentProps,
    WithStyles<
      | "root"
      | "progressBar"
      | "button"
      | "answers"
      | "question"
      | "disclaimer"
      | "modal"
      | "montserrat"
      | "answer"
      | "textarea"
    > {
  questionIndex?: number;
  pollId: string;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  progressBar: {
    maxWidth: 400,
    width: "100%",
    margin: "auto",
  },
  button: {
    margin: "auto",
    transition: "color cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    color: "white",
    borderColor: "#fff",
    position: "relative",
    zIndex: 99999,
    "&:hover": {
      backgroundColor: "white",
      color: "#4a4a4a",
    },
  },
  question: {
    marginBottom: 40,
  },
  answers: {
    marginBottom: 40,
    width: "100%",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  answer: {
    width: "100%",
    color: "white",
    fontSize: "16px",
    backgroundColor: "transparent",
    height: 200,
    padding: 10,
    borderRadius: "15px",
    border: "1px solid",
    "&:focus": {
      outline: "none",
    },
  },
  disclaimer: {
    marginTop: 30,
  },
  modal: {
    backgroundColor: "transparent",
    color: "white",
    padding: theme.spacing(2),
    boxShadow: "none",
  },
  montserrat: {
    fontFamily: "Montserrat",
  },
  textarea: {
    width: "100%",
    maxWidth: 500,
    color: "white",
    fontSize: "16px",
    backgroundColor: "transparent",
    height: 200,
    padding: 10,
    borderRadius: "15px",
    border: "1px solid",
    "&::placeholder": {
      color: "rgba(255,255,255,0.5)",
      fontFamily: "Domaine",
    },
    "&:focus": {
      outline: "none",
    },
  },
});

const PollComponent = observer(({ classes, presenter }: OwnProps & PresenterProps<PollPresenter>) => {
  if (presenter.loading) {
    return <LoadingAnimation />;
  }

  let delay = 100;

  return (
    (presenter.currentQuestion && (
      <div className={classes.root} key={presenter.pollId}>
        <SlideFade direction={"up"} delay={delay}>
          <div className={classes.progressBar}>
            <ProgressBar subject="Vraag" total={presenter.questionCount} current={presenter.currentQuestionIndex + 1} />
          </div>
        </SlideFade>

        <SlideFade direction={"up"} delay={(delay += 100)}>
          <div className={classes.question}>
            <Typography variant="h6" className={classes.montserrat} align="center">
              {presenter.currentQuestion.question}
            </Typography>
          </div>
        </SlideFade>

        <SlideFade direction={"up"} delay={(delay += 100)}>
          <div className={classes.answers}>
            {presenter.currentQuestion.type === "multiselect" && (
              <MultipleChoice
                multiselect
                options={toJS(presenter.currentQuestion.options) || []}
                answers={presenter.currentValue || []}
                onChange={presenter.updateValue}
              />
            )}

            {presenter.currentQuestion.type === "rating" && (
              <HeartSlider
                maxValue={5}
                onChange={presenter.updateValue}
                currentValue={presenter.currentValue}
              ></HeartSlider>
            )}

            {presenter.currentQuestion.type === "select" && (
              <MultipleChoice
                options={toJS(presenter.currentQuestion.options) || []}
                answers={presenter.currentValue || []}
                onChange={presenter.updateValue}
              />
            )}

            {presenter.currentQuestion.type === "text" && (
              <textarea
                className={classes.textarea}
                placeholder="Type hier jouw bericht"
                onChange={(e) => presenter.updateValue(e.target.value)}
                defaultValue={(presenter.currentValue && presenter.currentValue[0]) || ""}
              />
            )}
          </div>
        </SlideFade>

        {presenter.errorMessage && (
          <SlideFade direction={"up"} delay={(delay += 100)}>
            <Typography gutterBottom variant={"body2"}>
              {presenter.errorMessage}
            </Typography>
          </SlideFade>
        )}

        <SlideFade direction={"up"} delay={(delay += 100)}>
          <div>
            <Button onClick={presenter.onSubmit} className={classes.button} variant="outlined">
              Antwoord verzenden
            </Button>
          </div>
        </SlideFade>

        <SlideFade direction={"up"} delay={(delay += 100)}>
          <Typography className={classes.disclaimer}>
            <small>Jouw antwoorden worden vertrouwelijk gebruikt.</small>
          </Typography>
        </SlideFade>
      </div>
    )) || (
   
        <Typography gutterBottom variant="h4" align="center">
          dankjewel
        </Typography>
    )
  );
});

export const Poll:any = withStyles(styles)(
  withRouter(
    withPresenter<PollPresenter, OwnProps>(
      ({ match, history, location, staticContext, questionIndex, pollId }, { provider }) =>
        new PollPresenter(pollId, questionIndex, provider.poll, {
          match,
          history,
          location,
          staticContext,
        }),
      PollComponent
    )
  )
);
