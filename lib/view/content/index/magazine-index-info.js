import * as React from "react";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { RenderWhile } from "../components/render-while";
var styles = function (theme) { return ({
    root: {
        margin: "auto",
    },
    drawerBackdrop: {},
    header: {},
    content: {},
    backdrop: {
        backgroundColor: "none",
    },
    role: {
        marginBottom: theme.spacing(1),
    },
}); };
var MagazineIndexInfoComponent = function (_a) {
    var magazine = _a.magazine, classes = _a.classes;
    // const [open, setOpen] = React.useState(false);
    return (React.createElement(React.Fragment, null,
        React.createElement(Typography, { className: classnames(classes.montserrat, classes.title), variant: "h4", color: "primary", style: { marginBottom: "1em", lineHeight: 1 } }, magazine.description),
        React.createElement(RenderWhile, { desktop: true, print: true },
            React.createElement("div", null,
                React.createElement(Typography, null,
                    "Bondig is vanaf editie 51 een interactief magazine. Je vindt deze magazines op deze pagina. ",
                    React.createElement("br", null),
                    React.createElement("br", null),
                    "Op zoek naar eerdere edities?",
                    " ",
                    React.createElement("a", { href: "https://ncf.nl/bondig-magazine/", target: "_blank", rel: "noopener noreferrer" }, "Klik dan hier."))))));
};
export var MagazineIndexInfo = withStyles(styles)(MagazineIndexInfoComponent);
// <RenderWhile mobile>
// <Collapse in={open}>
//   {Object.keys(magazine.editors).map((role, user) => {
//     return (
//       <>
//         <Typography
//           style={{ fontFamily: "Montserrat", fontWeight: "bold" }}
//         >
//           {/* {magazine.editors[role].type.charAt(0).toUpperCase() + magazine.editors[role].type.substring(1).replace(/([A-Z])/g, ' $1')} */}
//           {magazine.editors[role].type}
//         </Typography>
//         {magazine.editors[role].users.map((user: string) => (
//           <Typography variant="h5" style={{ margin: "0" }} key={user}>
//             {user}
//           </Typography>
//         ))}
//       </>
//     );
//   })}
// </Collapse>
// <Button
//   color="primary"
//   variant="contained"
//   style={{ display: "flex", marginLeft: "auto" }}
//   onClick={() => setOpen(!open)}
// >
//   {open ? "Verberg" : "Lees meer"}
// </Button>
// </RenderWhile>
//# sourceMappingURL=magazine-index-info.js.map