import * as React from "react";
declare const LoadingAnimation: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<string>>;
export default LoadingAnimation;
