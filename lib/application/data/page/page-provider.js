var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider } from "../../network/graphql-provider";
import { PageModel } from "./page";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
var DefaultPageProvider = /** @class */ (function (_super) {
    __extends(DefaultPageProvider, _super);
    function DefaultPageProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = PageModel;
        _this.listOperation = function () {
            return graphqlOperation(Query.PageCollection, {});
        };
        _this.createOperation = function (page) {
            return graphqlOperation(Mutation.CreatePage, { input: page.serialize() });
        };
        _this.fetchOperation = function (page) {
            return graphqlOperation(Query.Page, { id: page.id });
        };
        _this.updateOperation = function (page) {
            return graphqlOperation(Mutation.UpdatePage, {
                input: page.serialize()
            });
        };
        _this.deleteOperation = function (page) {
            return graphqlOperation(Mutation.DeletePage, { id: page.id });
        };
        return _this;
    }
    return DefaultPageProvider;
}(GraphQLProvider));
export { DefaultPageProvider };
//# sourceMappingURL=page-provider.js.map