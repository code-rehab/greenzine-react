var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { withStyles } from "@material-ui/styles";
import { RenderWhile } from "../../../content/components/render-while";
import { ArticleCoverDesktop } from "./device/desktop";
import { ArticleCoverMobile } from "./device/mobile";
import { ArticleCoverPrint } from "./device/print";
var styles = function (theme) { return ({
    root: {},
}); };
export var LayoutArticleCover = withStyles(styles)(function (props) {
    props.data.forEach(function (d, i) {
        d.animation = {
            duration: 700,
            delay: i * 100,
        };
    });
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { desktop: true },
            React.createElement(ArticleCoverDesktop, __assign({}, props))),
        React.createElement(RenderWhile, { mobile: true },
            React.createElement(ArticleCoverMobile, __assign({}, props))),
        React.createElement(RenderWhile, { print: true },
            React.createElement(ArticleCoverPrint, __assign({}, props)))));
});
//# sourceMappingURL=layout-article-cover.js.map