export const PollFragment = `
    id
    questions {
        id
      type
      question
      answers
      options
      validations
    }
    submissions {
        pollID
        questionID
        answers
    }
`;
