import { observer } from "mobx-react";
import * as React from "react";
import { withRouter } from "react-router";
import { withPresenter } from "../../helpers/with-presenter";
import { RenderWhile } from "../content/components/render-while";
import { ArticlePresenter } from "./article-presenter";
import { ArticleDesktopContent } from "./device/article/desktop";
import { ArticleMobileContent } from "./device/article/mobile";
import { ArticlePrintContent } from "./device/article/print";
import { ArticleTabletContent } from "./device/article/tablet";
var ArticlePageComponent = observer(function (_a) {
    var presenter = _a.presenter;
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { print: true },
            React.createElement(ArticlePrintContent, { presenter: presenter })),
        React.createElement(RenderWhile, { desktop: true },
            React.createElement(ArticleDesktopContent, { presenter: presenter })),
        React.createElement(RenderWhile, { tablet: true },
            React.createElement(ArticleTabletContent, { presenter: presenter })),
        React.createElement(RenderWhile, { mobile: true },
            React.createElement(ArticleMobileContent, { presenter: presenter }))));
});
export var ArticlePage = withRouter(withPresenter(function (_a, _b) {
    var match = _a.match, history = _a.history, location = _a.location, staticContext = _a.staticContext;
    var interactor = _b.interactor, provider = _b.provider;
    return new ArticlePresenter(interactor.magazine, provider.magazine, interactor.article, provider.article, {
        match: match,
        history: history,
        location: location,
        staticContext: staticContext,
    });
}, ArticlePageComponent));
//# sourceMappingURL=article-page.js.map