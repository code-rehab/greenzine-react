var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { computed, observable, toJS } from "mobx";
import { removeNullValues } from "../../../../application/network/helpers";
var LayoutRendererPresenter = /** @class */ (function () {
    function LayoutRendererPresenter(_layout, _elements, _printInteractor) {
        var _this = this;
        if (_elements === void 0) { _elements = []; }
        this._elements = _elements;
        this._printInteractor = _printInteractor;
        this.rowHeight = 12;
        this.margin = [24, 0];
        this.cols = 24;
        this.containerPadding = [0, 0];
        this._layout = [];
        this._refs = {};
        this._tmpRefs = {};
        this.mount = function () {
            _this.setup();
        };
        this.setupResizeObserver = function () {
            // this._resizeObserver = new ResizeObserver((data: any) => {
            //   console.log("Observed element",data[0].target.innerText,data[0].contentRect);
            //   if(data[0].contentRect.height){
            //     // data[0].target.style.backgroundColor = "red";
            //     data[0].target.style.height = data[0].contentRect.height.toFixed(2) +"px";
            //   }
            // });
            // console.log("Observer created");
        };
        this.setup = function () {
            _this.updateElementSizes();
            window.addEventListener("resize", function () {
                clearTimeout(_this._resizeTimeout);
                _this._resizeTimeout = setTimeout(_this.updateElementSizes, 1000);
            });
        };
        this.unmount = function () {
            //
        };
        this.updateElementSizes = function () { return __awaiter(_this, void 0, void 0, function () {
            var _refs;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, timeout(300)];
                    case 1:
                        _a.sent();
                        console.log('Update Sizes');
                        _refs = Object.keys(this._tmpRefs).reduce(function (refs, key) {
                            var item = _this._tmpRefs[key];
                            refs[item.id] = { height: (item.ref && item.ref.clientHeight) || 0 };
                            return refs;
                        }, {});
                        this._refs = __assign(__assign({}, this._refs), _refs);
                        return [2 /*return*/];
                }
            });
        }); };
        this.registerRef = function (id, ref) {
            if (ref) {
                _this._tmpRefs[id] = { id: id, ref: ref };
                // console.log("ref created");
                // if(!this._resizeObserver){
                //   this.setupResizeObserver();
                // }else{
                //   this._resizeObserver.observe(ref);
                // }
            }
        };
        this._layout = _layout;
    }
    Object.defineProperty(LayoutRendererPresenter.prototype, "elements", {
        get: function () {
            var _this = this;
            return this._elements.filter(function (el) {
                return _this.layout.find(function (l) {
                    return l.i === el.id;
                });
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LayoutRendererPresenter.prototype, "layout", {
        get: function () {
            var _this = this;
            return removeNullValues(toJS(this._layout || [])
                .map(function (l) {
                var h = Math.ceil(_this._refs[l.i] &&
                    _this._refs[l.i].height &&
                    _this._refs[l.i].height / (_this.rowHeight + _this.margin[1])) || 2;
                l.h = h;
                l.minW = 2;
                l.w = l.w < 2 ? _this.cols : l.w;
                if (_this._printInteractor.printActive) {
                    l.w = 24;
                }
                return l;
            })
                .filter(function (l) {
                return !_this._printInteractor.printActive || l.h > 1;
            }));
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "rowHeight", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "margin", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "cols", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "containerPadding", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "_layout", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "_refs", void 0);
    __decorate([
        observable
    ], LayoutRendererPresenter.prototype, "_resizeObserver", void 0);
    __decorate([
        computed
    ], LayoutRendererPresenter.prototype, "layout", null);
    return LayoutRendererPresenter;
}());
export { LayoutRendererPresenter };
function timeout(ms) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    setTimeout(resolve, ms);
                })];
        });
    });
}
//# sourceMappingURL=layout-renderer-presenter.js.map