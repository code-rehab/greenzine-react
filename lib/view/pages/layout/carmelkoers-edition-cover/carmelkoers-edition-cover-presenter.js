var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed } from "mobx";
var CarmelkoersEditionCoverPresenter = /** @class */ (function () {
    function CarmelkoersEditionCoverPresenter(_magazineInteractor, _articleInteractor, _printInteractor) {
        var _this = this;
        this._magazineInteractor = _magazineInteractor;
        this._articleInteractor = _articleInteractor;
        this._printInteractor = _printInteractor;
        this.mount = function () { };
        this.unmount = function () { };
        this.nextArticle = function () {
            _this._articleInteractor.nextArticle();
        };
        this.selectArticle = function (article) {
            _this._articleInteractor.selectArticle(article);
        };
        //
    }
    Object.defineProperty(CarmelkoersEditionCoverPresenter.prototype, "edition", {
        get: function () {
            return this._magazineInteractor.selectedEdition;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarmelkoersEditionCoverPresenter.prototype, "magazine", {
        get: function () {
            return this._magazineInteractor.selectedMagazine;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        computed
    ], CarmelkoersEditionCoverPresenter.prototype, "edition", null);
    __decorate([
        computed
    ], CarmelkoersEditionCoverPresenter.prototype, "magazine", null);
    return CarmelkoersEditionCoverPresenter;
}());
export { CarmelkoersEditionCoverPresenter };
//# sourceMappingURL=carmelkoers-edition-cover-presenter.js.map