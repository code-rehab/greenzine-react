var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BaseModule } from "../module";
import { DefaultBusinessInteractors } from "./interactors";
import { DefaultBusinessProviders } from "./providers";
var DefaultBusinessModule = /** @class */ (function (_super) {
    __extends(DefaultBusinessModule, _super);
    function DefaultBusinessModule(_network) {
        var _this = _super.call(this) || this;
        _this._network = _network;
        return _this;
    }
    Object.defineProperty(DefaultBusinessModule.prototype, "interactor", {
        get: function () {
            return this.loadModule("interactor", DefaultBusinessInteractors, this.provider);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessModule.prototype, "provider", {
        get: function () {
            return this.loadModule("provider", DefaultBusinessProviders, this._network);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessModule.prototype, "mockMode", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultBusinessModule.prototype, "printLayout", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    return DefaultBusinessModule;
}(BaseModule));
export { DefaultBusinessModule };
//# sourceMappingURL=business.js.map