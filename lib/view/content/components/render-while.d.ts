import { WithStyles } from "@material-ui/styles";
import * as React from "react";
interface OwnProps extends WithStyles<"root"> {
    children: any;
    mobile?: boolean;
    tablet?: boolean;
    print?: boolean;
    desktop?: boolean;
}
export declare const RenderWhile: React.ComponentType<Pick<OwnProps, "children" | "mobile" | "tablet" | "print" | "desktop" | "innerRef"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
