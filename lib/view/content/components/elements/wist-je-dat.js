import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { TextBoard } from "../text-board";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {},
            _a[theme.breakpoints.down("sm")] = {
                padding: theme.spacing(3, 0),
            },
            _a),
        secondary: {
            fontFamily: "Montserrat",
            fontWeight: 500,
        },
        articleCoverTitle: {
            "&$secondary": {
                fontWeight: 300,
            },
        },
        title: {
            fontWeight: 500,
        },
        italic: {
            fontStyle: "italic",
        },
        strong: {
            fontWeight: 700,
        },
        fatHeader: {
            fontFamily: "Montserrat",
            fontWeight: 500,
            marginBottom: "1em",
            paddingBottom: "1em",
            borderBottom: "solid 5px",
        },
    });
};
export var WistJeDat = withStyles(styles)(function (props) {
    var animation = props.animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement("div", { className: props.classes.root },
            React.createElement(TextBoard, { image: "/article/images/vent-turquoise.png", color: props.stepBackground, titleColor: props.stepColor, title: React.createElement(React.Fragment, null,
                    React.createElement(Typography, { variant: "h5" }, props.step)), subtitle: React.createElement(Typography, { variant: "h5", style: { fontStyle: "italic" } },
                    React.createElement("span", { dangerouslySetInnerHTML: { __html: props.title } })) },
                React.createElement(Typography, null,
                    React.createElement("span", { dangerouslySetInnerHTML: { __html: props.description } }))))));
});
//# sourceMappingURL=wist-je-dat.js.map