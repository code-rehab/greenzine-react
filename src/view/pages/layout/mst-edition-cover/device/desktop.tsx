import * as React from "react";
import { Theme, WithStyles, withStyles, StyleRules, Typography, Grid, Button, Hidden } from "@material-ui/core";

import { observer } from "mobx-react";
import { MSTEditionCoverPresenter } from "../mst-edition-cover-presenter";
import { MSTEditionCoverLogos } from "../partials/logos";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { mapEvent } from "../../../../../helpers/formatters";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
// import { ZoomFade } from "../../../../content/components/effects/zoom-fade";

type OwnProps = { config: any; presenter: MSTEditionCoverPresenter } & WithStyles<
  | "root"
  | "subtitle"
  | "title"
  | "highlight"
  | "highlightCategory"
  | "highlightTitle"
  | "highlights"
  | "buttonReadMore"
  | "coverImage"
  | "logo"
  | "info"
  | "content"
  | "backgroundShape"
>;
const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "relative",
    [theme.breakpoints.up("lg")]: {
      fontSize: "1.4em",
      padding: "5vw",
    },
    [theme.breakpoints.up("md")]: {
      width: "100%",
      padding: "0 72px",
      display: "flex",
      flexDirection: "column",
    },
  },

  backgroundShape: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    "&:after": {
      content: "' '",
      display: "block",
      position: "fixed",
      backgroundImage: "url('/article/images/mst/mst_cover_bg.svg')",
      backgroundPosition: "bottom right",
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      zIndex: -1,
    },
  },

  logo: {
    position: "absolute",
    left: 0,
    top: 0,
    maxWidth: 370,
    [theme.breakpoints.down("md")]: {
      maxWidth: 220,
      left: 40,
    },
  },
  title: {
    position: "relative",
    marginBottom: theme.spacing(2),
    color: "#1C9B9B",
    [theme.breakpoints.down("md")]: {
      // maxWidth: "3em",
      paddingTop: 15,
      fontSize: "2em",
      "& br": {
        display: "none",
      },
    },
    "&::before": {
      content: '""',
      position: "absolute",
      left: -60,
      top: 10,
      width: 38,
      height: 33,
      backgroundImage: "url('/assets/images/mst/pijltje.svg')",
      backgroundSize: "contain",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      display: "block",
    },
  },
  subtitle: {
    marginBottom: theme.spacing(2),
    color: "#4D4D4F",
    maxWidth: 500,
    fontWeight: 700,
    fontSize: ".9em",
    [theme.breakpoints.down("md")]: {
      fontSize: 16,
    },
  },

  buttonReadMore: {
    color: "#fff",
    backgroundColor: theme.palette.secondary.main,
    border: 0,
    maxWidth: 185,
    marginBottom: theme.spacing(3),
    fontFamily: "Museo",
    fontWeight: 700,
    padding: "7px 36px",
    "&:hover": {
      backgroundColor: theme.palette.secondary.light,
    },
  },

  coverImage: {
    maxWidth: 800,
    height: "auto",
    marginLeft: 100,
    [theme.breakpoints.down("md")]: {
      position: "relative",
      maxWidth: "80%",
      margin: "100px 10% 0 10%",
    },
  },
  info: {
    // backgroundColor: "green",
    display: "flex",
    margin: theme.spacing(3, 0),
    [theme.breakpoints.down("md")]: {
      flexDirection: "column-reverse",
    },
  },
  content: {
    paddingLeft: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",

    paddingTop: 40,
    [theme.breakpoints.down("md")]: {
      // marginTop: 150,
      marginLeft: 60,
      paddingBottom: 50,
      paddingRight: theme.spacing(3),
    },
  },
});

export const LayoutEditionCoverDesktop = withStyles(styles)(
  observer(({ classes, config, presenter }: OwnProps) => {
    const styles = config.styles || {};

    return (
      <div className={classes.root}>
        <div className={classes.backgroundShape}></div>
        <ZoomFade timeout={200}>
          <img className={classes.logo} src={"/article/images/mst/mst-logo.svg"} alt="MST" />
        </ZoomFade>

        <div className={classes.info}>
          {/* Content */}

          <div className={classes.content}>
            <SlideFade direction="up" timeout={1000}>
              <Typography className={classes.title} variant="h2" style={styles.title || {}}>
                MST{" "}
                <Hidden smUp>
                  <br />
                </Hidden>
                Jaarbericht 2019
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1100}>
              <Typography className={classes.subtitle} variant="h3" style={styles.subtitle || {}}>
                <span dangerouslySetInnerHTML={{ __html: config.head_article.secondary }} />
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1200}>
              <Button onClick={presenter.nextArticle} variant="outlined" className={classes.buttonReadMore}>
                Lees verder
              </Button>
            </SlideFade>
          </div>

          <div>
            <ZoomFade timeout={1900}>
              <img className={classes.coverImage} src={config.cover_image} alt="" style={{}} />
            </ZoomFade>
          </div>
        </div>
      </div>
    );
  })
);
