import * as React from "react";
import {
  Theme,
  WithStyles,
  withStyles,
  StyleRules,
  Typography,
  Grid,
  Button,
  Hidden,
} from "@material-ui/core";

import { observer } from "mobx-react";
import { CarmelkoersEditionCoverPresenter } from "../carmelkoers-edition-cover-presenter";
import { BondigEditionCoverLogos } from "../partials/logos";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { mapEvent } from "../../../../../helpers/formatters";

import { Carousel } from "../../../../interface/partials/carousel";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
import zIndex from "@material-ui/core/styles/zIndex";
// import { ZoomFade } from "../../../../content/components/effects/zoom-fade";

type OwnProps = { config: any; presenter: CarmelkoersEditionCoverPresenter } & WithStyles<
  | "root"
  | "primary"
  | "secondary"
  | "highlight"
  | "highlightCategory"
  | "highlightTitle"
  | "highlights"
  | "buttonReadMore"
  | "imageWrap"
  | "image"
  | "content"
  | "logo"
>;
const styles = (theme: Theme): StyleRules => ({
  content: {
    position: "relative",
    zIndex: 999,
    [theme.breakpoints.down("md")]: {
      "& h1": {
        fontSize: "3.4em",
      },
      "& h3": {
        fontSize: "2em",
      },
      "& span br": {
        display: "none",
      },
    },
  },
  root: {
    // overlay
    "&::before": {
      content: "' '",
      position: "absolute",
      top: 0,
      left: 0,
      bottom: 0,
      width: "40vw",
      background: "linear-gradient(to right,#000,transparent)",
      zIndex: 0,
    },

    [theme.breakpoints.up("md")]: {
      width: "100%",
      minHeight: "calc(100vh - 60px)",
      padding: theme.spacing(6),
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: "1.4em",
      padding: "5vw",
    },

    [theme.breakpoints.down("md")]: {
      // paddingBottom: 120,
      minHeight: "100vh",
      display: "flex",
      flexDirection: "column",
    },
  },
  primary: {
    fontSize: "calc(3em + 1.1vw)",
    marginBottom: theme.spacing(2),

    [theme.breakpoints.down("sm")]: {
      fontSize: "5em",
    },
  },
  secondary: {
    fontSize: "1.1em",
    marginBottom: theme.spacing(2),

    [theme.breakpoints.down("md")]: {
      fontSize: "2.5em",
    },

    [theme.breakpoints.down("sm")]: {
      fontSize: "3em",
    },
  },
  highlights: {
    display: "inline-block",
    zIndex: 99,
    position: "fixed",

    transformOrigin: "left",
    transform: "scale(.6)",
    left: 20,
    bottom: 70,

    [theme.breakpoints.up("md")]: {
      left: 60,
      bottom: 100,
      transform: "scale(1)",
    },

    [theme.breakpoints.up("lg")]: {
      left: 60,
      bottom: 90,
      transformOrigin: "left",
    },

    [theme.breakpoints.up("xl")]: {
      left: 100,
      bottom: 100,
      transformOrigin: "left",
    },
  },
  highlight: {
    // borderLeft: "1px solid #f0c33b",
    padding: theme.spacing(0.5, 10, 0, 1) + " !important",
    maxWidth: 375,
    marginTop: theme.spacing(2),

    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      top: 0,
      width: 1,
      backgroundColor: "#D8D8D8",
      height: 150,
    },
  },
  highlightCategory: {
    color: "#f0c33b",
    textTransform: "uppercase",
    marginTop: theme.spacing(-1),
    marginBottom: theme.spacing(1),
    fontFamily: "Montserrat",
    fontWeight: 600,
    // fontSize: "0.8em"
  },
  highlightTitle: {
    fontSize: ".85em",
    lineHeight: 1.25,
    textDecoration: "underline",
    cursor: "pointer",
    "&:hover": {
      "& a": {
        color: "#f0c33b",
      },
    },

    "& > a": {
      transition: "color 0.2s ease",
      color: "#fff",
    },
  },
  buttonReadMore: {
    padding: "7px 53px",
    borderRadius: 40,
    marginTop: theme.spacing(3),
    marginLeft: 50,
    fontSize: "1em",
    [theme.breakpoints.up("md")]: {
      fontSize: "2em",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: ".75em",
    },

    ["@media (max-height:800px)"]: { marginTop: 15 },
  },
  logo: {
    position: "absolute",
    right: 100,
    top: 100,

    [theme.breakpoints.down("md")]: {
      right: 30,
      top: 30,
      transformOrigin: "right",
      transform: "scale(.55)",
    },
  },
  imageWrap: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,

    [theme.breakpoints.down("md")]: {
      position: "fixed",
      maxWidth: "60%",
      top: "unset",
      left: "unset",
      bottom: "5%",
      right: "5%",
      width: 500,
    },
  },
  image: {
    // Mobile /////////////////////
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(10),
      width: "80%",
      marginLeft: "10%",
      "@media (orientation: landscape)": {
        marginTop: 45,
        display: "none",
      },
    },

    // Tablet portait /////////////////////
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10),
      width: "80%",
      marginLeft: "10%",
      "@media (orientation: landscape)": {
        display: "flex",
      },
    },
    // tablet landscape and smaller desktop
    [theme.breakpoints.up("md")]: {
      width: "50%",
      marginTop: theme.spacing(0),
      marginLeft: "0%",
    },
    //larger desktop
    [theme.breakpoints.up("lg")]: {
      width: "40%",
      marginTop: theme.spacing(6),
      marginLeft: "0%",
    },
  },
});

export const LayoutEditionCoverDesktop = withStyles(styles)(
  observer(({ classes, config, presenter }: OwnProps) => {
    const styles = config.styles || {};

    return (
      <div style={{ position: "relative", width: "100%" }}>
        <div className={classes.root}>
          <div className={classes.logo}>
            <img src="/assets/images/carmel/logo.svg" />
          </div>
          <div className={classes.content}>
            <SlideFade direction="up" timeout={1000}>
              <Typography className={classes.primary} variant="h1" style={styles.primary || {}}>
                <span dangerouslySetInnerHTML={{ __html: config.head_article.primary }} />
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1100}>
              <Typography className={classes.secondary} variant="h3" style={styles.secondary || {}}>
                <span dangerouslySetInnerHTML={{ __html: config.head_article.secondary }} />
              </Typography>
            </SlideFade>

            <SlideFade direction="up" timeout={1200}>
              <img
                src="/assets/images/carmel/toekomstbouwers.svg"
                style={styles.image || {}}
                className={classes.image}
              />
              <br />
              <Button
                onClick={presenter.nextArticle}
                variant="contained"
                color="primary"
                className={classes.buttonReadMore}
              >
                Let's go!
              </Button>
            </SlideFade>
          </div>
          <div className={classes.highlights}>
            <Grid container justify="flex-start">
              <Hidden mdUp>
                <Carousel color={"#f0c33b"} data={config.highlightedContent} />
              </Hidden>
              <Hidden smDown>
                {(config.highlightedContent || []).map((content: any, index: number) => (
                  <SlideFade key={index} direction="up" timeout={1400 + index * 100}>
                    <Grid item className={classes.highlight}>
                      <Typography className={classes.highlightCategory}>{content.title}</Typography>
                      <Typography variant={"body2"} className={classes.highlightTitle}>
                        <div onClick={mapEvent(presenter.selectArticle, content.link)}>
                          {content.content}
                        </div>
                      </Typography>
                    </Grid>
                  </SlideFade>
                ))}
              </Hidden>
            </Grid>
          </div>
        </div>
      </div>
    );
  })
);
