import * as React from "react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { EditionBall } from "../../../../content/components/edition-ball";
import { withStyles, Grid, Typography } from "@material-ui/core";
import { observer } from "mobx-react";
var styles = function (theme) {
    var _a, _b, _c, _d;
    return ({
        root: (_a = {
                display: "flex",
                justifyContent: "space-between",
                alignItems: "flex-start"
            },
            _a[theme.breakpoints.down("sm")] = {
                padding: theme.spacing(4, 0)
            },
            _a),
        logo1: (_b = {
                display: "inline-block",
                "@media all and (-ms-high-contrast:none)": {
                    width: 600
                },
                "& img": (_c = {
                        marginBottom: 20
                    },
                    _c[theme.breakpoints.down("sm")] = {
                        width: 120,
                        paddingRight: 25
                    },
                    _c)
            },
            _b[theme.breakpoints.down("sm")] = {
                maxWidth: "160px"
            },
            _b),
        logo2: (_d = {},
            _d[theme.breakpoints.down("md")] = {
                width: "15vw"
            },
            _d)
    });
};
export var BondigEditionCoverLogos = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, logo1 = _a.logo1, logo2 = _a.logo2, editionNr = _a.editionNr;
    return (React.createElement("header", { className: classes.root },
        React.createElement(SlideFade, { direction: "up", timeout: 900 },
            React.createElement("div", { className: classes.logo1 },
                React.createElement(Grid, { container: true, spacing: 0, alignItems: "center" },
                    React.createElement(Grid, { item: true, sm: true, style: { position: "relative" } },
                        React.createElement("img", { src: logo1 || "", alt: "Bondig" }),
                        React.createElement("div", { style: { position: "absolute", top: -5, right: -5 } },
                            React.createElement(EditionBall, null,
                                "nr. ",
                                editionNr || ""))),
                    React.createElement(Grid, { item: true, sm: true },
                        React.createElement(Typography, { variant: "body1", style: { maxWidth: 300, lineHeight: 1, marginLeft: 5, marginTop: -12 } },
                            "digitaal magazine ",
                            React.createElement("br", null),
                            " voor leden van de NCF"))))),
        React.createElement(SlideFade, { direction: "up", timeout: 900 },
            React.createElement("img", { className: classes.logo2, src: logo2, alt: "Logo" }))));
}));
//# sourceMappingURL=logos.js.map