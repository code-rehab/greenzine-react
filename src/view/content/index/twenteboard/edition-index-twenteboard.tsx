import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { toJS } from "mobx";
import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
import { RenderWhile } from "../../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
    marginTop: theme.spacing(3),
  },

  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  title: {
    color: "#fff",
    // margin: "20px 0",
    // fontFamily: "Amaranth",
    // lineHeight: 0.9,
    // fontSize: 42,
    "& span": {
      color: "#F90000",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: 50,
      fontSize: "2.6em",
    },
  },
  intro: {
    fontWeight: 700,
    margin: "20px 0",
    color: "#FFF",
  },
  logo: {
    marginTop: theme.spacing(4),
  },
  description: {
    // fontSize: 17,
    color: "#fff",
    paddingRight: theme.spacing(1),
  },
  collapseContainer: {
    marginBottom: theme.spacing(3),
    position: "relative",

    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <div className={classes.root}>
      <Typography variant={"h1"} className={classes.title}>
        <span>Investeren</span> <br/>
        in Twente
      </Typography>

      <Typography className={classes.intro} paragraph>
      Publieksversie voortgangsrapportage investeringsprogramma’s
      </Typography>

      <Typography className={classes.description} paragraph>
      In 2020 heeft de Twente Board een nieuw strategisch plan 2020 – 2025 ontwikkeld, voor het benutten van economische kansen. Het behoud en de versterking van bedrijvigheid, de werkgelegenheid en het verdienvermogen van Twente staan hierin centraal, met als doel het stimuleren van de brede welvaart in de
regio. 
      </Typography>


    </div>
  );
};

const EditionIndexTwenteboardComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div>
        <RenderWhile desktop tablet print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        <RenderWhile mobile>
          <Collapse
            in={open}
            collapsedHeight={110}
            classes={{ wrapper: classes.collapseContainer }}
          >
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }}
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile>
      </div>
    </>
  );
};

export const EditionIndexTwenteboard = withStyles(styles)(
  EditionIndexTwenteboardComponent
);
