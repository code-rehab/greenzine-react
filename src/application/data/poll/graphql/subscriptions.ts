import { PollFragment } from "./fragments";

export const PollCreated = `
subscription {
  userCreated {
    ${PollFragment}
  }
}
`;

export const PollUpdated = `
subscription {
  userUpdated {
    ${PollFragment}
  }
}
`;

export const PollDeleted = `
subscription {
  userDeleted {
    ${PollFragment}
  }
}
`;
