import * as React from "react";
import { observer } from "mobx-react";
import GridLayout, { WidthProvider, Layout } from "react-grid-layout";

import { toJS } from "mobx";

import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
import { LayoutRendererPresenter } from "./layout-renderer-presenter";
import { makeStyles } from "@material-ui/core";
import { RenderElement, AnyElement } from "@coderehab/greenzeen-content";

const GridLayoutRenderer = WidthProvider(GridLayout);

interface OwnProps {
  elements: AnyElement[];
  layout: Layout[];
  print?: boolean;
}

const useStyles = makeStyles({
  root: {
    margin: "0 !important",

    "& .react-grid-item": {
      touchAction: "auto !important",
      margin: "0 !important",
    },
  },
});

export const RendererComponent = observer(
  ({ presenter, print }: OwnProps & PresenterProps<LayoutRendererPresenter>) => {
    const { layout, elements, registerRef, containerPadding, rowHeight, margin, cols } = presenter;
    const classes = useStyles();  
    return (
      <div className={classes.root} >
        <GridLayoutRenderer
          layout={toJS(layout)}
          isResizable={false}
          isDraggable={false}
          isDroppable={false}
          margin={margin}
          cols={cols}
          rowHeight={rowHeight}
          containerPadding={containerPadding}
          autoSize={true}
        >
          {elements.map((element) => (
            <div key={element.id}>
              <div ref={(ref) => registerRef(element.id, ref)} style={{ padding: 1 }}>
                <RenderElement print={print} element={element} />
              </div>
            </div>
          ))}
        </GridLayoutRenderer>
      </div>
    );
  }
);

export const LayoutRenderer = withPresenter<LayoutRendererPresenter, OwnProps>(
  ({ layout, elements }, { interactor }) =>
    new LayoutRendererPresenter(layout, elements, interactor.print),
  RendererComponent
);
