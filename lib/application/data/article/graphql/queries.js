import { ArticleFragment } from "./fragments";
export var Article = "\n  query Article($id:ID!) {\n    Article(id:$id){\n      " + ArticleFragment + "\n    }\n  }\n";
export var ArticleCollection = "\n  query ArticleCollection {\n    ArticleCollection{\n      " + ArticleFragment + "\n    }\n  }\n";
//# sourceMappingURL=queries.js.map