import { EditionFragment } from "./fragments";
export var Edition = "\n  query Edition($id:ID!) {\n    Edition(id:$id){\n      " + EditionFragment + "\n    }\n  }\n";
export var EditionCollection = "\n  query EditionCollection($magazine:ID!) {\n    Magazine(id:$magazine) {\n      editions{\n      " + EditionFragment + "\n      }\n    }\n  }\n";
//# sourceMappingURL=queries.js.map