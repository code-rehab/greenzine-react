import * as React from "react";
import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
import { CarmelkennismakingEditionCoverPresenter } from "./carmelkennismaking-edition-cover-presenter";
// import { LayoutEditionCoverMobile } from "./device/mobile";

const Component = ({ config, presenter }: OwnProps & PresenterProps<CarmelkennismakingEditionCoverPresenter>) => {
  return <LayoutEditionCoverDesktop config={config} presenter={presenter} />;
};

interface OwnProps {
  config: any;
}

export const LayoutCarmelkennismakingEditionCover = withPresenter<CarmelkennismakingEditionCoverPresenter, OwnProps>(
  (_props, { interactor }) =>
    new CarmelkennismakingEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print),
  Component
);
