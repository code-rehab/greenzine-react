import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Magazine, MagazineData, MagazineModel } from "./magazine";
export interface MagazineProvider extends GraphQLProviderProps<Magazine, MagazineData> {
}
export declare class DefaultMagazineProvider extends GraphQLProvider<Magazine, MagazineData> implements MagazineProvider {
    model: typeof MagazineModel;
    protected listOperation: () => {
        query: any;
        variables: {};
    };
    protected createOperation: (magazine: MagazineData) => {
        query: any;
        variables: {};
    };
    protected fetchOperation: (magazine: MagazineData) => {
        query: any;
        variables: {};
    };
    protected updateOperation: (magazine: Magazine) => {
        query: any;
        variables: {};
    };
    protected deleteOperation: (magazine: MagazineData) => {
        query: any;
        variables: {};
    };
}
