var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { withStyles } from "@material-ui/styles";
import { SlideFade } from "./slide-fade";
var styles = function (theme) { return ({
    root: {},
}); };
export var animationComponents = {
    fadeUp: React.createElement(SlideFade, { direction: "up" }),
    fadeLeft: React.createElement(SlideFade, { direction: "left" }),
    fadeRight: React.createElement(SlideFade, { direction: "right" }),
};
export var AnimationWrapper = withStyles(styles)(function (_a) {
    var classes = _a.classes, children = _a.children, style = _a.style, animation = _a.animation, baseTimeout = _a.baseTimeout;
    var animationSet = animation ? (animationComponents[animation] ? (animationComponents[animation]) : (React.createElement(React.Fragment, null))) : (React.createElement(React.Fragment, null));
    return (React.createElement(React.Fragment, null, React.Children.map(children, function (child, index) {
        if (!child) {
            return React.createElement(React.Fragment, null);
        }
        return React.cloneElement(animationSet, {
            style: __assign({}, child.props.style),
            timeout: (baseTimeout || 0) + index * 100,
        }, child);
    })));
});
//# sourceMappingURL=animation-wrapper.js.map