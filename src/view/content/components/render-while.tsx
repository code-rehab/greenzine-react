import { Theme, useTheme, withStyles } from "@material-ui/core";
import { StyleRules, WithStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import * as React from "react";
import { useMediaQuery } from "react-responsive";
import { PresenterProps, withPresenter } from "../../../helpers/with-presenter";
import { RenderWhilePresenter } from "./render-while-presenter";

interface OwnProps extends WithStyles<"root"> {
  children: any;
  mobile?: boolean;
  tablet?: boolean;
  print?: boolean;
  desktop?: boolean;
}

const styles = (theme: Theme): StyleRules => ({
  root: {},
});

const RenderWhileComponent = observer((props: OwnProps & PresenterProps<RenderWhilePresenter>) => {
  const { children, mobile, tablet, print, desktop, presenter } = props;
  const theme = useTheme();
  const isPrint = presenter.isPrint;
  const isMobile = useMediaQuery({
    maxWidth: theme.breakpoints.values.md - 1,
  });
  const isTablet = useMediaQuery({
    minWidth: theme.breakpoints.values.md,
    maxWidth: theme.breakpoints.values.lg - 1,
  });

  const isDesktop = useMediaQuery({ minWidth: theme.breakpoints.values.lg });

  const isIE11 = useMediaQuery({
    query: `@media screen and (-ms-high-contrast: active), screen and (-ms-high-contrast: none)`,
  });

  if (print && isPrint) {
    return <>{children}</>;
  }

  if (mobile && isMobile && !isPrint && !isIE11) {
    return <>{children}</>;
  }

  if (tablet && isTablet && !isPrint) {
    return <>{children}</>;
  }

  if (desktop && isDesktop && !isPrint) {
    return <>{children}</>;
  }

  return null;
});

export const RenderWhile = withStyles(styles)(
  withPresenter<RenderWhilePresenter, OwnProps>(
    (props, { interactor }) => new RenderWhilePresenter(interactor.print),
    RenderWhileComponent
  )
);
