import * as React from "react";
import { WithStyles } from "@material-ui/core";
declare type CarouselItem = {
    title: string;
    content: any;
    link?: any;
    color?: string;
};
interface OwnProps extends WithStyles<"root" | "bulletWrapper" | "bullet" | "title" | "content" | "contentWrapper" | "link"> {
    data: CarouselItem[];
    color?: any;
}
export declare const Carousel: React.ComponentType<Pick<OwnProps, "color" | "data"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
