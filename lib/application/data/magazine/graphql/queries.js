import { MagazineFragment } from "./fragments";
export var Magazine = "\n  query Magazine($id:ID!) {\n    Magazine(id:$id){\n      " + MagazineFragment + "\n    }\n  }\n";
export var MagazineCollection = "\n  query MagazineCollection {\n    MagazineCollection{\n      " + MagazineFragment + "\n    }\n  }\n";
//# sourceMappingURL=queries.js.map