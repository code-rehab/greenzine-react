import { ElementType, ElementBase } from "../element";

export interface IndexElementProps {
  category: string;
}

export interface IndexElement extends ElementBase {
  component: ElementType.Index;
  props: IndexElementProps;
}
