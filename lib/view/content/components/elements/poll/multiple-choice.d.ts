import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<any> {
    options: Array<string | number | boolean>;
    answers: Array<string | number | boolean>;
    multiselect?: boolean;
    onChange?(value: Array<string | number | boolean>): void;
}
export declare const MultipleChoice: React.ComponentType<Pick<OwnProps, "onChange" | "options" | "answers" | "multiselect"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
