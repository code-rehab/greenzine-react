import { computed, observable, toJS } from "mobx";
import { Layout } from "react-grid-layout";
import { IPresenter } from "../../../../helpers/with-presenter";
import { removeNullValues } from "../../../../application/network/helpers";
import { AnyElement } from "@coderehab/greenzeen-content";
import { PrintInteractor } from "../../../../application/business/interactor/print-interactor";
import { ResizeObserver } from 'resize-observer';

export class LayoutRendererPresenter implements IPresenter {
  @observable public rowHeight: number = 12;
  @observable public margin: [number, number] = [24, 0];
  @observable public cols: number = 24;
  @observable public containerPadding: [number, number] = [0, 0];
  @observable public _layout: Layout[] = [];
  @observable private _refs: Record<string, { height: number }> = {};
  @observable private _resizeObserver: any;
  private _resizeTimeout: any;

  private _tmpRefs: Record<string, { id: string; ref: HTMLDivElement }> = {};

  constructor(
    _layout: Layout[],
    protected _elements: AnyElement[] = [],
    protected _printInteractor: PrintInteractor
  ) {
    this._layout = _layout;
  }

  public get elements(): AnyElement[] {
    return this._elements.filter((el) => {
      return this.layout.find((l) => {
        return l.i === el.id;
      });
    });
  }


  @computed public get layout(): Layout[] {
    return removeNullValues(
      toJS(this._layout || [])
        .map((l: any) => {
          const h =
            Math.ceil(
              this._refs[l.i] &&
                this._refs[l.i].height &&
                this._refs[l.i].height / (this.rowHeight + this.margin[1])
            ) || 2;

          l.h = h;
          l.minW = 2;
          l.w = l.w < 2 ? this.cols : l.w;

          if (this._printInteractor.printActive) {
            l.w = 24;
          }

          return l;
        })
        .filter((l) => {
          return !this._printInteractor.printActive || l.h > 1;
        })
    );
  }

  public mount = () => {
    this.setup();
  };

  public setupResizeObserver = () => {

    // this._resizeObserver = new ResizeObserver((data: any) => {
    //   console.log("Observed element",data[0].target.innerText,data[0].contentRect);
    //   if(data[0].contentRect.height){
    //     // data[0].target.style.backgroundColor = "red";
    //     data[0].target.style.height = data[0].contentRect.height.toFixed(2) +"px";
    //   }
    // });
    // console.log("Observer created");

  }

  public setup = () => {


   
    this.updateElementSizes();
    window.addEventListener("resize", () => {
      clearTimeout(this._resizeTimeout);
      this._resizeTimeout = setTimeout(this.updateElementSizes, 1000);
    });
  };

  public unmount = () => {
    //
  };

  protected updateElementSizes = async () => {
    await timeout(300);
    console.log('Update Sizes');
    const _refs: Record<string, any> = Object.keys(this._tmpRefs).reduce((refs: any, key: any) => {
      const item = this._tmpRefs[key];
      refs[item.id] = { height: (item.ref && item.ref.clientHeight) || 0 };
      return refs;
    }, {});

    this._refs = {
      ...this._refs,
      ..._refs,
    };
  };

  public registerRef = (id: string, ref: HTMLDivElement | null) => {
    if (ref) {
      this._tmpRefs[id] = { id, ref };
      // console.log("ref created");
      // if(!this._resizeObserver){
      //   this.setupResizeObserver();
      // }else{
      //   this._resizeObserver.observe(ref);
      // }
    }

  };
}

async function timeout(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
