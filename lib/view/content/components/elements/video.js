import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        flexDirection: "column",
        paddingBottom: theme.spacing(2),
        width: "100%"
    },
    videoWrapper: {
        position: "relative",
        paddingBottom: "56.25%",
        marginBottom: 15,
        height: 0,
        width: "100%",
        zIndex: 1
    },
    video: {
        marginTop: theme.spacing(2),
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        background: "#000",
        border: "none"
    },
    caption: {
        marginTop: theme.spacing(1),
        paddingLeft: theme.spacing(2),
        lineHeight: "2em",
        color: "inherit",
        position: "relative",
        "&::before": {
            content: "''",
            left: "10px",
            top: "-24px",
            zIndex: 2,
            width: 1,
            height: 36,
            backgroundColor: "currentColor",
            position: "absolute"
        }
    }
}); };
export var Video = withStyles(styles)(function (_a) {
    var classes = _a.classes, embed = _a.embed, caption = _a.caption, maxWidth = _a.maxWidth, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement("div", { className: classes.root, style: { maxWidth: maxWidth } },
        React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
            React.createElement("div", { className: classes.videoWrapper },
                React.createElement("iframe", { title: "video", src: embed, className: classes.video })),
            caption ? (React.createElement(Typography, { variant: "caption", className: classes.caption }, caption)) : (""))));
});
//# sourceMappingURL=video.js.map