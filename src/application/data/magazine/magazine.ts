import { Application } from "../../application";
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { Edition, EditionData, EditionModel } from "../edition/edition";

export interface MagazineData {
  id: string;
  slug: string;
  title: string;
  favicon: string;
  version: string;
  content: string;
  type: string;
  config: MagazineConfig;
  description: any[];
  image: string;
  logowhite: string;
  subtitle: string;
  editors: any;
  logo: string;
  editions: EditionData[];
  contactDetails: any;
  about: any;
}

export interface MagazineConfig {
  theme?: string;
  poll?: string;
  print?: string;
  gaCode?: string;
  tenant?: string;
}

export interface Magazine extends GraphQLModel<MagazineData>, MagazineData {
  editions: Edition[];
}

export interface MagazineValues {}

export class MagazineModel extends GraphQLBase<MagazineData> implements Magazine {
  @data public title = "";
  @data public favicon = "";
  @data public content = "";
  @data public type = "";
  @data public config = {};
  @data public index = "";
  @data public slug = "";
  @data public version = "";
  @data public image = "";
  @data public logowhite = "";
  @data public description = [];
  @data public subtitle = "";
  @data public logo = "";
  @data public editors = {};
  @data public contactDetails = {};
  @data public about = {};

  protected _editionsInitialized: boolean = false;
  public get editions(): Edition[] {
    const editionRecords = (this.record && this.record.editions) || [];
    const provider = Application.business.provider.edition;

    return editionRecords.map((data) => new EditionModel(data, provider)) as Edition[];
  }
}
