import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        transform: "translate(25%, -60%)",
        borderRadius: "100%",
        whiteSpace: "nowrap",
        color: "#e9550d",
        backgroundColor: "#FFC586",
        width: theme.spacing(4),
        height: theme.spacing(4),
        right: 0,
        top: 0,
    },
}); };
var EditionBallComponent = function (_a) {
    var classes = _a.classes, children = _a.children;
    return (React.createElement("div", { className: classes.root },
        React.createElement(Typography, { variant: "subtitle1", style: { margin: 0 } }, children && children)));
};
export var EditionBall = withStyles(styles)(EditionBallComponent);
//# sourceMappingURL=edition-ball.js.map