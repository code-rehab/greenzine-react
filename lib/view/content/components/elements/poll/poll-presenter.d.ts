import { RouteComponentProps } from "react-router";
import { Poll, PollQuestion } from "../../../../../application/data/poll/poll";
import { PollProvider } from "../../../../../application/data/poll/poll-provider";
import { IPresenter } from "../../../../../helpers/with-presenter";
import { BasicPagePresenter } from "../../../../pages/_page-basic-presenter";
export declare class PollPresenter extends BasicPagePresenter implements IPresenter {
    pollId: string;
    private _pollProvider;
    protected _router: RouteComponentProps;
    constructor(pollId: string, _questionIndex: number | undefined, _pollProvider: PollProvider, _router: RouteComponentProps);
    poll: Poll;
    currentValue: any;
    loading: boolean;
    dialogClosed: boolean;
    errorMessage: string | null;
    fetched: boolean;
    currentQuestionIndex: number;
    get finished(): boolean;
    get currentQuestion(): PollQuestion | undefined;
    get questionCount(): number;
    get isFinalQuestion(): boolean;
    get isFinished(): boolean;
    get state(): {
        state: {};
        finished: boolean;
    };
    mount: () => Promise<void>;
    unmount: () => void;
    initPoll: (id: string) => void;
    onSubmit: () => void;
    submitAnswer: (value: string | number | any[], id: string) => Promise<void>;
    previousQuestion: () => void;
    nextQuestion: () => void;
    updateValue: (value: any) => void;
    closeDialog: () => void;
    private _getPollState;
    private _setPollState;
}
