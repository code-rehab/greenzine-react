import { Store } from "../storage/store";
import { BaseModel, Model, RecordData } from "./base-model";
import { GraphQLProviderProps } from "./graphql-provider";
export interface GraphQLModel<TRecord extends RecordData> extends Model {
    loading: boolean;
    fetched: boolean;
    record: RecordData & Partial<TRecord>;
    isNew: boolean;
    fetch(): Promise<void>;
    save(): Promise<void>;
    delete(): Promise<void>;
    serialize(data?: TRecord): TRecord;
    provider: GraphQLProviderProps<GraphQLModel<TRecord>, TRecord>;
    updateProperty(key: keyof TRecord, value: any): void;
}
declare type GraphQLProvider<TRecord extends RecordData> = GraphQLProviderProps<GraphQLModel<TRecord>, TRecord> & Store<TRecord>;
export declare class GraphQLBase<TRecord extends RecordData> extends BaseModel<TRecord> {
    private _provider;
    loading: boolean;
    fetched: boolean;
    isNew: boolean;
    get provider(): GraphQLProvider<TRecord>;
    constructor(_record: TRecord, _provider: GraphQLProviderProps<GraphQLModel<TRecord>, TRecord> | undefined);
    fetch: () => Promise<void>;
    save: () => Promise<void>;
    delete: () => Promise<void>;
    updateProperty: (key: string | keyof TRecord, value: any) => void;
}
export {};
