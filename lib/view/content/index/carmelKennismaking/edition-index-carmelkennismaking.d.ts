import { WithStyles } from "@material-ui/core";
import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
interface OwnProps extends WithStyles<any> {
    edition: EditionData;
}
export declare const EditionIndexCarmelKennismaking: React.ComponentType<Pick<OwnProps, "edition"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
