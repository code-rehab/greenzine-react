import { PollFragment } from "./fragments";
export var CreatePoll = "\nmutation CreatePoll($input:inputPoll!) {\n  createPoll(input:$input){\n    " + PollFragment + "\n  }\n}";
export var UpdatePoll = "\nmutation updatePoll($input:inputPoll!) {\n  updatePoll(input:$input) {\n    " + PollFragment + "\n  }\n}\n";
export var DeletePoll = "\nmutation DeletePoll($id:ID!) {\n  deletePoll(id:$id){\n    " + PollFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map