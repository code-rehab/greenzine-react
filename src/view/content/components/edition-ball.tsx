import * as React from "react";

import { Theme, Typography, withStyles, WithStyles } from "@material-ui/core";
import { StyleRules } from "@material-ui/core/styles";

interface OwnProps extends WithStyles<"root" | "text"> {
  children?: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    transform: "translate(25%, -60%)",
    borderRadius: "100%",
    whiteSpace: "nowrap",
    color: "#e9550d",
    backgroundColor: "#FFC586",
    width: theme.spacing(4),
    height: theme.spacing(4),
    right: 0,
    top: 0,
  },
});

const EditionBallComponent = ({ classes, children }: OwnProps) => {
  return (
    <div className={classes.root}>
      <Typography variant="subtitle1" style={{ margin: 0 }}>
        {children && children}
      </Typography>
    </div>
  );
};

export const EditionBall = withStyles(styles)(EditionBallComponent);
