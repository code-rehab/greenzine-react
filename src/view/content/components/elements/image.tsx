import * as React from "react";
import { Theme, Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { NavLink } from "react-router-dom";
import { inherits } from "util";
// import $ from 'jquery';

const styles = (theme: Theme) => ({
  root: {
    margin: 0,
    display: "flex",
    flexDirection: "column" as "column",
    paddingBottom: theme.spacing(2),
    // overflow: "hidden"
    // [theme.breakpoints.down("sm")]: {
    //   margin: "auto",
    //   width: "50%"
    // }
  },
  imgHover: {
    transition: theme.transitions.create(["transform", "background"], {
      duration: "0.3s",
    }),
    "&:hover img": {
      transform: "translateY(-15px)",
    },
  },
  image: {
    width: "100%",
    flexShrink: 0,
    flexGrow: 0,
    overflow: "hidden",
    msInterpolationMode: "bicubic",
    transition: theme.transitions.create(["transform", "background"], {
      duration: "0.3s",
    }),
  },
  caption: {
    display: "block",
    marginLeft: theme.spacing(2),
    lineHeight: "2em",
    position: "relative" as "relative",
    color: "inherit",
    marginTop: -5,
    "&::before": {
      content: "''",
      left: -10,
      top: "-36px",
      zIndex: 2,
      width: 1,
      height: 54,
      backgroundColor: "currentColor",
      position: "absolute" as "absolute",
    },
  },
});

export const Image = withStyles(styles)((props: any) => {
  const animation = props.animation || { duration: 0, delay: 0 };

  return (
    <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
      {props.url ? (
        <figure className={props.classes.root} style={props.styles || {}}>
          {props.url.includes("https://") ||
          props.url.includes("https://") ||
          props.url.includes("mailto:") ? (
            <a href={props.url} target={props.target ? "_blank" : ""}>
              <picture>
                {props.mobilesrc && <source media="(max-width: 960px)" srcSet={props.mobilesrc} />}
                <img
                  id="image"
                  alt=""
                  onLoad={(elem: any) => {
                    window.dispatchEvent(new Event("resize"));
                  }}
                  src={props.src}
                  className={props.classes.image}
                  style={{ boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" }}
                />
              </picture>
            </a>
          ) : (
            <NavLink to={props.url} className={props.classes.imgHover}>
              <picture>
                {props.mobilesrc && <source media="(max-width: 960px)" srcSet={props.mobilesrc} />}
                <img
                  id="image"
                  alt=""
                  onLoad={(elem: any) => {
                    window.dispatchEvent(new Event("resize"));
                  }}
                  src={props.src}
                  className={props.classes.image}
                  style={{ boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" }}
                />
              </picture>
            </NavLink>
          )}
          {props.caption && (
            <Typography variant="caption" className={props.classes.caption}>
              {props.caption}
            </Typography>
          )}
        </figure>
      ) : (
        <figure className={props.classes.root} style={props.styles || {}}>
          <picture>
            {props.mobilesrc && <source media="(max-width: 960px)" srcSet={props.mobilesrc} />}

            <img
              id="image"
              alt=""
              onLoad={(elem: any) => {
                window.dispatchEvent(new Event("resize"));
              }}
              src={props.src}
              style={{ boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" }}
              className={props.classes.image}
            />
          </picture>
          {props.caption && (
            <Typography variant="caption" className={props.classes.caption}>
              {props.caption}
            </Typography>
          )}
        </figure>
      )}
    </SlideFade>
  );
});
