import { RouteComponentProps } from "react-router";
export declare type PageTransition = undefined | "slide-left" | "slide-right" | "slide-up" | "slide-down" | "cross-fade" | "fade-in-out" | "circle-grow" | "slide";
export declare type PageTransitionInterceptor = (route: RouteInfo, routerprops: RouteComponentProps) => PageTransition;
export interface RouteInfo {
    path: string;
    title: string;
    component: any;
    mainmenu?: boolean;
    disabled?: boolean;
    public?: boolean;
    redirect?: string;
    transition?: PageTransition | PageTransitionInterceptor;
}
export interface PossibleRouterParams {
    magazine?: string;
    edition?: string;
    article?: string;
    page?: string;
}
export interface RedirectionParams {
    from: string;
    to: string;
}
export declare const redirections: RedirectionParams[];
export declare const routeMap: Record<"index.editions" | "index.articles" | "article.homepage" | "article.page" | "error.404", RouteInfo>;
export declare type RouteNames = keyof typeof routeMap;
export declare const route: (name: "index.editions" | "index.articles" | "article.homepage" | "article.page" | "error.404", args?: Record<string, string | number | boolean> | undefined) => string;
export declare const routes: RouteInfo[];
export declare const checkRedirect: () => RedirectionParams;
