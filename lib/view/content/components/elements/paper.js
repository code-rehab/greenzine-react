var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { withStyles, Paper as MuiPaper } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { LayoutGrid } from "../../../pages/layout/grid/grid";
var styles = function (theme) { return ({
    root: {
        position: "relative",
        overflow: "hidden",
        padding: "30px 65px 30px 35px",
        background: "linear-gradient(-135deg, transparent 40px, white 40px)",
        margin: theme.spacing(0, 0, 3, 0),
    },
    cutout: {
        position: "absolute",
        height: "55px",
        width: "55px",
        top: 0,
        right: 0,
    },
}); };
export var Paper = withStyles(styles)(function (_a) {
    var data = _a.data, layoutConfig = _a.layoutConfig, cutoutColor = _a.cutoutColor, animation = _a.animation, paper = _a.paper, renderFor = _a.renderFor, classes = _a.classes;
    animation = animation || { duration: 0, delay: 0 };
    cutoutColor =
        "linear-gradient(-135deg, transparent 40px, " + (cutoutColor || "#ff6c7b") + " 40px)";
    if (renderFor === "print") {
        return React.createElement(LayoutGrid, { config: JSON.parse(layoutConfig || "{}"), data: data || [] });
    }
    data = data.map(function (item) {
        item.props = JSON.parse(item.props);
        return item;
    });
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement(MuiPaper, __assign({ elevation: 0, className: classes.root }, paper),
            React.createElement("div", { className: classes.cutout, style: {
                    background: cutoutColor,
                } }),
            React.createElement(LayoutGrid, { config: JSON.parse(layoutConfig || "{}"), data: data || [] }))));
});
//# sourceMappingURL=paper.js.map