import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { Page } from "../../../../application/data/page/page";
declare type OwnProps = React.PropsWithChildren<{
    page: Page;
    print?: boolean;
    style?: React.CSSProperties;
}> & WithStyles<"root" | "container">;
export declare const PageContainer: React.ComponentType<Pick<OwnProps, "style" | "children" | "page" | "print"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
