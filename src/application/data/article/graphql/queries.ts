import { ArticleFragment } from "./fragments";

export const Article = `
  query Article($id:ID!) {
    Article(id:$id){
      ${ArticleFragment}
    }
  }
`;

export const ArticleCollection = `
  query ArticleCollection {
    ArticleCollection{
      ${ArticleFragment}
    }
  }
`;
