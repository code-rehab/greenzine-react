import * as React from "react";
// import { RenderElement } from "./renderElement";
import { RenderElement, createElement, ElementType, } from "@coderehab/greenzeen-content";
export var CoverButton = function (_a) {
    var id = _a.id, index = _a.index, image = _a.image, title = _a.title, onSelect = _a.onSelect, version = _a.version;
    return (React.createElement("div", { onClick: function () { return onSelect(id); } },
        React.createElement(RenderElement, { element: createElement({
                component: ElementType.CoverButton,
                config: {
                    image: image,
                    title: title,
                    index: index.toString(),
                    style: {},
                },
            }) })));
};
//# sourceMappingURL=cover-button.js.map