import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Article, ArticleRecord, ArticleModel } from "./article";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";

export interface ArticleProvider extends GraphQLProviderProps<Article, ArticleRecord> {}

export class DefaultArticleProvider extends GraphQLProvider<Article, ArticleRecord> implements ArticleProvider {
  public model = ArticleModel;

  public listOperation = () => {
    return graphqlOperation(Query.ArticleCollection, {});
  };

  public createOperation = (article: Article) => {
    return graphqlOperation(Mutation.CreateArticle, { input: article });
  };

  public fetchOperation = (article: Article) => {
    return graphqlOperation(Query.Article, { id: article.id });
  };

  public updateOperation = (article: Article) => {
    return graphqlOperation(Mutation.UpdateArticle, {
      input: article.serialize()
    });
  };

  public deleteOperation = (article: Article) => {
    return graphqlOperation(Mutation.DeleteArticle, { id: article.id });
  };
}
