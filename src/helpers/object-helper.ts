/**
 * append an empty property nested based on dot notation
 */
export const appendProperty = (object: any, prop: string | string[], index: number = 0, value: any = null): any => {
  if (typeof prop === "string") {
    return appendProperty(object, prop.split("."), index, value);
  }
  if (prop[index]) {
    if (!prop[index + 1]) {
      object[prop[index]] = value;
    } else {
      object[prop[index]] = object[prop[index]] || {};
    }
    return appendProperty(object[prop[index]], prop, index + 1, value);
  }
};
