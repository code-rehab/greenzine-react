import * as React from "react";
import { Grid } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";
import { Element } from "../../../../../application/data/element/element";

export const LayoutColumnsDesktop = ({ data }: any) => {
  return (
    <div>
      <Grid container spacing={4} {...data.container}>
        {data.sections.map((cfg: any, i: number) => {
          return (
            <Grid key={i} item xs {...(cfg.props || {})}>
              {cfg.data.map((d: Element, index: number) => {
                return <RenderElement key={i + "-" + index} element={d} />;
              })}
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};
