import * as React from "react";
import { Theme, withStyles, Paper as MuiPaper, StyleRules } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { LayoutGrid } from "../../../pages/layout/grid/grid";

const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "relative",
    overflow: "hidden",
    padding: "30px 65px 30px 35px",
    background: "linear-gradient(-135deg, transparent 40px, white 40px)",
    margin: theme.spacing(0, 0, 3, 0),
  },
  cutout: {
    position: "absolute",
    height: "55px",
    width: "55px",
    top: 0,
    right: 0,
  },
});

export const Paper = withStyles(styles)(
  ({ data, layoutConfig, cutoutColor, animation, paper, renderFor, classes }: any) => {
    animation = animation || { duration: 0, delay: 0 };
    cutoutColor =
      "linear-gradient(-135deg, transparent 40px, " + (cutoutColor || "#ff6c7b") + " 40px)";

    if (renderFor === "print") {
      return <LayoutGrid config={JSON.parse(layoutConfig || "{}")} data={data || []} />;
    }

    data = data.map((item: any) => {
      item.props = JSON.parse(item.props);
      return item;
    });

    return (
      <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
        <MuiPaper elevation={0} className={classes.root} {...paper}>
          <div
            className={classes.cutout}
            style={{
              background: cutoutColor,
            }}
          ></div>
          <LayoutGrid config={JSON.parse(layoutConfig || "{}")} data={data || []} />
        </MuiPaper>
      </SlideFade>
    );
  }
);
