var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { Grid } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";
export var LayoutColumnsMobile = function (_a) {
    var data = _a.data;
    return (React.createElement(Grid, { container: true, spacing: data.container.mobileSpacing || 0 }, data.sections.map(function (cfg) {
        return (React.createElement(Grid, __assign({ item: true, xs: 12 }, (cfg.props || {})), cfg.data.map(function (d) {
            return React.createElement(RenderElement, { element: d });
        })));
    })));
};
//# sourceMappingURL=mobile.js.map