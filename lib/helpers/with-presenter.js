var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { Application } from "../application/application";
export function withPresenter(presenter, Component) {
    return /** @class */ (function (_super) {
        __extends(Contract, _super);
        function Contract() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Contract.prototype.getPresenter = function () {
            if (!this._presenter) {
                this._presenter = presenter(this.props, {
                    router: Application.router,
                    business: Application.business,
                    network: Application.network,
                    provider: Application.business.provider,
                    interactor: Application.business.interactor
                });
            }
            return this._presenter;
        };
        Contract.prototype.componentDidMount = function () {
            this.getPresenter().mount();
        };
        Contract.prototype.componentWillUnmount = function () {
            this.getPresenter().unmount();
        };
        Contract.prototype.render = function () {
            return React.createElement(Component, __assign({}, this.props, { presenter: this.getPresenter() }));
        };
        return Contract;
    }(React.Component));
}
//# sourceMappingURL=with-presenter.js.map