import { MagazineFragment } from "./fragments";
export var MagazineCreated = "\nsubscription {\n  magazineCreated {\n    " + MagazineFragment + "\n  }\n}\n";
export var MagazineUpdated = "\nsubscription {\n  magazineUpdated {\n    " + MagazineFragment + "\n  }\n}\n";
export var MagazineDeleted = "\nsubscription {\n  magazineDeleted {\n    " + MagazineFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map