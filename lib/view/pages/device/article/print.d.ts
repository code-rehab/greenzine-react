import * as React from "react";
import { ArticlePresenter } from "../../article-presenter";
import { WithStyles } from "@material-ui/styles";
interface PrintContentProps extends WithStyles<"root" | "container" | "button" | "content" | "nav"> {
    presenter: ArticlePresenter;
}
export declare const ArticlePrintContent: React.ComponentType<Pick<PrintContentProps, "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
