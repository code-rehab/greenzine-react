var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core";
import { faCircle, faDotCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ArticleButton } from "../../pages/layout/bondig-edition-cover/device/article-button";
// import { faCircle } from "@fortawesome/free-solid-svg-icons"
var styles = function (theme) { return ({
    root: {
        display: "block",
        flexDirection: "column",
        // position: "fixed",
        bottom: 100,
        left: 40,
        WebkitTapHighlightColor: "#0000",
    },
    bulletWrapper: {
        display: "flex",
        flexDirection: "row",
    },
    bullet: {
        marginTop: "0.75em",
        marginRight: 5,
        fontSize: "1.5em",
    },
    contentWrapper: {
        display: "flex",
        flexDirection: "column",
    },
}); };
var CarouselComponent = /** @class */ (function (_super) {
    __extends(CarouselComponent, _super);
    function CarouselComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.interval = 3000;
        _this.index = 0;
        return _this;
    }
    CarouselComponent.prototype.next = function () {
        var data = this.props.data;
        this.index + 1 < data.length ? this.index++ : (this.index = 0);
    };
    CarouselComponent.prototype.componentDidMount = function () {
        var _this = this;
        setInterval(function () { return _this.next(); }, this.interval);
    };
    CarouselComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, data = _a.data, classes = _a.classes;
        return (React.createElement("div", { className: classes.root },
            React.createElement("div", { className: classes.contentWrapper },
                React.createElement(ArticleButton, { id: data[this.index].link, title: data[this.index].content, category: data[this.index].title })),
            React.createElement("div", { className: classes.bulletWrapper }, data.map(function (item, index) {
                return (React.createElement(FontAwesomeIcon, { key: index, color: "#fff", icon: _this.index === index ? faDotCircle : faCircle, className: classes.bullet }));
            }))));
    };
    __decorate([
        observable
    ], CarouselComponent.prototype, "index", void 0);
    CarouselComponent = __decorate([
        observer
    ], CarouselComponent);
    return CarouselComponent;
}(React.Component));
export var Carousel = withStyles(styles)(CarouselComponent);
//# sourceMappingURL=carousel.js.map