import { RouteInfo } from "../config/routes";
export declare const TransitionItem: (route: RouteInfo) => (routerprops: any) => JSX.Element;
