import * as React from "react";
import { WithStyles } from "@material-ui/core";
declare type OwnProps = {
    logo: string;
} & WithStyles<"root" | "logo">;
export declare const MSTEditionCoverLogos: React.ComponentType<Pick<OwnProps, "logo"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
