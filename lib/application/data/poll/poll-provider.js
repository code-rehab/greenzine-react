var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider } from "../../network/graphql-provider";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
import { PollModel } from "./poll";
var DefaultPollProvider = /** @class */ (function (_super) {
    __extends(DefaultPollProvider, _super);
    function DefaultPollProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = PollModel;
        _this.listOperation = function () {
            return graphqlOperation(Query.PollCollection, {});
        };
        _this.createOperation = function (poll) {
            return graphqlOperation(Mutation.CreatePoll, { input: poll.serialize() });
        };
        _this.fetchOperation = function (poll) {
            return graphqlOperation(Query.Poll, { id: poll.id });
        };
        _this.updateOperation = function (poll) {
            return graphqlOperation(Mutation.UpdatePoll, poll.serialize());
        };
        _this.deleteOperation = function (poll) {
            return graphqlOperation(Mutation.DeletePoll, { id: poll.id });
        };
        return _this;
    }
    return DefaultPollProvider;
}(GraphQLProvider));
export { DefaultPollProvider };
//# sourceMappingURL=poll-provider.js.map