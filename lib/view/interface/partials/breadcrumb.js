import * as React from "react";
import { withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { observer } from "mobx-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {
                position: "fixed",
                backgroundImage: "url('https://dg8n28xhgyfkm.cloudfront.net/carmel-koers-2025/kruimelpad2.svg')",
                backgroundSize: "cover",
                color: "#000",
                top: "10px",
                right: "0",
                padding: "12px 40px",
                fontSize: "1.4em",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontFamily: "franchise"
            },
            _a[theme.breakpoints.down("md")] = {
                display: "none",
            },
            _a),
        icon: {
            marginRight: 20,
        },
        arrow: {
            margin: "0 10px",
        },
        title: {
            fontWeight: "bold",
            textTransform: "uppercase",
            textDecoration: "underline",
        },
        article: {
            fontWeight: "bold",
            textTransform: "uppercase",
        },
        // Used by kennismaking carmel
        clean: {
            backgroundImage: "unset",
            backgroundColor: "#fff",
            fontSize: 15,
            fontWeight: "bold",
            fontFamily: "PT Sans",
            color: "#000",
            "& svg": {
                fontSize: 12,
            },
        },
    });
};
export var Breadcrumb = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, title = _a.title, article = _a.article, url = _a.url, icon = _a.icon;
    return (React.createElement(React.Fragment, null, article &&
        article.title &&
        article.title.toLowerCase() !== "cover" &&
        process.env.REACT_APP_BREADCRUMB == "true" ? (React.createElement("div", { className: classnames(process.env.REACT_APP_BREADCRUMB_STYLE == "clean" ? [classes.root, classes.clean] : [classes.root]) },
        " ",
        icon ? React.createElement(FontAwesomeIcon, { size: "2x", icon: faMapMarkerAlt, className: classes.icon }) : "",
        React.createElement(NavLink, { className: classes.title, to: "/" + (url && url.id) || "/" }, process.env.REACT_APP_BREADCRUMB_TITLE || process.env.REACT_APP_NAME),
        React.createElement("span", { className: classes.article },
            React.createElement(FontAwesomeIcon, { size: "1x", icon: faChevronRight, className: classes.arrow }),
            article && article.title),
        " ")) : ("")));
}));
//# sourceMappingURL=breadcrumb.js.map