import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { StorageRecord } from "../../storage/store";

export type PollQuestionType = "rating" | "multiselect" | "select" | "text";

export interface PollQuestion {
  id: string;
  type: PollQuestionType;
  question: string;
  answers: Array<string | number | boolean>;
  options?: Array<string | number | boolean>;
  validations?: string[];
}

export interface PollAttributes {
  id?: string;
  name?: string;
  questions?: PollQuestion[];
  pollData?: PollData;
  status?: string;
}

export interface PollData extends StorageRecord {
  id: string;
  name: string;
  attributes: PollAttributes;
  questions: PollQuestion[];
  submissions: PollSubmission[];
}

export interface PollSubmission {
  pollID: string;
  questionID: string;
  answers: string[];
  type: string;
}

export interface Poll extends GraphQLModel<PollData>, PollData {
  // addDesign(data: Design): Design;
  // removeDesign(id: string): void;
  groupedSubmissions: any;
}

export class PollModel extends GraphQLBase<PollData>
  implements GraphQLModel<PollData> {
  public typename: string = "Poll";

  @data public name = "";
  @data public questions = [];
  @data public pollData = {};
  @data public status = "";
  @data public attributes = {};
  @data public submissions: PollSubmission[] = [];

  public process() {}
}
