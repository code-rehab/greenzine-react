import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { ArticlePresenter } from "../../article-presenter";
interface OwnProps extends WithStyles<"root"> {
    presenter: ArticlePresenter;
}
export declare const ArticleMobileContent: React.ComponentType<Pick<OwnProps, "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
