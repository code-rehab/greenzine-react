import { ElementType, ElementBase } from "../element";
export interface PopupElementProps {
    content: string;
}
export interface QuoteElement extends ElementBase {
    component: ElementType.Popup;
    props: PopupElementProps;
}
