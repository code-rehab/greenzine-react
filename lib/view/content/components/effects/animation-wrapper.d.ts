import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root"> {
    children: any;
    style?: React.CSSProperties;
    animation?: keyof typeof animationComponents;
    baseTimeout?: number;
}
export declare const animationComponents: {
    fadeUp: JSX.Element;
    fadeLeft: JSX.Element;
    fadeRight: JSX.Element;
};
export declare const AnimationWrapper: React.ComponentType<Pick<OwnProps, "animation" | "style" | "children" | "baseTimeout"> & import("@material-ui/styles").StyledComponentProps<string>>;
export {};
