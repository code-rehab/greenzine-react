var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { API } from "aws-amplify";
import { computed, observable, toJS } from "mobx";
import { BasicPagePresenter } from "../../../../pages/_page-basic-presenter";
var PollPresenter = /** @class */ (function (_super) {
    __extends(PollPresenter, _super);
    function PollPresenter(pollId, _questionIndex, _pollProvider, _router) {
        var _this = _super.call(this, _router) || this;
        _this.pollId = pollId;
        _this._pollProvider = _pollProvider;
        _this._router = _router;
        _this.currentValue = null;
        _this.loading = false;
        _this.dialogClosed = false;
        _this.errorMessage = null;
        _this.fetched = false;
        _this.currentQuestionIndex = 0;
        _this.mount = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.fetched) return [3 /*break*/, 2];
                        this.loading = true;
                        return [4 /*yield*/, this.poll.fetch()];
                    case 1:
                        _a.sent();
                        this.loading = false;
                        _a.label = 2;
                    case 2:
                        this.initPoll(this.pollId);
                        return [2 /*return*/];
                }
            });
        }); };
        _this.unmount = function () {
            //
        };
        _this.initPoll = function (id) {
            var stored = _this._getPollState(id);
            var state = stored.state;
            _this.poll.questions.forEach(function (question, index) {
                var questionState = state && state[question.id];
                question.answers = (questionState && questionState.answers) || [];
            });
            _this.nextQuestion();
        };
        _this.onSubmit = function () {
            _this.errorMessage = null;
            if (_this.currentValue && _this.currentValue.length) {
                _this.submitAnswer(_this.currentValue, _this.pollId);
                _this.nextQuestion();
                _this.currentValue =
                    (_this.currentQuestion && _this.currentQuestion.answers) || null;
            }
            else {
                _this.errorMessage = "Gelieve eerst een antwoord te geven.";
            }
        };
        _this.submitAnswer = function (value, id) { return __awaiter(_this, void 0, void 0, function () {
            var question, formattedValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        question = this.currentQuestion;
                        formattedValue = [];
                        if (!question) return [3 /*break*/, 2];
                        if (typeof value === "string") {
                            formattedValue = [value];
                        }
                        else if (typeof value === "number") {
                            formattedValue = [value.toString()];
                        }
                        else if (Array.isArray(toJS(value))) {
                            formattedValue = value;
                        }
                        else {
                        }
                        question.answers = formattedValue;
                        return [4 /*yield*/, API.graphql({
                                query: "mutation submitPollAnswer($pollID:ID!, $input:inputPollQuestion){\n      submitPollAnswer(pollID:$pollID, input:$input){\n        id\n      }\n    }",
                                variables: {
                                    pollID: id,
                                    input: toJS(question)
                                }
                            })];
                    case 1:
                        _a.sent();
                        this._setPollState(id);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); };
        _this.previousQuestion = function () {
            _this.currentQuestionIndex--;
        };
        _this.nextQuestion = function () {
            var nextQuestionIndex = undefined;
            _this.poll.questions.forEach(function (question, index) {
                nextQuestionIndex =
                    nextQuestionIndex === undefined && !question.answers.length
                        ? index
                        : nextQuestionIndex;
            });
            _this.currentQuestionIndex =
                nextQuestionIndex === undefined
                    ? _this.poll.questions.length
                    : nextQuestionIndex;
        };
        _this.updateValue = function (value) {
            _this.currentValue = value;
        };
        _this.closeDialog = function () {
            _this.dialogClosed = true;
        };
        _this._setPollState = function (id) {
            window.localStorage.setItem("poll-state-" + id, JSON.stringify(_this.state));
        };
        _this.poll = _this._pollProvider.get(_this.pollId);
        if (_questionIndex) {
            _this.currentQuestionIndex = _questionIndex;
        }
        return _this;
    }
    Object.defineProperty(PollPresenter.prototype, "finished", {
        get: function () {
            var answerCount = this.poll.questions.reduce(function (count, _a) {
                var answers = _a.answers;
                return (answers || []).length ? count + 1 : count;
            }, 0);
            return answerCount >= this.poll.questions.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollPresenter.prototype, "currentQuestion", {
        get: function () {
            return this.poll.questions[this.currentQuestionIndex || 0];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollPresenter.prototype, "questionCount", {
        get: function () {
            return this.poll.questions.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollPresenter.prototype, "isFinalQuestion", {
        get: function () {
            var index = this.currentQuestionIndex;
            return index >= this.poll.questions.length + 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollPresenter.prototype, "isFinished", {
        get: function () {
            return this.finished;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollPresenter.prototype, "state", {
        get: function () {
            var state = this.poll.questions.reduce(function (result, _a) {
                var id = _a.id, answers = _a.answers;
                result[id] = { answers: answers };
                return result;
            }, {});
            return {
                state: state,
                finished: this.finished
            };
        },
        enumerable: true,
        configurable: true
    });
    PollPresenter.prototype._getPollState = function (id) {
        var status = window.localStorage.getItem("poll-state-" + id);
        return (status && JSON.parse(status)) || {};
    };
    __decorate([
        observable
    ], PollPresenter.prototype, "poll", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "currentValue", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "loading", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "dialogClosed", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "errorMessage", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "fetched", void 0);
    __decorate([
        observable
    ], PollPresenter.prototype, "currentQuestionIndex", void 0);
    __decorate([
        computed
    ], PollPresenter.prototype, "finished", null);
    __decorate([
        computed
    ], PollPresenter.prototype, "currentQuestion", null);
    __decorate([
        computed
    ], PollPresenter.prototype, "questionCount", null);
    __decorate([
        computed
    ], PollPresenter.prototype, "isFinalQuestion", null);
    __decorate([
        computed
    ], PollPresenter.prototype, "isFinished", null);
    return PollPresenter;
}(BasicPagePresenter));
export { PollPresenter };
//# sourceMappingURL=poll-presenter.js.map