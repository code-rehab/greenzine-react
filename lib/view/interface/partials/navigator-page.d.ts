import * as React from "react";
import { InterfacePresenter } from "../interface-presenter";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<"root" | "dot" | "active" | "button" | "visible" | "rotatedRight" | "rotatedLeft"> {
    presenter: InterfacePresenter;
}
export declare const PageNavigator: React.ComponentType<Pick<OwnProps, "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
