import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { MSTEdition2020CoverPresenter } from "../mst-edition-cover-presenter";
declare type OwnProps = {
    config: any;
    presenter: MSTEdition2020CoverPresenter;
} & WithStyles<"root" | "subtitle" | "title" | "highlight" | "highlightCategory" | "highlightTitle" | "highlights" | "buttonReadMore" | "coverImage" | "logo" | "info" | "content" | "backgroundShape">;
export declare const LayoutEdition2020CoverDesktop: React.ComponentType<Pick<OwnProps, "config" | "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
