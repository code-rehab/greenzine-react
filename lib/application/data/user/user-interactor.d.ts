import { BaseModule } from "../../module";
import { User } from "./user";
import { UserProvider } from "./user-provider";
export interface UserInteractor {
    currentUser: User;
    logout: () => void;
}
export declare class DefaultUserInteractor extends BaseModule<any, any, any> implements UserInteractor {
    protected userProvider: UserProvider;
    _currentUser: User | null;
    constructor(userProvider: UserProvider);
    get currentUser(): User;
    set currentUser(user: User);
    logout: () => void;
}
