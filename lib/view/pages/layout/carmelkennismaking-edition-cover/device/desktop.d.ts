import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { CarmelkennismakingEditionCoverPresenter } from "../carmelkennismaking-edition-cover-presenter";
declare type OwnProps = {
    config: any;
    presenter: CarmelkennismakingEditionCoverPresenter;
} & WithStyles<"root" | "title" | "subtitle" | "highlight" | "highlightCategory" | "highlightTitle" | "highlights" | "buttonReadMore" | "imageWrap" | "image" | "content" | "logo">;
export declare const LayoutEditionCoverDesktop: React.ComponentType<Pick<OwnProps, "config" | "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
