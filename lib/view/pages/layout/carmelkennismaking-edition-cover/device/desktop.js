import * as React from "react";
import { withStyles, Typography, Grid, Button, Hidden, } from "@material-ui/core";
import { observer } from "mobx-react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { mapEvent } from "../../../../../helpers/formatters";
import { Carousel } from "../../../../interface/partials/carousel";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g;
    return ({
        logo: (_a = {
                position: "absolute",
                left: 200,
                top: 100
            },
            _a[theme.breakpoints.down("md")] = {
                left: 20,
                top: 30,
                transformOrigin: "left",
                transform: "scale(.55)",
            },
            _a),
        root: (_b = {
                // overlay
                "&::before": {
                    content: "' '",
                    position: "absolute",
                    top: 0,
                    left: 0,
                    bottom: 0,
                    width: "100vw",
                    background: "linear-gradient(to right,#000,transparent)",
                    zIndex: 0,
                }
            },
            _b[theme.breakpoints.up("md")] = {
                width: "100%",
                minHeight: "calc(100vh - 60px)",
                padding: theme.spacing(6),
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
            },
            _b[theme.breakpoints.up("lg")] = {
                fontSize: "1.4em",
                padding: "5vw",
            },
            _b[theme.breakpoints.down("md")] = {
                // paddingBottom: 120,
                minHeight: "100vh",
                display: "flex",
                flexDirection: "column",
            },
            _b),
        content: (_c = {
                position: "absolute",
                zIndex: 999,
                top: 0,
                bottom: 0,
                left: 200,
                // poaddingLeft: 200,
                display: "flex",
                flexDirection: "column",
                alignItems: "start",
                justifyContent: "center"
            },
            _c[theme.breakpoints.up("md")] = {
                width: "65vw",
            },
            _c[theme.breakpoints.down("md")] = {
                top: -100,
                left: 20,
                fontSize: "2em",
                "& h1": {
                    fontSize: "3.4em",
                },
                "& h3": {
                    fontSize: "2em",
                },
                "& span br": {
                    display: "none",
                },
            },
            _c),
        title: {
            fontSize: "calc(1rem + 2.2em + 1vw)",
            marginBottom: theme.spacing(2),
            '@media screen and (max-height: 860px) and (max-width: 1400px)': {
                fontSize: "calc(1rem + 1.4em + 1vw)",
            },
        },
        subtitle: (_d = {
                marginBottom: theme.spacing(2)
            },
            _d[theme.breakpoints.down("md")] = {
                fontSize: 22
            },
            _d),
        buttonReadMore: {
            padding: "7px 38px",
            borderRadius: 40,
            marginTop: theme.spacing(3),
            // marginLeft: 50,
            fontFamily: "Amaranth",
            fontSize: 17,
        },
        highlights: (_e = {
                display: "inline-block",
                zIndex: 999,
                position: "fixed",
                transformOrigin: "left",
                transform: "scale(1)",
                left: 20,
                bottom: 100
            },
            _e[theme.breakpoints.up("md")] = {
                left: 60,
                bottom: 100,
                transform: "scale(1)",
            },
            _e[theme.breakpoints.up("lg")] = {
                left: 60,
                bottom: 90,
                transformOrigin: "left",
            },
            _e[theme.breakpoints.up("xl")] = {
                left: 100,
                bottom: 100,
                transformOrigin: "left",
            },
            _e['@media screen and (max-height: 860px) and (max-width: 1400px)'] = {
                bottom: 60,
            },
            _e),
        highlight: {
            padding: theme.spacing(0.5, 5, 0, 1) + " !important",
            maxWidth: 375,
            marginTop: theme.spacing(2),
            transformOrigin: "center",
            '@media screen and (max-height: 860px) and (max-width: 1400px)': {
                maxWidth: 230,
                transform: "scale(.8)"
            },
            '@media screen and (max-width: 1200px)': {
                maxWidth: 180,
                transform: "scale(.8)"
            },
            "&:before": {
                content: "''",
                position: "absolute",
                left: 0,
                top: 0,
                width: 1,
                backgroundColor: "#D8D8D8",
                height: 150,
            },
        },
        highlightCategory: {
            color: "#f0c33b",
            textTransform: "uppercase",
            marginTop: theme.spacing(-1),
            marginBottom: theme.spacing(1),
            fontFamily: "Amaranth",
            fontSize: 17,
        },
        highlightTitle: {
            fontSize: 25,
            '@media screen and (max-width: 1200px)': {
                fontSize: 20,
            },
            lineHeight: 1.25,
            textDecoration: "underline",
            fontFamily: "Amaranth",
            cursor: "pointer",
            "&:hover": {
                "& a": {
                    color: "#f0c33b",
                },
            },
            "& > a": {
                transition: "color 0.2s ease",
                color: "#fff",
            },
        },
        imageWrap: (_f = {
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            },
            _f[theme.breakpoints.down("md")] = {
                position: "fixed",
                maxWidth: "60%",
                top: "unset",
                left: "unset",
                bottom: "5%",
                right: "5%",
                width: 500,
            },
            _f),
        image: (_g = {},
            // Mobile /////////////////////
            _g[theme.breakpoints.up("xs")] = {
                marginTop: theme.spacing(10),
                width: "80%",
                marginLeft: "10%",
            },
            // Tablet portait /////////////////////
            _g[theme.breakpoints.up("sm")] = {
                marginTop: theme.spacing(10),
                width: "80%",
                marginLeft: "10%",
            },
            // tablet landscape and smaller desktop
            _g[theme.breakpoints.up("md")] = {
                width: "50%",
                marginTop: theme.spacing(0),
                marginLeft: "0%",
            },
            //larger desktop
            _g[theme.breakpoints.up("lg")] = {
                width: "40%",
                marginTop: theme.spacing(6),
                marginLeft: "0%",
            },
            _g),
    });
};
export var LayoutEditionCoverDesktop = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, config = _a.config, presenter = _a.presenter;
    var styles = config.styles || {};
    return (React.createElement("div", { style: { position: "relative", width: "100%" } },
        React.createElement("div", { className: classes.root },
            React.createElement("div", { className: classes.logo },
                React.createElement("img", { src: "/assets/images/carmel/logo.svg" })),
            React.createElement("div", { className: classes.content },
                React.createElement(SlideFade, { direction: "up", timeout: 1200 },
                    React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                        React.createElement(Typography, { className: classes.title, variant: "h1", style: styles.title || {} }, "Ruimte in verbinding")),
                    React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                        React.createElement(Typography, { className: classes.subtitle, variant: "h4", style: styles.subtitle || {} }, "Kennismaken met Carmel")),
                    React.createElement(Button, { onClick: presenter.nextArticle, variant: "contained", color: "primary", className: classes.buttonReadMore }, "Lees verder"))),
            React.createElement("div", { className: classes.highlights },
                React.createElement(Grid, { container: true, justify: "flex-start" },
                    React.createElement(Hidden, { mdUp: true },
                        React.createElement(Carousel, { color: "#f0c33b", data: config.highlightedContent })),
                    React.createElement(Hidden, { smDown: true }, (config.highlightedContent || []).map(function (content, index) { return (React.createElement(SlideFade, { key: index, direction: "up", timeout: 1400 + index * 100 },
                        React.createElement(Grid, { item: true, className: classes.highlight },
                            React.createElement(Typography, { className: classes.highlightCategory }, content.title),
                            React.createElement(Typography, { variant: "body2", className: classes.highlightTitle },
                                React.createElement("div", { onClick: mapEvent(presenter.selectArticle, content.link) }, content.content))))); })))))));
}));
//# sourceMappingURL=desktop.js.map