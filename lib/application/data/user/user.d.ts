import { RecordData } from "../../network/base-model";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
export interface UserAttributes {
    email?: string;
    username?: string;
    phone_number?: string;
    address?: string;
    birthdate?: string;
    name?: string;
    middle_name?: string;
    family_name?: string;
    gender?: string;
    given_name?: string;
    locale?: string;
    groups?: string[];
    nickname?: string;
    picture?: string;
    preferred_username?: string;
    profile?: string;
    timezone?: string;
    updated_at?: string;
    website?: string;
}
export interface UserRecord extends RecordData {
    username: string;
    attributes: UserAttributes;
    login_history: string[];
    created_at: string;
    updated_at: string;
    status: string;
}
export interface User extends GraphQLModel<UserRecord>, UserRecord {
}
export declare class UserModel extends GraphQLBase<UserRecord> implements User {
    username: string;
    created_at: string;
    updated_at: string;
    status: string;
    attributes: {};
    login_history: never[];
}
