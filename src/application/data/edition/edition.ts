import { Application } from "../../application";
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { Article, ArticleRecord } from "../article/article";
import { Magazine, MagazineData } from "../magazine/magazine";

export interface EditionData {
  id: string;
  index: string;
  public: string; 
  pollSlug: string;
  subtitle: string;
  slug: string;
  title: string;
  description: string;
  image: string;
  articles: ArticleRecord[];
  magazineId: string;
  theme?: string;
}

export interface Edition extends GraphQLModel<EditionData>, EditionData {
  articles: Article[];
}

export interface EditionValues {}

export class EditionModel extends GraphQLBase<EditionData> implements Edition {
  @data public slug: string = "";
  @data public title: string = "";
  @data public public: string = "false";
  @data public pollSlug: string = "";
  @data public index: string = "";
  @data public version: string = "";
  @data public content: string = "";
  @data public type: string = "";
  @data public description: any = "";
  @data public image: string = "";
  @data public subtitle: string = "";
  @data public editors: any = "";
  @data public coverArticle: string = "";
  @data public magazineId: string = "";

  public get articles(): Article[] {
    return (this.record.articles || []).map((data) => Application.business.provider.article.create(data));
  }
}
