import * as React from "react";
import classnames from "classnames";
import { withStyles } from "@material-ui/styles";
import { Hidden, Button } from "@material-ui/core";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTh, faPrint, faTimes, faNewspaper, } from "@fortawesome/free-solid-svg-icons";
import { observer } from "mobx-react";
import { route } from "../../../config/routes";
import { ArticleNavigator } from "./navigator-article";
var styles = function (theme) {
    var _a, _b, _c, _d;
    return ({
        root: {
            fontFamily: "Montserrat",
            fontWeight: 500,
            position: "fixed",
            bottom: 0,
            background: "#fff",
            color: "#4A4A4A",
            display: "flex",
            justifyContent: "space-between",
            borderTop: "1px solid #E7E7E7",
            pointerEvents: "all",
            zIndex: 1400,
        },
        menuLeft: (_a = {
                display: "inline-flex",
                position: "absolute",
                top: 0,
                right: "auto",
                height: "100%"
            },
            _a[theme.breakpoints.down("sm")] = {
                position: "static",
            },
            _a),
        logo: (_b = {
                width: 90,
                height: 28,
                marginTop: 2
            },
            _b[theme.breakpoints.down("sm")] = {
                maxWidth: 60,
            },
            _b),
        top: { top: 0, left: 0, right: 0, height: theme.spacing(5) },
        bottom: { bottom: 0, left: 0, right: 0, height: theme.spacing(5) },
        left: { top: 0, bottom: 0, left: 0, width: theme.spacing(5) },
        right: { top: 0, bottom: 0, right: 0, width: theme.spacing(5) },
        editionBtn: (_c = {
                fontSize: "12px",
                fontWeight: 500,
                color: "#4A4A4A",
                padding: theme.spacing(2),
                height: "auto",
                display: "flex",
                alignItems: "center",
                textDecoration: "none",
                borderRight: "1px solid #E7E7E7",
                borderLeft: "1px solid #E7E7E7",
                marginLeft: 0,
                transition: "background .3s"
            },
            _c[theme.breakpoints.down("sm")] = {
                border: "none",
            },
            _c["&:hover"] = {
                backgroundColor: "#eee",
            },
            _c["&.active"] = {},
            _c["&:last-of-type"] = {
                borderLeft: "none",
            },
            _c),
        tooltip: {
            fontSize: 13,
            fontFamily: "Arial",
        },
        link: (_d = {
                fontSize: "12px",
                fontWeight: 500,
                color: "#4A4A4A",
                padding: theme.spacing(2),
                height: "auto",
                display: "flex",
                alignItems: "center",
                textDecoration: "none",
                borderRight: "1px solid #E7E7E7",
                borderLeft: "1px solid #E7E7E7",
                marginLeft: 0,
                transition: "background .3s"
            },
            _d[theme.breakpoints.down("sm")] = {
                border: "none",
            },
            _d["&:hover"] = {
                backgroundColor: "#eee",
            },
            _d["&.active"] = {},
            _d["&:last-of-type"] = {
                borderLeft: "none",
            },
            _d),
    });
};
var ButtonArticleIndex = function (_a) {
    var className = _a.className, magazine = _a.magazine, edition = _a.edition;
    return (React.createElement(NavLink, { className: className, to: route("index.articles", {
            magazine: magazine || "",
            edition: edition || "",
        }) },
        React.createElement(FontAwesomeIcon, { icon: faTh, size: "2x" }),
        React.createElement(Hidden, { mdDown: true },
            React.createElement("p", { style: { marginLeft: "14px" } }, "inhoudsopgave"))));
};
export var Navigator = withStyles(styles)(observer(function (_a) {
    var presenter = _a.presenter, classes = _a.classes, location = _a.location;
    var magazine = presenter.magazine, edition = presenter.edition, article = presenter.article, articles = presenter.articles, toNextArticle = presenter.toNextArticle, toPreviousArticle = presenter.toPreviousArticle, 
    // isSingleMagazine,
    isPrint = presenter.isPrint;
    return (React.createElement("div", { className: classnames(classes.root, classes[location || "bottom"]) },
        React.createElement("div", { className: classes.menuLeft },
            magazine && (React.createElement(NavLink, { to: route("index.editions", {
                    magazine: magazine.id || "",
                }), className: classes.link },
                React.createElement("img", { alt: magazine.title, src: process.env.REACT_APP_NAVIGATOR_LOGO, className: classes.logo }))),
            edition && magazine && magazine.editions.length > 1 && (React.createElement(NavLink, { className: classes.editionBtn, to: route("index.editions", {
                    magazine: magazine.id || "",
                }) },
                React.createElement(Hidden, { mdDown: true },
                    React.createElement("p", null, "andere edities")),
                React.createElement(Hidden, { lgUp: true, smDown: true },
                    React.createElement(FontAwesomeIcon, { icon: faNewspaper, size: "2x" })))),
            magazine && edition && article && (React.createElement(Hidden, { smDown: true },
                React.createElement(ButtonArticleIndex, { className: classes.link, magazine: magazine.id, edition: edition.id })))),
        article && (React.createElement(ArticleNavigator, { list: (articles || []).map(function (article) { return article.id; }), current: article.id, onNext: toNextArticle, onPrevious: toPreviousArticle })),
        magazine && edition && article && (React.createElement(Hidden, { mdUp: true },
            React.createElement(ButtonArticleIndex, { className: classes.link, magazine: magazine.id, edition: edition.id }))),
        React.createElement("div", { style: {
                position: "absolute",
                display: process.env.REACT_APP_PRINTMODE == "true" ? "flex" : "none",
                alignItems: "center",
                right: 0,
                padding: 0,
                height: "100%",
                overflowY: "hidden",
            } }, magazine && edition && article && (React.createElement(Hidden, { mdDown: true },
            React.createElement("div", null,
                React.createElement(Button, { className: classes.link, onClick: presenter.printArticle },
                    React.createElement(FontAwesomeIcon, { size: "2x", icon: isPrint ? faTimes : faPrint }))))))));
}));
//# sourceMappingURL=navigator.js.map