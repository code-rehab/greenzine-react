import { extendObservable } from "mobx";

export function data(target: any, key: string) {
  Object.defineProperty(target, key, {
    set(firstValue: any) {
      extendObservable(
        this,
        {
          get [key](this: any) {
            return this.changes[key] || (this.record && this.record[key]) || firstValue;
          },
          set [key](value: any) {
            this.changes = { ...this.changes, [key]: value };
          },
        },
        {}
      );
    },
  });
}
