import * as React from "react";
import { withStyles } from "@material-ui/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { Typography } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        marginBottom: "1em"
    },
    contactLink: {
        marginRight: "auto",
        marginLeft: theme.spacing(2.4),
        display: "flex",
        flexDirection: "column"
    },
    faIcon: {
        fontSize: theme.spacing(1.5),
        width: theme.spacing(2)
    }
}); };
var LocationInfoComponent = function (_a) {
    var classes = _a.classes, role = _a.role, address = _a.address, zipcode = _a.zipcode, city = _a.city, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement("div", { className: classes.root },
            React.createElement("div", null,
                React.createElement(FontAwesomeIcon, { className: classes.faIcon, icon: faMapMarkerAlt, style: { margin: "5px 0 0 0" } })),
            React.createElement("div", { className: classes.contactLink },
                React.createElement(Typography, null,
                    React.createElement("small", null, role)),
                React.createElement(Typography, null,
                    React.createElement("small", null, address)),
                React.createElement(Typography, null,
                    React.createElement("small", null,
                        zipcode,
                        " ",
                        city))))));
};
export var LocationInfo = withStyles(styles)(LocationInfoComponent);
//# sourceMappingURL=location-info.js.map