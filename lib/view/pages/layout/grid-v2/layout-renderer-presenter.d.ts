import { Layout } from "react-grid-layout";
import { IPresenter } from "../../../../helpers/with-presenter";
import { AnyElement } from "@coderehab/greenzeen-content";
import { PrintInteractor } from "../../../../application/business/interactor/print-interactor";
export declare class LayoutRendererPresenter implements IPresenter {
    protected _elements: AnyElement[];
    protected _printInteractor: PrintInteractor;
    rowHeight: number;
    margin: [number, number];
    cols: number;
    containerPadding: [number, number];
    _layout: Layout[];
    private _refs;
    private _resizeObserver;
    private _resizeTimeout;
    private _tmpRefs;
    constructor(_layout: Layout[], _elements: AnyElement[], _printInteractor: PrintInteractor);
    get elements(): AnyElement[];
    get layout(): Layout[];
    mount: () => void;
    setupResizeObserver: () => void;
    setup: () => void;
    unmount: () => void;
    protected updateElementSizes: () => Promise<void>;
    registerRef: (id: string, ref: HTMLDivElement | null) => void;
}
