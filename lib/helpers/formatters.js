export function mapEvent(handler) {
    var params = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        params[_i - 1] = arguments[_i];
    }
    return handler
        ? function (e) {
            return handler.apply(void 0, params.map(function (param) {
                return param && typeof param === "function" ? param(e) : param;
            }));
        }
        : undefined;
}
export function nodeValue(e) {
    return e && e.target && (e.target.nodeValue || e.target.value);
}
//# sourceMappingURL=formatters.js.map