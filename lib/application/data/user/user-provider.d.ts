import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { User, UserModel, UserRecord } from "./user";
export interface UserProvider extends GraphQLProviderProps<User, UserRecord> {
    fetchAuthUser(): Promise<User>;
}
export declare class DefaultUserProvider extends GraphQLProvider<User, UserRecord> implements UserProvider {
    model: typeof UserModel;
    fetchAuthUser(): Promise<User>;
    protected listOperation: () => {
        query: any;
        variables: {};
    };
    protected createOperation: (user: User) => {
        query: any;
        variables: {};
    };
    protected fetchOperation: (user: User) => {
        query: any;
        variables: {};
    };
    protected updateOperation: (user: User) => {
        query: any;
        variables: {};
    };
    protected deleteOperation: (user: User) => {
        query: any;
        variables: {};
    };
}
