import { PollFragment } from "./fragments";

export const Poll = `
query Poll($id:ID!) {
  Poll(id:$id) {
    ${PollFragment}
  }
}`;

export const PollCollection = `
query PollCollection {
  PollCollection {
    ${PollFragment}
  }
}`;
