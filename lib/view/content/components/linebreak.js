import * as React from "react";
import { withStyles, Hidden, Typography } from "@material-ui/core";
import { RenderWhile } from "./render-while";
var styles = function (theme) { return ({
    root: {}
}); };
var LineBreakComponent = function (_a) {
    var classes = _a.classes, gutterBottom = _a.gutterBottom;
    return (React.createElement(Hidden, { smDown: true },
        React.createElement(RenderWhile, { mobile: true, desktop: true },
            React.createElement("br", null),
            gutterBottom && React.createElement(Typography, { variant: "h5", gutterBottom: true }))));
};
export var BR = withStyles(styles)(LineBreakComponent);
//# sourceMappingURL=linebreak.js.map