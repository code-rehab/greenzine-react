import { BaseSyntheticEvent } from "react";
export declare function mapEvent<T extends (...args: any[]) => any>(handler?: T, ...params: Parameters<T>): ((e: BaseSyntheticEvent<object, any, any>) => any) | undefined;
export declare function nodeValue(e: BaseSyntheticEvent): string | null;
