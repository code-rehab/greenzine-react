import * as React from "react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { withStyles, Grid } from "@material-ui/core";
import { observer } from "mobx-react";
var styles = function (theme) {
    var _a, _b, _c, _d;
    return ({
        root: (_a = {
                display: "flex",
                justifyContent: "space-between",
                alignItems: "flex-start"
            },
            _a[theme.breakpoints.down("sm")] = {
                padding: theme.spacing(4, 0),
            },
            _a),
        logo: (_b = {
                display: "inline-block",
                "@media all and (-ms-high-contrast:none)": {
                    width: 600,
                },
                "& img": (_c = {
                        marginBottom: 20
                    },
                    _c[theme.breakpoints.down("sm")] = {
                        width: 120,
                        paddingRight: 25,
                    },
                    _c)
            },
            _b[theme.breakpoints.down("sm")] = {
                maxWidth: "160px",
            },
            _b),
        logo2: (_d = {},
            _d[theme.breakpoints.down("md")] = {
                width: "15vw",
            },
            _d),
    });
};
export var MSTEdition2020CoverLogos = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, logo = _a.logo;
    return (React.createElement("header", { className: classes.root },
        React.createElement(SlideFade, { direction: "up", timeout: 900 },
            React.createElement("div", { className: classes.logo },
                React.createElement(Grid, { container: true, spacing: 0, alignItems: "center" },
                    React.createElement(Grid, { item: true, sm: true, style: { position: "relative" } },
                        React.createElement("img", { src: logo || "", alt: "MST" })),
                    React.createElement(Grid, { item: true, sm: true }))))));
}));
//# sourceMappingURL=logos.js.map