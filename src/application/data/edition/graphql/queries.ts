import { EditionFragment } from "./fragments";

export const Edition = `
  query Edition($id:ID!) {
    Edition(id:$id){
      ${EditionFragment}
    }
  }
`;

export const EditionCollection = `
  query EditionCollection($magazine:ID!) {
    Magazine(id:$magazine) {
      editions{
      ${EditionFragment}
      }
    }
  }
`;
