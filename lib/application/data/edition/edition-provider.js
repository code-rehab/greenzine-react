var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider } from "../../network/graphql-provider";
import { EditionModel } from "./edition";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
var DefaultEditionProvider = /** @class */ (function (_super) {
    __extends(DefaultEditionProvider, _super);
    function DefaultEditionProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = EditionModel;
        _this.listOperation = function (variables) {
            if (variables === void 0) { variables = {}; }
            return graphqlOperation(Query.EditionCollection, variables);
        };
        _this.createOperation = function (edition) {
            return graphqlOperation(Mutation.CreateEdition, { input: edition });
        };
        _this.fetchOperation = function (edition) {
            return graphqlOperation(Query.Edition, {
                id: edition.id,
            });
        };
        _this.updateOperation = function (edition) {
            return graphqlOperation(Mutation.UpdateEdition, { input: edition });
        };
        _this.deleteOperation = function (edition) {
            return graphqlOperation(Mutation.DeleteEdition, { id: edition.id });
        };
        return _this;
    }
    return DefaultEditionProvider;
}(GraphQLProvider));
export { DefaultEditionProvider };
//# sourceMappingURL=edition-provider.js.map