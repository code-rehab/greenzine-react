import * as React from "react";
interface OwnProps {
    articles: Array<{
        link: string;
        content: string;
        title: string;
    }>;
    classes?: Record<"root" | "articleWrapper", string>;
}
export declare const EditionHighlights: React.FC<OwnProps>;
export {};
