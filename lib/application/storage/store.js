var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed, observable } from "mobx";
export var JSONSerializer = {
    serialize: function (data) { return JSON.stringify(data); },
    deserialize: function (data) { return (typeof data === "string" ? JSON.parse(data || "{}") : data); },
};
var DefaultStore = /** @class */ (function () {
    function DefaultStore() {
        var _this = this;
        this._records = {};
        this.emptyStorage = function () {
            _this._records = {};
        };
        this.updateRecord = function (id, record) {
            return _this.setRecord(id, __assign(__assign({}, _this.getRecord(id)), record));
        };
        this.deleteRecord = function (id) {
            var item = _this.getRecord(id);
            if (item) {
                delete _this._records[id];
            }
            return item;
        };
        // protected _serializeRecord: RecordSerializeFunction<T, TData> = instance => {
        //   const data: any = toJS(instance.data);
        //   Object.keys(this.serializers).forEach(key => {
        //     if (data[key]) {
        //       data[key] = this.serializers[key].serialize(data[key]);
        //     }
        //   });
        //   return { ...data, id: instance.id };
        // };
        //=======================================
        // Function mapping
        //=======================================
    }
    Object.defineProperty(DefaultStore.prototype, "allRecords", {
        get: function () {
            var _this = this;
            return Object.keys(this._records).map(function (key) { return _this._records[key]; });
        },
        enumerable: true,
        configurable: true
    });
    //=======================================
    // Core functions
    //=======================================
    DefaultStore.prototype.getRecord = function (id) {
        return this._records[id];
    };
    DefaultStore.prototype.setRecord = function (id, data) {
        this._records[id] = data;
        return this._records[id];
    };
    DefaultStore.prototype.injectList = function (list) {
        var _this = this;
        this._records = __assign(__assign({}, this._records), list.reduce(function (result, data) {
            result[data.id] = __assign(__assign({}, (_this._records[data.id] || {})), data);
            return result;
        }, {}));
    };
    __decorate([
        observable
    ], DefaultStore.prototype, "_records", void 0);
    __decorate([
        computed
    ], DefaultStore.prototype, "allRecords", null);
    return DefaultStore;
}());
export { DefaultStore };
//# sourceMappingURL=store.js.map