import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Edition, EditionData, EditionModel } from "./edition";
export interface EditionProvider extends GraphQLProviderProps<Edition, EditionData> {
}
export declare class DefaultEditionProvider extends GraphQLProvider<Edition, EditionData> implements EditionProvider {
    model: typeof EditionModel;
    protected listOperation: (variables?: Record<string, any>) => {
        query: any;
        variables: {};
    };
    protected createOperation: (edition: EditionData) => {
        query: any;
        variables: {};
    };
    protected fetchOperation: (edition: EditionData) => {
        query: any;
        variables: {};
    };
    protected updateOperation: (edition: EditionData) => {
        query: any;
        variables: {};
    };
    protected deleteOperation: (edition: EditionData) => {
        query: any;
        variables: {};
    };
}
