import * as React from "react";
import { Layout } from "react-grid-layout";
import { PresenterProps } from "../../../../helpers/with-presenter";
import { LayoutRendererPresenter } from "./layout-renderer-presenter";
import { AnyElement } from "@coderehab/greenzeen-content";
interface OwnProps {
    elements: AnyElement[];
    layout: Layout[];
    print?: boolean;
}
export declare const RendererComponent: ({ presenter, print }: OwnProps & PresenterProps<LayoutRendererPresenter>) => JSX.Element;
export declare const LayoutRenderer: React.ComponentClass<OwnProps, any>;
export {};
