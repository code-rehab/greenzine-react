import * as React from "react";
import { withPresenter } from "../../../../helpers/with-presenter";
import { BondigEditionCoverPresenter } from "./bondig-edition-cover-presenter";
import { LayoutEditionCoverDesktop } from "./device/desktop";
var Component = function (_a) {
    var config = _a.config, presenter = _a.presenter;
    return React.createElement(LayoutEditionCoverDesktop, { config: config, presenter: presenter });
};
export var LayoutBondigEditionCover = withPresenter(function (_props, _a) {
    var interactor = _a.interactor;
    return new BondigEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print);
}, Component);
//# sourceMappingURL=bondig-edition-cover.js.map