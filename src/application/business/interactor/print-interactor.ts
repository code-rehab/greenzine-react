import { observable } from "mobx";

export class PrintInteractor {
  @observable public printActive: boolean = false;
}
