import { ArticleFragment } from "./fragments";
export var ArticleCreated = "\nsubscription {\n  magazineCreated {\n    " + ArticleFragment + "\n  }\n}\n";
export var ArticleUpdated = "\nsubscription {\n  magazineUpdated {\n    " + ArticleFragment + "\n  }\n}\n";
export var ArticleDeleted = "\nsubscription {\n  magazineDeleted {\n    " + ArticleFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map