import { ElementType, ElementBase } from "../element";

export interface DividerElementProps {
  style?: any;
}

export interface DividerElement extends ElementBase {
  component: ElementType.Divider;
  props: DividerElementProps;
}
