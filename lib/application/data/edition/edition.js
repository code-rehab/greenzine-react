var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Application } from "../../application";
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase } from "../../network/graphql-model";
var EditionModel = /** @class */ (function (_super) {
    __extends(EditionModel, _super);
    function EditionModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.slug = "";
        _this.title = "";
        _this.public = "false";
        _this.pollSlug = "";
        _this.index = "";
        _this.version = "";
        _this.content = "";
        _this.type = "";
        _this.description = "";
        _this.image = "";
        _this.subtitle = "";
        _this.editors = "";
        _this.coverArticle = "";
        _this.magazineId = "";
        return _this;
    }
    Object.defineProperty(EditionModel.prototype, "articles", {
        get: function () {
            return (this.record.articles || []).map(function (data) { return Application.business.provider.article.create(data); });
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        data
    ], EditionModel.prototype, "slug", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "title", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "public", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "pollSlug", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "index", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "version", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "content", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "type", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "description", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "image", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "subtitle", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "editors", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "coverArticle", void 0);
    __decorate([
        data
    ], EditionModel.prototype, "magazineId", void 0);
    return EditionModel;
}(GraphQLBase));
export { EditionModel };
//# sourceMappingURL=edition.js.map