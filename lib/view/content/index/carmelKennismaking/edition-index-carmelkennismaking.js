import { Button, Collapse, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "auto",
            marginTop: theme.spacing(10),
        },
        drawerBackdrop: {},
        header: {},
        content: {},
        backdrop: {
            backgroundColor: "transparant",
        },
        title: (_a = {
                color: "#fff",
                margin: "20px 0",
                fontFamily: "Amaranth",
                lineHeight: 0.9,
                fontSize: 42,
                "& br": {
                    display: "none",
                }
            },
            _a[theme.breakpoints.down("sm")] = {
                marginTop: 50,
                fontSize: "2.6em",
            },
            _a),
        subtitle: {
            fontWeight: 700,
            margin: "20px 0",
            color: "#4D4D4F",
        },
        logo: {
            marginTop: theme.spacing(4),
        },
        description: {
            // fontSize: 17,
            paddingRight: theme.spacing(1),
        },
        collapseContainer: {
            marginBottom: theme.spacing(3),
            position: "relative",
            "&:after": {
                content: "''",
                display: "block",
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
            },
            "&.MuiCollapse-entered": {
                "&:after": {
                    boxShadow: "none",
                },
            },
        },
        contentPiece: {
            fontFamily: "Montserrat",
        },
        marginBottom: {
            marginBottom: "1em",
        },
    });
};
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement("div", { className: classes.root },
        React.createElement(Typography, { variant: "h2", className: classes.title }, "Een kennismaking met Carmel"),
        React.createElement(Typography, { className: classes.description, paragraph: true }, "Met trots kan ik zeggen dat ik bij Carmel werk. Werken bij Carmel betekent dat jij, als uniek persoon, deel uitmaakt van de Carmelgemeenschap. Maak in dit kennismakingskatern een reis door onze geschiedenis en verken onze waarden. Het nodigt uit tot gesprek. Ontdek hoe jij met jouw bijdrage Carmel verrijkt."),
        React.createElement(Typography, { className: classes.description, paragraph: true },
            "Karin van Oort",
            React.createElement("br", null),
            "(voorzitter College van Bestuur)"),
        React.createElement("div", { className: classes.logo },
            React.createElement("img", { src: "/assets/images/carmel/logo-diap.svg" }))));
};
var EditionIndexCarmelKennismakingComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, tablet: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })),
            React.createElement(RenderWhile, { mobile: true },
                React.createElement(Collapse, { in: open, collapsedHeight: 110, classes: { wrapper: classes.collapseContainer } },
                    React.createElement(Info, { edition: edition, classes: classes })),
                React.createElement(Button, { color: "primary", variant: "contained", style: { display: "flex", marginLeft: "auto" }, onClick: function () { return setOpen(!open); } }, open ? "Verberg" : "Lees meer")))));
};
export var EditionIndexCarmelKennismaking = withStyles(styles)(EditionIndexCarmelKennismakingComponent);
//# sourceMappingURL=edition-index-carmelkennismaking.js.map