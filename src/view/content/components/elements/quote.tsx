import { faQuoteLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { StyleRules, Theme, Typography, withStyles } from "@material-ui/core";
import classNames from "classnames";
import * as React from "react";
import { SlideFade } from "../effects/slide-fade";
import color from "@material-ui/core/colors/amber";

const styles = (theme: Theme): StyleRules => ({
  root: {
    borderTop: "5px solid",
    paddingTop: 10,
    marginBottom: theme.spacing(3),
  },
  quote: {
    color: "inherit",
    fontFamily: "Domaine",
    fontStyle: "italic",
  },
  quoteSymbol: {
    fontWeight: 500,
    position: "relative",
    fontSize: "5em",
    marginBottom: -15,
    [theme.breakpoints.up("md")]: {
      fontSize: "2em",
      bottom: -50,
      marginTop: -50,
      marginBottom: 30,
    },

    color: "inherit",
  },
  symbol: {
    marginTop: theme.spacing(2),
    width: 20,
    height: 20,
  },
  author: {
    marginTop: theme.spacing(1),
  },
  print: {
    borderColor: "#000 !important",
    padding: 0,
  },
  mobile: {
    marginBottom: theme.spacing(3),
  },

  mstVersion: {
    fontSize: "calc(1rem + 0.4em)",
    fontFamily: "Museo",
    fontWeight: 700,
    lineHeight: 1.3,
    letterSpacing: "0.02em",
    textTransform: "unset",
  },
});

export const Quote = withStyles(styles)(
  ({ classes, author, content, color, renderFor, animation, customStyle, dash }: any) => {
    animation = animation || { duration: 0, delay: 0 };

    return (
      <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
        {/* MST VERSION, TODO: [GM-369] design quote options */}
        {process.env.REACT_APP_CLIENT_THEME == "mst" ? (
          <div className={classNames(classes.root, classes[renderFor])} style={{ color: color }}>
            <Typography variant="h4" style={{ fontSize: "2em", marginBottom: -15 }}>
              “
            </Typography>
            <Typography
              variant="h4"
              className={classes.mstVersion}
              gutterBottom
              style={{ ...customStyle }}
            >
              {content}
            </Typography>
            {author ? (
              <Typography variant="body2" className={classes.author} style={{ ...customStyle }}>
                {dash ? "-" : ""} {author}
              </Typography>
            ) : (
              ""
            )}
          </div>
        ) : (
          <div className={classNames(classes.root, classes[renderFor])} style={{ color: color }}>
            {/* <FontAwesomeIcon icon={faQuoteLeft} color="inherit" /> */}
            <Typography
              variant="h4"
              className={classes.quoteSymbol}
              gutterBottom
              style={{ ...customStyle }}
            >
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="quote-left"
                className="svg-inline--fa fa-quote-left fa-w-16 "
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                color="inherit"
                style={{ width: ".5em", verticalAlign: "-0.125em", marginBottom: 15 }}
              >
                <path
                  fill="currentColor"
                  d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"
                ></path>
              </svg>
            </Typography>
            <Typography
              variant="h4"
              className={classes.quote}
              gutterBottom
              style={{ ...customStyle }}
            >
              {content}
            </Typography>
            {author ? (
              <Typography variant="body2" className={classes.author} style={{ ...customStyle }}>
                {dash ? "-" : ""} {author}
              </Typography>
            ) : (
              ""
            )}
          </div>
        )}
      </SlideFade>
    );
  }
);
