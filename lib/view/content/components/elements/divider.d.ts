import * as React from "react";
export declare const Divider: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<"line">>;
