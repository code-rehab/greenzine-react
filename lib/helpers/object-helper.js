/**
 * append an empty property nested based on dot notation
 */
export var appendProperty = function (object, prop, index, value) {
    if (index === void 0) { index = 0; }
    if (value === void 0) { value = null; }
    if (typeof prop === "string") {
        return appendProperty(object, prop.split("."), index, value);
    }
    if (prop[index]) {
        if (!prop[index + 1]) {
            object[prop[index]] = value;
        }
        else {
            object[prop[index]] = object[prop[index]] || {};
        }
        return appendProperty(object[prop[index]], prop, index + 1, value);
    }
};
//# sourceMappingURL=object-helper.js.map