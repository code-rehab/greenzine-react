var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link as MuiLink, List as MuiList, ListItem, ListItemIcon, ListItemText, } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import classNames from "classnames";
import * as React from "react";
import { Link } from "react-router-dom";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) {
    return {
        root: {
            margin: "0 0 36px 0",
        },
        item: {
            borderBottom: "1px solid",
            "& *": {
                color: "inherit",
            },
        },
        big: {
            "& $primary": {
                fontFamily: "Montserrat",
                fontWeight: 700,
            },
            "& $secondary": {
                fontFamily: "Montserrat",
                fontWeight: 400,
            },
        },
        primary: {
            lineHeight: 1,
        },
        secondary: {},
    };
};
export var List = withStyles(styles)(function (_a) {
    var classes = _a.classes, animation = _a.animation, items = _a.items, variant = _a.variant;
    animation = animation || { duration: 0, delay: 0 };
    items = items || [];
    return (React.createElement(MuiList, { className: classes.root }, items.map(function (item, index) {
        var href = item.href || "";
        var props = {};
        if (variant !== "big") {
            props.dense = true;
        }
        if (href) {
            props.button = true;
        }
        return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay + index * 75 },
            React.createElement("div", null,
                React.createElement(ListItem, __assign({ component: href.startsWith("/") ? Link : MuiLink, href: href, to: href, target: href.startsWith("/") ? undefined : "_blank", className: classNames(classes.item, classes[variant]) }, props),
                    item.icon && (React.createElement(ListItemIcon, null, (item.icon.icon && React.createElement(FontAwesomeIcon, { icon: item.icon })) || item.icon)),
                    variant !== "big" ? (variant === "small" ? (
                    //small
                    React.createElement(ListItemText, { primary: item.primary, secondary: item.secondary })) : (
                    //default
                    React.createElement(ListItemText, { primary: item.primary, secondary: item.secondary }))) : (
                    //big
                    React.createElement(ListItemText, { primary: item.primary, secondary: item.secondary, classes: {
                            primary: classes.primary,
                            secondary: classes.secondary,
                        } })),
                    React.createElement(FontAwesomeIcon, { icon: faChevronRight })))));
    })));
});
//# sourceMappingURL=list.js.map