var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider } from "../../network/graphql-provider";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
import { MagazineModel } from "./magazine";
var DefaultMagazineProvider = /** @class */ (function (_super) {
    __extends(DefaultMagazineProvider, _super);
    function DefaultMagazineProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = MagazineModel;
        _this.listOperation = function () {
            return graphqlOperation(Query.MagazineCollection, {});
        };
        _this.createOperation = function (magazine) {
            return graphqlOperation(Mutation.CreateMagazine, { input: magazine });
        };
        _this.fetchOperation = function (magazine) {
            return graphqlOperation(Query.Magazine, { id: magazine.id });
        };
        _this.updateOperation = function (magazine) {
            return graphqlOperation(Mutation.UpdateMagazine, {
                input: {
                    id: magazine.id,
                    title: magazine.title,
                    description: magazine.description,
                    config: magazine.config
                }
            });
        };
        _this.deleteOperation = function (magazine) {
            return graphqlOperation(Mutation.DeleteMagazine, { id: magazine.id });
        };
        return _this;
    }
    return DefaultMagazineProvider;
}(GraphQLProvider));
export { DefaultMagazineProvider };
//# sourceMappingURL=magazine-provider.js.map