import { makeStyles, Typography } from "@material-ui/core";
import * as React from "react";
import { useArticleInteractor } from "../../../../hooks/interactors";
var useStyles = makeStyles(function (theme) {
    var _a;
    return ({
        root: (_a = {
                cursor: "pointer"
            },
            _a[theme.breakpoints.up("md")] = {
                maxWidth: 320
            },
            _a[theme.breakpoints.up("lg")] = {
                maxWidth: 270,
            },
            _a),
        title: {
            textDecoration: "underline",
            lineHeight: "1.33",
            fontSize: "calc(14px + 0.2vw)",
        },
        category: {
            color: "#FFC583",
            marginTop: "-6px",
            fontFamily: "Montserrat",
            fontWeight: 600,
            marginBottom: 12,
            textTransform: "uppercase",
            fontSize: "calc(14px + 0.2vw)",
            lineHeight: "1.33",
        },
    });
});
export var ArticleButton = function (props) {
    var classes = useStyles(props);
    var interactor = useArticleInteractor();
    return (React.createElement("div", { className: classes.root, onClick: function () { return interactor.selectArticle(props.id); } },
        React.createElement(Typography, { className: classes.category }, props.category),
        React.createElement(Typography, { variant: "body2", className: classes.title }, props.title)));
};
//# sourceMappingURL=article-button.js.map