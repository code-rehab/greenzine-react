import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Poll, PollData, PollModel } from "./poll";
export interface PollProvider extends GraphQLProviderProps<Poll, PollData> {
}
export declare class DefaultPollProvider extends GraphQLProvider<Poll, PollData> implements PollProvider {
    model: typeof PollModel;
    protected listOperation: () => {
        query: any;
        variables: {};
    };
    protected createOperation: (poll: Poll) => {
        query: any;
        variables: {};
    };
    protected fetchOperation: (poll: Poll) => {
        query: any;
        variables: {};
    };
    protected updateOperation: (poll: Poll) => {
        query: any;
        variables: {};
    };
    protected deleteOperation: (poll: Poll) => {
        query: any;
        variables: {};
    };
}
