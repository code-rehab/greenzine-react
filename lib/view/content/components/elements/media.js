import * as React from "react";
import { withStyles, Typography } from "@material-ui/core";
import classnames from "classnames";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            margin: "10px auto 20px auto",
            width: "100%"
        },
        overlay: {
            position: "absolute",
            width: "100%",
            height: "100%",
            left: 0,
            top: 0,
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
        },
        blurb: {
            display: "inline-block",
            padding: "25px 0 0 15px",
            borderLeft: "1px solid",
            position: "absolute",
            left: "10%",
            top: "100%",
            transform: "translate(0, -20px)",
            textTransform: "none",
            fontFamily: "Montserrat",
            textAlign: "left",
            zIndex: 0
        },
        media: {
            border: "none",
            maxWidth: "100%"
            // width: "100%",
            // height: "250px"
        },
        videoWrapper: {
            position: "relative",
            paddingBottom: "56.25%",
            marginBottom: 15,
            height: 0,
            width: "100%",
            zIndex: 1
        },
        video: {
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            background: "#000"
        },
        inner: (_a = {
                position: "relative"
            },
            // width: "fit-content",
            _a[theme.breakpoints.down("md")] = {
                margin: "0 auto",
                display: "flex"
            },
            _a)
    });
};
export var Media = withStyles(styles)(function (_a) {
    var children = _a.children, classes = _a.classes, embed = _a.embed, image = _a.image, width = _a.width, height = _a.height, className = _a.className, style = _a.style, caption = _a.caption, maxWidth = _a.maxWidth, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement("div", { className: classnames(classes.root, className), style: { maxWidth: maxWidth ? maxWidth : "100%" } },
            React.createElement("div", { className: classes.inner },
                image && !embed && (React.createElement("img", { width: width ? width : "unset", height: height ? height : "unset", src: image, className: classes.media, alt: "" })),
                embed && (React.createElement("div", { className: classes.videoWrapper },
                    React.createElement("iframe", { title: embed, width: width, allowFullScreen: true, height: height, src: embed, className: classnames(classes.media, classes.video) }))),
                caption && (React.createElement(Typography, { variant: "caption", className: classes.blurb }, caption)),
                children))));
});
//# sourceMappingURL=media.js.map