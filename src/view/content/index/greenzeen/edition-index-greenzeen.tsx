import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { toJS } from "mobx";
import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
import { RenderWhile } from "../../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
  },

  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  title: {
  },
  subtitle: {
    fontWeight: 700,
    margin: "20px 0",
    color: "#4D4D4F",
  },
  description: {
    paddingRight: theme.spacing(1),
  },
  collapseContainer: {
    marginBottom: theme.spacing(3),
    position: "relative",
    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <>
      <img src="https://d6j399hnl3eyg.cloudfront.net/greenzeen/marktonderzoek-v1/Group%20474.svg" width="132" style={{marginBottom: 40}}/>
      <Typography variant={"h2"} className={classes.title}>
        Over dit marktonderzoek
      </Typography>
      <Typography className={classes.description} paragraph>
      Greenzeen is een nieuw product uit de koker van creative bastards en code rehab. 
      We zijn al ruim 2 jaar met de ontwikkeling bezig en dit jaar moet het gebeuren, we gaan de markt op! Super spannend! 
    </Typography>
    <Typography>
    Om meer inzicht te krijgen in onze doelgroep hebben we wat vragen verzameld voor mensen in de marketing-communicatie hoek. 
      Heel fijn dat je mee wilt werken. Alvast bedankt voor je bijdrage!
    </Typography>
    </>
  );
};

const EditionIndexGreenzeenComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  console.log(edition);
  return (
    <>
      <div>
        <RenderWhile desktop print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        {/* <RenderWhile mobile>
          <Collapse in={open} collapsedHeight={110} classes={{ container: classes.collapseContainer }}>            
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }} 
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile> */}
      </div>
    </>
  );
};

export const EditionIndexGreenzeen = withStyles(styles)(EditionIndexGreenzeenComponent);
