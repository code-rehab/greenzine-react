import { IPresenter } from "../../helpers/with-presenter";
import { RouteComponentProps } from "react-router";
import { History } from "history";
import { PossibleRouterParams } from "../../config/routes";
export declare class BasicPagePresenter implements IPresenter {
    protected _router: RouteComponentProps<PossibleRouterParams>;
    loading: boolean;
    protected history: History;
    constructor(_router: RouteComponentProps<PossibleRouterParams>);
    mount: () => void;
    unmount: () => void;
    back: (e: any) => void;
}
