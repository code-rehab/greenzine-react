import { GraphQLOptions } from "@aws-amplify/api/lib/types";
import { API } from "aws-amplify";
import { parse } from "graphql";
import { v4 as uuid } from "uuid";
import { DefaultStore } from "../storage/store";
import { RecordData } from "./base-model";
import { GraphQLModel } from "./graphql-model";
import { NetworkModule } from "./network";
import { GraphQLCollection, Collection } from "./graphql-collection";

export type ModelClass<TData extends RecordData> = {
  new (...args: any[]): GraphQLModel<TData>;
};

export interface GraphQLProviderProps<T extends GraphQLModel<TData>, TData extends RecordData> {
  typename: string;
  model: ModelClass<TData>;
  get(id: string): T;
  save(instance: T): Promise<void>;
  create: (data?: Partial<TData>, persist?: boolean) => T;
  createInstance: (data: Partial<TData>) => T;
  fetch(item: T): Promise<void>;
  fetchList(variables?: Record<string, any>): Promise<void>;
  update(item: T): Promise<void>;
  delete(item: T): Promise<void>;
  subscribe(key: string, handler: (model: T) => void): void;
  collect(items?: TData[]): Collection<T>;
  injectList(list: TData[]): void;
}

export abstract class GraphQLProvider<
  T extends GraphQLModel<TData>,
  TData extends RecordData
> extends DefaultStore<TData> {
  public subscribers: Record<string, Array<(model: T) => void>> = {};
  public useMock: boolean = false;
  public typename: string = "";

  public abstract model: ModelClass<TData>;

  constructor(private _network: NetworkModule) {
    super();
    if ((this as any)._subscriptions) {
      (this as any)._subscriptions.map((subscribe: any) => {
        subscribe(this);
      });
    }
  }

  protected listOperation = (variables: Record<string, any>): GraphQLOptions => {
    throw new Error("Fetch operation not set");
  };

  protected fetchOperation = (_item: T): GraphQLOptions => {
    throw new Error("Fetch operation not set");
  };

  protected createOperation = (_item: T): GraphQLOptions => {
    throw new Error("Create operation not set");
  };

  protected updateOperation = (_item: T): GraphQLOptions => {
    throw new Error("Update operation not set");
  };

  protected deleteOperation = (_item: T): GraphQLOptions => {
    throw new Error("Delete operation not set");
  };

  public collect = (items?: TData[]) => {
    const collection: Collection<T> = new GraphQLCollection(items, this);
    return collection;
  };

  public get = (id: string) => {
    return this.createInstance({ id } as Partial<TData>);
  };

  public create = (data: Partial<TData> = {}, persist: boolean = false) => {
    const instance: T = this.createInstance(data);
    instance.isNew = true;

    if (persist) {
      instance.save();
    }

    return instance;
  };

  public createInstance(data: Partial<TData>): T {
    const instance: T = new (this as any).model({ id: uuid(), ...data }, this);
    return instance;
  }

  public subscribe(key: string, handler: (model: T) => void) {
    this.subscribers[key] = this.subscribers[key] || [];
    this.subscribers[key].push(handler);
  }

  public fetch = async (item: T) => {
    const operation = this.fetchOperation(item);
    await this.query(item, operation);
  };

  public fetchList = async (variables: Record<string, any> = {}) => {
    const operation = this.listOperation(variables);
    await this.query(null, operation, true);
  };

  public save = async (instance: T) => {
    const operationFunc = (this as any).createOperation;
    const operation = operationFunc(instance);

    if (operation) {
      await API.graphql(operation);
      this.setRecord(instance.id, instance.serialize());
    }
  };

  public async update(item: T) {
    const operation = this.updateOperation(item);

    await this.query(item, operation);
  }

  public delete = async (item: T) => {
    const operation = this.deleteOperation(item);
    await this.query(item, operation);
    this.deleteRecord(item.id);
  };

  public async query(item: T | null, operation: GraphQLOptions, expectList: boolean = false) {
    const response = await this._network.fetch(
      operation.query as string,
      operation.variables,
      this.useMock
    );
    const parsedQuery = parse(operation.query.toString());

    let queryName = "";
    if ("name" in parsedQuery.definitions[0]) {
      queryName = (parsedQuery.definitions[0].name && parsedQuery.definitions[0].name.value) || "";
    }

    const result = response.data && response.data[queryName];

    if (item && !expectList) {
      item.record = this.updateRecord(item.id, { ...result });
    }

    if (expectList && Array.isArray(result)) {
      this.injectList(result);
    }

    if (response.errors) {
      console.error(response.errors);
    }
  }
}
