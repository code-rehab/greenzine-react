import { ArticleProvider, DefaultArticleProvider } from "../data/article/article-provider";
import { DefaultEditionProvider, EditionProvider } from "../data/edition/edition-provider";
import { DefaultMagazineProvider, MagazineProvider } from "../data/magazine/magazine-provider";
import { DefaultPollProvider, PollProvider } from "../data/poll/poll-provider";
import { BaseModule } from "../module";
import { NetworkModule } from "../network/network";
import { BusinessProviders } from "./business";
import { DefaultPageProvider, PageProvider } from "../data/page/page-provider";

export class DefaultBusinessProviders extends BaseModule<BusinessProviders, {}, {}> implements BusinessProviders {
  constructor(protected _network: NetworkModule) {
    super();
  }

  public get magazine(): MagazineProvider {
    return this.loadProvider("magazine", DefaultMagazineProvider, this._network);
  }

  public get edition(): EditionProvider {
    return this.loadProvider("edition", DefaultEditionProvider, this._network);
  }

  public get article(): ArticleProvider {
    return this.loadProvider("article", DefaultArticleProvider, this._network);
  }

  public get poll(): PollProvider {
    return this.loadProvider("poll", DefaultPollProvider, this._network);
  }

  public get page(): PageProvider {
    return this.loadProvider("page", DefaultPageProvider, this._network);
  }
}
