import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { BondigEditionCoverPresenter } from "../bondig-edition-cover-presenter";
declare type OwnProps = {
    config: any;
    presenter: BondigEditionCoverPresenter;
} & WithStyles<"root" | "logo" | "ncf_logo" | "logoText" | "index" | "contentWrapper" | "title" | "subtitle" | "buttonReadMore" | "editionHighlights" | "imageWrap" | "image" | "toEdge">;
export declare const LayoutEditionCoverDesktop: React.ComponentType<Pick<OwnProps, "config" | "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
