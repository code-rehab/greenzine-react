var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import classNames from "classnames";
import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            "&.primary": {
                fontFamily: "Domaine",
                fontWeight: 500,
            },
            "&.secondary": {
                fontFamily: "Montserrat",
                fontWeight: 500,
            },
        },
        primary: {
            fontFamily: "Domaine",
            fontWeight: "500 !important",
        },
        secondary: {
            fontFamily: "Montserrat",
            fontWeight: "500 !important",
        },
        articleCoverTitle: {
            "&$secondary": {
                fontWeight: 300,
            },
        },
        title: {
            fontWeight: 500,
        },
        italic: {
            fontStyle: "italic",
        },
        strong: {
            fontWeight: 700,
        },
        fatHeader: {
            fontFamily: "Montserrat",
            fontWeight: 500,
            marginBottom: "1em",
            paddingBottom: "1em",
            borderBottom: "solid 5px",
        },
        "big-lot-of-content": (_a = {},
            _a[theme.breakpoints.up("md")] = {
                fontSize: "3em",
            },
            _a),
    });
};
export var BondigTypography = withStyles(styles)(function (props) {
    var clsNames = (props.classNames || []).map(function (name) { return props.classes[name]; });
    var animation = props.animation || { duration: 0, delay: 0 };
    var style = typeof props.style === "string" ? JSON.parse(props.style) : props.style;
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement(Typography, { variant: props.variant, align: props.align || "left", paragraph: props.paragraph, gutterBottom: props.gutterBottom, className: classNames.apply(void 0, __spreadArrays([props.classes.root], clsNames, [props.font || ""])), style: __assign(__assign(__assign({}, props.customStyle), style), { fontWeight: props.fontWeight || 300, color: props.color }) || {} },
            React.createElement("span", { dangerouslySetInnerHTML: { __html: props.children } }),
            props.endItem ? (React.createElement("img", { src: "https://d3m8frrq3asli4.cloudfront.net/mst/end-" + props.endColor + ".svg", style: { position: "relative", bottom: -5, left: 20 } })) : (""))));
});
//# sourceMappingURL=typography.js.map