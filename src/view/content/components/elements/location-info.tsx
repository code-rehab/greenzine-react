import * as React from "react";
import { withStyles, StyleRules } from "@material-ui/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { Theme, Typography } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    marginBottom: "1em"
  },
  contactLink: {
    marginRight: "auto",
    marginLeft: theme.spacing(2.4),
    display: "flex",
    flexDirection: "column"
  },
  faIcon: {
    fontSize: theme.spacing(1.5),
    width: theme.spacing(2)
  }
});

const LocationInfoComponent = ({ classes, role, address, zipcode, city, animation }: any) => {
  animation = animation || { duration: 0, delay: 0 };
  return (
    <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
      <div className={classes.root}>
        <div>
          <FontAwesomeIcon className={classes.faIcon} icon={faMapMarkerAlt} style={{ margin: "5px 0 0 0" }} />
        </div>

        <div className={classes.contactLink}>
          <Typography>
            <small>{role}</small>
          </Typography>
          <Typography>
            <small>{address}</small>
          </Typography>
          <Typography>
            <small>
              {zipcode} {city}
            </small>
          </Typography>
        </div>
      </div>
    </SlideFade>
  );
};

export const LocationInfo = withStyles(styles)(LocationInfoComponent);
