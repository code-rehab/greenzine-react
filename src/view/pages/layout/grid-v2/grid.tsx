import * as React from "react";
import { Page } from "../../../../application/data/page/page";

import { Article } from "../../../../application/data/article/article";
import { observer } from "mobx-react";
import { LayoutRenderer } from "./layout-renderer";
import { PageContainer } from "./page-container";
import { ArticleRedirect } from "../../../content/components/article-redirect";
import { ArticlePresenter } from "../../article-presenter";

interface OwnProps {
  article: Article;
  page: Page;
  breakpoint?: "sm" | "md" | "lg";
  style?: React.CSSProperties;
  presenter?: ArticlePresenter;
  print?: boolean;
}

export const GridV2 = observer(({ print, page, breakpoint, style, presenter }: OwnProps) => {
  page.data.reverse().forEach((d: any, i: number) => {
    d.animation = {
      duration: 700,
      delay: i * 50,
    };
  });

  return (
    <PageContainer print={print} page={page} style={style || {}}>
      <LayoutRenderer
        print={print || false}
        layout={page.layouts[breakpoint || "lg"] || []}
        elements={page.data}
      />
      {presenter &&
        !presenter.hasNextPage &&
        presenter.hasNextArticle &&
        presenter.currentPage &&
        presenter.currentPage.layout !== "bondig-edition-cover" &&
        presenter.currentPage.layout !== "mst-edition-2020-cover" &&
        presenter.currentPage.layout !== "carmelkennismaking-edition-cover" && 
        presenter.currentPage.layout !== "carmelkoers-edition-cover" && (
          <ArticleRedirect
            onClick={presenter.toNextArticle}
            thumbnail={
              presenter.nextArticle &&
              (presenter.nextArticle.featuredImage
                ? presenter.nextArticle.featuredImage
                : presenter.nextArticle.image)
            }
          >
            {presenter.nextArticle && presenter.nextArticle.title}
          </ArticleRedirect>
        )}
    </PageContainer>
  );
});
