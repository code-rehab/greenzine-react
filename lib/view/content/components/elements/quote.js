var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Typography, withStyles } from "@material-ui/core";
import classNames from "classnames";
import * as React from "react";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) {
    var _a;
    return ({
        root: {
            borderTop: "5px solid",
            paddingTop: 10,
            marginBottom: theme.spacing(3),
        },
        quote: {
            color: "inherit",
            fontFamily: "Domaine",
            fontStyle: "italic",
        },
        quoteSymbol: (_a = {
                fontWeight: 500,
                position: "relative",
                fontSize: "5em",
                marginBottom: -15
            },
            _a[theme.breakpoints.up("md")] = {
                fontSize: "2em",
                bottom: -50,
                marginTop: -50,
                marginBottom: 30,
            },
            _a.color = "inherit",
            _a),
        symbol: {
            marginTop: theme.spacing(2),
            width: 20,
            height: 20,
        },
        author: {
            marginTop: theme.spacing(1),
        },
        print: {
            borderColor: "#000 !important",
            padding: 0,
        },
        mobile: {
            marginBottom: theme.spacing(3),
        },
        mstVersion: {
            fontSize: "calc(1rem + 0.4em)",
            fontFamily: "Museo",
            fontWeight: 700,
            lineHeight: 1.3,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
    });
};
export var Quote = withStyles(styles)(function (_a) {
    var classes = _a.classes, author = _a.author, content = _a.content, color = _a.color, renderFor = _a.renderFor, animation = _a.animation, customStyle = _a.customStyle, dash = _a.dash;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay }, process.env.REACT_APP_CLIENT_THEME == "mst" ? (React.createElement("div", { className: classNames(classes.root, classes[renderFor]), style: { color: color } },
        React.createElement(Typography, { variant: "h4", style: { fontSize: "2em", marginBottom: -15 } }, "\u201C"),
        React.createElement(Typography, { variant: "h4", className: classes.mstVersion, gutterBottom: true, style: __assign({}, customStyle) }, content),
        author ? (React.createElement(Typography, { variant: "body2", className: classes.author, style: __assign({}, customStyle) },
            dash ? "-" : "",
            " ",
            author)) : (""))) : (React.createElement("div", { className: classNames(classes.root, classes[renderFor]), style: { color: color } },
        React.createElement(Typography, { variant: "h4", className: classes.quoteSymbol, gutterBottom: true, style: __assign({}, customStyle) },
            React.createElement("svg", { "aria-hidden": "true", focusable: "false", "data-prefix": "fas", "data-icon": "quote-left", className: "svg-inline--fa fa-quote-left fa-w-16 ", role: "img", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 512 512", color: "inherit", style: { width: ".5em", verticalAlign: "-0.125em", marginBottom: 15 } },
                React.createElement("path", { fill: "currentColor", d: "M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z" }))),
        React.createElement(Typography, { variant: "h4", className: classes.quote, gutterBottom: true, style: __assign({}, customStyle) }, content),
        author ? (React.createElement(Typography, { variant: "body2", className: classes.author, style: __assign({}, customStyle) },
            dash ? "-" : "",
            " ",
            author)) : ("")))));
});
//# sourceMappingURL=quote.js.map