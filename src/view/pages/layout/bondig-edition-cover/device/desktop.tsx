import * as React from "react";
import {
  Theme,
  WithStyles,
  withStyles,
  StyleRules,
  Typography,
  Grid,
  Button,
  Hidden,
} from "@material-ui/core";

import { observer } from "mobx-react";
import { BondigEditionCoverPresenter } from "../bondig-edition-cover-presenter";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
import classNames from "classnames";
import { EditionBall } from "../../../../content/components/edition-ball";
import { EditionHighlights } from "./highlights";

type OwnProps = { config: any; presenter: BondigEditionCoverPresenter } & WithStyles<
  | "root"
  | "logo"
  | "ncf_logo"
  | "logoText"
  | "index"
  | "contentWrapper"
  | "title"
  | "subtitle"
  | "buttonReadMore"
  | "editionHighlights"
  | "imageWrap"
  | "image"  
  | "toEdge"
>;
const styles = (theme: Theme): StyleRules => ({
  
  root: {
    display: "flex",
    height: "calc(100vh - 80px)",
    flexDirection: "column",    
    justifyContent: "center",
    position: "absolute",
    zIndex:0,

    [theme.breakpoints.down("sm")]: {
      position: "relative"
    },

    // overlay
    "&:after": {
      content: "' '",
      display: "block",
      position: "fixed",
      background: "linear-gradient(to bottom, rgba(0,0,0,0) 50%, rgba(0,0,0,0.6));",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      zIndex: 10
    },
  
  },

  // NCF logo
  ncf_logo: {
    position: "fixed",
    right: 60,
    top: 90,
    zIndex: 999,
    [theme.breakpoints.down("sm")]: {
      top: "unset",
      bottom: 90  ,    
      right: 30,
      maxWidth: "20%"
    },
  },
  // Bondig Logo
  logo: {
    position: "fixed",
    left: 60,
    top: 90,
    display: "flex",
    width: 600, 
    [theme.breakpoints.down("sm")]: {
      width: 300,
      left: 30
    }
  },

  logoText:{
   maxWidth: 300, 
   lineHeight: 1, 
   marginLeft: -36, 
   marginTop: -5,
   [theme.breakpoints.down("sm")]: {
    position: "absolute",
    left: 0,
    top: 70,
    marginLeft: 0, 
    marginTop: 0,
  },
  },

  // Index: edition: (55)
  index:{
    position: "absolute",
    right: 34,
    top: -34,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "100%",
    whiteSpace: "nowrap",
    color: "#e9550d",
    backgroundColor: "#FFC586",
    width: theme.spacing(4),
    height: theme.spacing(4),
    fontWeight: 700,
    fontSize: 15,  
  
    [theme.breakpoints.down("sm")]: {
      right: -14,
      top: -40,
    },

  },

  // Content
  contentWrapper: {
   display: "flex",
   flexDirection: "column",
   margin: "0 60px",
   position: "relative",
   zIndex: 30,
    [theme.breakpoints.down("sm")]: {
      margin: "0",
    },
  },
  title: {
    marginBottom: theme.spacing(2),
    fontSize: "9em",
    [theme.breakpoints.down("md")]: {
      fontSize: "9em",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "3em",
      marginBottom: theme.spacing(1),
    },
  },
  subtitle: {
    fontSize: "2em",
    marginBottom: theme.spacing(2),
  },
  buttonReadMore: {
    color: "#fff",
    borderColor: "#fff",
    padding: "3px 36px",
    maxWidth: 200
  },
  // end Content
  editionHighlights: {
    position: "fixed",
    left: 60,
    bottom: 120,
    width: "50vw",
    zIndex: 20,
    [theme.breakpoints.down("sm")]: {
      left: 30,
      bottom: 80,
    } 
  },  
  // Image component
  image: {
    width: "100%",
    position: "fixed",
    right: 60,
    bottom: 60,
    maxWidth: "60vw",
    [theme.breakpoints.down("sm")]: {
      right: 30,
      bottom: 60,
    },
  },
  toEdge: {
    bottom: 0,
    right: 0,
  },

  // end Image
});

export const LayoutEditionCoverDesktop = withStyles(styles)(
  observer(({ classes, config, presenter }: OwnProps) => {
    const styles = config.styles || {};
    const edition = presenter && presenter.edition && presenter.edition.id;

    return (
      <div style={{ position: "relative", width: "100%" }}>
        <div className={classes.root}>

        <SlideFade direction="up" timeout={900}>
          <div className={classes.logo}>
            <Grid container spacing={0} alignItems="center">
              <Grid item sm={true} style={{ position: "relative" }}>
                <img style={{maxWidth: "100%"}} src={"https://d6j399hnl3eyg.cloudfront.net/ncf/bondig/bondig-logo-white.svg"} alt="Bondig" />
                <div className={classes.index}>
                  nr. {edition || "15"}
                </div>
              </Grid>
              <Grid item sm={true}>
                <Typography variant="body1" className={classes.logoText}>
                  digitaal magazine <br /> voor leden van de NCF
                </Typography>
              </Grid>
            </Grid>
          </div>
        </SlideFade>

        <img src="https://d6j399hnl3eyg.cloudfront.net/ncf/bondig/LG+NCF+Wit.svg" className={classes.ncf_logo} />

          {config.cover_image ? ( 
            <>

              {/* With animation */}
              {config.cover_image_animation == "true" ? (
                <ZoomFade timeout={1900}>
                  <img
                    style={styles.image || {}}
                    className={
                      config.cover_image_style == "edge"
                        ? classNames(classes.image, classes.toEdge)
                        : classes.image
                    }
                    src={config.cover_image}
                    alt=""
                  />
                </ZoomFade>
              ) : (
                <img
                  style={styles.image || {}}
                  className={
                    config.cover_image_position == "edge"
                      ? classNames(classes.image, classes.toEdge)
                      : classes.image
                  }
                  src={config.cover_image}
                  alt=""
                />
              )}
            </>
          ) : (
            ""
          )}

          <div className={classes.contentWrapper} >
            <SlideFade direction="up" timeout={1000}>
              <Typography className={classes.title} variant="h1" style={styles.primary || {}}>
                <span dangerouslySetInnerHTML={{ __html: config.head_article.primary }} />
              </Typography>
            </SlideFade>

            {config.head_article.secondary ? (
              <SlideFade direction="up" timeout={1100}>
                <Typography
                  className={classes.subtitle}
                  variant="h3"
                  style={styles.secondary || {}}
                >
                  <span dangerouslySetInnerHTML={{ __html: config.head_article.secondary }} />
                </Typography>
              </SlideFade>
            ) : (
              ""
            )}

            <SlideFade direction="up" timeout={1200}>
              <Button
                onClick={presenter.nextArticle}
                variant="outlined"
                className={classes.buttonReadMore}
              >
                Bekijk artikel
              </Button>
            </SlideFade>
          </div>
          <div className={classes.editionHighlights}>
            <Grid container justify="flex-start">
              <EditionHighlights articles={config.highlightedContent} />
            </Grid>
          </div>
        </div>
      </div>
    );
  })
);