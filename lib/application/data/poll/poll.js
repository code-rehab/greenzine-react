var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase } from "../../network/graphql-model";
var PollModel = /** @class */ (function (_super) {
    __extends(PollModel, _super);
    function PollModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.typename = "Poll";
        _this.name = "";
        _this.questions = [];
        _this.pollData = {};
        _this.status = "";
        _this.attributes = {};
        _this.submissions = [];
        return _this;
    }
    PollModel.prototype.process = function () { };
    __decorate([
        data
    ], PollModel.prototype, "name", void 0);
    __decorate([
        data
    ], PollModel.prototype, "questions", void 0);
    __decorate([
        data
    ], PollModel.prototype, "pollData", void 0);
    __decorate([
        data
    ], PollModel.prototype, "status", void 0);
    __decorate([
        data
    ], PollModel.prototype, "attributes", void 0);
    __decorate([
        data
    ], PollModel.prototype, "submissions", void 0);
    return PollModel;
}(GraphQLBase));
export { PollModel };
//# sourceMappingURL=poll.js.map