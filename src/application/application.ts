import { RouteComponentProps } from "react-router";
import { BusinessModules, DefaultBusinessModule } from "./business/business";
import { BaseModule } from "./module";
import { DefaultNetworkModule, NetworkModule } from "./network/network";
// import { PossibleRouterParams } from "../config/routes";

export interface IApplication {
  network: NetworkModule;
  business: BusinessModules;
  router: RouteComponentProps<any> | undefined;
}

class DefaultApplication
  extends BaseModule<
    {},
    {},
    {
      network: NetworkModule;
      business: BusinessModules;
    }
  >
  implements IApplication {
  public router: RouteComponentProps<any> | undefined = undefined;

  public get network() {
    return this.loadModule("network", DefaultNetworkModule);
  }

  public get business() {
    return this.loadModule("business", DefaultBusinessModule, this.network);
  }
}

export const Application: IApplication = new DefaultApplication();
