import { observable } from "mobx";
import { route } from "../../../config/routes";
import { Application } from "../../application";
import { Edition } from "../edition/edition";
import { Magazine } from "./magazine";
import { MagazineProvider } from "./magazine-provider";

export interface MagazineInteractor {
  selectedMagazine: Magazine | undefined;
  selectedEdition: Edition | undefined;
  selectMagazine(id: string, redirect?: boolean): Promise<void>;
  selectEdition(id: string, redirect?: boolean): Promise<void>;
}

export class DefaultMagazineInteractor implements MagazineInteractor {
  constructor(private _magazineProvider: MagazineProvider) {
    //
  }

  @observable public selectedMagazine: Magazine | undefined = undefined;
  @observable public selectedEdition: Edition | undefined = undefined;

  public selectMagazine = async (id: string, redirect: boolean = true) => {
    if (this.selectedMagazine && this.selectedMagazine.id === id) {
      return;
    }

    const magazine = this._magazineProvider.get(id);

    if (magazine) {
      await magazine.fetch();
      this.selectedMagazine = magazine;

      if (redirect) {
        Application.router &&
          Application.router.history.push(
            route("index.editions", {
              magazine: this.selectedMagazine.id
            })
          );
      }
    }
  };

  public selectEdition = async (id: string, redirect: boolean = true) => {
    if (this.selectedEdition && this.selectedEdition.id === id) {
      return;
    }

    const edition =
      this.selectedMagazine &&
      this.selectedMagazine.editions.filter(edition => {
        return edition.id === id;
      });

    this.selectedEdition = edition && edition[0];

    if (redirect && this.selectedMagazine && this.selectedEdition) {
      Application.router &&
        Application.router.history.push(
          route("index.articles", {
            magazine: this.selectedMagazine.id,
            edition: this.selectedEdition.id
          })
        );
    }
  };
}
