import { IPresenter } from "../../helpers/with-presenter";
import { PagePresenter } from "./_page-default-presenter";
import { observable, computed } from "mobx";
import { Page } from "../../application/data/page/page";

export class ArticlePresenter extends PagePresenter implements IPresenter {
  public currentPageComponent: Element | undefined = undefined;

  @observable public atBottom: boolean = false;
  @observable public atTop: boolean = true;
  @observable public filter: string = "";
  @observable public _pages: Page[] = [];

  public get enrichedTemp() {
    return false;
  }

  public get layout() {
    return "div";
  }

  public get contentData() {
    if (this.currentPage && this.currentPage.content) {
      return this.currentPage.content;
    }

    return {};
  }

  public get pages(): any {
    return (
      this.article &&
      this.article.pageOrder &&
      (this.article.pageOrder.map((id) => this._pages.find((p) => p.id === id)) as Page[])
    );
  }

  public get content() {
    return "TODO";
    // this._contentConfig = pageContent[this.currentPage ? this.currentPage.id : "404"];
    // return this._contentConfig;
  }

  public content_by_id(id: string) {
    return "TODO";
    // this._contentConfig = pageContent[id];
    // return this._contentConfig && this._contentConfig;
  }

  public mount = async () => {};

  public unmount = () => {
    //
  };

  public selectArticle = (id: string) => {
    this._articleInteractor.selectArticle(id);
  };

  public toNextPage = () => {
    this._articleInteractor.nextPage();
  };

  public toPreviousPage = () => {
    this._articleInteractor.previousPage();
  };

  public toNextArticle = () => {
    this._articleInteractor.nextArticle();
  };

  public toPreviousArticle = () => {
    this._articleInteractor.previousArticle();
  };

  public selectPage = (id: string) => {
    this.currentPageComponent = this.pages.find((page: any) => page.id === id);
    this.currentPageComponent && this._articleInteractor.selectPage(this.currentPageComponent.id);
    this.filter = this.currentPage ? this.currentPage.filter! : "";
  };

  public setPage() {
    this.currentPageComponent && this.currentPageComponent.scrollIntoView();
  }

  public handleKeyDown = (e: KeyboardEvent) => {
    //
  };
}
