import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { toJS } from "mobx";
import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
import { RenderWhile } from "../../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
    marginTop: theme.spacing(10),
  },

  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  title: {
    color: "#fff",
    margin: "20px 0",
    fontFamily: "Amaranth",
    lineHeight: 0.9,
    fontSize: 42,
    "& br": {
      display: "none",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: 50,
      fontSize: "2.6em",
    },
  },
  subtitle: {
    fontWeight: 700,
    margin: "20px 0",
    color: "#4D4D4F",
  },
  logo: {
    marginTop: theme.spacing(4),
  },
  description: {
    // fontSize: 17,
    paddingRight: theme.spacing(1),
  },
  collapseContainer: {
    marginBottom: theme.spacing(3),
    position: "relative",

    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <div className={classes.root}>
      <Typography variant={"h2"} className={classes.title}>
        Een kennismaking met Carmel
      </Typography>

      <Typography className={classes.description} paragraph>
        Met trots kan ik zeggen dat ik bij Carmel werk. Werken bij Carmel betekent dat jij, als uniek persoon, deel uitmaakt van de Carmelgemeenschap. Maak in dit kennismakingskatern een reis door onze geschiedenis en verken onze waarden. Het nodigt uit tot gesprek. Ontdek hoe jij met jouw bijdrage Carmel verrijkt.
      </Typography>

      <Typography className={classes.description} paragraph>
        Karin van Oort<br/>(voorzitter College van Bestuur)
      </Typography>

      <div className={classes.logo}>
        <img src="/assets/images/carmel/logo-diap.svg" />
      </div>
    </div>
  );
};

const EditionIndexCarmelKennismakingComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div>
        <RenderWhile desktop tablet print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        <RenderWhile mobile>
          <Collapse
            in={open}
            collapsedHeight={110}
            classes={{ wrapper: classes.collapseContainer }}
          >
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }}
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile>
      </div>
    </>
  );
};

export const EditionIndexCarmelKennismaking = withStyles(styles)(
  EditionIndexCarmelKennismakingComponent
);
