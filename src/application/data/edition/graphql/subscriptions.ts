import { EditionFragment } from "./fragments";

export const EditionCreated = `
subscription {
  magazineCreated {
    ${EditionFragment}
  }
}
`;

export const EditionUpdated = `
subscription {
  magazineUpdated {
    ${EditionFragment}
  }
}
`;

export const EditionDeleted = `
subscription {
  magazineDeleted {
    ${EditionFragment}
  }
}
`;
