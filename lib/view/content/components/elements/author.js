import * as React from "react";
import { withStyles, Typography } from "@material-ui/core";
import { RenderWhile } from "../render-while";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) { return ({
    root: {
        padding: theme.spacing(2, 1),
        display: "flex",
        alignItems: "center",
    },
    avatarBubble: {
        position: "relative",
        borderRadius: "100%",
        marginRight: theme.spacing(2),
        width: theme.spacing(6),
        height: theme.spacing(6),
        backgroundColor: "white",
        backgroundPosition: "center",
        backgroundSize: "cover",
        "@media print": {
            display: "none",
        },
    },
    credit: {
        fontFamily: "Montserrat",
        textTransform: "none",
        fontWeight: 500,
        fontSize: "14px",
        "& b": {
            fontWeight: "bold",
        },
    },
    textWrapper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        textAlign: "start",
    },
}); };
export var Author = withStyles(styles)(function (_a) {
    var classes = _a.classes, avatar = _a.avatar, credits = _a.credits, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    credits = credits || [];
    return (React.createElement(React.Fragment, null,
        React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
            React.createElement("div", { className: classes.root },
                React.createElement(RenderWhile, { tablet: true, desktop: true },
                    React.createElement("div", { className: classes.avatarBubble, style: { backgroundImage: "url(" + avatar + ")" } })),
                React.createElement(RenderWhile, { desktop: true, mobile: true },
                    React.createElement("div", { className: classes.textWrapper },
                        React.createElement(Typography, { variant: "body2", className: classes.credit }, credits.map(function (credit, index) {
                            return (React.createElement("span", { key: index, style: { display: "block" } },
                                React.createElement("b", null,
                                    credit.credit,
                                    ": "),
                                credit.name));
                        })))))),
        React.createElement(RenderWhile, { print: true }, Array.isArray(credits) ? (credits.map(function (credit, index) {
            return (React.createElement(Typography, { variant: "body2", className: classes.credit },
                React.createElement("span", { key: index, style: { display: "block" } },
                    React.createElement("b", null,
                        credit.credit,
                        ": "),
                    credit.name)));
        })) : (React.createElement(React.Fragment, null)))));
});
//# sourceMappingURL=author.js.map