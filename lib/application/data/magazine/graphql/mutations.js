import { MagazineFragment } from "./fragments";
export var CreateMagazine = "\nmutation CreateMagazine($input:inputMagazine!) {\ncreateMagazine(input:$input){\n  " + MagazineFragment + "\n}\n}\n";
export var UpdateMagazine = "\nmutation updateMagazine($input:inputMagazine!) {\n  updateMagazine(input:$input) {\n    " + MagazineFragment + "\n  }\n}\n";
export var DeleteMagazine = "\nmutation DeleteMagazine($id:ID!) {\n  deleteMagazine(id:$id){\n    " + MagazineFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map