import * as React from "react";
import { RouteComponentProps } from "react-router";
export declare const Error404Content: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<string>>;
export declare const Error404: React.ComponentClass<RouteComponentProps<{}, import("react-router").StaticContext, unknown>, any>;
