import { MagazineFragment } from "./fragments";

export const Magazine = `
  query Magazine($id:ID!) {
    Magazine(id:$id){
      ${MagazineFragment}
    }
  }
`;

export const MagazineCollection = `
  query MagazineCollection {
    MagazineCollection{
      ${MagazineFragment}
    }
  }
`;
