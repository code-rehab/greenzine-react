import { ArticleProvider } from "../data/article/article-provider";
import { EditionProvider } from "../data/edition/edition-provider";
import { MagazineProvider } from "../data/magazine/magazine-provider";
import { PollProvider } from "../data/poll/poll-provider";
import { BaseModule } from "../module";
import { NetworkModule } from "../network/network";
import { BusinessProviders } from "./business";
import { PageProvider } from "../data/page/page-provider";
export declare class DefaultBusinessProviders extends BaseModule<BusinessProviders, {}, {}> implements BusinessProviders {
    protected _network: NetworkModule;
    constructor(_network: NetworkModule);
    get magazine(): MagazineProvider;
    get edition(): EditionProvider;
    get article(): ArticleProvider;
    get poll(): PollProvider;
    get page(): PageProvider;
}
