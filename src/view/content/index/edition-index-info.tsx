import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { toJS } from "mobx";
import * as React from "react";
import { EditionData } from "../../../application/data/edition/edition";
import { RenderWhile } from "../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
  },
  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  collapseConainer: {
    marginBottom: theme.spacing(3),
    position: "relative",
    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <>
      {Array.isArray(description) ? (
        description.map((contentPiece: any) => {
          return (
            <Typography
              style={{ ...contentPiece.style }}
              className={classnames(
                classes.contentPiece,
                contentPiece.marginBottom && classes.marginBottom
              )}
              paragraph
            >
              {contentPiece.element ? (
                <contentPiece.element style={{ ...contentPiece.style }}>
                  <span dangerouslySetInnerHTML={{ __html: contentPiece.content }} />
                </contentPiece.element>
              ) : (
                contentPiece.content
              )}
            </Typography>
          );
        })
      ) : (
        <Typography>{description}</Typography>
      )}
    </>
  );
};

const EditionIndexInfoComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div>
        <Typography
          className={classnames(classes.montserrat, classes.title)}
          variant="h4"
          color="primary"
          style={{ marginBottom: "1em", lineHeight: 1 }}
        >
          {edition.subtitle}
        </Typography>

        <RenderWhile tablet desktop print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        <RenderWhile mobile>
          <Collapse
            in={open}
            collapsedHeight={120}
            classes={{ wrapper: classes.collapseConainer }}
          >
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }}
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile>
      </div>
    </>
  );
};

export const EditionIndexInfo = withStyles(styles)(EditionIndexInfoComponent);
