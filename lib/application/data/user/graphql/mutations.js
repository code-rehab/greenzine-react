import { UserFragment } from "./fragments";
export var CreateUser = "\nmutation CreateUser($input:inputUser!) {\n  createUser(input:$input){\n    " + UserFragment + "\n  }\n}";
export var UpdateUser = "\nmutation updateUser($input:inputUser!) {\n  updateUser(input:$input) {\n    " + UserFragment + "\n  }\n}\n";
export var DeleteUser = "\nmutation DeleteUser($id:ID!) {\n  deleteUser(id:$id){\n    " + UserFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map