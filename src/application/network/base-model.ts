import { observable, toJS, computed } from "mobx";

interface DefaultRecordData {
  id: string;
}

interface ModelFunctions<TData extends DefaultRecordData> {
  serialize(): RecordData<TData>;
  defaultValues: Partial<TData>;
  changes: Partial<TData>;
}

export type RecordData<TData extends DefaultRecordData = DefaultRecordData> = TData;

export type Model<TData extends RecordData = RecordData> = ModelFunctions<TData> & TData;

export class BaseModel<TData extends RecordData> implements Model {
  @observable public changes: Partial<TData> = {};
  @observable protected _record: RecordData & Partial<TData>;
  public defaultValues: Partial<TData> = {};

  constructor(_record: RecordData & Partial<TData>) {
    this._record = _record;
  }

  @computed public get record(): RecordData & Partial<TData> {
    return this._record;
  }

  public set record(record: RecordData & Partial<TData>) {
    this._record = record;
  }

  public get id(): string {
    return this._record.id;
  }

  public serialize = (): TData => {
    return this._values;
  };

  public take = (keys: Array<keyof TData>) => {
    const data = this._values;
    return keys.reduce((result, key) => {
      result[key] = data[key];
      return result;
    }, {} as TData);
  };

  protected get _values(): TData {
    return toJS({
      ...this.defaultValues,
      ...this.record,
      ...this.changes,
      id: this.id
    }) as TData;
  }
}
