import { withStyles } from "@material-ui/styles";
import { observer } from "mobx-react";
import * as React from "react";
import { withPresenter } from "../../helpers/with-presenter";
import { InterfacePresenter } from "./interface-presenter";
import { Navigator } from "./partials/navigator";
import { PageNavigator } from "./partials/navigator-page";
import { PollButton } from "./partials/poll-button";
import { Breadcrumb } from "./partials/breadcrumb";
var styles = function (theme) { return ({
    root: {
        position: "absolute",
        top: 0,
        left: 0,
        "@media print": {
            display: "none",
        },
    },
}); };
export var Component = observer(function (_a) {
    var classes = _a.classes, presenter = _a.presenter;
    return (React.createElement("div", { className: classes.root },
        React.createElement(PageNavigator, { presenter: presenter }),
        React.createElement(Navigator, { location: "bottom", presenter: presenter }),
        React.createElement(Breadcrumb, { icon: process.env.REACT_APP_BREADCRUMB_ICON == "true" ? true : false, title: "Carmel Toekomstbouwers", article: presenter.article, url: presenter.edition }),
        React.createElement(PollButton, { presenter: presenter })));
});
export var DefaultInterface = withStyles(styles)(withPresenter(function (_props, _a) {
    var interactor = _a.interactor, provider = _a.provider;
    return new InterfacePresenter(interactor.magazine, provider.magazine, interactor.article, provider.article, interactor.print);
}, Component));
//# sourceMappingURL=interface.js.map