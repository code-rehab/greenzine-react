import classNames from "classnames";
import * as React from "react";

import { StyleRules, Theme, Typography, withStyles } from "@material-ui/core";

import { SlideFade } from "../effects/slide-fade";

const styles = (theme: Theme): StyleRules => ({
  root: {
    "&.primary": {
      fontFamily: "Domaine",
      fontWeight: 500,
    },
    "&.secondary": {
      fontFamily: "Montserrat",
      fontWeight: 500,
    },
  },

  primary: {
    fontFamily: "Domaine",
    fontWeight: "500 !important" as any,
  },

  secondary: {
    fontFamily: "Montserrat",
    fontWeight: "500 !important" as any,
  },

  articleCoverTitle: {
    "&$secondary": {
      fontWeight: 300,
    },
  },
  title: {
    fontWeight: 500,
  },
  italic: {
    fontStyle: "italic",
  },
  strong: {
    fontWeight: 700,
  },
  fatHeader: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    marginBottom: "1em",
    paddingBottom: "1em",
    borderBottom: "solid 5px",
  },

  "big-lot-of-content": {
    [theme.breakpoints.up("md")]: {
      fontSize: "3em",
    },
  },
});

export const BondigTypography = withStyles(styles)((props: any) => {
  const clsNames = (props.classNames || []).map((name: string) => props.classes[name]);
  const animation = props.animation || { duration: 0, delay: 0 };
  const style = typeof props.style === "string" ? JSON.parse(props.style) : props.style;

  return (
    <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
      <Typography
        variant={props.variant}
        align={props.align || "left"}
        paragraph={props.paragraph}
        gutterBottom={props.gutterBottom}
        className={classNames(props.classes.root, ...clsNames, ...[props.font || ""])}
        style={
          {
            ...props.customStyle,
            ...style,
            fontWeight: props.fontWeight || 300,
            color: props.color,
          } || {}
        }
      >
        {/* TODO: [OMA-651] find another solution then parse HTML directly */}
        <span dangerouslySetInnerHTML={{ __html: props.children }} />
        {props.endItem ? (
          <img
            src={"https://d3m8frrq3asli4.cloudfront.net/mst/end-" + props.endColor + ".svg"}
            style={{ position: "relative", bottom: -5, left: 20 }}
          />
        ) : (
          ""
        )}
      </Typography>
    </SlideFade>
  );
});
