import { PageFragment } from "./fragments";
export var CreatePage = "\nmutation CreatePage($input:inputPage!) {\ncreatePage(input:$input){\n  " + PageFragment + "\n}\n}\n";
export var UpdatePage = "\nmutation updatePage($input:inputPage!) {\n  updatePage(input:$input) {\n    " + PageFragment + "\n  }\n}\n";
export var DeletePage = "\nmutation DeletePage($id:ID!) {\n  deletePage(id:$id){\n    " + PageFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map