import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { Magazine } from "../../../application/data/magazine/magazine";
interface OwnProps extends WithStyles<any> {
    magazine: Magazine;
}
export declare const MagazineIndexInfo: React.ComponentType<Pick<OwnProps, "magazine"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
