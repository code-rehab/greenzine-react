import * as React from "react";
import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
import { BondigEditionCoverPresenter } from "./bondig-edition-cover-presenter";
import { LayoutEditionCoverDesktop } from "./device/desktop";

const Component = ({ config, presenter }: OwnProps & PresenterProps<BondigEditionCoverPresenter>) => {
  return <LayoutEditionCoverDesktop config={config} presenter={presenter} />;
};

interface OwnProps {
  config: any;
}

export const LayoutBondigEditionCover = withPresenter<BondigEditionCoverPresenter, OwnProps>(
  (_props, { interactor }) =>
    new BondigEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print),
  Component
);
