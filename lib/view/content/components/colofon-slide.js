import * as React from "react";
import { withStyles, Typography, Grid, Link } from "@material-ui/core";
import { BR } from "./linebreak";
var styles = function (theme) { return ({
    root: {
        width: "100%",
    },
}); };
export var Colofon = withStyles(styles)(function (_a) {
    var classes = _a.classes;
    return (React.createElement("div", { className: classes.root, ref: function (elem) {
            if (elem) {
                elem.style.height = elem.clientHeight + "px";
            }
        } },
        React.createElement(Grid, { container: true },
            React.createElement(Grid, { item: true, xs: 12 },
                React.createElement(Typography, { variant: "h4", style: {
                        fontWeight: 400,
                        lineHeight: "1.1 !important",
                        marginBottom: 12,
                    } }, "Colofon")),
            React.createElement(Grid, { item: true, xs: 12, sm: 4, style: {
                    display: "flex",
                    flexDirection: "column",
                } },
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Hoofdredactie")),
                React.createElement(Typography, { paragraph: true, className: classes.text },
                    React.createElement("small", { className: classes.small }, "Chris Regtop")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Redactie")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small }, "Frank Verweij")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small }, "Peter van Diepen")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small }, "Gerard Pereboom")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small }, "Ad van der Donk")),
                React.createElement(Typography, { paragraph: true, className: classes.text },
                    React.createElement("small", { className: classes.small }, "Carien Scholtmeijer")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Eindredactie")),
                React.createElement(Typography, { paragraph: true, className: classes.text },
                    React.createElement("small", { className: classes.small }, "Daphne Duif")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Cartoonist")),
                React.createElement(Typography, { paragraph: true, className: classes.text },
                    React.createElement("small", { className: classes.small }, "Cornelis den Otter")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Concept en ontwerp")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small },
                        React.createElement(Link, { href: "https://creativebastards.nl", color: "inherit", underline: "always", target: "_blank" }, "Creative Bastards")))),
            React.createElement(Grid, { item: true, xs: 12, sm: 4 },
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Technische realisatie")),
                React.createElement(Typography, { paragraph: true },
                    React.createElement("small", { className: classes.small },
                        React.createElement(Link, { href: "https://code.rehab/", color: "inherit", underline: "always", target: "_blank", paragraph: true }, "Code.Rehab"))),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Opmaak")),
                React.createElement(Typography, { paragraph: true },
                    React.createElement("small", { className: classes.small }, "Creative Bastards")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "E-mail redactie")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small },
                        "Heb je vragen of ",
                        React.createElement(BR, null),
                        "opmerkingen over Bondig?")),
                React.createElement(Typography, { paragraph: true },
                    React.createElement("small", { className: classes.small },
                        "Mail dan naar \u00A0",
                        React.createElement(Link, { href: "mailto:redactie@ncf.nl", color: "inherit", underline: "always", paragraph: true }, "redactie@ncf.nl"))),
                React.createElement(Typography, { paragraph: true, className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Verschijnt 6x per jaar")),
                React.createElement(Typography, { paragraph: true, className: classes.text },
                    React.createElement("small", { className: classes.small }, "Ingenomen standpunten in het blad vertegenwoordigen niet noodzakelijkerwijs die van de NCF.")),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "\u00A9 NCF 2020"))))));
});
//# sourceMappingURL=colofon-slide.js.map