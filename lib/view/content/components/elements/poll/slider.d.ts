import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root" | "black" | "slider" | "heartBar" | "heart"> {
    currentValue?: number[];
    minValue?: number;
    maxValue?: number;
    onChange?(value: string[]): void;
}
export declare const HeartSlider: React.ComponentType<Pick<OwnProps, "innerRef" | "onChange" | "currentValue" | "minValue" | "maxValue"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
