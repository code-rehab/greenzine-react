import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root" | "counter" | "buttonNext" | "buttonPrevious" | "buttonDisabled" | "button"> {
    list: string[];
    current: string;
    onNext(index: number): void;
    onPrevious(index: number): void;
}
export declare const ArticleNavigator: React.ComponentType<Pick<OwnProps, "list" | "current" | "onNext" | "onPrevious"> & import("@material-ui/styles").StyledComponentProps<string>>;
export {};
