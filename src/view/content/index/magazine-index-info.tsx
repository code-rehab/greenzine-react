import * as React from "react";

import { Typography, Theme, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classnames from "classnames";
import { RenderWhile } from "../components/render-while";
import { Magazine } from "../../../application/data/magazine/magazine";

interface OwnProps extends WithStyles<any> {
  magazine: Magazine;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
  },
  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "none",
  },
  role: {
    marginBottom: theme.spacing(1),
  },
});

const MagazineIndexInfoComponent = ({ magazine, classes }: OwnProps) => {
  // const [open, setOpen] = React.useState(false);
  return (
    <>
      <Typography
        className={classnames(classes.montserrat, classes.title)}
        variant="h4"
        color="primary"
        style={{ marginBottom: "1em", lineHeight: 1 }}
      >
        {magazine.description}
      </Typography>
      <RenderWhile desktop print>
        <div>
          <Typography>
            Bondig is vanaf editie 51 een interactief magazine. Je vindt deze magazines op deze pagina. <br />
            <br />
            Op zoek naar eerdere edities?{" "}
            <a href="https://ncf.nl/bondig-magazine/" target="_blank" rel="noopener noreferrer">
              Klik dan hier.
            </a>
          </Typography>
        </div>
      </RenderWhile>
    </>
  );
};

export const MagazineIndexInfo = withStyles(styles)(MagazineIndexInfoComponent);

// <RenderWhile mobile>
// <Collapse in={open}>
//   {Object.keys(magazine.editors).map((role, user) => {
//     return (
//       <>
//         <Typography
//           style={{ fontFamily: "Montserrat", fontWeight: "bold" }}
//         >
//           {/* {magazine.editors[role].type.charAt(0).toUpperCase() + magazine.editors[role].type.substring(1).replace(/([A-Z])/g, ' $1')} */}
//           {magazine.editors[role].type}
//         </Typography>
//         {magazine.editors[role].users.map((user: string) => (
//           <Typography variant="h5" style={{ margin: "0" }} key={user}>
//             {user}
//           </Typography>
//         ))}
//       </>
//     );
//   })}
// </Collapse>
// <Button
//   color="primary"
//   variant="contained"
//   style={{ display: "flex", marginLeft: "auto" }}
//   onClick={() => setOpen(!open)}
// >
//   {open ? "Verberg" : "Lees meer"}
// </Button>
// </RenderWhile>
