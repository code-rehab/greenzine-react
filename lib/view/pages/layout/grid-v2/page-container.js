var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core";
import { take } from "../../../../helpers/general";
import { useMediaQuery } from "react-responsive";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {
                width: "100vw",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                padding: "calc(" + theme.spacing(2) + "px + 3vw)",
                paddingBottom: "calc(" + theme.spacing(7) + "px + 3vw)",
                fontSize: "calc(1rem + 0.3vw)"
            },
            _a[theme.breakpoints.up("md")] = {
                minHeight: "100vh",
            },
            _a[theme.breakpoints.down("md")] = {
                fontSize: "0.5em",
            },
            _a[theme.breakpoints.down("sm")] = {
                fontSize: "0.5em",
            },
            _a),
        container: {
            width: "100%",
            margin: "auto",
        },
    });
};
export var PageContainer = withStyles(styles)(observer(function (_a) {
    var page = _a.page, classes = _a.classes, children = _a.children, style = _a.style, print = _a.print;
    var isMobile = useMediaQuery({ query: "(max-device-width: 640px)" });
    var rootStyle = __assign(__assign({}, take(page.composedStyleObj, [
        "background",
        "backgroundColor",
        "backgroundImage",
        "backgroundPosition",
        "backgroundRepeat",
        "backgroundSize",
        "color",
        "padding",
    ])), style);
    if (rootStyle.padding) {
        rootStyle.paddingBottom = "calc(" + rootStyle.paddingBottom + " + 60px)";
    }
    // rootStyle.minHeight = "100vh";
    // console.clear;
    // isMobile ? (rootStyle.minHeight = "unset") : (rootStyle.minHeight = print ? "unset" : "100vh");
    // mobile check here
    // rootStyle.color = rootStyle.color + " !important";
    // rootStyle.position = "relative";
    var containerStyle = take(page.composedStyleObj, ["maxWidth"]);
    containerStyle.maxWidth =
        typeof containerStyle.maxWidth === "string"
            ? containerStyle.maxWidth
            : containerStyle.maxWidth + "px";
    containerStyle.maxWidth = "calc(" + containerStyle.maxWidth + " + 5vw)";
    containerStyle.margin = 0;
    // containerStyle.justifyContent = "unset";
    // containerStyle.borderBottom = "solid 10px purple";
    // if (process.env.REACT_APP_ENVIRONMENT !== "management") {
    //   containerStyle.display = "flex";
    //   containerStyle.flexDirection = "column";
    //   containerStyle.justifyContent = "center";
    // }
    containerStyle.height = print ? "auto" : "100%";
    // overrule the colors and use the color set on the page.
    // let overRuleColor = false;
    // if (rootStyle.color && Array.isArray(children)) {
    //   children.map(function (item: any) {
    //     if (item && item["props"] && item["props"]["elements"]) {
    //       const elements = item["props"]["elements"];
    //       elements.map(function (elem: any) {
    //         if (elem && elem["props"]) {
    //           elem["props"]["customStyle"] = { color: rootStyle.color };
    //         }
    //       });
    //     }
    //   });
    // }
    if (print) {
        return React.createElement(React.Fragment, null, children);
    }
    return (React.createElement("article", { className: classes.root, style: rootStyle, ref: function (elem) {
            if (elem) {
                if (window.document.documentMode) {
                    setTimeout(function () {
                        if (print) {
                        }
                        else {
                            elem.style.height = elem.clientHeight + "px";
                        }
                    }, 500);
                }
            }
        } },
        React.createElement("div", { className: classes.container, style: containerStyle }, children)));
}));
//# sourceMappingURL=page-container.js.map