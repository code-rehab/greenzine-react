import * as React from "react";
import { withStyles, Typography, Theme } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";
import { SlideFade } from "./effects/slide-fade";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";

interface OwnProps extends WithStyles<"root" | "imageContainer" | "image" | "nextArticle" | "article" | "arrow" | "icon"> {
  thumbnail: any;
  children?: any;
  onClick?: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    // margin: theme.spacing(2, 2, 2, 2),
    display: "flex",
    justifyContent: "center",
    height: "100px",
    backgroundColor: "#0000",
    border: "none",
    cursor: "pointer",
    position: "relative",
    alignItems: "center",
    "&:hover": {
      "& $arrow": {
        transform: "translate(-50%, 50%) scale(1.2)",
      },
    },
    [theme.breakpoints.down("xs")]: {
      marginRight: "auto",
    },
  },
  imageContainer: {
    borderRadius: "100%",
    height: "100px",
    width: "100px",
    overflow: "hidden",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    marginRight: theme.spacing(1),
    [theme.breakpoints.down("xs")]: {
      height: "70px",
      width: "70px",
      marginRight: theme.spacing(2),
    },
  },
  image: {
    height: "100%",
    width: "auto",
  },

  nextArticle:{
    fontFamily: "Montserrat !important",
    [theme.breakpoints.down("sm")]: {
      maxWidth: 200
    },
  },
  article:{
    fontFamily: "Montserrat !important"
  }
});

export const ArticleRedirect = withStyles(styles)(({ children, thumbnail, classes, onClick }: OwnProps) => (
  <SlideFade delay={900} direction={"up"} >
    <div className={classes.root} onClick={onClick}  id="next-article">
      <div className={classes.imageContainer}>
        <img src={thumbnail} className={classes.image} alt="" />
      </div>

      {children && (
        <div className={classes.nextArticle}>
          <Typography style={{fontFamily: "Montserrat"}}>
            <strong>Volgend artikel:</strong>
          </Typography>
          <Typography style={{ lineHeight: 1, fontFamily: "Montserrat"}}>
            <small>{children}</small>
          </Typography>
        </div>
      )}
    </div>
  </SlideFade>
));
