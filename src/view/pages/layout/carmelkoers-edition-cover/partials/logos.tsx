import * as React from "react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { EditionBall } from "../../../../content/components/edition-ball";
import { Theme, WithStyles, StyleRules, withStyles, Grid, Typography } from "@material-ui/core";
import { observer } from "mobx-react";

type OwnProps = { logo1: string; logo2: string; editionNr: string } & WithStyles<"root" | "logo1" | "logo2">;

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(4, 0)
    }
  },
  logo1: {
    display: "inline-block",
    "@media all and (-ms-high-contrast:none)": {
      width: 600
    },
    "& img": {
      marginBottom: 20,
      [theme.breakpoints.down("sm")]: {
        width: 120,
        paddingRight: 25
      }
    },
    [theme.breakpoints.down("sm")]: {
      maxWidth: "160px"
    }
  },
  logo2: {
    [theme.breakpoints.down("md")]: {
      width: "15vw"
    }
  }
});

export const BondigEditionCoverLogos = withStyles(styles)(
  observer(({ classes, logo1, logo2, editionNr }: OwnProps) => {
    return (
      <header className={classes.root}>
        <SlideFade direction="up" timeout={900}>
          <div className={classes.logo1}>
            <Grid container spacing={0} alignItems="center">
              <Grid item sm={true} style={{ position: "relative" }}>
                <img src={logo1 || ""} alt="Bondig" />
                <div style={{ position: "absolute", top: -5, right: -5 }}>
                  <EditionBall>nr. {editionNr || ""}</EditionBall>
                </div>
              </Grid>
              <Grid item sm={true}>
                <Typography variant="body1" style={{ maxWidth: 300, lineHeight: 1, marginLeft: 5, marginTop: -12 }}>
                  digitaal magazine <br /> voor leden van de NCF
                </Typography>
              </Grid>
            </Grid>
          </div>
        </SlideFade>

        <SlideFade direction="up" timeout={900}>
          <img className={classes.logo2} src={logo2} alt="Logo" />
        </SlideFade>
      </header>
    );
  })
);
