import {
  ArticleInteractor,
  DefaultArticleInteractor
} from "../data/article/article-interactor";
import {
  DefaultMagazineInteractor,
  MagazineInteractor
} from "../data/magazine/magazine-interactor";
import { BaseModule } from "../module";
import { BusinessInteractors, BusinessProviders } from "./business";
import { PrintInteractor } from "./interactor/print-interactor";

export class DefaultBusinessInteractors
  extends BaseModule<{}, BusinessInteractors, {}>
  implements BusinessInteractors {
  constructor(private _providers: BusinessProviders) {
    super();
  }
  public get magazine(): MagazineInteractor {
    return this.loadInteractor(
      "magazine",
      DefaultMagazineInteractor,
      this._providers.magazine
    );
  }

  public get article(): ArticleInteractor {
    return this.loadInteractor(
      "article",
      DefaultArticleInteractor,
      this._providers.article,
      this.magazine
    );
  }

  public get print(): PrintInteractor {
    return this.loadInteractor("print", PrintInteractor);
  }
}
