import * as React from "react";
import { withStyles } from "@material-ui/core";
var styles = function (theme) { return ({
    root: {
        backgroundColor: "#fff",
        padding: theme.spacing(2)
    },
}); };
export var Popup = withStyles(styles)(function (_a) {
    var classes = _a.classes, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement("div", { className: classes.root }, "Popup here.."));
});
//# sourceMappingURL=popup.js.map