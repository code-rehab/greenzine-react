import { UserFragment } from "./fragments";
export var UserCreated = "\nsubscription {\n  userCreated {\n    " + UserFragment + "\n  }\n}\n";
export var UserUpdated = "\nsubscription {\n  userUpdated {\n    " + UserFragment + "\n  }\n}\n";
export var UserDeleted = "\nsubscription {\n  userDeleted {\n    " + UserFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map