import { computed, observable, toJS } from "mobx";
import * as React from "react";
import { Layout } from "react-grid-layout";
import { Article } from "../../../../application/data/article/article";
import { IPresenter } from "../../../../helpers/with-presenter";

import { Page } from "../../../../application/data/page/page";
import { v4 as uuid } from "uuid";
import { removeNullValues } from "../../../../application/network/helpers";

function replaceAll(str: string, find: string, replace: string) {
  return str && str.replace(new RegExp(find, "g"), replace);
}
export class GridPresenter implements IPresenter {
  @observable public rowHeight: number = 12;
  @observable public margin: [number, number] = [24, 0];
  @observable public cols: number = 24;
  @observable private _refs: Record<string, { height: number }> = {};
  @observable public isPublishing: boolean = false;
  @observable public pageEditing: boolean = false;
  private _resizeTimeout: any;
  private _tmpRefs: Record<string, { id: string; ref: HTMLDivElement }> = {};
  @observable public gridWidth: string;

  constructor(public article: Article, public page: Page) {
    //
  }

  @computed public get layout(): Layout[] {
    return removeNullValues(
      toJS((this.page.layouts && this.page.layouts.lg) || []).map((l: any) => {
        const h =
          Math.ceil(
            this._refs[l.i] &&
              this._refs[l.i].height &&
              this._refs[l.i].height / (this.rowHeight + this.margin[1])
          ) || 2;

        l.h = h;
        l.minW = 2;
        l.w = l.w < 2 ? this.cols : l.w;

        return l;
      })
    );
  }

  @computed public get style(): React.CSSProperties {
    const distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];

    let articleStyleStr = replaceAll(
      this.article.style || "{}",
      "/article/images/",
      distribution + "/ncf/bondig/"
    );
    let pageStyleStr = replaceAll(
      this.page.style || "{}",
      "/article/images/",
      distribution + "/ncf/bondig/"
    );

    const articleStyle = JSON.parse(articleStyleStr);
    const pageStyle = JSON.parse(pageStyleStr);
    const styles = { ...articleStyle, ...pageStyle };

    if (pageStyle.maxWidth) {
      this.gridWidth = pageStyle.maxWidth;
    }
    delete styles.maxWidth;
    delete styles.backgroundColor;
    delete styles.background;
    delete styles.color;

    return styles;
  }
  
  
  mount = () => { 

  

    this.updateElementSizes();
    window.addEventListener("resize", () => {
      clearTimeout(this._resizeTimeout);
      this._resizeTimeout = setTimeout(this.updateElementSizes, 500);
    });
  };

  unmount = () => {
    //
  };

  private updateElementSizes = () => {
    setTimeout(() => {
      const _refs: Record<string, any> = Object.keys(this._tmpRefs).reduce(
        (refs: any, key: any) => {
          const item = this._tmpRefs[key];
          refs[item.id] = { height: (item.ref && item.ref.clientHeight) || 0 };
          return refs;
        },
        {}
      );

      this._refs = {
        ...this._refs,
        ..._refs,
      };
    }, 200);
  };

  public registerRef = (id: string, ref: HTMLDivElement | null) => {
    if (ref) {
      this._tmpRefs[id] = { id, ref };
    }
  };
}
