import { UserFragment } from "./fragments";

export const UserCreated = `
subscription {
  userCreated {
    ${UserFragment}
  }
}
`;

export const UserUpdated = `
subscription {
  userUpdated {
    ${UserFragment}
  }
}
`;

export const UserDeleted = `
subscription {
  userDeleted {
    ${UserFragment}
  }
}
`;
