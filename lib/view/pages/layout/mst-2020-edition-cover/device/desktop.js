import * as React from "react";
import { withStyles, Typography, Button } from "@material-ui/core";
import { observer } from "mobx-react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g;
    return ({
        root: (_a = {
                position: "relative"
            },
            _a[theme.breakpoints.up("lg")] = {
                fontSize: "1.4em",
                padding: "5vw",
            },
            _a[theme.breakpoints.up("md")] = {
                width: "100%",
                padding: "0 72px",
                display: "flex",
                flexDirection: "column",
            },
            _a),
        backgroundShape: {
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            "&:after": {
                content: "' '",
                display: "block",
                position: "fixed",
                backgroundImage: "url('https://d6j399hnl3eyg.cloudfront.net/mst2020/COVER-Group 1629-def.jpg')",
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: -1,
            },
        },
        logo: (_b = {
                position: "absolute",
                right: 0,
                top: 0,
                maxWidth: 240
            },
            _b[theme.breakpoints.down("md")] = {
                maxWidth: 220,
                right: 40,
            },
            _b),
        title: (_c = {
                position: "relative",
                marginBottom: theme.spacing(2),
                color: "#FFF",
                fontFamily: "Poppins",
                fontSize: 160,
                lineHeight: "88%",
                zIndex: 2
            },
            _c[theme.breakpoints.down("md")] = {
                paddingTop: 15,
                fontSize: 60,
                "& br": {
                    display: "none",
                },
            },
            _c),
        subtitle: (_d = {
                marginBottom: theme.spacing(2),
                color: "#FFF",
                maxWidth: 500,
                fontWeight: 700,
                fontSize: 19
            },
            _d[theme.breakpoints.down("md")] = {
                fontSize: 16,
            },
            _d),
        buttonReadMore: {
            color: "#fff",
            backgroundColor: "#3DBCBD",
            borderRadius: 0,
            border: 0,
            maxWidth: 185,
            marginBottom: theme.spacing(3),
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 15,
            padding: "7px 12px",
            "&:hover": {
                backgroundColor: "#2aa2a2",
                color: "#fff"
            },
        },
        coverImage: (_e = {
                maxWidth: 750,
                position: "relative",
                zIndex: 0,
                height: "auto",
                marginTop: 100,
                marginLeft: -100
            },
            _e[theme.breakpoints.down("md")] = {
                position: "relative",
                maxWidth: "80%",
                margin: "100px 10% 0 10%",
            },
            _e),
        info: (_f = {
                // backgroundColor: "green",
                display: "flex",
                margin: theme.spacing(3, 0)
            },
            _f[theme.breakpoints.down("md")] = {
                flexDirection: "column-reverse",
            },
            _f),
        content: (_g = {
                paddingLeft: theme.spacing(3),
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                paddingTop: 40
            },
            _g[theme.breakpoints.down("md")] = {
                // marginTop: 150,
                marginLeft: 60,
                paddingBottom: 50,
                paddingRight: theme.spacing(3),
            },
            _g),
    });
};
export var LayoutEdition2020CoverDesktop = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, config = _a.config, presenter = _a.presenter;
    var styles = config.styles || {};
    return (React.createElement("div", { className: classes.root },
        React.createElement("div", { className: classes.backgroundShape }),
        React.createElement(ZoomFade, { timeout: 200 },
            React.createElement("img", { className: classes.logo, src: "https://d6j399hnl3eyg.cloudfront.net/mst2020/Logo%20MST.svg", alt: "MST" })),
        React.createElement("div", { className: classes.info },
            React.createElement("div", { className: classes.content },
                React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                    React.createElement(Typography, { className: classes.title, variant: "h2", style: styles.title || {} },
                        "Jaar ",
                        React.createElement("br", null),
                        "bericht ",
                        React.createElement("br", null),
                        "2020")),
                React.createElement(SlideFade, { direction: "up", timeout: 1100 },
                    React.createElement(Typography, { className: classes.subtitle, variant: "h3", style: styles.subtitle || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.secondary } }))),
                React.createElement(SlideFade, { direction: "up", timeout: 1200 },
                    React.createElement(Button, { onClick: presenter.nextArticle, variant: "outlined", className: classes.buttonReadMore }, "Lees verder"))),
            React.createElement("div", null,
                React.createElement(ZoomFade, { timeout: 1900 },
                    React.createElement("img", { className: classes.coverImage, src: config.cover_image, alt: "", style: {} }))))));
}));
//# sourceMappingURL=desktop.js.map