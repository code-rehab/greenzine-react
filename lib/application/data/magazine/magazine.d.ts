import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { Edition, EditionData } from "../edition/edition";
export interface MagazineData {
    id: string;
    slug: string;
    title: string;
    favicon: string;
    version: string;
    content: string;
    type: string;
    config: MagazineConfig;
    description: any[];
    image: string;
    logowhite: string;
    subtitle: string;
    editors: any;
    logo: string;
    editions: EditionData[];
    contactDetails: any;
    about: any;
}
export interface MagazineConfig {
    theme?: string;
    poll?: string;
    print?: string;
    gaCode?: string;
    tenant?: string;
}
export interface Magazine extends GraphQLModel<MagazineData>, MagazineData {
    editions: Edition[];
}
export interface MagazineValues {
}
export declare class MagazineModel extends GraphQLBase<MagazineData> implements Magazine {
    title: string;
    favicon: string;
    content: string;
    type: string;
    config: {};
    index: string;
    slug: string;
    version: string;
    image: string;
    logowhite: string;
    description: never[];
    subtitle: string;
    logo: string;
    editors: {};
    contactDetails: {};
    about: {};
    protected _editionsInitialized: boolean;
    get editions(): Edition[];
}
