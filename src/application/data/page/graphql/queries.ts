import { PageFragment } from "./fragments";

export const Page = `
  query Page($id:ID!) {
    Page(id:$id){
      ${PageFragment}
    }
  }
`;

export const PageCollection = `
  query PageCollection {
    PageCollection{
      ${PageFragment}
    }
  }
`;
