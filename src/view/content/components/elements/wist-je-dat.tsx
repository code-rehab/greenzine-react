import * as React from "react";
import { Typography, Theme, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { TextBoard } from "../text-board";

const styles = (theme: Theme) => ({
  root: {
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(3, 0),
    },
  },
  secondary: {
    fontFamily: "Montserrat",
    fontWeight: 500,
  },
  articleCoverTitle: {
    "&$secondary": {
      fontWeight: 300,
    },
  },
  title: {
    fontWeight: 500,
  },
  italic: {
    fontStyle: "italic",
  },
  strong: {
    fontWeight: 700,
  },
  fatHeader: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    marginBottom: "1em",
    paddingBottom: "1em",
    borderBottom: "solid 5px",
  },
});

export const WistJeDat = withStyles(styles)((props: any) => {
  const animation = props.animation || { duration: 0, delay: 0 };

  return (
    <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
      <div className={props.classes.root}>
        <TextBoard
          image="/article/images/vent-turquoise.png"
          color={props.stepBackground}
          titleColor={props.stepColor}
          title={
            <>
              <Typography variant="h5">{props.step}</Typography>
            </>
          }
          subtitle={
            <Typography variant="h5" style={{ fontStyle: "italic" }}>
              <span dangerouslySetInnerHTML={{ __html: props.title }} />
            </Typography>
          }
        >
          <Typography>
            <span dangerouslySetInnerHTML={{ __html: props.description }} />
          </Typography>
        </TextBoard>
      </div>
    </SlideFade>
  );
});
