var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Button, Collapse, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import classnames from "classnames";
import * as React from "react";
import { RenderWhile } from "../components/render-while";
var styles = function (theme) { return ({
    root: {
        margin: "auto",
    },
    drawerBackdrop: {},
    header: {},
    content: {},
    backdrop: {
        backgroundColor: "transparant",
    },
    collapseConainer: {
        marginBottom: theme.spacing(3),
        position: "relative",
        "&:after": {
            content: "''",
            display: "block",
            position: "absolute",
            left: 0,
            top: 0,
            width: "100%",
            height: "100%",
            boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
        },
        "&.MuiCollapse-entered": {
            "&:after": {
                boxShadow: "none",
            },
        },
    },
    contentPiece: {
        fontFamily: "Montserrat",
    },
    marginBottom: {
        marginBottom: "1em",
    },
}); };
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement(React.Fragment, null, Array.isArray(description) ? (description.map(function (contentPiece) {
        return (React.createElement(Typography, { style: __assign({}, contentPiece.style), className: classnames(classes.contentPiece, contentPiece.marginBottom && classes.marginBottom), paragraph: true }, contentPiece.element ? (React.createElement(contentPiece.element, { style: __assign({}, contentPiece.style) },
            React.createElement("span", { dangerouslySetInnerHTML: { __html: contentPiece.content } }))) : (contentPiece.content)));
    })) : (React.createElement(Typography, null, description))));
};
var EditionIndexInfoComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(Typography, { className: classnames(classes.montserrat, classes.title), variant: "h4", color: "primary", style: { marginBottom: "1em", lineHeight: 1 } }, edition.subtitle),
            React.createElement(RenderWhile, { tablet: true, desktop: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })),
            React.createElement(RenderWhile, { mobile: true },
                React.createElement(Collapse, { in: open, collapsedHeight: 120, classes: { wrapper: classes.collapseConainer } },
                    React.createElement(Info, { edition: edition, classes: classes })),
                React.createElement(Button, { color: "primary", variant: "contained", style: { display: "flex", marginLeft: "auto" }, onClick: function () { return setOpen(!open); } }, open ? "Verberg" : "Lees meer")))));
};
export var EditionIndexInfo = withStyles(styles)(EditionIndexInfoComponent);
//# sourceMappingURL=edition-index-info.js.map