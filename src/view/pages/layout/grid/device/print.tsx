import * as React from "react";
import { RenderElement } from "../../../../content/components/renderElement";
import { Grid } from "@material-ui/core";
import { Element } from "../../../../../application/data/element/element";

export const LayoutColumnsPrint = ({ data }: any) => {
  if (data.container.print === true) {
    return (
      <Grid container spacing={1}>
        {data.sections.map((cfg: any) => {
          return (
            <Grid item xs={12} {...(cfg.props || {})}>
              {cfg.data.map((d: Element) => {
                return <RenderElement element={d} />;
              })}
            </Grid>
          );
        })}
      </Grid>
    );
  }

  return data.sections.map((cfg: any) => {
    return cfg.data.map((d: any) => <RenderElement element={d} />);
  });
};
