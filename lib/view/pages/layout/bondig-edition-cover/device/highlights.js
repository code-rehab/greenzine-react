import { makeStyles } from "@material-ui/core";
import * as React from "react";
import { RenderWhile } from "../../../../content/components/render-while";
import { Carousel } from "../../../../interface/partials/carousel";
import { ArticleButton } from "./article-button";
var useStyles = makeStyles(function () { return ({
    root: {
        display: "flex",
        maxWidth: "75vw",
    },
    articleWrapper: {
        // flex: 1,
        padding: "0 20px 0 10px",
        borderLeft: "1px solid #FFC583",
    },
}); });
export var EditionHighlights = function (_a) {
    var articles = _a.articles;
    var classes = useStyles();
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { mobile: true },
            React.createElement(Carousel, { color: "#FFC583", data: articles })),
        React.createElement(RenderWhile, { tablet: true, desktop: true, print: true },
            React.createElement("div", { className: classes.root }, articles.map(function (article) { return (React.createElement("div", { key: article.title, className: classes.articleWrapper },
                React.createElement(ArticleButton, { id: article.link, category: article.title, title: article.content }))); })))));
};
//# sourceMappingURL=highlights.js.map