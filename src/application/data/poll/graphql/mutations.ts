import { PollFragment } from "./fragments";

export const CreatePoll = `
mutation CreatePoll($input:inputPoll!) {
  createPoll(input:$input){
    ${PollFragment}
  }
}`;

export const UpdatePoll = `
mutation updatePoll($input:inputPoll!) {
  updatePoll(input:$input) {
    ${PollFragment}
  }
}
`;

export const DeletePoll = `
mutation DeletePoll($id:ID!) {
  deletePoll(id:$id){
    ${PollFragment}
  }
}
`;
