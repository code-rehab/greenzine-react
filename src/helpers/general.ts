export function replaceAll(str: string, find: string, replace: string) {
  return str && str.replace(new RegExp(find, "g"), replace);
}

export function take<TRecord>(
  obj: TRecord,
  keys: Array<keyof TRecord>,
  returnUndefinedValues: boolean = false
) {
  return keys.reduce((result, key) => {
    if (obj[key] || returnUndefinedValues) {
      result[key] = obj[key];
    }
    return result;
  }, {} as Partial<TRecord>);
}
