import * as React from "react";
import { Theme, withStyles } from "@material-ui/core";
import classNames from "classnames";
import { SlideFade } from "../effects/slide-fade";

export const Divider = withStyles((theme: Theme) => ({
  line: {
    borderBottom: "5px solid",
    margin: theme.spacing(3, 0)
  }
}))(({ classes, variant, animation, styles }: any) => {
  animation = animation || { duration: 0, delay: 0 };

  return (
    <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
      <div className={classNames(classes[variant])} style={styles || {}} />
    </SlideFade>
  );
});
