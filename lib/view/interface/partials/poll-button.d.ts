import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { InterfacePresenter } from "../interface-presenter";
interface OwnProps extends WithStyles<"root" | "montserrat" | "icon" | "open"> {
    presenter: InterfacePresenter;
}
export declare const PollButton: React.ComponentType<Pick<OwnProps, "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
