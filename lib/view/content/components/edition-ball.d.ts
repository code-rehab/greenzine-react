import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<"root" | "text"> {
    children?: any;
}
export declare const EditionBall: React.ComponentType<Pick<OwnProps, "children"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
