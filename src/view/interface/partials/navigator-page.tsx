import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp, faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { observer } from "mobx-react";
import { InterfacePresenter } from "../interface-presenter";
import { StyleRules, withStyles, WithStyles, Theme } from "@material-ui/core";
import classNames from "classnames";
import { SlideFade } from "../../content/components/effects/slide-fade";
import { mapEvent } from "../../../helpers/formatters";
import { RenderWhile } from "../../content/components/render-while";

interface OwnProps
  extends WithStyles<"root" | "dot" | "active" | "button" | "visible" | "rotatedRight" | "rotatedLeft"> {
  presenter: InterfacePresenter;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "fixed",
    right: 0,
    top: 0,
    width: theme.spacing(8),
    padding: theme.spacing(1),
    bottom: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    textAlign: "center",
  },
  dot: {
    margin: theme.spacing(0.8) + "px auto",
    width: 16,
    height: 16,
    borderRadius: "100%",
    border: "1px solid",
    position: "relative",
    transition: "all 0.3s, transform 0.2s",
    cursor: "pointer",

    "&:before": {
      transition: "opacity 0.3s",
      borderRadius: "100%",
      content: "' '",
      background: "currentColor",
      opacity: 0,
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },

    "&:hover": {
      "&:before": {
        opacity: 0.2,
      },
    },

    "&:after": {
      content: "' '",
      position: "absolute",
      top: 0,
      left: 0,
      background: "#000",
      border: "5px solid",
      borderRadius: "100%",
      margin: 2,
      opacity: 0,
      transition: "opacity 0.9s",
    },
  },
  active: {
    "&:after": {
      opacity: 1,
    },
  },
  button: {
    position: "relative",
    overflow: "hidden",
    margin: theme.spacing(1.5) + "px auto",
    visibility: "hidden",
    opacity: 0,
    border: "1px solid",
    borderRadius: "100%",
    width: theme.spacing(4),
    height: theme.spacing(4),
    transform: "rotate(0)",
    transition: "color 0.9s, border-width 0.3s, transform 0.3s",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",

    "&:before": {
      transition: "opacity 0.3s",
      borderRadius: "100%",
      content: "' '",
      background: "currentColor",
      opacity: 0,
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    "&:hover": {
      // background: "currentColor"
      "&:before": {
        opacity: 0.2,
      },
      // borderWidth: 10
    },
  },
  visible: { visibility: "visible", opacity: 1 },
  rotatedRight: { transform: "rotate(-90deg)" },
  rotatedLeft: { transform: "rotate(-90deg)" },
});

export const PageNavigator = withStyles(styles)(
  observer(({ presenter, classes }: OwnProps) => {
    // if (!presenter.isArticle || presenter.pages.length <= 1) {
    //   return null;
    // }

    const curPage = presenter.page && presenter.page.id;
    const styles = (presenter.article && presenter.article.style && JSON.parse(presenter.article.style)) || {};
    let pageStyles = (presenter.page && presenter.page.style && JSON.parse(presenter.page.style)) || styles;

    const onPrevious =
      presenter.hasPreviousArticle && !presenter.hasPreviousPage
        ? presenter.toPreviousArticle
        : presenter.toPreviousPage;
    const onNext = presenter.hasNextArticle && !presenter.hasNextPage ? presenter.toNextArticle : presenter.toNextPage;

    let delay = 900;

    return (
      <RenderWhile tablet desktop>
        <div className={classes.root} key={presenter.article && presenter.article.id}>
          <div style={{ color: pageStyles.color }}>
            <SlideFade timeout={200} delay={(delay += 50)} direction={"down"}>
              <div>
                <div
                  style={{}}
                  className={classNames(
                    classes.button,
                    (presenter.hasPreviousPage && classes.visible) ||
                      (presenter.hasPreviousArticle && [classes.visible, classes.rotatedLeft])
                  )}
                  onClick={onPrevious}
                >
                  <FontAwesomeIcon icon={faChevronUp} />
                </div>
              </div>
            </SlideFade>

            {presenter.pages.length > 1 &&
              process.env.REACT_APP_SHOW_DOTS == "true" &&
              presenter.pages.map((page) => (
                <SlideFade timeout={200} delay={(delay += 50)} direction={"down"}>
                  <div key={page.id} onClick={mapEvent(presenter.selectPage, page.id)}>
                    <div className={classNames(classes.dot, curPage === page.id && classes.active)}></div>
                  </div>
                </SlideFade>
              ))}

            <SlideFade timeout={200} delay={(delay += 50)} direction={"down"}>
              <div>
                <div
                  style={{}}
                  className={classNames(
                    classes.button,
                    (presenter.hasNextPage && classes.visible) ||
                      (presenter.hasNextArticle && [classes.visible, classes.rotatedRight])
                  )}
                  onClick={onNext}
                >
                  <FontAwesomeIcon icon={faChevronDown} />
                </div>
              </div>
            </SlideFade>
          </div>
        </div>
      </RenderWhile>
    );
  })
);
