import * as React from "react";
import { Typography, Button, Theme, WithStyles, Grid } from "@material-ui/core";
import { withStyles, StyleRules } from "@material-ui/styles";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { mapEvent } from "../../../../../helpers/formatters";
import classnames from "classnames";

const styles = (theme: Theme): StyleRules => ({
  root: {
    maxWidth: 900
  },
  montserrat: {
    fontFamily: "Montserrat",
    fontWeight: 500
  },
  barRoot: {
    backgroundColor: "#fff5",
    height: 6
  },
  bar: {
    backgroundColor: "white"
  },
  question: {
    // maxWidth: 550
  },
  answer: {
    // minWidth: "100%",
    // width: "max-content"
  },
  selected: {
    backgroundColor: "#fff",
    color: "#005555",
    borderColor: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#005555",
      borderColor: "#fff"
    }
  },
  radio: {
    display: "flex",
    width: 17,
    height: 17,
    color: "white",
    border: "1px solid #fff",
    borderRadius: "100%",
    padding: 3,
    "& > span": {
      display: "block",
      borderRadius: "100%",
      flex: 1,
      background: "#fff"
    }
  },
  answers: {},
  buttonBefore: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    left: 0,
    top: 0,
    width: theme.spacing(3),
    height: "100%",
    position: "absolute"
  }
});

interface OwnProps extends WithStyles<any> {
  options: Array<string | number | boolean>;
  answers: Array<string | number | boolean>;
  multiselect?: boolean;
  onChange?(value: Array<string | number | boolean>): void;
}

@observer
class MultipleChoiceComponent extends React.Component<OwnProps> {
  @observable public value: string[] = [];

  private _onSelectHandler = (answer: string) => {
    const { multiselect, onChange, answers } = this.props;
    const index = answers.indexOf(answer);

    if (multiselect) {
      if (index >= 0) {
        answers.splice(index, 1);
      } else {
        answers.push(answer);
      }

      if (onChange) {
        onChange(answers);
      }
    } else {
      if (onChange) {
        onChange([answer]);
      }
    }
  };

  render() {
    const { classes, options, multiselect, answers } = this.props;

    return (
      <>
        <Grid spacing={2} container className={classes.root}>
          <Grid item xs={12}>
            <div className={classes.question}>
              {multiselect && (
                <Typography className={classes.montserrat} variant="subtitle2" align="center" paragraph>
                  Meerdere opties mogelijk
                </Typography>
              )}
            </div>
          </Grid>
          {options.map((answer: string | number | boolean, index: number) => {
            const selected = answers.indexOf(answer.toString()) > -1;

            return (
              <Grid item xs={12} md={6}>
                <Button
                  key={index}
                  disableRipple
                  className={classnames(classes.answer, !multiselect && selected && classes.selected)}
                  color="inherit"
                  variant="outlined"
                  fullWidth
                  onClick={mapEvent(this._onSelectHandler, answer.toString())}
                  classes={{ text: classes.buttonText }}
                >
                  <div className={classes.buttonBefore}>
                    {multiselect && <span className={classes.radio}>{selected && <span />}</span>}
                  </div>
                  {answer}
                </Button>
              </Grid>
            );
          })}
        </Grid>
      </>
    );
  }
}

export const MultipleChoice = withStyles(styles)(MultipleChoiceComponent);
