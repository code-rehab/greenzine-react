import * as React from "react";
import { Theme, Typography, withStyles, WithStyles, StyleRules } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    flexDirection: "column" as "column",
    paddingBottom: theme.spacing(2),
    width: "100%"
  },
  videoWrapper: {
    position: "relative",
    paddingBottom: "56.25%",
    marginBottom: 15,
    height: 0,
    width: "100%",
    zIndex: 1
  },
  video: {
    marginTop: theme.spacing(2),
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "#000",
    border: "none"
  },
  caption: {
    marginTop: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    lineHeight: "2em",
    color: "inherit",
    position: "relative" as "relative",
    "&::before": {
      content: "''",
      left: "10px",
      top: "-24px",
      zIndex: 2,
      width: 1,
      height: 36,
      backgroundColor: "currentColor",
      position: "absolute" as "absolute"
    }
  }
});

export const Video = withStyles(styles)(({ classes, embed, caption, maxWidth, animation }: any) => {
  animation = animation || { duration: 0, delay: 0 };

  return (
    <div className={classes.root} style={{ maxWidth }}>
      <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
        <div className={classes.videoWrapper}>
          <iframe title="video" src={embed} className={classes.video} />
        </div>
        {caption ? (
          <Typography variant="caption" className={classes.caption}>
            {caption}
          </Typography>
        ) : (
          ""
        )}
      </SlideFade>
    </div>
  );
});
