import * as React from "react";
import { RenderElement } from "../../../../content/components/renderElement";
export var ArticleCoverPrint = function (_a) {
    var data = _a.data;
    return data.map(function (d) { return React.createElement(RenderElement, { element: d }); });
};
//# sourceMappingURL=print.js.map