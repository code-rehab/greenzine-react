import { EditionFragment } from "./fragments";
export var CreateEdition = "\nmutation CreateEdition($input:inputEdition!) {\ncreateEdition(input:$input){\n  " + EditionFragment + "\n}\n}\n";
export var UpdateEdition = "\nmutation updateEdition($input:inputEdition!) {\n  updateEdition(input:$input) {\n    " + EditionFragment + "\n  }\n}\n";
export var DeleteEdition = "\nmutation DeleteEdition($id:ID!) {\n  deleteEdition(id:$id){\n    " + EditionFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map