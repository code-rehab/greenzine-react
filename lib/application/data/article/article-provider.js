var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider } from "../../network/graphql-provider";
import { ArticleModel } from "./article";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
var DefaultArticleProvider = /** @class */ (function (_super) {
    __extends(DefaultArticleProvider, _super);
    function DefaultArticleProvider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = ArticleModel;
        _this.listOperation = function () {
            return graphqlOperation(Query.ArticleCollection, {});
        };
        _this.createOperation = function (article) {
            return graphqlOperation(Mutation.CreateArticle, { input: article });
        };
        _this.fetchOperation = function (article) {
            return graphqlOperation(Query.Article, { id: article.id });
        };
        _this.updateOperation = function (article) {
            return graphqlOperation(Mutation.UpdateArticle, {
                input: article.serialize()
            });
        };
        _this.deleteOperation = function (article) {
            return graphqlOperation(Mutation.DeleteArticle, { id: article.id });
        };
        return _this;
    }
    return DefaultArticleProvider;
}(GraphQLProvider));
export { DefaultArticleProvider };
//# sourceMappingURL=article-provider.js.map