import * as React from "react";
import { withStyles, Typography, Grid, Button, } from "@material-ui/core";
import { observer } from "mobx-react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
import classNames from "classnames";
import { EditionHighlights } from "./highlights";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
    return ({
        root: (_a = {
                display: "flex",
                height: "calc(100vh - 80px)",
                flexDirection: "column",
                justifyContent: "center",
                position: "absolute",
                zIndex: 0
            },
            _a[theme.breakpoints.down("sm")] = {
                position: "relative"
            },
            // overlay
            _a["&:after"] = {
                content: "' '",
                display: "block",
                position: "fixed",
                background: "linear-gradient(to bottom, rgba(0,0,0,0) 50%, rgba(0,0,0,0.6));",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: 10
            },
            _a),
        // NCF logo
        ncf_logo: (_b = {
                position: "fixed",
                right: 60,
                top: 90,
                zIndex: 999
            },
            _b[theme.breakpoints.down("sm")] = {
                top: "unset",
                bottom: 90,
                right: 30,
                maxWidth: "20%"
            },
            _b),
        // Bondig Logo
        logo: (_c = {
                position: "fixed",
                left: 60,
                top: 90,
                display: "flex",
                width: 600
            },
            _c[theme.breakpoints.down("sm")] = {
                width: 300,
                left: 30
            },
            _c),
        logoText: (_d = {
                maxWidth: 300,
                lineHeight: 1,
                marginLeft: -36,
                marginTop: -5
            },
            _d[theme.breakpoints.down("sm")] = {
                position: "absolute",
                left: 0,
                top: 70,
                marginLeft: 0,
                marginTop: 0,
            },
            _d),
        // Index: edition: (55)
        index: (_e = {
                position: "absolute",
                right: 34,
                top: -34,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "100%",
                whiteSpace: "nowrap",
                color: "#e9550d",
                backgroundColor: "#FFC586",
                width: theme.spacing(4),
                height: theme.spacing(4),
                fontWeight: 700,
                fontSize: 15
            },
            _e[theme.breakpoints.down("sm")] = {
                right: -14,
                top: -40,
            },
            _e),
        // Content
        contentWrapper: (_f = {
                display: "flex",
                flexDirection: "column",
                margin: "0 60px",
                position: "relative",
                zIndex: 30
            },
            _f[theme.breakpoints.down("sm")] = {
                margin: "0",
            },
            _f),
        title: (_g = {
                marginBottom: theme.spacing(2),
                fontSize: "9em"
            },
            _g[theme.breakpoints.down("md")] = {
                fontSize: "9em",
            },
            _g[theme.breakpoints.down("sm")] = {
                fontSize: "3em",
                marginBottom: theme.spacing(1),
            },
            _g),
        subtitle: {
            fontSize: "2em",
            marginBottom: theme.spacing(2),
        },
        buttonReadMore: {
            color: "#fff",
            borderColor: "#fff",
            padding: "3px 36px",
            maxWidth: 200
        },
        // end Content
        editionHighlights: (_h = {
                position: "fixed",
                left: 60,
                bottom: 120,
                width: "50vw",
                zIndex: 20
            },
            _h[theme.breakpoints.down("sm")] = {
                left: 30,
                bottom: 80,
            },
            _h),
        // Image component
        image: (_j = {
                width: "100%",
                position: "fixed",
                right: 60,
                bottom: 60,
                maxWidth: "60vw"
            },
            _j[theme.breakpoints.down("sm")] = {
                right: 30,
                bottom: 60,
            },
            _j),
        toEdge: {
            bottom: 0,
            right: 0,
        },
    });
};
export var LayoutEditionCoverDesktop = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, config = _a.config, presenter = _a.presenter;
    var styles = config.styles || {};
    var edition = presenter && presenter.edition && presenter.edition.id;
    return (React.createElement("div", { style: { position: "relative", width: "100%" } },
        React.createElement("div", { className: classes.root },
            React.createElement(SlideFade, { direction: "up", timeout: 900 },
                React.createElement("div", { className: classes.logo },
                    React.createElement(Grid, { container: true, spacing: 0, alignItems: "center" },
                        React.createElement(Grid, { item: true, sm: true, style: { position: "relative" } },
                            React.createElement("img", { style: { maxWidth: "100%" }, src: "https://d6j399hnl3eyg.cloudfront.net/ncf/bondig/bondig-logo-white.svg", alt: "Bondig" }),
                            React.createElement("div", { className: classes.index },
                                "nr. ",
                                edition || "15")),
                        React.createElement(Grid, { item: true, sm: true },
                            React.createElement(Typography, { variant: "body1", className: classes.logoText },
                                "digitaal magazine ",
                                React.createElement("br", null),
                                " voor leden van de NCF"))))),
            React.createElement("img", { src: "https://d6j399hnl3eyg.cloudfront.net/ncf/bondig/LG+NCF+Wit.svg", className: classes.ncf_logo }),
            config.cover_image ? (React.createElement(React.Fragment, null, config.cover_image_animation == "true" ? (React.createElement(ZoomFade, { timeout: 1900 },
                React.createElement("img", { style: styles.image || {}, className: config.cover_image_style == "edge"
                        ? classNames(classes.image, classes.toEdge)
                        : classes.image, src: config.cover_image, alt: "" }))) : (React.createElement("img", { style: styles.image || {}, className: config.cover_image_position == "edge"
                    ? classNames(classes.image, classes.toEdge)
                    : classes.image, src: config.cover_image, alt: "" })))) : (""),
            React.createElement("div", { className: classes.contentWrapper },
                React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                    React.createElement(Typography, { className: classes.title, variant: "h1", style: styles.primary || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.primary } }))),
                config.head_article.secondary ? (React.createElement(SlideFade, { direction: "up", timeout: 1100 },
                    React.createElement(Typography, { className: classes.subtitle, variant: "h3", style: styles.secondary || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.secondary } })))) : (""),
                React.createElement(SlideFade, { direction: "up", timeout: 1200 },
                    React.createElement(Button, { onClick: presenter.nextArticle, variant: "outlined", className: classes.buttonReadMore }, "Bekijk artikel"))),
            React.createElement("div", { className: classes.editionHighlights },
                React.createElement(Grid, { container: true, justify: "flex-start" },
                    React.createElement(EditionHighlights, { articles: config.highlightedContent }))))));
}));
//# sourceMappingURL=desktop.js.map