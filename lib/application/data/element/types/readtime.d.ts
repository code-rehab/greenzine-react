import { ElementType, ElementBase } from "../element";
export interface ReadTimeElementProps {
    category: string;
    time?: string;
    align?: string;
}
export interface ReadTimeElement extends ElementBase {
    component: ElementType.ReadTime;
    props: ReadTimeElementProps;
}
