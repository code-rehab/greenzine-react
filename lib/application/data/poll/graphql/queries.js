import { PollFragment } from "./fragments";
export var Poll = "\nquery Poll($id:ID!) {\n  Poll(id:$id) {\n    " + PollFragment + "\n  }\n}";
export var PollCollection = "\nquery PollCollection {\n  PollCollection {\n    " + PollFragment + "\n  }\n}";
//# sourceMappingURL=queries.js.map