import * as React from "react";
import { withStyles, Typography, Button, Hidden } from "@material-ui/core";
import { observer } from "mobx-react";
import { SlideFade } from "../../../../content/components/effects/slide-fade";
import { ZoomFade } from "../../../../content/components/effects/zoom-fade";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g;
    return ({
        root: (_a = {
                position: "relative"
            },
            _a[theme.breakpoints.up("lg")] = {
                fontSize: "1.4em",
                padding: "5vw",
            },
            _a[theme.breakpoints.up("md")] = {
                width: "100%",
                padding: "0 72px",
                display: "flex",
                flexDirection: "column",
            },
            _a),
        backgroundShape: {
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            "&:after": {
                content: "' '",
                display: "block",
                position: "fixed",
                backgroundImage: "url('/article/images/mst/mst_cover_bg.svg')",
                backgroundPosition: "bottom right",
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: -1,
            },
        },
        logo: (_b = {
                position: "absolute",
                left: 0,
                top: 0,
                maxWidth: 370
            },
            _b[theme.breakpoints.down("md")] = {
                maxWidth: 220,
                left: 40,
            },
            _b),
        title: (_c = {
                position: "relative",
                marginBottom: theme.spacing(2),
                color: "#1C9B9B"
            },
            _c[theme.breakpoints.down("md")] = {
                // maxWidth: "3em",
                paddingTop: 15,
                fontSize: "2em",
                "& br": {
                    display: "none",
                },
            },
            _c["&::before"] = {
                content: '""',
                position: "absolute",
                left: -60,
                top: 10,
                width: 38,
                height: 33,
                backgroundImage: "url('/assets/images/mst/pijltje.svg')",
                backgroundSize: "contain",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                display: "block",
            },
            _c),
        subtitle: (_d = {
                marginBottom: theme.spacing(2),
                color: "#4D4D4F",
                maxWidth: 500,
                fontWeight: 700,
                fontSize: ".9em"
            },
            _d[theme.breakpoints.down("md")] = {
                fontSize: 16,
            },
            _d),
        buttonReadMore: {
            color: "#fff",
            backgroundColor: theme.palette.secondary.main,
            border: 0,
            maxWidth: 185,
            marginBottom: theme.spacing(3),
            fontFamily: "Museo",
            fontWeight: 700,
            padding: "7px 36px",
            "&:hover": {
                backgroundColor: theme.palette.secondary.light,
            },
        },
        coverImage: (_e = {
                maxWidth: 800,
                height: "auto",
                marginLeft: 100
            },
            _e[theme.breakpoints.down("md")] = {
                position: "relative",
                maxWidth: "80%",
                margin: "100px 10% 0 10%",
            },
            _e),
        info: (_f = {
                // backgroundColor: "green",
                display: "flex",
                margin: theme.spacing(3, 0)
            },
            _f[theme.breakpoints.down("md")] = {
                flexDirection: "column-reverse",
            },
            _f),
        content: (_g = {
                paddingLeft: theme.spacing(3),
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                paddingTop: 40
            },
            _g[theme.breakpoints.down("md")] = {
                // marginTop: 150,
                marginLeft: 60,
                paddingBottom: 50,
                paddingRight: theme.spacing(3),
            },
            _g),
    });
};
export var LayoutEditionCoverDesktop = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, config = _a.config, presenter = _a.presenter;
    var styles = config.styles || {};
    return (React.createElement("div", { className: classes.root },
        React.createElement("div", { className: classes.backgroundShape }),
        React.createElement(ZoomFade, { timeout: 200 },
            React.createElement("img", { className: classes.logo, src: "/article/images/mst/mst-logo.svg", alt: "MST" })),
        React.createElement("div", { className: classes.info },
            React.createElement("div", { className: classes.content },
                React.createElement(SlideFade, { direction: "up", timeout: 1000 },
                    React.createElement(Typography, { className: classes.title, variant: "h2", style: styles.title || {} },
                        "MST",
                        " ",
                        React.createElement(Hidden, { smUp: true },
                            React.createElement("br", null)),
                        "Jaarbericht 2019")),
                React.createElement(SlideFade, { direction: "up", timeout: 1100 },
                    React.createElement(Typography, { className: classes.subtitle, variant: "h3", style: styles.subtitle || {} },
                        React.createElement("span", { dangerouslySetInnerHTML: { __html: config.head_article.secondary } }))),
                React.createElement(SlideFade, { direction: "up", timeout: 1200 },
                    React.createElement(Button, { onClick: presenter.nextArticle, variant: "outlined", className: classes.buttonReadMore }, "Lees verder"))),
            React.createElement("div", null,
                React.createElement(ZoomFade, { timeout: 1900 },
                    React.createElement("img", { className: classes.coverImage, src: config.cover_image, alt: "", style: {} }))))));
}));
//# sourceMappingURL=desktop.js.map