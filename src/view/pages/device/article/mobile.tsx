import * as React from "react";
import { Theme, WithStyles } from "@material-ui/core";
import { withStyles, StyleRules } from "@material-ui/styles";
import { LayoutGrid } from "../../layout/grid/grid";
import { observer } from "mobx-react";
import { ArticlePresenter } from "../../article-presenter";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";
import { LayoutBondigEditionCover } from "../../layout/bondig-edition-cover/bondig-edition-cover";
import { ArticleRedirect } from "../../../content/components/article-redirect";
import { GridV2 } from "../../layout/grid-v2/grid";
import { take } from "../../../../helpers/general";
import { Page } from "../../../../application/data/page/page";
import { LayoutMSTEditionCover } from "../../layout/mst-edition-cover/mst-edition-cover";
import LoadingAnimation from "../../loading";
import { LayoutCarmelkoersEditionCover } from "../../layout/carmelkoers-edition-cover/carmelkoers-edition-cover";
import { LayoutCarmelkennismakingEditionCover } from "../../layout/carmelkennismaking-edition-cover/carmelkennismaking-edition-cover";
import { LayoutMSTEdition2020Cover } from "../../layout/mst-2020-edition-cover/mst-edition-cover";

const styles = (theme: Theme): StyleRules => ({
  root: {
    // minHeight: `100vh`,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(3, 3, 3, 3),
    fontSize: "calc(1rem + 0.3vw)",

    [theme.breakpoints.down("md")]: {
      fontSize: "0.5em",
    },

    [theme.breakpoints.down("sm")]: {
      fontSize: "calc(.5rem + 0.3vw)",
    },
  },
});

interface PageProps extends WithStyles<"root"> {
  page: any;
  article: any;
  style?: React.CSSProperties;
}

const Page = withStyles((theme: Theme) => ({
  root: {
    width: "100%",
  },
}))(({ page, classes, article, style }: PageProps) => {
  style = style || {};



  if (page.layouts && page.layouts.sm && page.layouts.sm.length && page.layout !== "carmelkennismaking-edition-cover") {
    return <GridV2 article={article} page={page} breakpoint="sm" style={style}></GridV2>;
  }

  const oldstyle = { ...(page.style || {}), ...(style || {}) };

  if (page.layout === "grid") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutGrid config={JSON.parse(page.layoutConfig || "{}")} data={page.data} />
      </div>
    );
  }

  if (page.layout === "bondig-edition-cover") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutBondigEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  }

  if (page.layout === "carmelkoers-edition-cover") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutCarmelkoersEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  }

  if (page.layout === "carmelkennismaking-edition-cover") {
    return (      
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutCarmelkennismakingEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  }

  if (page.layout === "mst-edition-cover") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutMSTEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  }

  if (page.layout === "mst-edition-2020-cover") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutMSTEdition2020Cover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  }

  if (page.layout === "articleCover") {
    return (
      <div className={classes.root} style={{ maxWidth: oldstyle.maxWidth }}>
        <LayoutArticleCover config={JSON.parse(page.layoutConfig || "{}")} data={page.data} style={page.style} />
      </div>
    );
  }

  // fall back to desktop
  else if (page.layouts && page.layouts.lg && page.layouts.lg.length) {
    
    return <GridV2 article={article} page={page} breakpoint="lg" style={style}></GridV2>;
  }

  return <div>Unknown layout</div>;
});

interface OwnProps extends WithStyles<"root"> {
  presenter: ArticlePresenter;
}

export const ArticleMobileContent = withStyles(styles)(
  observer(({ presenter, classes }: OwnProps) => {
    const { article } = presenter;

    if (presenter.loading) {
      return <LoadingAnimation />;
    }

    if (!presenter.currentPage || !presenter.article) {
      return <div>Page not found</div>;
    }

    const styles = JSON.parse(presenter.article.style) || {};
    const pages = [...presenter.article.pages];
    // const firstPAge = pages[0];

    return (
      <div
        style={{
          ...take(styles, ["background", "backgroundColor", "color"]),
          overflowX: "hidden",
        }}
      >
        {pages.map((page: Page, index) => {
          const pagestyles = JSON.parse(page.style || "{}") || {};
          const mobileStyle = (page.layoutConfig && JSON.parse(page.layoutConfig || "").mobileStyle) || {};

          return (
            <>
              <div
                key={page.id}
                className={
                  ((page.layout === "grid" ||
                    page.layout === "bondig-edition-cover" ||
                    page.layout === "articleCover") &&
                    classes.root) ||
                  ""
                }
                style={{
                  ...mobileStyle,
                  ...(index === 0 &&
                    take(pagestyles, [
                      "background",
                      "color",
                      "backgroundSize",
                      "backgroundPosition",
                      // "backgroundImage",
                      "backgroundRepeat",
                      "backgroundColor",
                      "alignItems",
                      "minHeight"
                    ])),
                }}
              >
                <div>
                  <Page
                    page={page}
                    article={article}
                    style={{
                      ...(index > 0 && {
                        paddingTop: 40,
                        paddingBottom: 0,
                        minHeight: "unset !important",
                        backgroundImage: "none",
                        justifyContent: "unset",
                        // backgroundColor: "inherit",
                      }),
                      ...(index === 1 &&
                        pages.length > 1 && {
                          minHeight: "unset !important",
                          paddingTop: 60,
                        }),
                      ...(index === 0 && { minHeight: "100vh" }),
                    }}
                  />

                  {index === pages.length - 1 &&
                    presenter.currentPage &&
                    presenter.currentPage.layout !== "bondig-edition-cover" &&
                    presenter.currentPage.layout !== "mst-edition-2020-cover" &&
                    presenter.currentPage.layout !== "carmelkoers-edition-cover" &&
                    presenter.currentPage.layout !== "carmelkennismaking-edition-cover" &&
                    presenter.nextArticle && (
                      <div style={{ transform: pages.length == 1 ? "translateY(-100px)" : "" }}>
                        <ArticleRedirect
                          onClick={presenter.toNextArticle}
                          thumbnail={
                            presenter.nextArticle &&
                            (presenter.nextArticle.featuredImage
                              ? presenter.nextArticle.featuredImage
                              : presenter.nextArticle.image)
                          }
                        >
                          {presenter.nextArticle && presenter.nextArticle.title}
                        </ArticleRedirect>
                      </div>
                    )}
                  {index === pages.length - 1 && pages.length > 1 && <div style={{ height: 120 }} />}
                </div>
              </div>
            </>
          );
        })}
      </div>
    );
  })
);
