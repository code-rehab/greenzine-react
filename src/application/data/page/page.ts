import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
// import { ElementModel, Element, ElementRecord } from "../element/element";
import { computed, observable } from "mobx";
import { Layout } from "react-grid-layout";
import { replaceAll, take } from "../../../helpers/general";
import { Article } from "../article/article";
import { createElement, AnyElement } from "@coderehab/greenzeen-content";
import { AnyRecord } from "dns";

export interface PageRecord {
  id: string;
  title: string;
  content?: any;
  background?: string;
  type?: string;
  filter?: string;
  overlay?: string;
  color?: string;
  layout?: string;
  layoutConfig?: any;
  style?: any;
  responsiveStyling?: any;
  data?: Partial<AnyElement>[];
  elements: string;
  slug?: string;
  layouts: {
    sm?: Layout[];
    md?: Layout[];
    lg?: Layout[];
  };
}

export interface Page extends GraphQLModel<PageRecord>, PageRecord {
  articleId: string | null;
  data: AnyElement[];
  styleObj: React.CSSProperties;
  composedStyleObj: React.CSSProperties;
  article: Article | undefined;
  elements: any;
}

export class PageModel extends GraphQLBase<PageRecord> implements Page {
  @observable public article: Article | undefined = undefined;
  public articleId: string | null = null;

  @data public id: string = "";
  @data public title: string = "";
  @data public content: any = "";
  @data public background: string = "";
  @data public type: string = "";
  @data public filter: string = "";
  @data public overlay: string = "";
  @data public color: string = "";
  @data public layout: string = "";
  @data public layoutConfig: any = "";
  @data public style: any = "";
  @data public responsiveStyling: any = "";
  @data public slug: string = "";
  @data public layouts = { lg: [], md: [], sm: [] };

  public elements: AnyElement[] = [];

  @computed public get data(): AnyElement[] {
    const elements = this.record.elements && JSON.parse(this.record.elements);

    return observable(
      (this.changes.data || elements || this.record.data || []).map((d: any) => createElement(d))
    );
  }

  public set data(data: AnyElement[]) {
    this.changes.data = data;
  }

  @computed
  public get styleObj() {
    const distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
    const style = JSON.parse(
      replaceAll(this.style || "{}", "/article/images/", distribution + "/ncf/bondig/")
    );

    style.maxWidth = style.maxWidth || 1200;
    return style;
  }

  @computed
  public get composedStyleObj(): React.CSSProperties {
    const articleStyles = (this.article && this.article.styleObj) || {};

    return {
      ...articleStyles,
      ...take(this.styleObj, Object.keys(this.styleObj)),
    };
  }
}
