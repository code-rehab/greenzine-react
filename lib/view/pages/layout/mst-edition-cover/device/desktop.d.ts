import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { MSTEditionCoverPresenter } from "../mst-edition-cover-presenter";
declare type OwnProps = {
    config: any;
    presenter: MSTEditionCoverPresenter;
} & WithStyles<"root" | "subtitle" | "title" | "highlight" | "highlightCategory" | "highlightTitle" | "highlights" | "buttonReadMore" | "coverImage" | "logo" | "info" | "content" | "backgroundShape">;
export declare const LayoutEditionCoverDesktop: React.ComponentType<Pick<OwnProps, "config" | "presenter"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
