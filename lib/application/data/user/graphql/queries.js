import { UserFragment } from "./fragments";
export var QueryUser = "\nquery User($id:ID!) {\n  User(id:$id) {\n    " + UserFragment + "\n  }\n}";
export var QueryUserCollection = "\nquery UserCollection {\n  UserCollection {\n    " + UserFragment + "\n  }\n}";
export var QueryUserDesigns = "\nquery User($id:ID!) {\n  User(id:$id) {\n    designs{\n      id\n      version\n      company\n      status\n      project\n      pdf\n    }\n  }\n}";
export var QueryUserNotifications = "\nquery User($id:ID!) {\n  User(id:$id) {\n    notifications{\n      id\n    }\n  }\n}";
//# sourceMappingURL=queries.js.map