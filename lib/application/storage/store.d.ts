export declare type RecordMapFunction<TData, T> = (data?: TData) => T;
export declare type RecordSerializeFunction<T, TData> = (instance: T) => TData;
export declare type RecordDefaultsFunction<T> = (overrides: Partial<T>) => T;
export interface Store<TData> {
    allRecords: TData[];
    deleteRecord: (id: string) => TData | undefined;
    getRecord: (id: string) => TData | undefined;
    setRecord: (id: string, data: TData) => TData;
    injectList(list: TData[]): void;
    updateRecord: (id: string, data: Partial<TData>) => TData;
    emptyStorage(): void;
}
export declare type RecordSerializers<T> = Record<string, Serializer<T>>;
export interface Serializer<T> {
    serialize: (data: T) => string;
    deserialize: (str: string) => T;
}
export declare const JSONSerializer: Serializer<any>;
export interface StorageRecord {
    id: string;
}
export declare class DefaultStore<TData extends StorageRecord> implements Store<TData> {
    private _records;
    get allRecords(): TData[];
    emptyStorage: () => void;
    getRecord(id: string): TData;
    setRecord(id: string, data: TData): TData;
    injectList(list: TData[]): void;
    updateRecord: (id: string, record: Partial<TData>) => TData;
    deleteRecord: (id: string) => TData;
}
