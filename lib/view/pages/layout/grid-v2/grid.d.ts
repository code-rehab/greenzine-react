import * as React from "react";
import { Page } from "../../../../application/data/page/page";
import { Article } from "../../../../application/data/article/article";
import { ArticlePresenter } from "../../article-presenter";
interface OwnProps {
    article: Article;
    page: Page;
    breakpoint?: "sm" | "md" | "lg";
    style?: React.CSSProperties;
    presenter?: ArticlePresenter;
    print?: boolean;
}
export declare const GridV2: ({ print, page, breakpoint, style, presenter }: OwnProps) => JSX.Element;
export {};
