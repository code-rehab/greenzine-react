import { MagazineInteractor } from "../magazine/magazine-interactor";
import { Article } from "./article";
import { Page } from "../page/page";
import { ArticleProvider } from "./article-provider";
export interface ArticleInteractor {
    articles: Article[];
    transition: any;
    selectArticle(id: string, redirect?: boolean): Promise<void>;
    selectedArticle: Article | undefined;
    selectedPage: Page | undefined;
    selectPage(id: string, redirect?: boolean): Promise<void>;
    nextPage(): void;
    previousPage(): void;
    nextArticle(): void;
    previousArticle(): void;
    pageIndex: number;
    articleIndex: number;
    pages: Page[];
    onArticleChange(func: () => void): void;
}
export declare class DefaultArticleInteractor implements ArticleInteractor {
    private _articleProvider;
    private _magazineInteractor;
    constructor(_articleProvider: ArticleProvider, _magazineInteractor: MagazineInteractor);
    created: Article | undefined;
    selectedArticle: Article | undefined;
    selectedPage: Page | undefined;
    previous: Article | undefined;
    transition: any;
    private _listeners;
    private _animating;
    get articles(): Article[];
    get articleIndex(): number;
    get pages(): Page[];
    get pageIndex(): number;
    nextArticle: () => void;
    previousArticle: () => void;
    nextPage: () => void;
    previousPage: () => void;
    selectPage: (id?: string | undefined, redirect?: boolean) => Promise<void>;
    selectArticle: (id: string, redirect?: boolean) => Promise<void>;
    onArticleChange: (func: () => void) => void;
}
