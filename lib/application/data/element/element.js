var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BaseModel } from "../../network/base-model";
import { data } from "../../network/decorators/graphql-data";
import { computed, observable } from "mobx";
import { v4 as uuid } from "uuid";
export var ElementType;
(function (ElementType) {
    ElementType["Typography"] = "Typography";
    ElementType["Quote"] = "Quote";
    ElementType["ReadTime"] = "ReadTime";
    ElementType["Video"] = "Video";
    ElementType["Image"] = "Image";
    ElementType["WistJeDat"] = "WistJeDat";
    ElementType["Divider"] = "Divider";
    ElementType["Author"] = "Author";
    ElementType["List"] = "List";
    ElementType["Popup"] = "Popup";
    ElementType["Index"] = "Index";
})(ElementType || (ElementType = {}));
var ElementModel = /** @class */ (function (_super) {
    __extends(ElementModel, _super);
    function ElementModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animation = { duration: 0, delay: 0 };
        _this.id = uuid();
        _this.section = "";
        _this.component = ElementType.Typography;
        _this.serialize = function () {
            return __assign(__assign({}, _this._values), { props: JSON.stringify(_this.props) });
        };
        return _this;
    }
    Object.defineProperty(ElementModel.prototype, "props", {
        get: function () {
            var props = this.changes.props || this.record.props;
            var distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
            if (!props || typeof props === "string") {
                props = replaceAll(props || "{}", "/article/images/", distribution + "/ncf/bondig/");
                props = observable(JSON.parse(props));
            }
            return (props || {});
        },
        set: function (props) {
            this.changes.props = props;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        data
    ], ElementModel.prototype, "id", void 0);
    __decorate([
        data
    ], ElementModel.prototype, "section", void 0);
    __decorate([
        data
    ], ElementModel.prototype, "component", void 0);
    __decorate([
        computed
    ], ElementModel.prototype, "props", null);
    return ElementModel;
}(BaseModel));
export { ElementModel };
function replaceAll(str, find, replace) {
    return str && str.replace(new RegExp(find, "g"), replace);
}
//# sourceMappingURL=element.js.map