import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<any> {
    config: any;
    data: any[];
    style?: any;
}
export declare const ArticleCoverMobile: React.ComponentType<Pick<OwnProps, "config" | "style" | "data"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
