export declare function replaceAll(str: string, find: string, replace: string): string;
export declare function take<TRecord>(obj: TRecord, keys: Array<keyof TRecord>, returnUndefinedValues?: boolean): Partial<TRecord>;
