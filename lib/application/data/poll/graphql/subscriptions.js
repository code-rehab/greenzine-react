import { PollFragment } from "./fragments";
export var PollCreated = "\nsubscription {\n  userCreated {\n    " + PollFragment + "\n  }\n}\n";
export var PollUpdated = "\nsubscription {\n  userUpdated {\n    " + PollFragment + "\n  }\n}\n";
export var PollDeleted = "\nsubscription {\n  userDeleted {\n    " + PollFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map