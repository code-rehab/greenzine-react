import * as React from "react";
import { PresenterProps, withPresenter } from "../../../../helpers/with-presenter";
import { MSTEdition2020CoverPresenter } from "./mst-edition-cover-presenter";
import { LayoutEdition2020CoverDesktop } from "./device/desktop";

const Component = ({ config, presenter }: OwnProps & PresenterProps<MSTEdition2020CoverPresenter>) => {
  return <LayoutEdition2020CoverDesktop config={config} presenter={presenter} />;
};

interface OwnProps {
  config: any;
}

export const LayoutMSTEdition2020Cover = withPresenter<MSTEdition2020CoverPresenter, OwnProps>(
  (_props, { interactor }) => new MSTEdition2020CoverPresenter(interactor.magazine, interactor.article, interactor.print),
  Component
);
