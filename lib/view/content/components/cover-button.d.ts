interface OwnProps {
    id: string;
    index: number;
    title: string;
    image: string;
    version?: string;
    onSelect(id: string): void;
}
export declare const CoverButton: ({ id, index, image, title, onSelect, version }: OwnProps) => JSX.Element;
export {};
