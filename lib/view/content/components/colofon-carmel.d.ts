import * as React from "react";
import { WithStyles } from "@material-ui/styles";
interface OwnProps extends WithStyles<"root" | "montserrat" | "small" | "text"> {
}
export declare const ColofonCarmel: React.ComponentType<Pick<OwnProps, "innerRef"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
