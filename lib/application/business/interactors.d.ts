import { ArticleInteractor } from "../data/article/article-interactor";
import { MagazineInteractor } from "../data/magazine/magazine-interactor";
import { BaseModule } from "../module";
import { BusinessInteractors, BusinessProviders } from "./business";
import { PrintInteractor } from "./interactor/print-interactor";
export declare class DefaultBusinessInteractors extends BaseModule<{}, BusinessInteractors, {}> implements BusinessInteractors {
    private _providers;
    constructor(_providers: BusinessProviders);
    get magazine(): MagazineInteractor;
    get article(): ArticleInteractor;
    get print(): PrintInteractor;
}
