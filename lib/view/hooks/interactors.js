import { Application } from "../../application/application";
export var useArticleInteractor = function () {
    return Application.business.interactor.article;
};
export var useMagazineInteractor = function () {
    return Application.business.interactor.magazine;
};
//# sourceMappingURL=interactors.js.map