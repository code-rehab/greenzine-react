import * as React from "react";
import { withStyles } from "@material-ui/core";
var styles = function (theme) { return ({
    root: {
        position: "fixed",
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        transform: "scale(1.5)",
    },
    primary: {
        stopColor: theme.palette.primary.main,
        fill: theme.palette.primary.main,
    },
}); };
var LoadingAnimation = withStyles(styles)(function (_a) {
    var classes = _a.classes;
    return (React.createElement("div", { className: classes.root },
        React.createElement("svg", { width: "38", height: "38", viewBox: "0 0 38 38", xmlns: "http://www.w3.org/2000/svg" },
            React.createElement("defs", null,
                React.createElement("linearGradient", { x1: "8.042%", y1: "0%", x2: "65.682%", y2: "23.865%", id: "a" },
                    React.createElement("stop", { className: classes.primary, stopOpacity: "0", offset: "0%" }),
                    React.createElement("stop", { className: classes.primary, stopOpacity: ".631", offset: "63.146%" }),
                    React.createElement("stop", { className: classes.primary, offset: "100%" }))),
            React.createElement("g", { fill: "none", fillRule: "evenodd" },
                React.createElement("g", { transform: "translate(1 1)" },
                    React.createElement("path", { d: "M36 18c0-9.94-8.06-18-18-18", id: "Oval-2", stroke: "url(#a)", strokeWidth: "2" },
                        React.createElement("animateTransform", { attributeName: "transform", type: "rotate", from: "0 18 18", to: "360 18 18", dur: "0.9s", repeatCount: "indefinite" })),
                    React.createElement("circle", { className: classes.primary, cx: "36", cy: "18", r: "1" },
                        React.createElement("animateTransform", { attributeName: "transform", type: "rotate", from: "0 18 18", to: "360 18 18", dur: "0.9s", repeatCount: "indefinite" })))))));
});
export default LoadingAnimation;
//# sourceMappingURL=loading.js.map