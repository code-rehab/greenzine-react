import { RouteComponentProps } from "react-router";
import { BusinessModules } from "./business/business";
import { NetworkModule } from "./network/network";
export interface IApplication {
    network: NetworkModule;
    business: BusinessModules;
    router: RouteComponentProps<any> | undefined;
}
export declare const Application: IApplication;
