import * as React from "react";
import { RenderElement } from "../../../../content/components/renderElement";

export const ArticleCoverPrint = ({ data }: any) => {
    return data.map((d: any) => <RenderElement element={d} />);
};
