import { ArticleInteractor } from "../data/article/article-interactor";
import { ArticleProvider } from "../data/article/article-provider";
import { EditionProvider } from "../data/edition/edition-provider";
import { MagazineInteractor } from "../data/magazine/magazine-interactor";
import { MagazineProvider } from "../data/magazine/magazine-provider";
import { PollProvider } from "../data/poll/poll-provider";
import { BaseModule } from "../module";
import { NetworkModule } from "../network/network";
import { PrintInteractor } from "./interactor/print-interactor";
import { DefaultBusinessInteractors } from "./interactors";
import { DefaultBusinessProviders } from "./providers";
import { PageProvider } from "../data/page/page-provider";

export interface BusinessInteractors {
  magazine: MagazineInteractor;
  article: ArticleInteractor;
  print: PrintInteractor;
}

export interface BusinessProviders {
  magazine: MagazineProvider;
  edition: EditionProvider;
  article: ArticleProvider;
  page: PageProvider;
  poll: PollProvider;
}

export interface BusinessModules {
  interactor: BusinessInteractors;
  provider: BusinessProviders;
}

export class DefaultBusinessModule extends BaseModule<{}, {}, BusinessModules> implements BusinessModules {
  constructor(private _network: NetworkModule) {
    super();
  }

  public get interactor(): BusinessInteractors {
    return this.loadModule("interactor", DefaultBusinessInteractors, this.provider);
  }

  public get provider(): BusinessProviders {
    return this.loadModule("provider", DefaultBusinessProviders, this._network);
  }

  public get mockMode(): boolean {
    return false;
  }

  public get printLayout(): boolean {
    return false;
  }
}
