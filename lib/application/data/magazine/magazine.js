var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Application } from "../../application";
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase } from "../../network/graphql-model";
import { EditionModel } from "../edition/edition";
var MagazineModel = /** @class */ (function (_super) {
    __extends(MagazineModel, _super);
    function MagazineModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.title = "";
        _this.favicon = "";
        _this.content = "";
        _this.type = "";
        _this.config = {};
        _this.index = "";
        _this.slug = "";
        _this.version = "";
        _this.image = "";
        _this.logowhite = "";
        _this.description = [];
        _this.subtitle = "";
        _this.logo = "";
        _this.editors = {};
        _this.contactDetails = {};
        _this.about = {};
        _this._editionsInitialized = false;
        return _this;
    }
    Object.defineProperty(MagazineModel.prototype, "editions", {
        get: function () {
            var editionRecords = (this.record && this.record.editions) || [];
            var provider = Application.business.provider.edition;
            return editionRecords.map(function (data) { return new EditionModel(data, provider); });
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        data
    ], MagazineModel.prototype, "title", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "favicon", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "content", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "type", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "config", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "index", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "slug", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "version", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "image", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "logowhite", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "description", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "subtitle", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "logo", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "editors", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "contactDetails", void 0);
    __decorate([
        data
    ], MagazineModel.prototype, "about", void 0);
    return MagazineModel;
}(GraphQLBase));
export { MagazineModel };
//# sourceMappingURL=magazine.js.map