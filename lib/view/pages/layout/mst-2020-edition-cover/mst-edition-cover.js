import * as React from "react";
import { withPresenter } from "../../../../helpers/with-presenter";
import { MSTEdition2020CoverPresenter } from "./mst-edition-cover-presenter";
import { LayoutEdition2020CoverDesktop } from "./device/desktop";
var Component = function (_a) {
    var config = _a.config, presenter = _a.presenter;
    return React.createElement(LayoutEdition2020CoverDesktop, { config: config, presenter: presenter });
};
export var LayoutMSTEdition2020Cover = withPresenter(function (_props, _a) {
    var interactor = _a.interactor;
    return new MSTEdition2020CoverPresenter(interactor.magazine, interactor.article, interactor.print);
}, Component);
//# sourceMappingURL=mst-edition-cover.js.map