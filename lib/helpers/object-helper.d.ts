/**
 * append an empty property nested based on dot notation
 */
export declare const appendProperty: (object: any, prop: string | string[], index?: number, value?: any) => any;
