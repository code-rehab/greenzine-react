import { BaseModule } from "../module";
export interface NetworkProviders {
}
export interface NetworkModule extends NetworkProviders {
    fetch(querystring: string, variables?: Record<string, any>, useMock?: boolean): Promise<any>;
}
export declare class DefaultNetworkModule extends BaseModule<NetworkProviders, {}, {}> implements NetworkModule {
    init: () => Promise<void>;
    fetch: (querystring: string, variables?: any, useMock?: boolean) => Promise<any>;
}
