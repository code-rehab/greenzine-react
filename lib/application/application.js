var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { DefaultBusinessModule } from "./business/business";
import { BaseModule } from "./module";
import { DefaultNetworkModule } from "./network/network";
var DefaultApplication = /** @class */ (function (_super) {
    __extends(DefaultApplication, _super);
    function DefaultApplication() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.router = undefined;
        return _this;
    }
    Object.defineProperty(DefaultApplication.prototype, "network", {
        get: function () {
            return this.loadModule("network", DefaultNetworkModule);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DefaultApplication.prototype, "business", {
        get: function () {
            return this.loadModule("business", DefaultBusinessModule, this.network);
        },
        enumerable: true,
        configurable: true
    });
    return DefaultApplication;
}(BaseModule));
export var Application = new DefaultApplication();
//# sourceMappingURL=application.js.map