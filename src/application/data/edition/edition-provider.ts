import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import { Edition, EditionData, EditionModel } from "./edition";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";

export interface EditionProvider extends GraphQLProviderProps<Edition, EditionData> {}

export class DefaultEditionProvider
  extends GraphQLProvider<Edition, EditionData>
  implements EditionProvider {
  public model = EditionModel;

  protected listOperation = (variables: Record<string, any> = {}) => {
    return graphqlOperation(Query.EditionCollection, variables);
  };

  protected createOperation = (edition: EditionData) => {
    return graphqlOperation(Mutation.CreateEdition, { input: edition });
  };

  protected fetchOperation = (edition: EditionData) => {
    return graphqlOperation(Query.Edition, {
      id: edition.id,
    });
  };

  protected updateOperation = (edition: EditionData) => {
    return graphqlOperation(Mutation.UpdateEdition, { input: edition });
  };

  protected deleteOperation = (edition: EditionData) => {
    return graphqlOperation(Mutation.DeleteEdition, { id: edition.id });
  };
}
