import * as React from "react";
import { observer } from "mobx-react-lite";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { Error404Content } from "../../error-404";
import { LayoutGrid } from "../../layout/grid/grid";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";
import { GridV2 } from "../../layout/grid-v2/grid";
import { PrintContainer } from "@coderehab/greenzeen-content";
var styles = function (theme) { return ({
    root: {
        "@media screen": {
            minHeight: "100vh",
            padding: theme.spacing(4, 2, 8, 2),
            backgroundColor: "#eee",
        },
    },
    container: {
        "@media screen": {
            position: "relative",
            margin: "auto",
            maxWidth: 900,
        },
    },
    button: {
        margin: theme.spacing(0, 0, 0, 1),
        "@media print": {
            display: "none",
        },
    },
    content: {
        fontSize: "0.4rem",
        "& h1, & h2, & h3, & h4, & h5, & h6": { margin: "1em 0 0.5em 0" },
        "& p": { margin: "0.5em 0 1.2em 0", lineHeight: 1.5, fontSize: 13 },
        "& br": { display: "none" },
        "& img": { display: "none" },
        color: "black",
        "& *": {
            background: "none !important",
        },
        "@media screen": {
            padding: 48,
            backgroundColor: "#fff",
            boxShadow: theme.shadows[3],
        },
    },
    nav: {
        position: "fixed",
        padding: theme.spacing(2),
        right: 0,
        top: 0,
    },
}); };
var Page = withStyles(function (theme) { return ({
    root: {
        "& *": {
            position: "relative",
        },
    },
}); })(function (_a) {
    var page = _a.page, classes = _a.classes;
    if (page.layouts && page.layouts.lg && page.layouts.lg.length) {
        return (React.createElement(GridV2, { article: { styleObj: {} }, page: page, breakpoint: "sm", print: true, style: {
                maxWidth: "100%",
                minHeight: 0,
                padding: 0,
                paddingBottom: 0,
                backgroundImage: "none !important",
                backgroundColor: "#fff",
                color: "#000 !important",
            } }));
    }
    if (page.layout === "grid") {
        return React.createElement(LayoutGrid, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data });
    }
    if (page.layout === "bondig-edition-cover") {
        return (React.createElement("div", { className: classes.root },
            React.createElement(Typography, null, "De printfunctie van de cover is momenteel niet beschikbaar")));
    }
    if (page.layout === "articleCover") {
        return (React.createElement(LayoutArticleCover, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data, style: page.style }));
    }
    return React.createElement(React.Fragment, null, "Unknown layout");
});
export var ArticlePrintContent = withStyles(styles)(observer(function (_a) {
    var classes = _a.classes, presenter = _a.presenter;
    if (presenter.loading) {
        return null;
    }
    if (!presenter.article || !presenter.article.pages.length) {
        return React.createElement(Error404Content, null);
    }
    return (React.createElement(PrintContainer, null, presenter.article.pages.map(function (page, index) {
        return React.createElement(Page, { key: index, page: page });
    })));
}));
//# sourceMappingURL=print.js.map