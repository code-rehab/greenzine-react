var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { withStyles } from "@material-ui/core";
import classnames from "classnames";
import { RenderWhile } from "../render-while";
var styles = function (theme) { return ({
    root: {
        opacity: 0,
        transition: "opacity 0.5s ease-out, transform 0.5s ease-out",
    },
    up: {
        transform: "translate(0%, -100px)",
    },
    down: {
        transform: "translate(0%, 100px)",
    },
    left: {
        transform: "translate(-100px, 0%)",
    },
    right: {
        transform: "translate(100px, 0%)",
    },
    animate: {
        opacity: 1,
        transform: "translate(0%, 0%)",
    },
}); };
var SlideFadeComponent = /** @class */ (function (_super) {
    __extends(SlideFadeComponent, _super);
    function SlideFadeComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            play: false,
        };
        return _this;
    }
    SlideFadeComponent.prototype.componentDidMount = function () {
        var _this = this;
        setTimeout(function () {
            _this.setState({ play: true });
        }, this.props.timeout || 0);
    };
    SlideFadeComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, children = _a.children, classes = _a.classes, direction = _a.direction, delay = _a.delay;
        return (React.createElement(React.Fragment, null,
            React.createElement(RenderWhile, { mobile: true, tablet: true, desktop: true }, children &&
                React.Children.map(children, function (child) {
                    if (child) {
                        return React.cloneElement(child, {
                            style: __assign(__assign({}, (child.props.style || {})), { transitionDelay: delay + "ms" }),
                            className: classnames(classes.root, classes[direction], _this.state.play && classes.animate, child.props.className),
                        });
                    }
                    else {
                        return "";
                    }
                })),
            React.createElement(RenderWhile, { print: true }, children)));
    };
    return SlideFadeComponent;
}(React.PureComponent));
export var SlideFade = withStyles(styles)(SlideFadeComponent);
//# sourceMappingURL=slide-fade.js.map