var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { API } from "aws-amplify";
import { parse } from "graphql";
import { v4 as uuid } from "uuid";
import { DefaultStore } from "../storage/store";
import { GraphQLCollection } from "./graphql-collection";
var GraphQLProvider = /** @class */ (function (_super) {
    __extends(GraphQLProvider, _super);
    function GraphQLProvider(_network) {
        var _this = _super.call(this) || this;
        _this._network = _network;
        _this.subscribers = {};
        _this.useMock = false;
        _this.typename = "";
        _this.listOperation = function (variables) {
            throw new Error("Fetch operation not set");
        };
        _this.fetchOperation = function (_item) {
            throw new Error("Fetch operation not set");
        };
        _this.createOperation = function (_item) {
            throw new Error("Create operation not set");
        };
        _this.updateOperation = function (_item) {
            throw new Error("Update operation not set");
        };
        _this.deleteOperation = function (_item) {
            throw new Error("Delete operation not set");
        };
        _this.collect = function (items) {
            var collection = new GraphQLCollection(items, _this);
            return collection;
        };
        _this.get = function (id) {
            return _this.createInstance({ id: id });
        };
        _this.create = function (data, persist) {
            if (data === void 0) { data = {}; }
            if (persist === void 0) { persist = false; }
            var instance = _this.createInstance(data);
            instance.isNew = true;
            if (persist) {
                instance.save();
            }
            return instance;
        };
        _this.fetch = function (item) { return __awaiter(_this, void 0, void 0, function () {
            var operation;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        operation = this.fetchOperation(item);
                        return [4 /*yield*/, this.query(item, operation)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        _this.fetchList = function (variables) {
            if (variables === void 0) { variables = {}; }
            return __awaiter(_this, void 0, void 0, function () {
                var operation;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            operation = this.listOperation(variables);
                            return [4 /*yield*/, this.query(null, operation, true)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        _this.save = function (instance) { return __awaiter(_this, void 0, void 0, function () {
            var operationFunc, operation;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        operationFunc = this.createOperation;
                        operation = operationFunc(instance);
                        if (!operation) return [3 /*break*/, 2];
                        return [4 /*yield*/, API.graphql(operation)];
                    case 1:
                        _a.sent();
                        this.setRecord(instance.id, instance.serialize());
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); };
        _this.delete = function (item) { return __awaiter(_this, void 0, void 0, function () {
            var operation;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        operation = this.deleteOperation(item);
                        return [4 /*yield*/, this.query(item, operation)];
                    case 1:
                        _a.sent();
                        this.deleteRecord(item.id);
                        return [2 /*return*/];
                }
            });
        }); };
        if (_this._subscriptions) {
            _this._subscriptions.map(function (subscribe) {
                subscribe(_this);
            });
        }
        return _this;
    }
    GraphQLProvider.prototype.createInstance = function (data) {
        var instance = new this.model(__assign({ id: uuid() }, data), this);
        return instance;
    };
    GraphQLProvider.prototype.subscribe = function (key, handler) {
        this.subscribers[key] = this.subscribers[key] || [];
        this.subscribers[key].push(handler);
    };
    GraphQLProvider.prototype.update = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var operation;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        operation = this.updateOperation(item);
                        return [4 /*yield*/, this.query(item, operation)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GraphQLProvider.prototype.query = function (item, operation, expectList) {
        if (expectList === void 0) { expectList = false; }
        return __awaiter(this, void 0, void 0, function () {
            var response, parsedQuery, queryName, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._network.fetch(operation.query, operation.variables, this.useMock)];
                    case 1:
                        response = _a.sent();
                        parsedQuery = parse(operation.query.toString());
                        queryName = "";
                        if ("name" in parsedQuery.definitions[0]) {
                            queryName = (parsedQuery.definitions[0].name && parsedQuery.definitions[0].name.value) || "";
                        }
                        result = response.data && response.data[queryName];
                        if (item && !expectList) {
                            item.record = this.updateRecord(item.id, __assign({}, result));
                        }
                        if (expectList && Array.isArray(result)) {
                            this.injectList(result);
                        }
                        if (response.errors) {
                            console.error(response.errors);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return GraphQLProvider;
}(DefaultStore));
export { GraphQLProvider };
//# sourceMappingURL=graphql-provider.js.map