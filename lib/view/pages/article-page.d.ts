import { ArticlePresenter } from "./article-presenter";
export interface PageContentProps {
    presenter: ArticlePresenter;
}
export declare const ArticlePage: any;
