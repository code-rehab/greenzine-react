import { ArticleFragment } from "./fragments";
export var CreateArticle = "\nmutation CreateArticle($input:inputArticle!) {\ncreateArticle(input:$input){\n  " + ArticleFragment + "\n}\n}\n";
export var UpdateArticle = "\nmutation updateArticle($input:inputArticle!) {\n  updateArticle(input:$input) {\n    " + ArticleFragment + "\n  }\n}\n";
export var DeleteArticle = "\nmutation DeleteArticle($id:ID!) {\n  deleteArticle(id:$id){\n    " + ArticleFragment + "\n  }\n}\n";
//# sourceMappingURL=mutations.js.map