import * as React from "react";
import { Layout } from "react-grid-layout";
import { Article } from "../../../../application/data/article/article";
import { IPresenter } from "../../../../helpers/with-presenter";
import { Page } from "../../../../application/data/page/page";
export declare class GridPresenter implements IPresenter {
    article: Article;
    page: Page;
    rowHeight: number;
    margin: [number, number];
    cols: number;
    private _refs;
    isPublishing: boolean;
    pageEditing: boolean;
    private _resizeTimeout;
    private _tmpRefs;
    gridWidth: string;
    constructor(article: Article, page: Page);
    get layout(): Layout[];
    get style(): React.CSSProperties;
    mount: () => void;
    unmount: () => void;
    private updateElementSizes;
    registerRef: (id: string, ref: HTMLDivElement | null) => void;
}
