import * as React from "react";
export declare const Image: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<"image" | "caption" | "root" | "imgHover">>;
