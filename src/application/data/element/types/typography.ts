import { ElementBase, ElementType } from "../element";

export interface TypographyElementProps {
  children: string;
  gutterBottom: boolean;
  paragraph: boolean;
  variant: boolean;
  align?: string;
  font?: string;
  fontWeight?: number;
  color?: string;
  endItem?: boolean;
  endColor?: string;
}

export interface TypographyElement extends ElementBase {
  component: ElementType.Typography;
  props: TypographyElementProps;
}
