import { GraphQLOptions } from "@aws-amplify/api/lib/types";
import { DefaultStore } from "../storage/store";
import { RecordData } from "./base-model";
import { GraphQLModel } from "./graphql-model";
import { NetworkModule } from "./network";
import { Collection } from "./graphql-collection";
export declare type ModelClass<TData extends RecordData> = {
    new (...args: any[]): GraphQLModel<TData>;
};
export interface GraphQLProviderProps<T extends GraphQLModel<TData>, TData extends RecordData> {
    typename: string;
    model: ModelClass<TData>;
    get(id: string): T;
    save(instance: T): Promise<void>;
    create: (data?: Partial<TData>, persist?: boolean) => T;
    createInstance: (data: Partial<TData>) => T;
    fetch(item: T): Promise<void>;
    fetchList(variables?: Record<string, any>): Promise<void>;
    update(item: T): Promise<void>;
    delete(item: T): Promise<void>;
    subscribe(key: string, handler: (model: T) => void): void;
    collect(items?: TData[]): Collection<T>;
    injectList(list: TData[]): void;
}
export declare abstract class GraphQLProvider<T extends GraphQLModel<TData>, TData extends RecordData> extends DefaultStore<TData> {
    private _network;
    subscribers: Record<string, Array<(model: T) => void>>;
    useMock: boolean;
    typename: string;
    abstract model: ModelClass<TData>;
    constructor(_network: NetworkModule);
    protected listOperation: (variables: Record<string, any>) => GraphQLOptions;
    protected fetchOperation: (_item: T) => GraphQLOptions;
    protected createOperation: (_item: T) => GraphQLOptions;
    protected updateOperation: (_item: T) => GraphQLOptions;
    protected deleteOperation: (_item: T) => GraphQLOptions;
    collect: (items?: TData[] | undefined) => Collection<T>;
    get: (id: string) => T;
    create: (data?: Partial<TData>, persist?: boolean) => T;
    createInstance(data: Partial<TData>): T;
    subscribe(key: string, handler: (model: T) => void): void;
    fetch: (item: T) => Promise<void>;
    fetchList: (variables?: Record<string, any>) => Promise<void>;
    save: (instance: T) => Promise<void>;
    update(item: T): Promise<void>;
    delete: (item: T) => Promise<void>;
    query(item: T | null, operation: GraphQLOptions, expectList?: boolean): Promise<void>;
}
