import * as createPalette from "@material-ui/core/styles/createPalette";

declare module "@material-ui/core/styles/createPalette" {
  interface PaletteOptions {
    editionBall: {
      color: string;
      backgroundColor: string;
      fontWeight?: number;
      bottom?: number;
      top?: number;
      left?: number;
      right?: number;
    };
    quote: {
      color: string;
      backgroundColor: string;
      fontWeight?: number;
      bottom?: number;
    };
    coverButton: any;
  }

  interface Palette {
    editionBall: {
      color: string;
      backgroundColor: string;
      fontWeight?: number;
      bottom?: number;
      top?: number;
      left?: number;
      right?: number;
    };
    quote: {
      color: string;
      backgroundColor: string;
      fontWeight?: number;
      bottom?: number;
    };
    coverButton: any;
  }
}
