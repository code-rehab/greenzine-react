import * as React from "react";
import { withStyles, Theme } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";
import classnames from "classnames";
import { RenderWhile } from "../render-while";

interface OwnProps extends WithStyles<"root" | "up" | "down" | "left" | "right" | "animate"> {
  children?: any;
  timeout?: number;
  delay?: number;
  direction: "up" | "down" | "left" | "right";
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    opacity: 0,
    transition: "opacity 0.5s ease-out, transform 0.5s ease-out",
  },
  up: {
    transform: "translate(0%, -100px)",
  },
  down: {
    transform: "translate(0%, 100px)",
  },
  left: {
    transform: "translate(-100px, 0%)",
  },
  right: {
    transform: "translate(100px, 0%)",
  },
  animate: {
    opacity: 1,
    transform: "translate(0%, 0%)",
  },
});

class SlideFadeComponent extends React.PureComponent<OwnProps, { play: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = {
      play: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ play: true });
    }, this.props.timeout || 0);
  }

  public render() {
    const { children, classes, direction, delay } = this.props;
    return (
      <>
        <RenderWhile mobile tablet desktop>
          {children &&
            React.Children.map(children, (child: any) => {
              if (child) {
                return React.cloneElement(child, {
                  style: {
                    ...(child.props.style || {}),
                    transitionDelay: delay + "ms",
                  },
                  className: classnames(
                    classes.root,
                    classes[direction],
                    this.state.play && classes.animate,
                    child.props.className
                  ),
                });
              } else {
                return "";
              }
            })}
        </RenderWhile>
        <RenderWhile print>{children}</RenderWhile>
      </>
    );
  }
}

export const SlideFade = withStyles(styles)(SlideFadeComponent);
