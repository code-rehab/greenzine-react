import { IPresenter } from "../../../helpers/with-presenter";
import { PrintInteractor } from "../../../application/business/interactor/print-interactor";
export declare class RenderWhilePresenter implements IPresenter {
    private _printInteractor;
    get isPrint(): boolean;
    constructor(_printInteractor: PrintInteractor);
    mount(): void;
    unmount(): void;
}
