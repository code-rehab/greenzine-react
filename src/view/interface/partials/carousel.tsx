import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { WithStyles, Theme, withStyles, Typography } from "@material-ui/core";
import { faCircle, faDotCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { StyleRules } from "@material-ui/styles";
import { mapEvent } from "../../../helpers/formatters";
import { ArticlePresenter } from "../../pages/article-presenter";
import { ArticleButton } from "../../pages/layout/bondig-edition-cover/device/article-button";
// import { faCircle } from "@fortawesome/free-solid-svg-icons"

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "block",
    flexDirection: "column",
    // position: "fixed",
    bottom: 100,
    left: 40,
    WebkitTapHighlightColor: "#0000",
    // backgroundColor: "#000",
  },
  bulletWrapper: {
    display: "flex",
    flexDirection: "row",
  },
  bullet: {
    marginTop: "0.75em",
    marginRight: 5,
    fontSize: "1.5em",
  },
  contentWrapper: {
    display: "flex",
    flexDirection: "column",
  },
});

type CarouselItem = {
  title: string;
  content: any;
  link?: any;
  color?: string;
};

interface OwnProps
  extends WithStyles<
    "root" | "bulletWrapper" | "bullet" | "title" | "content" | "contentWrapper" | "link"
  > {
  data: CarouselItem[];
  color?: any;
}

@observer
class CarouselComponent extends React.Component<OwnProps> {
  private interval = 3000;
  @observable private index: number = 0;

  private next() {
    const { data } = this.props;
    this.index + 1 < data.length ? this.index++ : (this.index = 0);
  }

  componentDidMount() {
    setInterval(() => this.next(), this.interval);
  }

  render() {
    const { data, classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.contentWrapper}>
          <ArticleButton
            id={data[this.index].link}
            title={data[this.index].content}
            category={data[this.index].title}
          />
        </div>
        <div className={classes.bulletWrapper}>
          {data.map((item, index) => {
            return (
              <FontAwesomeIcon
                key={index}
                color="#fff"
                icon={this.index === index ? faDotCircle : faCircle}
                className={classes.bullet}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export const Carousel = withStyles(styles)(CarouselComponent);
