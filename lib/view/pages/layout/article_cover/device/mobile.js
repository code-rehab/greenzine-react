import * as React from "react";
import { withStyles } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";
import classNames from "classnames";
var styles = function (theme) { return ({
    root: {
        minHeight: "calc(100vh - 60px)",
        display: "flex",
        alignItems: "center",
    },
    centered: {
        textAlign: "center",
        "& > *": {
            display: "inline-block",
            margin: "auto",
            textAlign: "center",
        },
    },
}); };
export var ArticleCoverMobile = withStyles(styles)(function (_a) {
    var config = _a.config, data = _a.data, classes = _a.classes, style = _a.style;
    return (React.createElement("section", { className: classNames(classes.root), style: { backgroundImage: style.backgroundImage } },
        React.createElement("div", { className: classNames(classes.inner, classes[config.variant]) }, data.map(function (d) {
            return (React.createElement("div", null,
                React.createElement(RenderElement, { element: d })));
        }))));
});
//# sourceMappingURL=mobile.js.map