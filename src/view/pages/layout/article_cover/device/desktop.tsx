import * as React from "react";
import { WithStyles, StyleRules, Theme, withStyles } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";

import classNames from "classnames";

const styles = (theme: Theme): StyleRules => ({
  root: {
    textAlign: "left",
    padding: "0 7vw",
    "& > *": {
      display: "flex",
      marginRight: "auto",
      flexDirection: "column",
    },
  },
  centered: {
    textAlign: "center",
    "& > *": {
      display: "inline-block",
      margin: "auto",
      textAlign: "center",
      "& > *": {
        display: "inline-block",
        margin: "auto",
        textAlign: "center",
      },
    },
  },
  right: {
    textAlign: "right",
    "& > *": {
      marginLeft: "auto",
    },
  },
});

interface OwnProps extends WithStyles<any> {
  config: any;
  data: any[];
  style?: any;
}

export const ArticleCoverDesktop = withStyles(styles)(({ config, data, classes, style }: OwnProps) => {
  return (
    <section className={classNames(classes.root)}>
      <div className={classNames(classes.inner, classes[config.variant])}>
        {data.map((d: any) => {
          return (
            <div>
              <RenderElement element={d} />
            </div>
          );
        })}
      </div>
    </section>
  );
});
