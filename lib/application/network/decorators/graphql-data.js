var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { extendObservable } from "mobx";
export function data(target, key) {
    Object.defineProperty(target, key, {
        set: function (firstValue) {
            var _a;
            extendObservable(this, (_a = {},
                Object.defineProperty(_a, key, {
                    get: function () {
                        return this.changes[key] || (this.record && this.record[key]) || firstValue;
                    },
                    enumerable: true,
                    configurable: true
                }),
                Object.defineProperty(_a, key, {
                    set: function (value) {
                        var _a;
                        this.changes = __assign(__assign({}, this.changes), (_a = {}, _a[key] = value, _a));
                    },
                    enumerable: true,
                    configurable: true
                }),
                _a), {});
        },
    });
}
//# sourceMappingURL=graphql-data.js.map