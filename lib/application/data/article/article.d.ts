import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { PageRecord, Page } from "../page/page";
import { Collection } from "../../network/graphql-collection";
export interface ArticleRecord {
    id: string;
    index?: string;
    title: string;
    description?: string;
    content?: string;
    slug?: string;
    image?: string;
    backgroundImage?: string;
    pages: PageRecord[];
    type?: string;
    color?: string;
    featuredImage?: string;
    style?: any;
    pageOrder?: string[];
}
export interface Article extends GraphQLModel<ArticleRecord>, ArticleRecord {
    pages: Page[];
    styleObj?: any;
}
export declare class ArticleModel extends GraphQLBase<ArticleRecord> implements Article {
    protected _pagesInitialized: boolean;
    protected _pages: Page[];
    title: string;
    content: string;
    index: string;
    slug: string;
    image: string;
    backgroundImage: string;
    description: string;
    config: string;
    type: string;
    color: string;
    featuredImage: string;
    style: string;
    data: never[];
    pageOrder: never[];
    serialize: () => any;
    get pageCollection(): Collection<Page>;
    get pages(): Page[];
    get styleObj(): any;
}
