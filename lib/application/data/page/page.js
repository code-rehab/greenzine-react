var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase } from "../../network/graphql-model";
// import { ElementModel, Element, ElementRecord } from "../element/element";
import { computed, observable } from "mobx";
import { replaceAll, take } from "../../../helpers/general";
import { createElement } from "@coderehab/greenzeen-content";
var PageModel = /** @class */ (function (_super) {
    __extends(PageModel, _super);
    function PageModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.article = undefined;
        _this.articleId = null;
        _this.id = "";
        _this.title = "";
        _this.content = "";
        _this.background = "";
        _this.type = "";
        _this.filter = "";
        _this.overlay = "";
        _this.color = "";
        _this.layout = "";
        _this.layoutConfig = "";
        _this.style = "";
        _this.responsiveStyling = "";
        _this.slug = "";
        _this.layouts = { lg: [], md: [], sm: [] };
        _this.elements = [];
        return _this;
    }
    Object.defineProperty(PageModel.prototype, "data", {
        get: function () {
            var elements = this.record.elements && JSON.parse(this.record.elements);
            return observable((this.changes.data || elements || this.record.data || []).map(function (d) { return createElement(d); }));
        },
        set: function (data) {
            this.changes.data = data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PageModel.prototype, "styleObj", {
        get: function () {
            var distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
            var style = JSON.parse(replaceAll(this.style || "{}", "/article/images/", distribution + "/ncf/bondig/"));
            style.maxWidth = style.maxWidth || 1200;
            return style;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PageModel.prototype, "composedStyleObj", {
        get: function () {
            var articleStyles = (this.article && this.article.styleObj) || {};
            return __assign(__assign({}, articleStyles), take(this.styleObj, Object.keys(this.styleObj)));
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], PageModel.prototype, "article", void 0);
    __decorate([
        data
    ], PageModel.prototype, "id", void 0);
    __decorate([
        data
    ], PageModel.prototype, "title", void 0);
    __decorate([
        data
    ], PageModel.prototype, "content", void 0);
    __decorate([
        data
    ], PageModel.prototype, "background", void 0);
    __decorate([
        data
    ], PageModel.prototype, "type", void 0);
    __decorate([
        data
    ], PageModel.prototype, "filter", void 0);
    __decorate([
        data
    ], PageModel.prototype, "overlay", void 0);
    __decorate([
        data
    ], PageModel.prototype, "color", void 0);
    __decorate([
        data
    ], PageModel.prototype, "layout", void 0);
    __decorate([
        data
    ], PageModel.prototype, "layoutConfig", void 0);
    __decorate([
        data
    ], PageModel.prototype, "style", void 0);
    __decorate([
        data
    ], PageModel.prototype, "responsiveStyling", void 0);
    __decorate([
        data
    ], PageModel.prototype, "slug", void 0);
    __decorate([
        data
    ], PageModel.prototype, "layouts", void 0);
    __decorate([
        computed
    ], PageModel.prototype, "data", null);
    __decorate([
        computed
    ], PageModel.prototype, "styleObj", null);
    __decorate([
        computed
    ], PageModel.prototype, "composedStyleObj", null);
    return PageModel;
}(GraphQLBase));
export { PageModel };
//# sourceMappingURL=page.js.map