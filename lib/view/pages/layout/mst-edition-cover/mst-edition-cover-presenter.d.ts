import { PrintInteractor } from "../../../../application/business/interactor/print-interactor";
import { ArticleInteractor } from "../../../../application/data/article/article-interactor";
import { EditionData } from "../../../../application/data/edition/edition";
import { MagazineData } from "../../../../application/data/magazine/magazine";
import { MagazineInteractor } from "../../../../application/data/magazine/magazine-interactor";
import { IPresenter } from "../../../../helpers/with-presenter";
export declare class MSTEditionCoverPresenter implements IPresenter {
    protected _magazineInteractor: MagazineInteractor;
    protected _articleInteractor: ArticleInteractor;
    protected _printInteractor: PrintInteractor;
    mount: () => void;
    unmount: () => void;
    constructor(_magazineInteractor: MagazineInteractor, _articleInteractor: ArticleInteractor, _printInteractor: PrintInteractor);
    get edition(): EditionData | undefined;
    get magazine(): MagazineData | undefined;
    nextArticle: () => void;
    selectArticle: (article: any) => void;
}
