var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// import { Design } from "../design/design";
import { Auth } from "aws-amplify";
import { observable } from "mobx";
import { BaseModule } from "../../module";
var DefaultUserInteractor = /** @class */ (function (_super) {
    __extends(DefaultUserInteractor, _super);
    function DefaultUserInteractor(userProvider) {
        var _this = _super.call(this) || this;
        _this.userProvider = userProvider;
        _this._currentUser = null;
        _this.logout = function () {
            _this._currentUser = null;
            Auth.signOut();
            window.location.reload();
        };
        return _this;
    }
    Object.defineProperty(DefaultUserInteractor.prototype, "currentUser", {
        get: function () {
            if (!this._currentUser) {
                throw new Error("User not set");
            }
            return this._currentUser;
        },
        set: function (user) {
            this._currentUser = user;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], DefaultUserInteractor.prototype, "_currentUser", void 0);
    return DefaultUserInteractor;
}(BaseModule));
export { DefaultUserInteractor };
//# sourceMappingURL=user-interactor.js.map