import { Theme, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import { observer } from "mobx-react";
import * as React from "react";
import { ArticleRedirect } from "../../../content/components/article-redirect";
import { ArticlePresenter } from "../../article-presenter";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";
import { LayoutBondigEditionCover } from "../../layout/bondig-edition-cover/bondig-edition-cover";
import { LayoutGrid } from "../../layout/grid/grid";
import { GridV2 } from "../../layout/grid-v2/grid";
import { take } from "../../../../helpers/general";
import { LayoutMSTEditionCover } from "../../layout/mst-edition-cover/mst-edition-cover";
import { LayoutCarmelkoersEditionCover } from "../../layout/carmelkoers-edition-cover/carmelkoers-edition-cover";
import { LayoutCarmelkennismakingEditionCover } from "../../layout/carmelkennismaking-edition-cover/carmelkennismaking-edition-cover";

import LoadingAnimation from "../../loading";
import { LayoutMSTEdition2020Cover } from "../../layout/mst-2020-edition-cover/mst-edition-cover";

const styles = (theme: Theme): StyleRules => ({
  root: {
    minHeight: `100vh`,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(5, 8, 9, 8),
    fontSize: "calc(1rem + 0.3vw)",

    [theme.breakpoints.down("md")]: {
      fontSize: "0.5em",
    },

    [theme.breakpoints.down("sm")]: {
      fontSize: "calc(1rem + 0.3vw)",
    },
  },
  grid: {
    margin: "auto",
    maxWidth: 1200,
    flex: 1,
  },
});

interface PageProps extends WithStyles<"root" | "grid"> {
  page: any;
  article: any;
}

export const Page = withStyles((theme: Theme) => ({
  root: {
    width: "100%",
    // maxwidth: 1200,
  },
  grid: {
    margin: "auto",
    maxWidth: 1200,
    flex: 1,
  },
}))(({ page, classes, article }: PageProps) => {
  const style = (page.style && JSON.parse(page.style)) || {};;

  // all below should be removed
  if (page.layout === "grid") {
    return (
      <div className={classes.root} style={{ maxWidth: 1200, marginTop: 0 }} >
        <LayoutGrid config={JSON.parse(page.layoutConfig || "{}")} data={page.data} />
      </div>
    );
  } else if (page.layout === "articleCover") {
    return (
      <div
        className={classes.root}
        style={{
          maxWidth: style.maxWidth,
          padding: style.padding,
          margin: style.margin,
        }}
      >
        <LayoutArticleCover
          config={JSON.parse(page.layoutConfig || "{}")}
          data={page.data}
          style={page.style}
        />
      </div>
    );
  } else if (page.layout === "bondig-edition-cover") {
    return (     
      // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
      <LayoutBondigEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      // </div>
    );
  } else if (page.layout === "carmelkoers-edition-cover") {
    return (
      // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
      <LayoutCarmelkoersEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      // </div>
    );
  } else if (page.layout === "carmelkennismaking-edition-cover") {
    return (
      <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        <LayoutCarmelkennismakingEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      </div>
    );
  } else if (page.layout === "mst-edition-cover") {
    return (
      // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
      <LayoutMSTEditionCover config={JSON.parse(page.layoutConfig || "{}")} />
      // </div>
    );
  }
  else if (page.layout === "mst-edition-2020-cover") {
    return (
      // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
      <LayoutMSTEdition2020Cover config={JSON.parse(page.layoutConfig || "{}")} />
      // </div>
    );
  }

  return <div>Unknown layout</div>;
});

interface OwnProps extends WithStyles<"root"> {
  presenter: ArticlePresenter;
}

export const ArticleDesktopContent = withStyles(styles)(
  observer(({ presenter, classes }: OwnProps) => {
    if (presenter.loading) {
      return <LoadingAnimation />;
    }

    if (!presenter.currentPage || !presenter.article) {
      return <div>Page not found</div>;
    }

    const page = presenter.currentPage;

    console.log(presenter.currentPage.layout);

    if (presenter.article && page && page.layouts && page.layouts.lg && page.layouts.lg.length) {
      return <GridV2 article={presenter.article} page={page} presenter={presenter}></GridV2>;
    }

    const styles =
      (presenter.article && presenter.article.style && JSON.parse(presenter.article.style)) || {};
    const pagestyles =
      (presenter.currentPage &&
        presenter.currentPage.style &&
        JSON.parse(presenter.currentPage.style)) ||
      {};

    return (
      <div
        style={take(styles, [
          "background",
          "backgroundColor",
          "backgroundImage",
          "backgroundSize",
          "backgroundPosition",
          "color",
        ])}
      >
        <div
          className={classes.root}
          style={{
            justifyContent: "space-evenly",

            ...[
              "background",
              "color",
              "backgroundSize",
              "backgroundPosition",
              "backgroundImage",
              "backgroundRepeat",
              "backgroundColor",
              "alignItems",
              "justifyContent",
              "padding",
              // "maxWidth",
            ].reduce((styles: any, key: string) => {
              if (pagestyles[key] !== undefined) {
                styles[key] = pagestyles[key];
              }
              return styles;
            }, {}),
          }}
          ref={(elem) => {
            if (elem) {
              if ((window.document as any).documentMode) {
                setTimeout(() => {
                  elem.style.height = elem.clientHeight + "px";
                  elem.style.display = "inherit";
                }, 500);
              }
            }
          }}
        >
          
          <Page article={presenter.article} page={presenter.currentPage} />
          {!presenter.hasNextPage &&
            page.layout !== "mst-edition-cover" &&
            presenter.hasNextArticle &&
            presenter.currentPage.layout !== "bondig-edition-cover" &&
            presenter.currentPage.layout !== "mst-edition-2020-cover" &&
            presenter.currentPage.layout !== "carmelkoers-edition-cover" &&
            presenter.currentPage.layout !== "carmelkennismaking-edition-cover" && (
              <ArticleRedirect
                onClick={presenter.toNextArticle}
                thumbnail={
                  presenter.nextArticle &&
                  (presenter.nextArticle.featuredImage
                    ? presenter.nextArticle.featuredImage
                    : presenter.nextArticle.image)
                }
              >
                {presenter.nextArticle && presenter.nextArticle.title}
              </ArticleRedirect>
            )}
        </div>
      </div>
    );
  })
);
