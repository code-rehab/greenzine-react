import * as React from "react";
import { withStyles, Typography, Theme } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";

interface OwnProps extends WithStyles<"root"> {
//   avatar: any;
//   credits: Credit[];
//   animation: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
      backgroundColor: "#fff",
      padding: theme.spacing(2)
  },

});

export const Popup = withStyles(styles)(({ classes, animation }: any) => {
  animation = animation || { duration: 0, delay: 0 };

  return (
    <div className={classes.root}>
      Popup here..
    </div>
  );
});
