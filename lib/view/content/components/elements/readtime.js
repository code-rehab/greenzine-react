import * as React from "react";
import { withStyles, Typography } from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import { RenderWhile } from "../render-while";
import { SlideFade } from "../effects/slide-fade";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {
                display: "flex",
                alignItems: "center",
                marginBottom: "0.5em"
            },
            _a[theme.breakpoints.up("md")] = {
                marginBottom: theme.spacing(2),
            },
            _a),
        icon: {
            marginLeft: 10,
            marginRight: 10,
            fontSize: 20,
        },
    });
};
export var ReadTime = withStyles(styles)(function (_a) {
    var classes = _a.classes, time = _a.time, category = _a.category, align = _a.align, animation = _a.animation;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement(React.Fragment, null,
        React.createElement(RenderWhile, { desktop: true, tablet: true, mobile: true },
            React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
                React.createElement("div", { className: classes.root, style: { justifyContent: "align", flexWrap: "wrap" } },
                    React.createElement(Typography, { variant: "body2" }, category),
                    time && (React.createElement(React.Fragment, null,
                        React.createElement(FontAwesomeIcon, { className: classes.icon, icon: faClock }),
                        React.createElement(Typography, { variant: "body2" },
                            "Leestijd: ",
                            time)))))),
        React.createElement(RenderWhile, { print: true },
            React.createElement(Typography, { variant: "body2" },
                category,
                " ",
                time && " - Leestijd: " + time))));
});
//# sourceMappingURL=readtime.js.map