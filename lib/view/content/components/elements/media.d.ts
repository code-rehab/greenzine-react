import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<any> {
    image?: any;
    children?: any;
    embed?: string;
    width?: string;
    height?: string;
    className?: any;
    style?: any;
    caption?: string;
    maxWidth?: string;
    animation?: any;
}
export declare const Media: React.ComponentType<Pick<OwnProps, "animation" | "style" | "image" | "children" | "caption" | "embed" | "height" | "maxWidth" | "width" | "className"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
