import { EditionFragment } from "./fragments";
export var EditionCreated = "\nsubscription {\n  magazineCreated {\n    " + EditionFragment + "\n  }\n}\n";
export var EditionUpdated = "\nsubscription {\n  magazineUpdated {\n    " + EditionFragment + "\n  }\n}\n";
export var EditionDeleted = "\nsubscription {\n  magazineDeleted {\n    " + EditionFragment + "\n  }\n}\n";
//# sourceMappingURL=subscriptions.js.map