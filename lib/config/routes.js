// import Application from "../application/application";
import { generatePath } from "react-router";
import { Application } from "../application/application";
import { PageIndex } from "../view/pages";
import { ArticlePage } from "../view/pages/article-page";
import { Error404 } from "../view/pages/error-404";
import { PageIndexMST } from "../view/pages/indexMST";
import { PageIndexMST2020 } from "../view/pages/indexMST2020";
import { PageIndexCarmelKoers } from "../view/pages/indexCarmelKoers";
import { PageIndexCarmelKennismaking } from "../view/pages/indexCarmelKennismaking";
import { PageIndexGreenzeen } from "../view/pages/indexGreenzeen";
import { PageIndexTwenteboard } from "../view/pages/indexTwenteboard";
export var redirections = [];
var PageIndexComponent = null;
console.log("THEME", process.env.REACT_APP_CLIENT_THEME);
switch (process.env.REACT_APP_CLIENT_THEME) {
    case "mst":
        // code block
        PageIndexComponent = PageIndexMST;
        break;
    case "mst2020":
        // code block
        PageIndexComponent = PageIndexMST2020;
        break;
    case "carmelKoers2025":
        // code block
        PageIndexComponent = PageIndexCarmelKoers;
        break;
    case "carmelKennismaking":
        // code block
        PageIndexComponent = PageIndexCarmelKennismaking;
        break;
    case "greenzeen":
        PageIndexComponent = PageIndexGreenzeen;
        break;
    case "twenteboard":
        // code block
        PageIndexComponent = PageIndexTwenteboard;
        break;
    default:
        PageIndexComponent = PageIndex;
    // code block
}
export var routeMap = {
    "index.editions": {
        path: "/",
        title: "Edities",
        component: PageIndex,
        transition: "fade-in-out",
    },
    "index.articles": {
        path: "/:edition",
        title: "Artikelen",
        component: PageIndexComponent,
        transition: "fade-in-out",
    },
    "article.homepage": {
        path: "/:edition/:article",
        title: "Artikelen",
        component: ArticlePage,
        transition: function () { return Application.business.interactor.article.transition; },
    },
    "article.page": {
        path: "/:edition/:article/:page",
        title: "Artikelen",
        component: ArticlePage,
        transition: function () { return Application.business.interactor.article.transition; },
    },
    "error.404": {
        path: "*",
        title: "Error 404 - Resource not found",
        component: Error404,
        transition: "fade-in-out",
    },
};
export var route = function (name, args) {
    return generatePath(routeMap[name].path, args);
};
export var routes = Object.keys(routeMap).map(function (name) {
    return routeMap[name];
});
export var checkRedirect = function () {
    return redirections.filter(function (_a) {
        var from = _a.from;
        return from.replace(/\/$/, "") === window.location.pathname.replace(/\/$/, "");
    })[0];
};
//# sourceMappingURL=routes.js.map