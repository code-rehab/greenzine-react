import * as React from "react";
import { withStyles, Typography, Grid, Link } from "@material-ui/core";
var styles = function (theme) { return ({
    root: {
        width: "100%",
    },
}); };
export var ColofonCarmel = withStyles(styles)(function (_a) {
    var classes = _a.classes;
    return (React.createElement("div", { className: classes.root, ref: function (elem) {
            if (elem) {
                elem.style.height = elem.clientHeight + "px";
            }
        }, style: { color: "#000" } },
        React.createElement(Grid, { container: true },
            React.createElement(Grid, { item: true, xs: 12 },
                React.createElement(Typography, { variant: "h4", style: {
                        fontWeight: 400,
                        lineHeight: "1.1 !important",
                        marginBottom: 12,
                    } }, "Colofon")),
            React.createElement(Grid, { item: true, xs: 12, sm: 12, style: {
                    display: "flex",
                    flexDirection: "column",
                } },
                React.createElement(Typography, { className: classes.montserrat, paragraph: true },
                    React.createElement("small", { className: classes.small },
                        "Teksten: Anne-Marie Bos, Titus Brandsma Instituut.",
                        React.createElement("br", null),
                        "Redactie: Sylvia Goossens, Franciska Soepboer, Fijke Hoogendijk (Stichting Carmelcollege) Suzanne Visser (Perspect).",
                        React.createElement("br", null),
                        "Wij hebben geprobeerd alle rechthebbenden van het beeldmateriaal te achterhalen.",
                        React.createElement("br", null),
                        "Personen of instanties die menen dat hun rechten desondanks niet gerespecteerd worden, ",
                        React.createElement("br", null),
                        "kunnen contact opnemen met de redactie via\u00A0",
                        React.createElement("a", { href: "mailto:communicatie@carmel.nl" }, "communicatie@carmel.nl"))),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Concept en ontwerp")),
                React.createElement(Typography, { className: classes.text },
                    React.createElement("small", { className: classes.small },
                        React.createElement(Link, { href: "https://creativebastards.nl", color: "inherit", underline: "always", target: "_blank" }, "Creative Bastards")))),
            React.createElement(Grid, { item: true, xs: 12, sm: 12 },
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "Technische realisatie")),
                React.createElement(Typography, { paragraph: true },
                    React.createElement("small", { className: classes.small },
                        React.createElement(Link, { href: "https://code.rehab/", color: "inherit", underline: "always", target: "_blank", paragraph: true }, "Code.Rehab"))),
                React.createElement(Typography, { className: classes.montserrat },
                    React.createElement("small", { className: classes.small }, "\u00A9 Stichting Carmelcollege"))))));
});
//# sourceMappingURL=colofon-carmel.js.map