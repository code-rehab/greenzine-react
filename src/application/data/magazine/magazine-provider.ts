import { graphqlOperation } from "aws-amplify";
import { GraphQLProvider, GraphQLProviderProps } from "../../network/graphql-provider";
import * as Mutation from "./graphql/mutations";
import * as Query from "./graphql/queries";
import { Magazine, MagazineData, MagazineModel } from "./magazine";

export interface MagazineProvider extends GraphQLProviderProps<Magazine, MagazineData> {}

export class DefaultMagazineProvider extends GraphQLProvider<Magazine, MagazineData> implements MagazineProvider {
  public model = MagazineModel;

  protected listOperation = () => {
    return graphqlOperation(Query.MagazineCollection, {});
  };

  protected createOperation = (magazine: MagazineData) => {
    return graphqlOperation(Mutation.CreateMagazine, { input: magazine });
  };

  protected fetchOperation = (magazine: MagazineData) => {
    return graphqlOperation(Query.Magazine, { id: magazine.id });
  };

  protected updateOperation = (magazine: Magazine) => {
    return graphqlOperation(Mutation.UpdateMagazine, {
      input: {
        id: magazine.id,
        title: magazine.title,
        description: magazine.description,
        config: magazine.config
      }
    });
  };

  protected deleteOperation = (magazine: MagazineData) => {
    return graphqlOperation(Mutation.DeleteMagazine, { id: magazine.id });
  };
}
