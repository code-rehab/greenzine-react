import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Drawer, Grid, withStyles, Zoom, } from "@material-ui/core";
import { observer } from "mobx-react";
import * as React from "react";
import { withRouter } from "react-router-dom";
import { withPresenter } from "../../helpers/with-presenter";
import { Colofon } from "../content/components/colofon-slide";
import { CoverButton } from "../content/components/cover-button";
import { DefaultIndexInfo } from "../content/index/default-index-info";
import { MagazineIndexInfo } from "../content/index/magazine-index-info";
import { PageIndexPresenter } from "./index-presenter";
import { EditionIndexCarmelKoers } from "../content/index/carmelkoers/edition-index-carmelkoers";
import LoadingAnimation from "./loading";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f, _g;
    return ({
        root: (_a = {
                display: "flex",
                justifyContent: "center",
                width: "100%",
                paddingTop: 60,
                backgroundPosition: "top 4vw center !important",
                backgroundSize: "130% !important",
                backgroundImage: "url('../assets/images/carmel/index-bg.png')",
                paddingBottom: "calc(5vw + " + theme.spacing(5) + "px)"
            },
            // Mobile /////////////////////
            _a[theme.breakpoints.up("xs")] = {
                // backgroundImage: "none",
                paddingTop: "30vw",
            },
            // Tablet portait /////////////////////
            _a[theme.breakpoints.up("sm")] = {
                paddingTop: "30vw",
            },
            // tablet landscape and smaller desktop
            _a[theme.breakpoints.up("md")] = {
                paddingTop: "15vw",
                backgroundPosition: "top right !important",
                backgroundSize: "75% !important",
                backgroundImage: "url('../assets/images/carmel/index-bg.png')",
            },
            //larger desktop
            _a[theme.breakpoints.up("lg")] = {
                backgroundPosition: "top center !important",
                backgroundSize: "75% !important",
                paddingTop: 260,
                "& $content": {
                    background: "transparent",
                },
            },
            _a),
        logo: (_b = {
                alignSelf: "flex-end",
                marginRight: 130,
                maxWidth: 325,
                position: "relative",
                bottom: -60
            },
            _b[theme.breakpoints.down("sm")] = {
                maxWidth: 250,
                bottom: 0,
                marginRight: 0,
            },
            _b),
        header: (_c = {
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                paddingBottom: "5vw"
            },
            _c[theme.breakpoints.down("sm")] = {
                marginTop: 20,
            },
            _c),
        content: {
            background: "rgb(252, 197, 0)",
            maxWidth: 1500,
            width: "100%",
            padding: "0 5vw",
        },
        grid: (_d = {},
            _d[theme.breakpoints.down("sm")] = {
                flexDirection: "column",
                alignItems: "center",
            },
            _d),
        paper: (_e = {
                width: "65vw",
                color: "white",
                // @Jordy Kommeren: "Fixes iOS scroll bug"
                // minHeight: "fit-content",
                height: "100vh",
                backgroundColor: "#E9550D",
                "::-webkit-scrollbar": {
                    display: "none",
                }
            },
            _e[theme.breakpoints.up("lg")] = {
                maxWidth: "1050px",
                padding: theme.spacing(5, 0),
            },
            _e[theme.breakpoints.down("sm")] = {
                minWidth: "100vw",
            },
            _e),
        montserrat: {
            fontFamily: "Montserrat",
            lineHeight: "1.3",
        },
        title: {
            fontWeight: 500,
        },
        aboutButtons: {
            marginTop: theme.spacing(2),
            backgroundColor: "transparant",
        },
        button: {
            padding: theme.spacing(1, 0, 1, 0),
            minWidth: "unset !important",
            fontWeight: 400,
            "&:hover": {
                background: "none",
                color: theme.palette.primary.main,
            },
        },
        closeButton: {
            padding: 10,
            borderRadius: "100%",
            border: "2px solid white",
            width: "50px !important",
            height: "50px !important",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor: "pointer",
            transition: "all 0.3s",
            "&:hover": {
                backgroundColor: "white",
                color: "#E9550D",
            },
        },
        icon: {
            color: "inherit",
            width: "25px !important",
            height: "25px !important",
        },
        aboutHeader: (_f = {
                position: "absolute",
                top: theme.spacing(3),
                right: theme.spacing(3)
            },
            _f[theme.breakpoints.up("lg")] = {
                top: theme.spacing(6),
                right: theme.spacing(6),
            },
            _f),
        aboutContent: (_g = {
                display: "flex",
                alignItems: "center"
            },
            _g[theme.breakpoints.up("lg")] = {
                padding: theme.spacing(3, 6, 6, 6),
            },
            _g[theme.breakpoints.down("md")] = {
                padding: theme.spacing(6, 1, 18, 1),
            },
            _g.minHeight = "100%",
            _g),
    });
};
var RenderIndexInfo = withStyles(styles)(function (_a) {
    var magazine = _a.magazine, edition = _a.edition, classes = _a.classes;
    var result;
    if (magazine) {
        if (edition) {
            result = React.createElement(EditionIndexCarmelKoers, { edition: edition });
        }
        else {
            result = React.createElement(MagazineIndexInfo, { magazine: magazine });
        }
    }
    else {
        result = React.createElement(DefaultIndexInfo, null);
    }
    return React.createElement("div", { className: classes.indexInfo }, result);
});
var Component = observer(function (_a) {
    var presenter = _a.presenter, classes = _a.classes, match = _a.match;
    var _b = React.useState({
        open: false,
        content: React.createElement(Colofon, null),
    }), state = _b[0], setState = _b[1];
    if (presenter.loading) {
        return React.createElement(LoadingAnimation, null);
    }
    return (React.createElement("div", { className: classes.root, style: {
            backgroundColor: "#FCC500",
            backgroundAttachment: "fixed",
            minHeight: "100vh",
            // backgroundImage: "url('https://dg8n28xhgyfkm.cloudfront.net/carmel-koers-2025/index-bg.png')",
            backgroundPosition: "40% -40px",
            backgroundSize: "60%",
            backgroundRepeat: "no-repeat",
        } },
        React.createElement("div", { className: classes.content },
            React.createElement(Grid, { container: true, className: classes.grid, spacing: 3 },
                React.createElement(Grid, { item: true, xs: 12, md: 12, lg: 3 },
                    React.createElement(RenderIndexInfo, { magazine: presenter.magazine, edition: presenter.edition })),
                React.createElement(Grid, { item: true, xs: 12, md: 12, lg: 8 },
                    React.createElement(Grid, { container: true, spacing: 3 }, presenter.items.map(function (record, index) { return (React.createElement(Zoom, { key: index, in: true, style: { transitionDelay: 300 + index * 50 + "ms" } },
                        React.createElement(Grid, { item: true, key: index, xs: 6, sm: 4, lg: 4 },
                            React.createElement(CoverButton, { version: "carmelkoers", id: record.id, index: index + 1, title: record.title, image: record.image, onSelect: presenter.selectItem })))); })),
                    React.createElement(Drawer, { anchor: "left", open: state.open, onClose: function () { return setState({ open: false, content: state.content }); }, classes: { root: classes.root, paper: classes.paper }, BackdropProps: { style: { backgroundColor: "transparent" } } },
                        React.createElement("div", { style: { margin: "auto" } },
                            React.createElement("div", { className: classes.aboutHeader },
                                React.createElement("div", { onClick: function () { return setState({ open: false, content: state.content }); }, className: classes.closeButton },
                                    React.createElement(FontAwesomeIcon, { icon: faTimes, className: classes.icon }))),
                            React.createElement("div", { className: classes.aboutContent }, state.content))))))));
});
export var PageIndexCarmelKoers = withStyles(styles)(withRouter(withPresenter(function (_a, _b) {
    var match = _a.match, history = _a.history, location = _a.location, staticContext = _a.staticContext;
    var interactor = _b.interactor, provider = _b.provider;
    return new PageIndexPresenter(interactor.magazine, provider.magazine, interactor.article, provider.article, {
        match: match,
        history: history,
        location: location,
        staticContext: staticContext,
    });
}, Component)));
//# sourceMappingURL=indexCarmelKoers.js.map