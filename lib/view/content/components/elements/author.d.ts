import * as React from "react";
export declare type Credit = {
    credit: string;
    name: string;
};
export declare const Author: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<string>>;
