import * as React from "react";
import { withPresenter } from "../../../../helpers/with-presenter";
// import { LayoutEditionCoverPrint } from "./device/print";
import { CarmelkoersEditionCoverPresenter } from "./carmelkoers-edition-cover-presenter";
// import { RenderWhile } from "../../../content/components/render-while";
import { LayoutEditionCoverDesktop } from "./device/desktop";
// import { LayoutEditionCoverMobile } from "./device/mobile";
var Component = function (_a) {
    var config = _a.config, presenter = _a.presenter;
    return React.createElement(LayoutEditionCoverDesktop, { config: config, presenter: presenter });
};
export var LayoutCarmelkoersEditionCover = withPresenter(function (_props, _a) {
    var interactor = _a.interactor;
    return new CarmelkoersEditionCoverPresenter(interactor.magazine, interactor.article, interactor.print);
}, Component);
//# sourceMappingURL=carmelkoers-edition-cover.js.map