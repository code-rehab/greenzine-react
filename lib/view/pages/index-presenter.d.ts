import { IPresenter } from "../../helpers/with-presenter";
import { PagePresenter } from "./_page-default-presenter";
export declare class PageIndexPresenter extends PagePresenter implements IPresenter {
    mount: () => Promise<void>;
    unmount: () => void;
    get items(): any[];
    get magazines(): import("../..").Collection<import("../..").Magazine>;
    get articles(): import("../..").Article[];
    get editions(): import("../..").Edition[];
    selectItem: (id: string) => void;
}
