import * as React from "react";
import { withStyles, Typography, Theme } from "@material-ui/core";
import { WithStyles, StyleRules } from "@material-ui/styles";
import { RenderWhile } from "../render-while";
import { SlideFade } from "../effects/slide-fade";

export type Credit = { credit: string; name: string };

interface OwnProps extends WithStyles<"root" | "mugshot" | "credit" | "avatarBubble" | "textWrapper"> {
  avatar: any;
  credits: Credit[];
  animation: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    padding: theme.spacing(2, 1),
    display: "flex",
    alignItems: "center",
    // [theme.breakpoints.up("md")]: {
    //   marginLeft: theme.spacing(-8),
    // },
  },
  avatarBubble: {
    position: "relative",
    borderRadius: "100%",
    marginRight: theme.spacing(2),
    width: theme.spacing(6),
    height: theme.spacing(6),
    backgroundColor: "white",
    backgroundPosition: "center",
    backgroundSize: "cover",
    "@media print": {
      display: "none",
    },
  },
  credit: {
    fontFamily: "Montserrat",
    textTransform: "none",
    fontWeight: 500,
    fontSize: "14px",

    "& b": {
      fontWeight: "bold",
    },
  },
  textWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    textAlign: "start",
  },
});

export const Author = withStyles(styles)(({ classes, avatar, credits, animation }: any) => {
  animation = animation || { duration: 0, delay: 0 };
  credits = credits || [];

  return (
    <>
      <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
        <div className={classes.root}>
          <RenderWhile tablet desktop>
            <div className={classes.avatarBubble} style={{ backgroundImage: `url(${avatar})` }}></div>
          </RenderWhile>

          <RenderWhile desktop mobile>
            <div className={classes.textWrapper}>
              <Typography variant={"body2"} className={classes.credit}>
                {credits.map((credit: any, index: any) => {
                  return (
                    <span key={index} style={{ display: "block" }}>
                      <b>{credit.credit}: </b>
                      {credit.name}
                    </span>
                  );
                })}
              </Typography>
            </div>
          </RenderWhile>
        </div>
      </SlideFade>

      <RenderWhile print>
        {Array.isArray(credits) ? (
          credits.map((credit: any, index: any) => {
            return (
              <Typography variant="body2" className={classes.credit}>
                <span key={index} style={{ display: "block" }}>
                  <b>{credit.credit}: </b>
                  {credit.name}
                </span>
              </Typography>
            );
          })
        ) : (
          <></>
        )}
      </RenderWhile>
    </>
  );
});
