var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var BaseModule = /** @class */ (function () {
    function BaseModule() {
        /* tslint:disable:no-implicit-any */
        this._cache = {
            provider: {},
            interactor: {},
            module: {}
        };
    }
    // tslint:disable-next-line
    BaseModule.prototype.loadProvider = function (key, ProviderClass) {
        var providerProps = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            providerProps[_i - 2] = arguments[_i];
        }
        if (!this._cache.provider[key]) {
            this._cache.provider[key] = new (ProviderClass.bind.apply(ProviderClass, __spreadArrays([void 0], providerProps)))();
        }
        return this._cache.provider[key];
    };
    // tslint:disable-next-line
    BaseModule.prototype.loadInteractor = function (key, InteractorClass) {
        var providerProps = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            providerProps[_i - 2] = arguments[_i];
        }
        if (!this._cache.interactor[key]) {
            this._cache.interactor[key] = new (InteractorClass.bind.apply(InteractorClass, __spreadArrays([void 0], providerProps)))();
        }
        return this._cache.interactor[key];
    };
    BaseModule.prototype.loadModule = function (key, ModuleClass) {
        var providerProps = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            providerProps[_i - 2] = arguments[_i];
        }
        if (!this._cache.module[key]) {
            this._cache.module[key] = new (ModuleClass.bind.apply(ModuleClass, __spreadArrays([void 0], providerProps)))();
        }
        return this._cache.module[key];
    };
    return BaseModule;
}());
export { BaseModule };
//# sourceMappingURL=module.js.map