import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Button,
  Drawer,
  Grid,
  Hidden,
  Theme,
  Typography,
  withStyles,
  WithStyles,
  Zoom,
} from "@material-ui/core";
import { StyleRules } from "@material-ui/styles";
import classnames from "classnames";
import { observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { EditionData } from "../../application/data/edition/edition";
import { Magazine } from "../../application/data/magazine/magazine";
import { PresenterProps, withPresenter } from "../../helpers/with-presenter";
import { About } from "../content/components/about-slide";
import { Colofon } from "../content/components/colofon-slide";
import { CoverButton } from "../content/components/cover-button";
import { EditionBall } from "../content/components/edition-ball";
import { DefaultIndexInfo } from "../content/index/default-index-info";
import { MagazineIndexInfo } from "../content/index/magazine-index-info";
import { PageIndexPresenter } from "./index-presenter";
import { EditionIndexCarmelKoers } from "../content/index/carmelkoers/edition-index-carmelkoers";
import LoadingAnimation from "./loading";

interface OwnProps
  extends RouteComponentProps,
    WithStyles<
      | "root"
      | "header"
      | "content"
      | "grid"
      | "paper"
      | "montserrat"
      | "title"
      | "button"
      | "closeButton"
      | "icon"
      | "aboutButtons"
      | "aboutHeader"
      | "aboutContent"
      | "logo"
    > {}

const styles = (theme: Theme): StyleRules => ({
  root: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    paddingTop: 60,
    backgroundPosition: "top 4vw center !important",
    backgroundSize: "130% !important",
    backgroundImage: "url('../assets/images/carmel/index-bg.png')",

    paddingBottom: `calc(5vw + ${theme.spacing(5)}px)`,

    // Mobile /////////////////////
    [theme.breakpoints.up("xs")]: {
      // backgroundImage: "none",
      paddingTop: "30vw",
    },
    // Tablet portait /////////////////////
    [theme.breakpoints.up("sm")]: {
      paddingTop: "30vw",
    },
    // tablet landscape and smaller desktop
    [theme.breakpoints.up("md")]: {
      paddingTop: "15vw",
      backgroundPosition: "top right !important",
      backgroundSize: "75% !important",
      backgroundImage: "url('../assets/images/carmel/index-bg.png')",
    },
    //larger desktop
    [theme.breakpoints.up("lg")]: {
      backgroundPosition: "top center !important",
      backgroundSize: "75% !important",
      paddingTop: 260,
      "& $content": {
        background: "transparent",
      },
    },
  },
  logo: {
    alignSelf: "flex-end",
    marginRight: 130,
    maxWidth: 325,
    position: "relative",
    bottom: -60,
    [theme.breakpoints.down("sm")]: {
      maxWidth: 250,
      bottom: 0,
      marginRight: 0,
    },
  },
  header: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    paddingBottom: "5vw",
    [theme.breakpoints.down("sm")]: {
      marginTop: 20,
    },
  },
  content: {
    background: "rgb(252, 197, 0)",
    maxWidth: 1500,
    width: "100%",
    padding: "0 5vw",
  },
  grid: {
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  paper: {
    width: "65vw",
    color: "white",
    // @Jordy Kommeren: "Fixes iOS scroll bug"
    // minHeight: "fit-content",
    height: "100vh",
    backgroundColor: "#E9550D",
    "::-webkit-scrollbar": {
      display: "none",
    },
    [theme.breakpoints.up("lg")]: {
      maxWidth: "1050px",
      padding: theme.spacing(5, 0),
    },
    [theme.breakpoints.down("sm")]: {
      minWidth: "100vw",
    },
  },
  montserrat: {
    fontFamily: "Montserrat",
    lineHeight: "1.3",
  },
  title: {
    fontWeight: 500,
  },
  aboutButtons: {
    marginTop: theme.spacing(2),
    backgroundColor: "transparant",
  },
  button: {
    padding: theme.spacing(1, 0, 1, 0),
    minWidth: "unset !important",
    fontWeight: 400,
    "&:hover": {
      background: "none",
      color: theme.palette.primary.main,
    },
  },
  closeButton: {
    padding: 10,
    borderRadius: "100%",
    border: "2px solid white",
    width: "50px !important",
    height: "50px !important",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    transition: "all 0.3s",
    "&:hover": {
      backgroundColor: "white",
      color: "#E9550D",
    },
  },
  icon: {
    color: "inherit",
    width: "25px !important",
    height: "25px !important",
  },
  aboutHeader: {
    position: "absolute",
    top: theme.spacing(3),
    right: theme.spacing(3),
    [theme.breakpoints.up("lg")]: {
      top: theme.spacing(6),
      right: theme.spacing(6),
    },
  },
  aboutContent: {
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.up("lg")]: {
      padding: theme.spacing(3, 6, 6, 6),
    },
    [theme.breakpoints.down("md")]: {
      padding: theme.spacing(6, 1, 18, 1),
    },
    minHeight: "100%",
  },
});

interface RenderIndexProps extends WithStyles {
  magazine?: Magazine;
  edition?: EditionData;
}

const RenderIndexInfo = withStyles(styles)(({ magazine, edition, classes }: RenderIndexProps) => {
  let result;
  if (magazine) {
    if (edition) {
      result = <EditionIndexCarmelKoers edition={edition} />;
    } else {
      result = <MagazineIndexInfo magazine={magazine} />;
    }
  } else {
    result = <DefaultIndexInfo />;
  }

  return <div className={classes.indexInfo}>{result}</div>;
});

const Component = observer(
  ({ presenter, classes, match }: OwnProps & PresenterProps<PageIndexPresenter>) => {
    const [state, setState] = React.useState({
      open: false,
      content: <Colofon />,
    });

    if (presenter.loading) {
      return <LoadingAnimation />;
    }

    return (
      <div
        className={classes.root}
        style={{
          backgroundColor: "#FCC500",
          backgroundAttachment: "fixed",
          minHeight: "100vh",
          // backgroundImage: "url('https://dg8n28xhgyfkm.cloudfront.net/carmel-koers-2025/index-bg.png')",
          backgroundPosition: "40% -40px",
          backgroundSize: "60%",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className={classes.content}>
          {/* <div className={classes.header}></div> */}
          <Grid container className={classes.grid} spacing={3}>
            <Grid item xs={12} md={12} lg={3}>
              <RenderIndexInfo magazine={presenter.magazine} edition={presenter.edition} />
            </Grid>

            <Grid item xs={12} md={12} lg={8}>
              <Grid container spacing={3}>
                {presenter.items.map((record: any, index: number) => (
                  <Zoom key={index} in style={{ transitionDelay: 300 + index * 50 + "ms" }}>
                    <Grid item key={index} xs={6} sm={4} lg={4}>
                      <CoverButton
                        version={"carmelkoers"}
                        id={record.id}
                        index={index + 1}
                        title={record.title}
                        image={record.image}
                        onSelect={presenter.selectItem}
                      />
                    </Grid>
                  </Zoom>
                ))}
              </Grid>
              <Drawer
                anchor="left"
                open={state.open}
                onClose={() => setState({ open: false, content: state.content })}
                classes={{ root: classes.root, paper: classes.paper }}
                BackdropProps={{ style: { backgroundColor: "transparent" } }}
              >
                <div style={{ margin: "auto" }}>
                  <div className={classes.aboutHeader}>
                    <div
                      onClick={() => setState({ open: false, content: state.content })}
                      className={classes.closeButton}
                    >
                      <FontAwesomeIcon icon={faTimes} className={classes.icon} />
                    </div>
                  </div>
                  <div className={classes.aboutContent}>{state.content}</div>
                </div>
              </Drawer>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
);

export const PageIndexCarmelKoers:any = withStyles(styles)(
  withRouter(
    withPresenter<PageIndexPresenter, OwnProps>(
      ({ match, history, location, staticContext }, { interactor, provider }) =>
        new PageIndexPresenter(
          interactor.magazine,
          provider.magazine,
          interactor.article,
          provider.article,
          {
            match,
            history,
            location,
            staticContext,
          }
        ),
      Component
    )
  )
);
