import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import * as React from "react";
import { RenderWhile } from "../../components/render-while";
var styles = function (theme) { return ({
    root: {
        margin: "auto",
    },
    drawerBackdrop: {},
    header: {},
    content: {},
    backdrop: {
        backgroundColor: "transparant",
    },
    title: {},
    subtitle: {
        fontWeight: 700,
        margin: "20px 0",
        color: "#4D4D4F",
    },
    description: {
        paddingRight: theme.spacing(1),
    },
    collapseContainer: {
        marginBottom: theme.spacing(3),
        position: "relative",
        "&:after": {
            content: "''",
            display: "block",
            position: "absolute",
            left: 0,
            top: 0,
            width: "100%",
            height: "100%",
            boxShadow: "inset 0px -70px 40px -40px rgba(255,255,255,1)",
        },
        "&.MuiCollapse-entered": {
            "&:after": {
                boxShadow: "none",
            },
        },
    },
    contentPiece: {
        fontFamily: "Montserrat",
    },
    marginBottom: {
        marginBottom: "1em",
    },
}); };
var Info = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var description = (edition && edition.description && JSON.parse(edition.description)) || "";
    return (React.createElement(React.Fragment, null,
        React.createElement("img", { src: "https://d6j399hnl3eyg.cloudfront.net/greenzeen/marktonderzoek-v1/Group%20474.svg", width: "132", style: { marginBottom: 40 } }),
        React.createElement(Typography, { variant: "h2", className: classes.title }, "Over dit marktonderzoek"),
        React.createElement(Typography, { className: classes.description, paragraph: true }, "Greenzeen is een nieuw product uit de koker van creative bastards en code rehab. We zijn al ruim 2 jaar met de ontwikkeling bezig en dit jaar moet het gebeuren, we gaan de markt op! Super spannend!"),
        React.createElement(Typography, null, "Om meer inzicht te krijgen in onze doelgroep hebben we wat vragen verzameld voor mensen in de marketing-communicatie hoek. Heel fijn dat je mee wilt werken. Alvast bedankt voor je bijdrage!")));
};
var EditionIndexGreenzeenComponent = function (_a) {
    var edition = _a.edition, classes = _a.classes;
    var _b = React.useState(false), open = _b[0], setOpen = _b[1];
    console.log(edition);
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement(RenderWhile, { desktop: true, print: true },
                React.createElement(Info, { edition: edition, classes: classes })))));
};
export var EditionIndexGreenzeen = withStyles(styles)(EditionIndexGreenzeenComponent);
//# sourceMappingURL=edition-index-greenzeen.js.map