var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { withStyles } from "@material-ui/styles";
import { observer } from "mobx-react";
import * as React from "react";
import { ArticleRedirect } from "../../../content/components/article-redirect";
import { LayoutArticleCover } from "../../layout/article_cover/layout-article-cover";
import { LayoutBondigEditionCover } from "../../layout/bondig-edition-cover/bondig-edition-cover";
import { LayoutGrid } from "../../layout/grid/grid";
import { GridV2 } from "../../layout/grid-v2/grid";
import { take } from "../../../../helpers/general";
import { LayoutMSTEditionCover } from "../../layout/mst-edition-cover/mst-edition-cover";
import { LayoutCarmelkoersEditionCover } from "../../layout/carmelkoers-edition-cover/carmelkoers-edition-cover";
import LoadingAnimation from "../../loading";
import { LayoutCarmelkennismakingEditionCover } from "../../layout/carmelkennismaking-edition-cover/carmelkennismaking-edition-cover";
import { LayoutMSTEdition2020Cover } from "../../layout/mst-2020-edition-cover/mst-edition-cover";
var styles = function (theme) {
    var _a;
    return ({
        root: (_a = {
                minHeight: "100vh",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                padding: theme.spacing(5, 8, 9, 8),
                fontSize: "calc(1rem + 0.3vw)"
            },
            _a[theme.breakpoints.down("md")] = {
                fontSize: "0.5em",
            },
            _a[theme.breakpoints.down("sm")] = {
                fontSize: "calc(1rem + 0.3vw)",
            },
            _a),
        grid: {
            margin: "auto",
            maxWidth: 1200,
            flex: 1,
        },
    });
};
export var Page = withStyles(function (theme) { return ({
    root: {
        width: "100%",
    },
    grid: {
        margin: "auto",
        maxWidth: 1200,
        flex: 1,
    },
}); })(function (_a) {
    var page = _a.page, classes = _a.classes, article = _a.article;
    var style = (page.style && JSON.parse(page.style)) || {};
    // all below should be removed
    if (page.layout === "grid") {
        return (React.createElement("div", { className: classes.root, style: { maxWidth: 1024, marginTop: 0 } },
            React.createElement(LayoutGrid, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data })));
    }
    else if (page.layout === "articleCover") {
        return (React.createElement("div", { className: classes.root, style: {
                maxWidth: style.maxWidth,
                padding: style.padding,
                margin: style.margin,
            } },
            " ",
            React.createElement(LayoutArticleCover, { config: JSON.parse(page.layoutConfig || "{}"), data: page.data, style: page.style })));
    }
    else if (page.layout === "bondig-edition-cover") {
        return (
        // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        React.createElement(LayoutBondigEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })
        // </div>
        );
    }
    else if (page.layout === "carmelkoers-edition-cover") {
        return (
        // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        React.createElement(LayoutCarmelkoersEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })
        // </div>
        );
    }
    else if (page.layout === "carmelkennismaking-edition-cover") {
        return (
        // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        React.createElement(LayoutCarmelkennismakingEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })
        // </div>
        );
    }
    else if (page.layout === "mst-edition-cover") {
        return (
        // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        React.createElement(LayoutMSTEditionCover, { config: JSON.parse(page.layoutConfig || "{}") })
        // </div>
        );
    }
    else if (page.layout === "mst-edition-2020-cover") {
        return (
        // <div className={classes.root} style={{ maxWidth: style.maxWidth }}>
        React.createElement(LayoutMSTEdition2020Cover, { config: JSON.parse(page.layoutConfig || "{}") })
        // </div>
        );
    }
    return React.createElement("div", null, "Unknown layout");
});
export var ArticleTabletContent = withStyles(styles)(observer(function (_a) {
    var presenter = _a.presenter, classes = _a.classes;
    if (presenter.loading) {
        return React.createElement(LoadingAnimation, null);
    }
    if (!presenter.currentPage || !presenter.article) {
        return React.createElement("div", null, "Page not found");
    }
    var page = presenter.currentPage;
    if (presenter.article && page && page.layouts && page.layouts.md && page.layouts.md.length) {
        return (React.createElement(GridV2, { article: presenter.article, page: page, presenter: presenter, breakpoint: "md" }));
    }
    // fall back to desktop
    else if (page.layouts && page.layouts.lg && page.layouts.lg.length) {
        return (React.createElement(GridV2, { article: presenter.article, page: page, presenter: presenter, breakpoint: "lg" }));
    }
    var styles = (presenter.article && presenter.article.style && JSON.parse(presenter.article.style)) || {};
    var pagestyles = (presenter.currentPage &&
        presenter.currentPage.style &&
        JSON.parse(presenter.currentPage.style)) ||
        {};
    return (React.createElement("div", { style: take(styles, [
            "background",
            "backgroundColor",
            "backgroundImage",
            "backgroundSize",
            "backgroundPosition",
            "color",
        ]) },
        React.createElement("div", { className: classes.root, style: __assign({ justifyContent: "space-evenly" }, [
                "background",
                "color",
                "backgroundSize",
                "backgroundPosition",
                "backgroundImage",
                "backgroundRepeat",
                "backgroundColor",
                "alignItems",
                "justifyContent",
                "padding",
            ].reduce(function (styles, key) {
                if (pagestyles[key] !== undefined) {
                    styles[key] = pagestyles[key];
                }
                return styles;
            }, {})), ref: function (elem) {
                if (elem) {
                    if (window.document.documentMode) {
                        setTimeout(function () {
                            elem.style.height = elem.clientHeight + "px";
                        }, 500);
                    }
                }
            } },
            React.createElement(Page, { article: presenter.article, page: presenter.currentPage }),
            !presenter.hasNextPage &&
                page.layout !== "mst-edition-cover" &&
                presenter.hasNextArticle &&
                presenter.currentPage.layout !== "bondig-edition-cover" &&
                presenter.currentPage.layout !== "mst-edition-2020-cover" &&
                presenter.currentPage.layout !== "carmelkoers-edition-cover" &&
                presenter.currentPage.layout !== "carmelkennismaking-edition-cover" && (React.createElement(ArticleRedirect, { onClick: presenter.toNextArticle, thumbnail: presenter.nextArticle &&
                    (presenter.nextArticle.featuredImage
                        ? presenter.nextArticle.featuredImage
                        : presenter.nextArticle.image) }, presenter.nextArticle && presenter.nextArticle.title)))));
}));
//# sourceMappingURL=tablet.js.map