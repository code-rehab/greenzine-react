import * as React from "react";

import { Theme, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { mapEvent } from "../../../helpers/formatters";
// import { RenderElement } from "./renderElement";
import {
  RenderElement,
  createElement,
  CoverButtonElement,
  ElementType,
} from "@coderehab/greenzeen-content";

interface OwnProps {
  id: string;
  index: number;
  title: string;
  image: string;
  version?: string;
  onSelect(id: string): void;
}

export const CoverButton = ({ id, index, image, title, onSelect, version }: OwnProps) => {
  return (
    <div onClick={() => onSelect(id)}>
      <RenderElement
        element={createElement<CoverButtonElement>({
          component: ElementType.CoverButton,
          config: {
            image,
            title,
            index: index.toString(),
            style: {},
          },
        })}
      />
    </div>
  );
};
