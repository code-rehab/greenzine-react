import * as React from "react";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { withPresenter } from "../../helpers/with-presenter";
import { BasicPagePresenter } from "./_page-basic-presenter";
var styles = function (theme) { return ({
    root: {
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    }
}); };
export var Error404Content = withStyles(styles)(function (props) { return (React.createElement("main", { className: props.classes.root },
    React.createElement(Typography, { variant: "h1", style: { color: "#000" } }, "404"),
    React.createElement(Typography, { variant: "body1" }, "Resource not found"))); });
var Component = function (props) {
    return React.createElement(Error404Content, null);
};
export var Error404 = withPresenter(function (props) { return new BasicPagePresenter(props); }, Component);
//# sourceMappingURL=error-404.js.map