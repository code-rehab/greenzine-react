import { computed, observable } from "mobx";
import { route } from "../../../config/routes";
import { Application } from "../../application";
import { MagazineInteractor } from "../magazine/magazine-interactor";
import { Article } from "./article";
import { Page } from "../page/page";
import { ArticleProvider } from "./article-provider";

export interface ArticleInteractor {
  articles: Article[];
  transition: any;
  selectArticle(id: string, redirect?: boolean): Promise<void>;
  selectedArticle: Article | undefined;
  selectedPage: Page | undefined;
  selectPage(id: string, redirect?: boolean): Promise<void>;
  nextPage(): void;
  previousPage(): void;
  nextArticle(): void;
  previousArticle(): void;
  pageIndex: number;
  articleIndex: number;
  pages: Page[];
  onArticleChange(func: () => void): void;
}

export class DefaultArticleInteractor implements ArticleInteractor {
  constructor(
    private _articleProvider: ArticleProvider,
    private _magazineInteractor: MagazineInteractor
  ) {}

  @observable public created: Article | undefined = undefined;
  @observable public selectedArticle: Article | undefined = undefined;
  @observable public selectedPage: Page | undefined = undefined;
  @observable public previous: Article | undefined = undefined;

  public transition: any = "cross-fade";
  private _listeners: Array<() => void> = [];
  private _animating: boolean = false;

  @computed public get articles(): Article[] {
    return (
      (this._magazineInteractor.selectedEdition &&
        this._magazineInteractor.selectedEdition.articles) ||
      []
    );
  }

  @computed public get articleIndex(): number {
    return this.articles.findIndex(
      article =>
        article.id === (this.selectedArticle && this.selectedArticle.id)
    );
  }

  @computed public get pages(): Page[] {
    return (this.selectedArticle && this.selectedArticle.pages) || [];
  }

  @computed public get pageIndex(): number {
    return this.pages.findIndex(
      page => page.id === (this.selectedPage && this.selectedPage.id)
    );
  }

  public nextArticle = () => {
    if (this._animating) {
      return;
    }

    this.transition = "slide-left";
    const index = this.articleIndex + 1;
    if (this.articles[index]) {
      this.selectArticle(this.articles[index].id);
    }
  };

  public previousArticle = () => {
    if (this._animating) {
      return;
    }

    this.transition = "slide-right";
    const index = this.articleIndex - 1;
    if (this.articles[index]) {
      this.selectArticle(this.articles[index].id);
    }
  };

  public nextPage = () => {
    if (this._animating) {
      return;
    }

    this.transition = "slide-up";
    const index = this.pageIndex + 1;
    if (this.pages[index]) {
      this.selectPage(this.pages[index].id);
    }
  };

  public previousPage = () => {
    if (this._animating) {
      return;
    }

    this.transition = "slide-down";
    const index = this.pageIndex - 1;
    if (this.pages[index]) {
      this.selectPage(this.pages[index].id);
    }
  };

  public selectPage = async (id?: string, redirect: boolean = true) => {
    if (id === (this.selectedPage && this.selectedPage.id) || this._animating) {
      return;
    }

    let result =
      this.selectedArticle &&
      this.selectedArticle.pages &&
      this.selectedArticle.pages.filter(page => {
        return page.id === id;
      });

    this.selectedPage = result && result[0];

    if (redirect && this.selectedPage) {
      this._animating = true;
      setTimeout(() => {
        this._animating = false;
      }, 500);

      Application.router &&
        Application.router.history.push(
          route("article.page", {
            magazine:
              (this._magazineInteractor.selectedMagazine &&
                this._magazineInteractor.selectedMagazine.id) ||
              "undefined",
            edition:
              (this._magazineInteractor.selectedEdition &&
                this._magazineInteractor.selectedEdition.id) ||
              "undefined",
            article:
              (this.selectedArticle &&
                (this.selectedArticle.slug || this.selectedArticle.id)) ||
              "undefined",
            page:
              (this.selectedPage &&
                (this.selectedPage.slug || this.selectedPage.id)) ||
              "undefined"
          })
        );
    }
  };

  public selectArticle = async (id: string, redirect: boolean = true) => {
    if (
      id === (this.selectedArticle && this.selectedArticle.id) ||
      this._animating
    ) {
      return;
    }

    const found = this._articleProvider.get(id);

    if (found) {
      this.selectedPage = undefined;
      await found.fetch();
      this.selectedArticle = found;
      this._listeners.forEach(func => func());
      const pages = (this.selectedArticle && this.selectedArticle.pages) || [];
      this.selectPage(pages[0] && pages[0].id, redirect);
    }
  };

  public onArticleChange = (func: () => void) => {
    this._listeners.push(func);
  };
}
