import { PageFragment } from "./fragments";

export const PageCreated = `
subscription {
  pageCreated {
    ${PageFragment}
  }
}
`;

export const PageUpdated = `
subscription {
  pageUpdated {
    ${PageFragment}
  }
}
`;

export const PageDeleted = `
subscription {
  pageDeleted {
    ${PageFragment}
  }
}
`;
