import * as React from "react";
import { Typography, WithStyles, Theme } from "@material-ui/core";
import { withStyles, StyleRules } from "@material-ui/styles";
import { InterfacePresenter } from "../interface-presenter";
import classnames from "classnames";
import { observer } from "mobx-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import { Article } from "../../../application/data/article/article";

interface OwnProps extends WithStyles<"root" | "title" | "article" | "icon" | "arrow" | "clean"> {
  //   presenter: InterfacePresenter;
  title?: string;
  article?: Article;
  url?: any;
  icon?: boolean;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    position: "fixed",
    backgroundImage: "url('https://dg8n28xhgyfkm.cloudfront.net/carmel-koers-2025/kruimelpad2.svg')",
    backgroundSize: "cover",
    color: "#000",
    top: "10px",
    right: "0",
    padding: "12px 40px",
    fontSize: "1.4em",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "franchise",
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  icon: {
    marginRight: 20,
  },
  arrow: {
    margin: "0 10px",
  },
  title: {
    fontWeight: "bold",
    textTransform: "uppercase",
    textDecoration: "underline",
  },
  article: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  // Used by kennismaking carmel
  clean: {
    backgroundImage: "unset",
    backgroundColor: "#fff",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "PT Sans",
    color: "#000",
    "& svg": {
      fontSize: 12,
    },
  },
});

export const Breadcrumb = withStyles(styles)(
  observer(({ classes, title, article, url, icon }: OwnProps) => {
    return (
      <>
        {article &&
        article.title &&
        article.title.toLowerCase() !== "cover" &&
        process.env.REACT_APP_BREADCRUMB == "true" ? (
          <div
            className={classnames(
              process.env.REACT_APP_BREADCRUMB_STYLE == "clean" ? [classes.root, classes.clean] : [classes.root]
            )}
          >
            {" "}
            {icon ? <FontAwesomeIcon size="2x" icon={faMapMarkerAlt} className={classes.icon} /> : ""}
            <NavLink className={classes.title} to={"/" + (url && url.id) || "/"}>
              {process.env.REACT_APP_BREADCRUMB_TITLE || process.env.REACT_APP_NAME}
            </NavLink>
            <span className={classes.article}>
              <FontAwesomeIcon size="1x" icon={faChevronRight} className={classes.arrow} />
              {article && article.title}
            </span>{" "}
          </div>
        ) : (
          ""
        )}
      </>
    );
  })
);
