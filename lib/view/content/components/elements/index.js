import * as React from "react";
import { withStyles } from "@material-ui/core";
var styles = function (theme) { return ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        transform: "translate(25%, -60%)",
        borderRadius: "100%",
        whiteSpace: "nowrap",
        color: "#e9550d",
        backgroundColor: "#FFC586",
        width: theme.spacing(4),
        height: theme.spacing(4),
        right: 0,
        top: 0,
    },
}); };
var IndexComponent = function (_a) {
    var classes = _a.classes, items = _a.items;
    console.log("COVER items", items);
    return (React.createElement("div", { className: classes.root }, "Indexer"));
};
export var Index = withStyles(styles)(IndexComponent);
//# sourceMappingURL=index.js.map