import * as React from "react";
import { WithStyles } from "@material-ui/core";
import { Article } from "../../../application/data/article/article";
interface OwnProps extends WithStyles<"root" | "title" | "article" | "icon" | "arrow" | "clean"> {
    title?: string;
    article?: Article;
    url?: any;
    icon?: boolean;
}
export declare const Breadcrumb: React.ComponentType<Pick<OwnProps, "title" | "article" | "icon" | "url"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
