import * as React from "react";
import { Theme, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import { RenderWhile } from "../../../content/components/render-while";
import { ArticleCoverDesktop } from "./device/desktop";
import { ArticleCoverMobile } from "./device/mobile";
import { ArticleCoverPrint } from "./device/print";

interface OwnProps extends WithStyles<any> {
  config: any;
  data: any;
  style: any;
}

const styles = (theme: Theme): StyleRules => ({
  root: {},
});

export const LayoutArticleCover = withStyles(styles)((props: OwnProps) => {
  props.data.forEach((d: any, i: number) => {
    d.animation = {
      duration: 700,
      delay: i * 100,
    };
  });

  return (
    <>
      <RenderWhile desktop>
        <ArticleCoverDesktop {...props} />
      </RenderWhile>
      <RenderWhile mobile>
        <ArticleCoverMobile {...props} />
      </RenderWhile>
      <RenderWhile print>
        <ArticleCoverPrint {...props} />
      </RenderWhile>
    </>
  );
});
