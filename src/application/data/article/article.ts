import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { ElementRecord } from "../element/element";
import { PageRecord, Page, PageModel } from "../page/page";
import { Application } from "../../application";
import { Collection } from "../../network/graphql-collection";
import { computed } from "mobx";
import { replaceAll } from "../../../helpers/general";

export interface ArticleRecord {
  id: string;
  index?: string;
  title: string;
  description?: string;
  content?: string;
  slug?: string;
  image?: string;
  backgroundImage?: string;
  pages: PageRecord[];
  type?: string;
  color?: string;
  featuredImage?: string;
  style?: any;
  pageOrder?: string[];
}

export interface Article extends GraphQLModel<ArticleRecord>, ArticleRecord {
  pages: Page[];
  styleObj?: any;
}

export class ArticleModel extends GraphQLBase<ArticleRecord> implements Article {
  protected _pagesInitialized: boolean = false;
  protected _pages: Page[];

  @data public title = "";
  @data public content = "";
  @data public index = "";
  @data public slug = "";
  @data public image = "";
  @data public backgroundImage = "";
  @data public description = "";
  @data public config = "";
  @data public type = "";
  @data public color = "";
  @data public featuredImage = "";
  @data public style = "{}";
  @data public data = [];
  @data public pageOrder = [];

  public serialize = () => {
    const serialized: any = this._values;
    serialized.pages = this.pages.map((page) => page.id);
    return serialized;
  };

  // public get pages(): Page[] {
  //   const pageRecords = (this.record && this.record.pages) || [];
  //   const provider = Application.business.provider.page;
  //   const pages = pageRecords.map((data) => new PageModel(data, provider));
  //   const pageOrder =
  //     this.pageOrder && this.pageOrder.length ? this.pageOrder : pages.map((p) => p.id);

  //   return pageOrder!.map((id) => pages.find((p) => p.id === id)) as Page[];
  // }

  public get pageCollection(): Collection<Page> {
    const pageRecords = (this.record && this.record.pages) || [];
    const provider = Application.business.provider.page;

    return provider.collect(pageRecords) as any;
  }

  public get pages(): Page[] {
    const pages = this.pageCollection.items.map((page) => {
      page.articleId = this.id;
      page.article = this;
      return page;
    });

    const pageOrder = this.pageOrder && this.pageOrder.length ? this.pageOrder : pages.map((p) => p.id);

    return (pageOrder || []).map((id) => pages.find((p) => p.id === id)).filter((p) => p) as Page[];
  }

  @computed
  public get styleObj() {
    const distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
    const style = JSON.parse(replaceAll(this.style || "{}", "/article/images/", distribution + "/ncf/bondig/"));

    return style;
  }
}
