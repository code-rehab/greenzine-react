import { Button, Collapse, Theme, Typography, WithStyles } from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";

import * as React from "react";
import { EditionData } from "../../../../application/data/edition/edition";
import { RenderWhile } from "../../components/render-while";

interface OwnProps extends WithStyles<any> {
  edition: EditionData;
}

const styles = (theme: Theme): StyleRules => ({
  root: {
    margin: "auto",
  },

  drawerBackdrop: {},
  header: {},
  content: {},
  backdrop: {
    backgroundColor: "transparant",
  },
  title: {
    color: "#000",
    margin: "50px 0 20px",
    lineHeight: 0.9,
    "& br": {
      display: "none",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: 50,
      fontSize: "2.6em",
    },
  },
  subtitle: {
    fontWeight: 700,
    margin: "20px 0",
    color: "#4D4D4F",
  },
  description: {
    paddingRight: theme.spacing(1),
  },
  collapseContainer: {
    marginBottom: theme.spacing(3),
    position: "relative",

    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      left: 0,
      bottom: 0,
      width: "100%",
      height: "100%",
      boxShadow: "inset 0px -70px 40px -40px rgb(252, 197, 0)",
    },
    "&.MuiCollapse-entered": {
      "&:after": {
        boxShadow: "none",
      },
    },
  },
  contentPiece: {
    fontFamily: "Montserrat",
  },
  marginBottom: {
    marginBottom: "1em",
  },
});

const Info = ({ edition, classes }: OwnProps) => {
  const description = (edition && edition.description && JSON.parse(edition.description)) || "";

  return (
    <div className={classes.description}>
      <Typography variant={"h2"} className={classes.title}>
        Toekomst-
        <br />
        bouwers
      </Typography>

      <Typography paragraph>
        Je staat op het punt om de publieksversie van onze Koers 2025 te lezen. Dit digitale
        magazine is een ingekorte en meer toegankelijke versie van het officiële koersdocument. In
        dit magazine vind je allerlei linkjes naar de formele tekst, die vaak wat meer verdieping
        biedt.
      </Typography>

      <Typography paragraph className={classes.description}>
        De volledige, officiële tekst van Koers 2025 lees je{" "}
        <a target="_blank" href="https://carmel.nl/specials/Koers2025/download-de-gehele-koers-pdf">
          hier
        </a>
        .
      </Typography>

      <Typography className={classes.description}>- Veel leesplezier!</Typography>
    </div>
  );
};

const EditionIndexCarmelKoersComponent = ({ edition, classes }: OwnProps) => {
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div>
        <RenderWhile desktop tablet print>
          <Info edition={edition} classes={classes} />
        </RenderWhile>

        <RenderWhile mobile>
          <Collapse
            in={open}
            collapsedHeight={210}
            classes={{ wrapper: classes.collapseContainer }}
          >
            <Info edition={edition} classes={classes} />
          </Collapse>
          <Button
            color="primary"
            variant="contained"
            style={{ display: "flex", marginLeft: "auto" }}
            onClick={() => setOpen(!open)}
          >
            {open ? "Verberg" : "Lees meer"}
          </Button>
        </RenderWhile>
      </div>
    </>
  );
};

export const EditionIndexCarmelKoers = withStyles(styles)(EditionIndexCarmelKoersComponent);
