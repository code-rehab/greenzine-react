import * as React from "react";
import { Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { NavLink } from "react-router-dom";
// import $ from 'jquery';
var styles = function (theme) { return ({
    root: {
        margin: 0,
        display: "flex",
        flexDirection: "column",
        paddingBottom: theme.spacing(2),
    },
    imgHover: {
        transition: theme.transitions.create(["transform", "background"], {
            duration: "0.3s",
        }),
        "&:hover img": {
            transform: "translateY(-15px)",
        },
    },
    image: {
        width: "100%",
        flexShrink: 0,
        flexGrow: 0,
        overflow: "hidden",
        msInterpolationMode: "bicubic",
        transition: theme.transitions.create(["transform", "background"], {
            duration: "0.3s",
        }),
    },
    caption: {
        display: "block",
        marginLeft: theme.spacing(2),
        lineHeight: "2em",
        position: "relative",
        color: "inherit",
        marginTop: -5,
        "&::before": {
            content: "''",
            left: -10,
            top: "-36px",
            zIndex: 2,
            width: 1,
            height: 54,
            backgroundColor: "currentColor",
            position: "absolute",
        },
    },
}); };
export var Image = withStyles(styles)(function (props) {
    var animation = props.animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay }, props.url ? (React.createElement("figure", { className: props.classes.root, style: props.styles || {} },
        props.url.includes("https://") ||
            props.url.includes("https://") ||
            props.url.includes("mailto:") ? (React.createElement("a", { href: props.url, target: props.target ? "_blank" : "" },
            React.createElement("picture", null,
                props.mobilesrc && React.createElement("source", { media: "(max-width: 960px)", srcSet: props.mobilesrc }),
                React.createElement("img", { id: "image", alt: "", onLoad: function (elem) {
                        window.dispatchEvent(new Event("resize"));
                    }, src: props.src, className: props.classes.image, style: { boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" } })))) : (React.createElement(NavLink, { to: props.url, className: props.classes.imgHover },
            React.createElement("picture", null,
                props.mobilesrc && React.createElement("source", { media: "(max-width: 960px)", srcSet: props.mobilesrc }),
                React.createElement("img", { id: "image", alt: "", onLoad: function (elem) {
                        window.dispatchEvent(new Event("resize"));
                    }, src: props.src, className: props.classes.image, style: { boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" } })))),
        props.caption && (React.createElement(Typography, { variant: "caption", className: props.classes.caption }, props.caption)))) : (React.createElement("figure", { className: props.classes.root, style: props.styles || {} },
        React.createElement("picture", null,
            props.mobilesrc && React.createElement("source", { media: "(max-width: 960px)", srcSet: props.mobilesrc }),
            React.createElement("img", { id: "image", alt: "", onLoad: function (elem) {
                    window.dispatchEvent(new Event("resize"));
                }, src: props.src, style: { boxShadow: props.shadow ? "20px 20px 40px rgba(0,0,0,0.16)" : "none" }, className: props.classes.image })),
        props.caption && (React.createElement(Typography, { variant: "caption", className: props.classes.caption }, props.caption))))));
});
//# sourceMappingURL=image.js.map