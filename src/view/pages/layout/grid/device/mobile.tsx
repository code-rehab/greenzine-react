import * as React from "react";
import { Grid } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";

export const LayoutColumnsMobile = ({ data }: any) => {
  return (
    <Grid container spacing={data.container.mobileSpacing || 0}>
      {data.sections.map((cfg: any) => {
        return (
          <Grid item xs={12} {...(cfg.props || {})}>
            {cfg.data.map((d: any) => {
              return <RenderElement element={d} />;
            })}
          </Grid>
        );
      })}
    </Grid>
  );
};
