import * as React from "react";
import { withStyles } from "@material-ui/core";
import classNames from "classnames";
import { SlideFade } from "../effects/slide-fade";
export var Divider = withStyles(function (theme) { return ({
    line: {
        borderBottom: "5px solid",
        margin: theme.spacing(3, 0)
    }
}); })(function (_a) {
    var classes = _a.classes, variant = _a.variant, animation = _a.animation, styles = _a.styles;
    animation = animation || { duration: 0, delay: 0 };
    return (React.createElement(SlideFade, { direction: "up", timeout: animation.duration, delay: animation.delay },
        React.createElement("div", { className: classNames(classes[variant]), style: styles || {} })));
});
//# sourceMappingURL=divider.js.map