import { ArticleInteractor } from "../data/article/article-interactor";
import { ArticleProvider } from "../data/article/article-provider";
import { EditionProvider } from "../data/edition/edition-provider";
import { MagazineInteractor } from "../data/magazine/magazine-interactor";
import { MagazineProvider } from "../data/magazine/magazine-provider";
import { PollProvider } from "../data/poll/poll-provider";
import { BaseModule } from "../module";
import { NetworkModule } from "../network/network";
import { PrintInteractor } from "./interactor/print-interactor";
import { PageProvider } from "../data/page/page-provider";
export interface BusinessInteractors {
    magazine: MagazineInteractor;
    article: ArticleInteractor;
    print: PrintInteractor;
}
export interface BusinessProviders {
    magazine: MagazineProvider;
    edition: EditionProvider;
    article: ArticleProvider;
    page: PageProvider;
    poll: PollProvider;
}
export interface BusinessModules {
    interactor: BusinessInteractors;
    provider: BusinessProviders;
}
export declare class DefaultBusinessModule extends BaseModule<{}, {}, BusinessModules> implements BusinessModules {
    private _network;
    constructor(_network: NetworkModule);
    get interactor(): BusinessInteractors;
    get provider(): BusinessProviders;
    get mockMode(): boolean;
    get printLayout(): boolean;
}
