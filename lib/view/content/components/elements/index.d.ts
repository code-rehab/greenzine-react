import * as React from "react";
import { WithStyles } from "@material-ui/core";
interface OwnProps extends WithStyles<"root" | "text"> {
    items: any;
}
export declare const Index: React.ComponentType<Pick<OwnProps, "items"> & import("@material-ui/core").StyledComponentProps<string>>;
export {};
