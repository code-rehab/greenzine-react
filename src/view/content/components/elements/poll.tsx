import * as React from "react";
import { Theme, Typography, withStyles } from "@material-ui/core";
import { SlideFade } from "../effects/slide-fade";
import { Poll } from "./poll/poll";
import { RenderWhile } from "../render-while";

const styles = (theme: Theme) => ({
  root: {},
});

export const PollElement = withStyles(styles)((props: any) => {
  const animation = props.animation || { duration: 0, delay: 0 };
  const { pollId } = props;

  return (
    <>
      <RenderWhile desktop tablet mobile>
        <SlideFade direction="up" timeout={animation.duration} delay={animation.delay}>
          <Poll pollId={pollId}></Poll>
        </SlideFade>
      </RenderWhile>

      <RenderWhile print>
        <Typography>De printfunctie van de poll is momenteel niet beschikbaar</Typography>
      </RenderWhile>
    </>
  );
});
