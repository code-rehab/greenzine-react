import * as React from "react";
export declare const ProgressBar: React.ComponentType<Pick<any, string | number | symbol> & import("@material-ui/core").StyledComponentProps<string>>;
