import * as React from "react";
import { observer } from "mobx-react";
import GridLayout, { WidthProvider } from "react-grid-layout";
import { toJS } from "mobx";
import { withPresenter } from "../../../../helpers/with-presenter";
import { LayoutRendererPresenter } from "./layout-renderer-presenter";
import { makeStyles } from "@material-ui/core";
import { RenderElement } from "@coderehab/greenzeen-content";
var GridLayoutRenderer = WidthProvider(GridLayout);
var useStyles = makeStyles({
    root: {
        margin: "0 !important",
        "& .react-grid-item": {
            touchAction: "auto !important",
            margin: "0 !important",
        },
    },
});
export var RendererComponent = observer(function (_a) {
    var presenter = _a.presenter, print = _a.print;
    var layout = presenter.layout, elements = presenter.elements, registerRef = presenter.registerRef, containerPadding = presenter.containerPadding, rowHeight = presenter.rowHeight, margin = presenter.margin, cols = presenter.cols;
    var classes = useStyles();
    return (React.createElement("div", { className: classes.root },
        React.createElement(GridLayoutRenderer, { layout: toJS(layout), isResizable: false, isDraggable: false, isDroppable: false, margin: margin, cols: cols, rowHeight: rowHeight, containerPadding: containerPadding, autoSize: true }, elements.map(function (element) { return (React.createElement("div", { key: element.id },
            React.createElement("div", { ref: function (ref) { return registerRef(element.id, ref); }, style: { padding: 1 } },
                React.createElement(RenderElement, { print: print, element: element })))); }))));
});
export var LayoutRenderer = withPresenter(function (_a, _b) {
    var layout = _a.layout, elements = _a.elements;
    var interactor = _b.interactor;
    return new LayoutRendererPresenter(layout, elements, interactor.print);
}, RendererComponent);
//# sourceMappingURL=layout-renderer.js.map