var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed, observable } from "mobx";
// import { Article } from "../../application/data/article/article";
var InterfacePresenter = /** @class */ (function () {
    // Initialisation
    function InterfacePresenter(_magazineInteractor, _magazineProvider, _articleInteractor, _articleProvider, _printInteractor) {
        var _this = this;
        this._magazineInteractor = _magazineInteractor;
        this._magazineProvider = _magazineProvider;
        this._articleInteractor = _articleInteractor;
        this._articleProvider = _articleProvider;
        this._printInteractor = _printInteractor;
        this._scrollTimeout = undefined;
        this._xDown = 0;
        this._yDown = 0;
        this._resetTimeout = null;
        this.pollOpen = true;
        this._pageYOffset = 0;
        this._scrollHeight = 0;
        this.aroundStart = false;
        this.aroundEnd = false;
        this.mount = function () {
            _this._articleInteractor.onArticleChange(_this.resetValues);
            _this._scrollHeight = document.body.scrollHeight;
            _this._pageYOffset = window.pageYOffset;
            document.addEventListener("touchstart", _this.handleTouchStart, false);
            document.addEventListener("touchmove", _this.handleTouchMove, false);
            window.addEventListener("keydown", _this.onKeyDown);
            window.addEventListener("wheel", _this.onScroll);
            window.onresize = _this.resetValues;
            window.onscroll = function () {
                _this._pageYOffset = window.pageYOffset;
            };
        };
        // Start/End Helpers
        this.resetValues = function () {
            _this._pageYOffset = window.pageYOffset;
            _this._scrollHeight = document.body.scrollHeight;
            _this.aroundStart = false;
            _this.aroundEnd = false;
            clearTimeout(_this._resetTimeout);
            _this._resetTimeout = setTimeout(function () {
                _this.checkScrollHeight();
            }, 1500);
        };
        this.checkScrollHeight = function () {
            if (_this._scrollHeight !== document.body.scrollHeight) {
                _this._scrollHeight = document.body.scrollHeight;
            }
            _this.aroundStart = _this._pageYOffset < 70;
            _this.aroundEnd = _this._pageYOffset + window.innerHeight > _this._scrollHeight - 70;
        };
        this.unmount = function () {
            window.removeEventListener("keydown", _this.onKeyDown);
        };
        // Actions
        this.selectPage = function (pageId) {
            var index = _this.pages.findIndex(function (page) { return page.id === pageId; });
            if (index === _this.pageIndex) {
                return;
            }
            if (index > _this.pageIndex) {
                _this._articleInteractor.transition = "slide-up";
            }
            else {
                _this._articleInteractor.transition = "slide-down";
            }
            _this._articleInteractor.selectPage(pageId);
        };
        this.toNextPage = function () {
            _this._pageYOffset = 0;
            _this._articleInteractor.nextPage();
        };
        this.toPreviousPage = function () {
            _this._pageYOffset = 0;
            _this._articleInteractor.previousPage();
        };
        this.toNextArticle = function () {
            _this._pageYOffset = 0;
            _this._articleInteractor.nextArticle();
        };
        this.toPreviousArticle = function () {
            _this._pageYOffset = 0;
            _this._articleInteractor.previousArticle();
        };
        this.hidePoll = function () {
            _this.pollOpen = false;
        };
        this.toPoll = function () {
            _this._articleInteractor.selectArticle((_this.edition && _this.edition.pollSlug) || "", true);
        };
        this.printArticle = function () {
            _this._printInteractor.printActive = !_this._printInteractor.printActive;
        };
        this.sharePage = function (e) {
            document.execCommand("copy", false, window.location.href);
            // Werkt niet?
        };
        // Privates
        this.onKeyDown = function (e) {
            if (e.defaultPrevented) {
                return;
            }
            switch (e.key) {
                case "Up":
                case "ArrowUp":
                    if (_this.isDesktop) {
                        _this.toPreviousPage();
                    }
                    break;
                case "Down":
                case "ArrowDown":
                    if (_this.isDesktop) {
                        _this.toNextPage();
                    }
                    break;
                case "Left":
                case "ArrowLeft":
                    _this.toPreviousArticle();
                    break;
                case "Right":
                case "ArrowRight":
                    _this.toNextArticle();
                    break;
                default:
                    break;
            }
        };
        this.onScroll = function (e) {
            var delta = Math.sign(e.deltaY);
            var strength = Math.abs(e.deltaY);
            var scrollStart = window.pageYOffset === 0;
            var scrollEnd = window.pageYOffset + window.innerHeight === document.body.scrollHeight;
            if ((scrollStart || scrollEnd) && strength > 250) {
                if (!_this.isDesktop || _this.isPrint) {
                    return;
                }
                _this.checkScrollHeight();
                if (!_this._scrollTimeout) {
                    if (delta > 0) {
                        if (_this.hasNextPage) {
                            _this.toNextPage();
                        }
                    }
                    if (delta < 0) {
                        if (_this.hasPreviousPage) {
                            _this.toPreviousPage();
                        }
                    }
                }
            }
            _this._scrollTimeout = setTimeout(function () {
                _this._scrollTimeout = undefined;
                _this.checkScrollHeight();
            }, 500);
        };
        this.getTouches = function (evt) {
            return (evt.touches || evt.originalEvent.touches // browser API
            ); // jQuery
        };
        this.handleTouchStart = function (evt) {
            var firstTouch = _this.getTouches(evt)[0];
            _this._xDown = firstTouch.clientX;
            _this._yDown = firstTouch.clientY;
        };
        this.handleTouchMove = function (evt) {
            if (!_this._xDown || !_this._yDown) {
                return;
            }
            var xUp = evt.touches[0].clientX;
            var yUp = evt.touches[0].clientY;
            var xDiff = _this._xDown - xUp;
            var yDiff = _this._yDown - yUp;
            if (Math.abs(xDiff) > Math.abs(yDiff)) {
                /*most significant*/
                if (xDiff > 15) {
                    _this.toNextArticle();
                }
                if (xDiff < -15) {
                    _this.toPreviousArticle();
                }
            }
            else {
                _this.checkScrollHeight();
                setTimeout(_this.checkScrollHeight, 500);
                if (yDiff > 0) {
                    /* up swipe */
                }
                else {
                    /* down swipe */
                }
            }
            /* reset values */
            _this._xDown = 0;
            _this._yDown = 0;
        };
        //
    }
    Object.defineProperty(InterfacePresenter.prototype, "articles", {
        // Lists
        get: function () {
            return this._articleInteractor.articles;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "pages", {
        get: function () {
            return this._articleInteractor.pages;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "magazine", {
        // Current Objects
        get: function () {
            return this._magazineInteractor.selectedMagazine;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "edition", {
        get: function () {
            return this._magazineInteractor.selectedEdition;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "article", {
        get: function () {
            return this._articleInteractor.selectedArticle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "page", {
        get: function () {
            return this._articleInteractor.selectedPage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "color", {
        get: function () {
            var color = this._articleInteractor.selectedPage && this._articleInteractor.selectedPage.color;
            return color || "white";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "pageIndex", {
        // Indexes
        get: function () {
            return this._articleInteractor.pageIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "articleIndex", {
        get: function () {
            return this._articleInteractor.articleIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "nextPage", {
        // Previous / Next components
        get: function () {
            return this.pageIndex + 1 < this.pages.length ? this.pages[this.pageIndex + 1] : undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "previousPage", {
        get: function () {
            return this.pageIndex - 1 >= 0 ? this.pages[this.pageIndex - 1] : undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "nextArticle", {
        get: function () {
            return this.articleIndex + 1 < this.articles.length ? this.articles[this.articleIndex + 1] : undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "previousArticle", {
        get: function () {
            return this.articleIndex - 1 >= 0 ? this.articles[this.articleIndex - 1] : undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "showPollButton", {
        // Booleans
        get: function () {
            return (this.pollOpen &&
                this.edition !== undefined &&
                !this.isPoll &&
                ((!this.isDesktop && !this.aroundEnd) || this.isDesktop));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "showNextArticle", {
        get: function () {
            if (this.nextArticle && this.aroundEnd) {
                if (this.isDesktop && !this.nextPage) {
                    return true;
                }
                if (!this.isDesktop) {
                    return true;
                }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isDesktop", {
        get: function () {
            return window.innerWidth > 960;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isPrint", {
        get: function () {
            return this._printInteractor.printActive;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isCover", {
        get: function () {
            return this.article && this.article.type === "cover" ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isPoll", {
        get: function () {
            return this.article && this.article.type === "poll" ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isArticle", {
        get: function () {
            if (!this.article)
                return false;
            return this.article.type === "article";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "hasNextPage", {
        get: function () {
            return this.nextPage ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "hasNextArticle", {
        get: function () {
            return this.nextArticle ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "hasPreviousPage", {
        get: function () {
            return this.previousPage ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "hasPreviousArticle", {
        get: function () {
            return this.previousArticle ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InterfacePresenter.prototype, "isSingleMagazine", {
        get: function () {
            return this._magazineProvider.collect().items.length === 1;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        observable
    ], InterfacePresenter.prototype, "pollOpen", void 0);
    __decorate([
        observable
    ], InterfacePresenter.prototype, "_pageYOffset", void 0);
    __decorate([
        observable
    ], InterfacePresenter.prototype, "_scrollHeight", void 0);
    __decorate([
        observable
    ], InterfacePresenter.prototype, "aroundStart", void 0);
    __decorate([
        observable
    ], InterfacePresenter.prototype, "aroundEnd", void 0);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "articles", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "pages", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "magazine", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "edition", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "article", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "page", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "color", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "pageIndex", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "articleIndex", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "nextPage", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "previousPage", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "nextArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "previousArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "showPollButton", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "showNextArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isDesktop", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isPrint", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isCover", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isPoll", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "hasNextPage", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "hasNextArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "hasPreviousPage", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "hasPreviousArticle", null);
    __decorate([
        computed
    ], InterfacePresenter.prototype, "isSingleMagazine", null);
    return InterfacePresenter;
}());
export { InterfacePresenter };
//# sourceMappingURL=interface-presenter.js.map