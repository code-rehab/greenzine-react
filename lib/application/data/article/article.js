var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { data } from "../../network/decorators/graphql-data";
import { GraphQLBase } from "../../network/graphql-model";
import { Application } from "../../application";
import { computed } from "mobx";
import { replaceAll } from "../../../helpers/general";
var ArticleModel = /** @class */ (function (_super) {
    __extends(ArticleModel, _super);
    function ArticleModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._pagesInitialized = false;
        _this.title = "";
        _this.content = "";
        _this.index = "";
        _this.slug = "";
        _this.image = "";
        _this.backgroundImage = "";
        _this.description = "";
        _this.config = "";
        _this.type = "";
        _this.color = "";
        _this.featuredImage = "";
        _this.style = "{}";
        _this.data = [];
        _this.pageOrder = [];
        _this.serialize = function () {
            var serialized = _this._values;
            serialized.pages = _this.pages.map(function (page) { return page.id; });
            return serialized;
        };
        return _this;
    }
    Object.defineProperty(ArticleModel.prototype, "pageCollection", {
        // public get pages(): Page[] {
        //   const pageRecords = (this.record && this.record.pages) || [];
        //   const provider = Application.business.provider.page;
        //   const pages = pageRecords.map((data) => new PageModel(data, provider));
        //   const pageOrder =
        //     this.pageOrder && this.pageOrder.length ? this.pageOrder : pages.map((p) => p.id);
        //   return pageOrder!.map((id) => pages.find((p) => p.id === id)) as Page[];
        // }
        get: function () {
            var pageRecords = (this.record && this.record.pages) || [];
            var provider = Application.business.provider.page;
            return provider.collect(pageRecords);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ArticleModel.prototype, "pages", {
        get: function () {
            var _this = this;
            var pages = this.pageCollection.items.map(function (page) {
                page.articleId = _this.id;
                page.article = _this;
                return page;
            });
            var pageOrder = this.pageOrder && this.pageOrder.length ? this.pageOrder : pages.map(function (p) { return p.id; });
            return (pageOrder || []).map(function (id) { return pages.find(function (p) { return p.id === id; }); }).filter(function (p) { return p; });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ArticleModel.prototype, "styleObj", {
        get: function () {
            var distribution = "//" + process.env["REACT_APP_MEDIA_DISTRIBUTION"];
            var style = JSON.parse(replaceAll(this.style || "{}", "/article/images/", distribution + "/ncf/bondig/"));
            return style;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        data
    ], ArticleModel.prototype, "title", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "content", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "index", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "slug", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "image", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "backgroundImage", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "description", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "config", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "type", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "color", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "featuredImage", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "style", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "data", void 0);
    __decorate([
        data
    ], ArticleModel.prototype, "pageOrder", void 0);
    __decorate([
        computed
    ], ArticleModel.prototype, "styleObj", null);
    return ArticleModel;
}(GraphQLBase));
export { ArticleModel };
//# sourceMappingURL=article.js.map