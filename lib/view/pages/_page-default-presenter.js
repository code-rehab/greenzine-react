var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { observable } from "mobx";
import { createRef } from "react";
import { routes } from "../../config/routes";
import { BasicPagePresenter } from "./_page-basic-presenter";
import { defaultMagazine } from "../../config/defaults";
import ReactGA from "react-ga4";
var PagePresenter = /** @class */ (function (_super) {
    __extends(PagePresenter, _super);
    function PagePresenter(_magazineInteractor, _magazineProvider, _articleInteractor, _articleProvider, _router) {
        var _this = _super.call(this, _router) || this;
        _this._magazineInteractor = _magazineInteractor;
        _this._magazineProvider = _magazineProvider;
        _this._articleInteractor = _articleInteractor;
        _this._articleProvider = _articleProvider;
        _this.loading = true;
        _this.article = undefined;
        _this.edition = undefined;
        _this.magazine = undefined;
        _this.currentPage = undefined;
        _this.hasNextPage = false;
        _this.hasNextArticle = false;
        _this.nextArticle = undefined;
        _this.setup = function () { return __awaiter(_this, void 0, void 0, function () {
            var params, editions, foundBySlug, foundBySlug, trackingId, pageIndex, articleIndex;
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.loading = true;
                        params = this._router.match.params;
                        if (!!this._magazineInteractor.selectedMagazine) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._magazineInteractor.selectMagazine(defaultMagazine.id, // set the default used magazine
                            false)];
                    case 1:
                        _c.sent();
                        _c.label = 2;
                    case 2:
                        if (!params.edition) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._magazineInteractor.selectEdition(params.edition, false)];
                    case 3:
                        _c.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        // select first edition if there is only one.
                        if (this._magazineInteractor.selectedMagazine && this._magazineInteractor.selectedMagazine.editions) {
                            editions = this._magazineInteractor.selectedMagazine.editions;
                            if (editions.length == 1) {
                                window.location.href = "/" + editions[0].id + "/" + editions[0].articles[0].id;
                            }
                            else {
                                this._magazineInteractor.selectedEdition = undefined;
                            }
                        }
                        else {
                            this._magazineInteractor.selectedEdition = undefined;
                        }
                        _c.label = 5;
                    case 5:
                        this.edition = this._magazineInteractor.selectedEdition;
                        if (!params.article) return [3 /*break*/, 7];
                        if (this.edition) {
                            foundBySlug = this.edition.articles.filter(function (art) { return art.slug === params.article; })[0];
                            params.article = (foundBySlug && foundBySlug.id) || params.article;
                        }
                        return [4 /*yield*/, this._articleInteractor.selectArticle(params.article || "", false)];
                    case 6:
                        _c.sent();
                        return [3 /*break*/, 8];
                    case 7:
                        this._articleInteractor.selectedArticle = undefined;
                        _c.label = 8;
                    case 8:
                        this.article = this._articleInteractor.selectedArticle;
                        if (!params.page) return [3 /*break*/, 10];
                        if (this.article) {
                            foundBySlug = this.article.pages.filter(function (p) { return p.slug === params.page; })[0];
                            params.page = (foundBySlug && foundBySlug.id) || params.page;
                        }
                        return [4 /*yield*/, this._articleInteractor.selectPage(params.page, false)];
                    case 9:
                        _c.sent();
                        return [3 /*break*/, 12];
                    case 10:
                        if (!(this.article && this.article.pages.length)) return [3 /*break*/, 12];
                        return [4 /*yield*/, this._articleInteractor.selectPage(this.article.pages[0].id)];
                    case 11:
                        _c.sent();
                        _c.label = 12;
                    case 12:
                        this.magazine = this._magazineInteractor.selectedMagazine;
                        this.currentPage = this._articleInteractor.selectedPage;
                        // Google analytics
                        if ((_a = this.magazine) === null || _a === void 0 ? void 0 : _a.config.gaCode) {
                            trackingId = (_b = this.magazine) === null || _b === void 0 ? void 0 : _b.config.gaCode;
                            if (trackingId) {
                                ReactGA.initialize(trackingId);
                            }
                        }
                        this.loading = false;
                        pageIndex = this._articleInteractor.pageIndex;
                        articleIndex = this._articleInteractor.articleIndex;
                        this.hasNextPage = this._articleInteractor.pages[pageIndex + 1] ? true : false;
                        this.hasNextArticle = this._articleInteractor.articles[articleIndex + 1] ? true : false;
                        this.nextArticle =
                            this._articleInteractor.articleIndex + 1 < this._articleInteractor.articles.length
                                ? this._articleInteractor.articles[this._articleInteractor.articleIndex + 1]
                                : undefined;
                        return [2 /*return*/];
                }
            });
        }); };
        _this.page = routes[_this._router.match.path];
        _this.setup();
        _this.currentPageRef = createRef();
        return _this;
    }
    __decorate([
        observable
    ], PagePresenter.prototype, "page", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "loading", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "article", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "edition", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "magazine", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "currentPage", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "currentPageRef", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "hasNextPage", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "hasNextArticle", void 0);
    __decorate([
        observable
    ], PagePresenter.prototype, "nextArticle", void 0);
    return PagePresenter;
}(BasicPagePresenter));
export { PagePresenter };
//# sourceMappingURL=_page-default-presenter.js.map