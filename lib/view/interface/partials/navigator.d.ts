import * as React from "react";
import { WithStyles } from "@material-ui/styles";
import { InterfacePresenter } from "../interface-presenter";
interface OwnProps extends WithStyles<"root" | "menuLeft" | "logo" | "top" | "bottom" | "left" | "right" | "link" | "editionBtn" | "tooltip"> {
    location?: "top" | "bottom" | "left" | "right";
    presenter: InterfacePresenter;
}
export declare const Navigator: React.ComponentType<Pick<OwnProps, "presenter" | "location"> & import("@material-ui/styles").StyledComponentProps<string>>;
export {};
