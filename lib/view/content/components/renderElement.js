var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from "react";
import { Typography } from "@material-ui/core";
import { Quote } from "./elements/quote";
import { BondigTypography } from "./elements/typography";
import { Image } from "./elements/image";
import { Paper } from "./elements/paper";
import { Video } from "./elements/video";
import { ReadTime } from "./elements/readtime";
import { Author } from "./elements/author";
import { Media } from "./elements/media";
import { List } from "./elements/list";
import { Divider } from "./elements/divider";
import { WistJeDat } from "./elements/wist-je-dat";
import { RenderWhile } from "./render-while";
import { PollElement } from "./elements/poll";
import { LocationInfo } from "./elements/location-info";
import { Index } from "./elements/index";
import { observer } from "mobx-react";
var components = {
    Typography: BondigTypography,
    Quote: Quote,
    Image: Image,
    Paper: Paper,
    Video: Video,
    ReadTime: ReadTime,
    Author: Author,
    Media: Media,
    List: List,
    Divider: Divider,
    Poll: PollElement,
    WistJeDat: WistJeDat,
    LocationInfo: LocationInfo,
    Index: Index
};
export var RenderElement = observer(function (_a) {
    var element = _a.element;
    var Component = element && element.component && components[element.component];
    var componentProps = element.props;
    if (typeof element.props === "string") {
        componentProps = JSON.parse(componentProps);
    }
    if (Component) {
        return (React.createElement(React.Fragment, null,
            React.createElement(RenderWhile, { desktop: true },
                React.createElement(Component, __assign({}, componentProps, { 
                    // style={element.style || {}}
                    animation: element.animation, renderFor: "desktop" }))),
            React.createElement(RenderWhile, { tablet: true },
                React.createElement(Component, __assign({}, componentProps, { 
                    // style={element.style || {}}
                    animation: element.animation, renderFor: "tablet" }))),
            React.createElement(RenderWhile, { mobile: true },
                React.createElement(Component, __assign({}, componentProps, { 
                    // style={element.style || {}}
                    animation: element.animation, renderFor: "mobile" }))),
            React.createElement(RenderWhile, { print: true },
                React.createElement(Component, __assign({}, componentProps, { 
                    // style={element.style || {}}
                    animation: element.animation, renderFor: "print" })))));
    }
    return React.createElement(Typography, null, "Element not found");
});
//# sourceMappingURL=renderElement.js.map