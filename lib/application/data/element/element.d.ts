import { BaseModel, Model, RecordData } from "../../network/base-model";
import { ElementUnion, ElementProps, AnimationProps } from "./types/element";
export declare enum ElementType {
    Typography = "Typography",
    Quote = "Quote",
    ReadTime = "ReadTime",
    Video = "Video",
    Image = "Image",
    WistJeDat = "WistJeDat",
    Divider = "Divider",
    Author = "Author",
    List = "List",
    Popup = "Popup",
    Index = "Index"
}
export interface ElementBase extends Model<ElementRecord> {
}
export interface ElementRecord extends RecordData {
    section: string;
    component: ElementType;
    props: ElementProps | string;
    animation: AnimationProps;
}
export declare type Element = ElementUnion;
export declare class ElementModel extends BaseModel<ElementRecord> implements ElementBase {
    animation: AnimationProps;
    id: string;
    section: string;
    component: ElementType;
    get props(): ElementProps;
    set props(props: ElementProps);
    serialize: () => {
        props: string;
        section: string;
        component: ElementType;
        animation: AnimationProps;
        id: string;
    };
}
