import * as React from "react";
import { WithStyles, StyleRules, Theme, withStyles } from "@material-ui/core";
import { RenderElement } from "../../../../content/components/renderElement";

import classNames from "classnames";

const styles = (theme: Theme): StyleRules => ({
  root: {
    minHeight: "calc(100vh - 60px)",
    display: "flex",
    alignItems: "center",
  },
  centered: {
    textAlign: "center",
    "& > *": {
      display: "inline-block",
      margin: "auto",
      textAlign: "center",
    },
  },
});

interface OwnProps extends WithStyles<any> {
  config: any;
  data: any[];
  style?: any;
}

export const ArticleCoverMobile = withStyles(styles)(({ config, data, classes, style }: OwnProps) => {
  return (
    <section className={classNames(classes.root)} style={{ backgroundImage: style.backgroundImage }}>
      <div className={classNames(classes.inner, classes[config.variant])}>
        {data.map((d: any) => {
          return (
            <div>
              <RenderElement element={d} />
            </div>
          );
        })}
      </div>
    </section>
  );
});
