import { observable } from "mobx";
import { Store } from "../storage/store";
import { BaseModel, Model, RecordData } from "./base-model";
import { GraphQLProviderProps } from "./graphql-provider";
import { appendProperty } from "../../helpers/object-helper";

export interface GraphQLModel<TRecord extends RecordData> extends Model {
  loading: boolean;
  fetched: boolean;
  record: RecordData & Partial<TRecord>;
  isNew: boolean;
  fetch(): Promise<void>;
  save(): Promise<void>;
  delete(): Promise<void>;
  serialize(data?: TRecord): TRecord;
  provider: GraphQLProviderProps<GraphQLModel<TRecord>, TRecord>;
  updateProperty(key: keyof TRecord, value: any): void;
}

type GraphQLProvider<TRecord extends RecordData> = GraphQLProviderProps<GraphQLModel<TRecord>, TRecord> &
  Store<TRecord>;

export class GraphQLBase<TRecord extends RecordData> extends BaseModel<TRecord> {
  @observable public loading: boolean = false;
  @observable public fetched: boolean = false;
  @observable public isNew: boolean = false;

  public get provider(): GraphQLProvider<TRecord> {
    if (!this._provider) {
      throw new Error("Provider is required");
    }
    return this._provider as GraphQLProvider<TRecord>;
  }

  constructor(_record: TRecord, private _provider: GraphQLProviderProps<GraphQLModel<TRecord>, TRecord> | undefined) {
    super(_record);
    this._record = { ...this.provider.getRecord(_record.id), ...this._record };
  }

  public fetch = async () => {
    this.loading = true;
    this.fetched = true;
    await this.provider.fetch(this);
    this.loading = false;
  };

  public save = async () => {
    this.loading = true;
    if (this.isNew) {
      await this.provider.save(this);
    } else {
      await this.provider.update(this);
    }
    this.loading = false;
  };

  public delete = async () => {
    this.loading = true;
    await this.provider.delete(this);
    this.loading = false;
  };

  public updateProperty = (key: keyof TRecord | string, value: any): void => {
    appendProperty(this, key.toString(), 0, value);
  };
}
