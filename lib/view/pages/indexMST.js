import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Drawer, Grid, withStyles, Zoom } from "@material-ui/core";
import { observer } from "mobx-react";
import * as React from "react";
import { withRouter } from "react-router-dom";
import { withPresenter } from "../../helpers/with-presenter";
import { Colofon } from "../content/components/colofon-slide";
import { CoverButton } from "../content/components/cover-button";
import { DefaultIndexInfo } from "../content/index/default-index-info";
import { MagazineIndexInfo } from "../content/index/magazine-index-info";
import { PageIndexPresenter } from "./index-presenter";
import { EditionIndexMST } from "../content/index/mst/edition-index-mst";
import LoadingAnimation from "./loading";
var styles = function (theme) {
    var _a, _b, _c, _d, _e, _f;
    return ({
        root: {
            display: "flex",
            justifyContent: "center",
            width: "100%",
            padding: "5vw",
            paddingBottom: "calc(5vw + " + theme.spacing(5) + "px)",
        },
        logo: (_a = {
                alignSelf: "flex-end",
                marginRight: 130,
                maxWidth: 325,
                position: "relative",
                bottom: -60
            },
            _a[theme.breakpoints.down("sm")] = {
                maxWidth: 250,
                bottom: 0,
                marginRight: 0,
            },
            _a),
        header: (_b = {
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                paddingBottom: "5vw"
            },
            _b[theme.breakpoints.down("sm")] = {
                marginTop: 20,
            },
            _b),
        content: {
            maxWidth: 1500,
            width: "100%",
        },
        grid: (_c = {},
            _c[theme.breakpoints.down("sm")] = {
                flexDirection: "column",
                alignItems: "center",
            },
            _c),
        paper: (_d = {
                width: "65vw",
                color: "white",
                // @Jordy Kommeren: "Fixes iOS scroll bug"
                // minHeight: "fit-content",
                height: "100vh",
                backgroundColor: "#E9550D",
                "::-webkit-scrollbar": {
                    display: "none",
                }
            },
            _d[theme.breakpoints.up("lg")] = {
                maxWidth: "1050px",
                padding: theme.spacing(5, 0),
            },
            _d[theme.breakpoints.down("sm")] = {
                minWidth: "100vw",
            },
            _d),
        montserrat: {
            fontFamily: "Montserrat",
            lineHeight: "1.3",
        },
        title: {
            fontWeight: 500,
        },
        aboutButtons: {
            marginTop: theme.spacing(2),
            backgroundColor: "transparant",
        },
        button: {
            padding: theme.spacing(1, 0, 1, 0),
            minWidth: "unset !important",
            fontWeight: 400,
            "&:hover": {
                background: "none",
                color: theme.palette.primary.main,
            },
        },
        closeButton: {
            padding: 10,
            borderRadius: "100%",
            border: "2px solid white",
            width: "50px !important",
            height: "50px !important",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor: "pointer",
            transition: "all 0.3s",
            "&:hover": {
                backgroundColor: "white",
                color: "#E9550D",
            },
        },
        icon: {
            color: "inherit",
            width: "25px !important",
            height: "25px !important",
        },
        aboutHeader: (_e = {
                position: "absolute",
                top: theme.spacing(3),
                right: theme.spacing(3)
            },
            _e[theme.breakpoints.up("lg")] = {
                top: theme.spacing(6),
                right: theme.spacing(6),
            },
            _e),
        aboutContent: (_f = {
                display: "flex",
                alignItems: "center"
            },
            _f[theme.breakpoints.up("lg")] = {
                padding: theme.spacing(3, 6, 6, 6),
            },
            _f[theme.breakpoints.down("md")] = {
                padding: theme.spacing(6, 1, 18, 1),
            },
            _f.minHeight = "100%",
            _f),
    });
};
var RenderIndexInfo = withStyles(styles)(function (_a) {
    var magazine = _a.magazine, edition = _a.edition, classes = _a.classes;
    var result;
    if (magazine) {
        if (edition) {
            result = React.createElement(EditionIndexMST, { edition: edition });
        }
        else {
            result = React.createElement(MagazineIndexInfo, { magazine: magazine });
        }
    }
    else {
        result = React.createElement(DefaultIndexInfo, null);
    }
    return React.createElement("div", { className: classes.indexInfo }, result);
});
var Component = observer(function (_a) {
    var presenter = _a.presenter, classes = _a.classes, match = _a.match;
    var _b = React.useState({
        open: false,
        content: React.createElement(Colofon, null),
    }), state = _b[0], setState = _b[1];
    if (presenter.loading) {
        return React.createElement(LoadingAnimation, null);
    }
    return (React.createElement("div", { className: classes.root, style: {
            backgroundImage: "url('https://d3m8frrq3asli4.cloudfront.net/mst/background-shapes/shape1.svg')",
            backgroundAttachment: "fixed",
            minHeight: "100vh",
            backgroundPosition: "bottom left",
            backgroundSize: "80%",
            backgroundRepeat: "no-repeat",
        } },
        React.createElement("div", { className: classes.content },
            React.createElement("div", { className: classes.header }, presenter.magazine && (React.createElement(React.Fragment, null,
                React.createElement("img", { src: "/article/images/mst/mst-logo.svg", className: classes.logo })))),
            React.createElement(Grid, { container: true, className: classes.grid, spacing: 3 },
                React.createElement(Grid, { item: true, xs: 12, md: 3, lg: 3 },
                    React.createElement(RenderIndexInfo, { magazine: presenter.magazine, edition: presenter.edition })),
                React.createElement(Grid, { item: true, xs: 12, md: 8, lg: 8 },
                    React.createElement(Grid, { container: true, spacing: 3 }, presenter.items.map(function (record, index) { return (React.createElement(Zoom, { key: index, in: true, style: { transitionDelay: 300 + index * 50 + "ms" } },
                        React.createElement(Grid, { item: true, key: index, xs: 6, sm: 4, lg: 4 },
                            React.createElement(CoverButton, { version: process.env.REACT_APP_CLIENT_THEME == "mst" ? "mst" : "", id: record.id, index: index + 1, title: record.title, image: record.image, onSelect: presenter.selectItem })))); })),
                    React.createElement(Drawer, { anchor: "left", open: state.open, onClose: function () { return setState({ open: false, content: state.content }); }, classes: { root: classes.root, paper: classes.paper }, BackdropProps: { style: { backgroundColor: "transparent" } } },
                        React.createElement("div", { style: { margin: "auto" } },
                            React.createElement("div", { className: classes.aboutHeader },
                                React.createElement("div", { onClick: function () { return setState({ open: false, content: state.content }); }, className: classes.closeButton },
                                    React.createElement(FontAwesomeIcon, { icon: faTimes, className: classes.icon }))),
                            React.createElement("div", { className: classes.aboutContent }, state.content))))))));
});
export var PageIndexMST = withStyles(styles)(withRouter(withPresenter(function (_a, _b) {
    var match = _a.match, history = _a.history, location = _a.location, staticContext = _a.staticContext;
    var interactor = _b.interactor, provider = _b.provider;
    return new PageIndexPresenter(interactor.magazine, provider.magazine, interactor.article, provider.article, {
        match: match,
        history: history,
        location: location,
        staticContext: staticContext,
    });
}, Component)));
//# sourceMappingURL=indexMST.js.map