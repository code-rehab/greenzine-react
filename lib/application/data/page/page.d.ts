import { GraphQLBase, GraphQLModel } from "../../network/graphql-model";
import { Layout } from "react-grid-layout";
import { Article } from "../article/article";
import { AnyElement } from "@coderehab/greenzeen-content";
export interface PageRecord {
    id: string;
    title: string;
    content?: any;
    background?: string;
    type?: string;
    filter?: string;
    overlay?: string;
    color?: string;
    layout?: string;
    layoutConfig?: any;
    style?: any;
    responsiveStyling?: any;
    data?: Partial<AnyElement>[];
    elements: string;
    slug?: string;
    layouts: {
        sm?: Layout[];
        md?: Layout[];
        lg?: Layout[];
    };
}
export interface Page extends GraphQLModel<PageRecord>, PageRecord {
    articleId: string | null;
    data: AnyElement[];
    styleObj: React.CSSProperties;
    composedStyleObj: React.CSSProperties;
    article: Article | undefined;
    elements: any;
}
export declare class PageModel extends GraphQLBase<PageRecord> implements Page {
    article: Article | undefined;
    articleId: string | null;
    id: string;
    title: string;
    content: any;
    background: string;
    type: string;
    filter: string;
    overlay: string;
    color: string;
    layout: string;
    layoutConfig: any;
    style: any;
    responsiveStyling: any;
    slug: string;
    layouts: {
        lg: never[];
        md: never[];
        sm: never[];
    };
    elements: AnyElement[];
    get data(): AnyElement[];
    set data(data: AnyElement[]);
    get styleObj(): any;
    get composedStyleObj(): React.CSSProperties;
}
