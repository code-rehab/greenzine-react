import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Link as MuiLink,
  List as MuiList,
  ListItem,
  ListItemIcon,
  ListItemText,
  Theme,
  WithStyles,
} from "@material-ui/core";
import { StyleRules, withStyles } from "@material-ui/styles";
import classNames from "classnames";
import * as React from "react";
import { Link } from "react-router-dom";
import { SlideFade } from "../effects/slide-fade";

interface OwnProps extends WithStyles<typeof styles> {
  icon?: any;
  items: any[];
  variant: string;
  animation?: any;
}

const styles = (theme: Theme): StyleRules => {
  return {
    root: {
      margin: "0 0 36px 0",
    },

    item: {
      borderBottom: "1px solid",
      "& *": {
        color: "inherit",
      },
    },
    big: {
      "& $primary": {
        fontFamily: "Montserrat",
        fontWeight: 700,
      },
      "& $secondary": {
        fontFamily: "Montserrat",
        fontWeight: 400,
      },
    },
    primary: {
      lineHeight: 1,
    },
    secondary: {},
  };
};

export const List = withStyles(styles)(({ classes, animation, items, variant }: any) => {
  animation = animation || { duration: 0, delay: 0 };
  items = items || [];

  return (
    <MuiList className={classes.root}>
      {items.map((item: any, index: number) => {
        const href: string = item.href || "";
        const props: any = {};

        if (variant !== "big") {
          props.dense = true;
        }

        if (href) {
          props.button = true;
        }

        return (
          <SlideFade
            direction="up"
            timeout={animation.duration}
            delay={animation.delay + index * 75}
          >
            <div>
              <ListItem
                component={href.startsWith("/") ? Link : MuiLink}
                href={href}
                to={href}
                target={href.startsWith("/") ? undefined : "_blank"}
                className={classNames(classes.item, classes[variant])}
                {...props}
              >
                {item.icon && (
                  <ListItemIcon>
                    {(item.icon.icon && <FontAwesomeIcon icon={item.icon} />) || item.icon}
                  </ListItemIcon>
                )}
                {variant !== "big" ? (
                  variant === "small" ? (
                    //small
                    <ListItemText primary={item.primary} secondary={item.secondary} />
                  ) : (
                    //default
                    <ListItemText primary={item.primary} secondary={item.secondary} />
                  )
                ) : (
                  //big
                  <ListItemText
                    primary={item.primary}
                    secondary={item.secondary}
                    classes={{
                      primary: classes.primary,
                      secondary: classes.secondary,
                    }}
                  />
                )}

                <FontAwesomeIcon icon={faChevronRight} />
              </ListItem>
            </div>
          </SlideFade>
        );
      })}
    </MuiList>
  );
});
